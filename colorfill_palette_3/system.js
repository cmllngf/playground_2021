class System {
  constructor(x, y, _w, _h) {
    this.x = x;
    this.y = y;
    this.size = _w;
    this.width = _w;
    this.height = _h;
    this.walkers = [];

    // for (let i = 0; i < 1; i++) {
    //   const wx = int(random(x, x + size));
    //   const wy = int(random(y, y + size));
    //   this.walkers.push(this.createWalker(wx, wy));
    // }
    // for (let i = 0; i < 1; i++) {
    // const wx = int(this.width / 2) + this.x;
    // const wy = int(this.height / 2) + this.y;
    // this.walkers.push(this.createWalker(wx, wy));
    // }

    const poisson_radius = 8;
    const poisson_k = 3;
    const network = this.poissonDiskSampling(poisson_radius, poisson_k);
    for (let i = 0; i < network.length; i++) {
      const { x, y } = network[i];
      this.walkers.push(this.createWalker(x, y));
    }
  }

  //returns false if finished
  update() {
    for (let i = 0; i < 100; i++) {
      this.walkers.forEach((walker) => {
        walker.update(visited, colors, pixel_size);
      });
    }
    this.walkers = this.walkers.filter((walker) => walker.alive);
    return this.walkers.length > 0 ? true : false;
  }

  createWalker(x = int(random(width)), y = int(random(height)), lifespan = -1) {
    const p = createVector(x, y);
    // const c = palette[int(random(palette.length))];

    // const imgIndex = get1DIndex(x, y);
    // colorMode(RGB);

    // const R = img.pixels[imgIndex];
    // const G = img.pixels[imgIndex + 1];
    // const B = img.pixels[imgIndex + 2];
    // const c = color(R, G, B);
    // colorMode(HSB);

    const c = getColorPaletteFromImage(x, y);

    seed(p, c);

    return new Walker(
      p,
      lifespan,
      this.x,
      this.x + this.width,
      this.y,
      this.y + this.height
    );
  }

  isValidPoint(grid, cellsize, gwidth, gheight, p, radius) {
    // if (img.pixels[(p.y * width + p.x) * 4 + 4] === 0) return false;

    /* Make sure the point is on the screen */
    if (
      p.x < this.x ||
      p.x >= this.x + this.width ||
      p.y < this.y ||
      p.y >= this.height + this.y
    )
      return false;

    /* Check neighboring eight cells */
    let xindex = floor(p.x / cellsize);
    let yindex = floor(p.y / cellsize);
    let i0 = max(xindex - 1, 0);
    let i1 = min(xindex + 1, gwidth - 1);
    let j0 = max(yindex - 1, 0);
    let j1 = min(yindex + 1, gheight - 1);

    // if (!grid[p.x]) return false;

    for (let i = i0; i <= i1; i++)
      for (let j = j0; j <= j1; j++)
        if (grid[i][j] != null)
          if (dist(grid[i][j].x, grid[i][j].y, p.x, p.y) < radius) return false;

    /* If we get here, return true */
    return true;
  }

  insertPoint(grid, cellsize, point) {
    let xindex = floor(point.x / cellsize);
    let yindex = floor(point.y / cellsize);
    // console.log(xindex, grid[xindex]);
    grid[xindex][yindex] = point;
  }

  poissonDiskSampling(radius, k) {
    let N = 2;
    /* The final set of points to return */
    let points = [];
    /* The currently "active" set of points */
    let active = [];
    /* Initial point p0 */
    let p0 = createVector(
      int(random(this.x, this.width + this.x)),
      int(random(this.y, this.height + this.y))
    );
    // p0 = createVector(100, 400);
    // p1 = createVector(700, 400);
    let grid = [];
    let cellsize = floor(radius / sqrt(N));

    /* Figure out no. of cells in the grid for our canvas */
    let ncells_width = ceil(this.width / cellsize) + 1;
    let ncells_height = ceil(this.height / cellsize) + 1;
    /* Allocate the grid an initialize all elements to null */
    for (let i = 0; i <= ncells_width; i++) {
      grid[i] = [];
      for (let j = 0; j <= ncells_height; j++) grid[i][j] = null;
    }

    this.insertPoint(grid, cellsize, p0);
    points.push(p0);
    active.push(p0);

    while (active.length > 0) {
      let random_index = int(random(active.length));
      let p = active[random_index];

      let found = false;
      for (let tries = 0; tries < k; tries++) {
        let theta = int(random(360));
        let new_radius = int(random(radius, 2 * radius));
        let pnewx = int(p.x + new_radius * cos(radians(theta)));
        let pnewy = int(p.y + new_radius * sin(radians(theta)));
        let pnew = createVector(pnewx, pnewy);

        if (
          !this.isValidPoint(
            grid,
            cellsize,
            ncells_width,
            ncells_height,
            pnew,
            radius
          )
        )
          continue;

        points.push(pnew);
        this.insertPoint(grid, cellsize, pnew);
        active.push(pnew);
        found = true;
        break;
      }

      /* If no point was found after k tries, remove p */
      if (!found) active.splice(random_index, 1);
    }

    return points;
  }
}
