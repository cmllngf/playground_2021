let canvas;
let seed;
let simplexNoise;

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";
const border = 160

function setup() {
  canvas = createCanvas(1080, 1080);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
}

function draw() {
  background(darkColor)
  noLoop()
  const w = 11
  const otherAngle = PI/2
  for (let x = -100; x < width+ 100; x+=30) {
    for (let y = -100; y < height + 100; y+=30) {
      noStroke()
      fill(lightColor)
      rectMode(CENTER)
      strokeCap(PROJECT)
      push()
      translate(x, y)
      
      const noiseValue = simplexNoise.noise2D(x * .01, y * .01)
      const angle = random() > .94 ? otherAngle : noiseValue > 0 ? PI/4 : -PI/4 // good
      // const angle = dist(x,y,width/2, height/2) > 200 ? PI/4 : -PI/4 // bad
      // const angle = x > border * 2 && x < width - border * 2 && y > border * 2 && y < height - border * 2 ? PI/4 : -PI/4
      
      rotate(angle)
      rect(0,0, w, angle == otherAngle ? 55 : 53)

      pop()
    }
  }
  
  rectMode(CORNER)
  fill(darkColor);
  noStroke();
  rect(0, 0, width, border);
  rect(0, 0, border, height);
  rect(width - border, 0, border, height);
  rect(0, height - border, width, border);
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `SolLeWitt_${seed}`, "png");
}
