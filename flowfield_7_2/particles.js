class Particle {
  constructor(
    x = int(random(width)),
    y = int(random(height)),
    // col = color(random(0, 60))
    col = accentColor
  ) {
    // const x = int(random(-width / 2, width * 1.5));
    // const y = int(random(-height / 2, height * 1.5));
    this.pos = [createVector(x, y)];
    this.prevPos = createVector(x, y);
    this.vel = createVector(0, 0);
    // this.vel.normalize();
    this.acc = createVector(0, 0);
    this.maxSpeed = 4;
    // this.maxLifeTime = random(100, 30);
    this.maxLifeTime = 40;
    this.lifetime = 0;
    // this.col = random() > 0.05 ? color(0) : color(255);
    // this.col = map(this.maxLifeTime, 100, 150, 0, 150, true);
    // this.col = color(random(255), random(255), random(255));
    // this.col = random(100, 255);
    // this.col = col;
    colorMode(HSB);
    this.col = color(hue(col), saturation(col), brightness(col) + random(5));
  }

  update() {
    if (this.lifetime < this.maxLifeTime) {
      this.vel.add(this.acc);
      this.vel.limit(this.maxSpeed);
      // this.prevPos = this.pos.copy();
      this.pos.push(p5.Vector.add(this.pos[this.pos.length - 1], this.vel));
      this.acc.mult(0);
      // this.edges();
      this.lifetime++;
      return true;
    }
    return false;
  }

  applyForce(force) {
    this.acc.add(force);
  }

  follow(flowfield) {
    const x = floor(this.pos[this.pos.length - 1].x / scale);
    const y = floor(this.pos[this.pos.length - 1].y / scale);
    const vector = flowfield[y * cols + x];
    this.applyForce(vector);
  }

  display() {
    stroke(this.col);
    strokeWeight(2);
    noFill();
    // point(this.pos.x, this.pos.y);
    beginShape();
    for (let i = 0; i < this.pos.length; i++) {
      const p = this.pos[i];
      curveVertex(p.x, p.y);
    }
    endShape();
  }

  getPos() {
    return this.pos[this.pos.length - 1];
  }

  edges() {
    if (this.pos.x < border) {
      this.pos.x = width - border;
      this.prevPos = this.pos.copy();
    }
    if (this.pos.x > width - border) {
      this.pos.x = border;
      this.prevPos = this.pos.copy();
    }
    if (this.pos.y < border) {
      this.pos.y = height - border;
      this.prevPos = this.pos.copy();
    }
    if (this.pos.y > height - border) {
      this.pos.y = border;
      this.prevPos = this.pos.copy();
    }
  }
}
