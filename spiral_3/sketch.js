let canvas;
let seed;
let simplexNoise;
let t = 0;
let palette = [];

const things = [];

const numFrame = 120;
const recording = false;

palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
palette = ["#020224", "#01104F", "#003C81", "#01669E", "#5075A2", "#6F91AD"];
palette = [255];

palette = ["red", "blue"];
// const bgColor = "#010101";
const bgColor = 20;
const accentColor = "#f5f5ff";

function setup() {
  canvas = createCanvas(1080, 1080);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  if (recording) capturer.start();
  background(bgColor);
}

let p = 0;
let ss = 0;
function draw() {
  translate(width / 2, height / 2);
  t = (frameCount / numFrame) % 1;

  // for (let i = 0; i < 100; i++) {
  //   const x = randomGaussian(0, randomGaussian(0, 100));
  //   const y = randomGaussian(0, randomGaussian(0, 40));
  //   stroke(250);
  //   point(x, y);
  // }

  // for (let i = 0; i < 1000; i++) {
  //   const m = randomGaussian(0, 100);
  //   const x = randomGaussian(0, m);
  //   const y = randomGaussian(0, m);
  //   stroke(250);
  //   point(x, y);
  // }

  // for (let i = 0; i < 1000; i++) {
  //   const m = randomGaussian(0, ss);
  //   const s = randomGaussian(0, m);
  //   // const phi = random(TWO_PI);
  //   // const phi = map(s, 0, width / 2, 0, TWO_PI);
  //   // const phi = map(s, 0, width, 0, random(TWO_PI * 2));
  //   // const phi = map(s, 0, width, 0, random(TWO_PI * 10));

  //   const x = cos(p) * ss;
  //   const y = sin(p) * s;
  //   stroke(250);
  //   point(x, y);
  // }
  // p += 02;
  // ss += 01;

  for (let i = 0; i < 1000; i++) {
    // const m = randomGaussian(0, ss);
    const s = randomGaussian(0, 30);
    const phi = floor(random(4)) * HALF_PI;

    const x = cos(phi) * s;
    const y = sin(phi) * s;
    stroke(250);
    point(x, y);
  }

  // fill(bgColor);
  // noStroke();
  // circle(width / 2, height / 2, 40);

  // if (frameCount == numFrame) noLoop();

  if (recording) {
    capturer.capture(canvas.canvas);
    if (frameCount >= 3 * numFrame) {
      capturer.stop();
      capturer.save();
      noLoop();
    }
  }
}

function ease(p) {
  return 3 * p * p - 2 * p * p * p;
}

function ease2(p, g) {
  if (p < 0.5) return 0.5 * pow(2 * p, g);
  else return 1 - 0.5 * pow(2 * (1 - p), g);
}

function softplus(q, p) {
  const qq = q + p;
  if (qq <= 0) {
    return 0;
  }
  if (qq >= 2 * p) {
    return qq - p;
  }
  return (1 / (4 * p)) * qq * qq;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `spiral_3_${seed}`, "png");
}
