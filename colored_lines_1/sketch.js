let canvas;
let seed;
let simplexNoise;
const border = 70;
let palette;
const title = "SINUSOIDAL";

palette = ["#450A04", "#6A2B02", "#CC8942", "#C8AC85", "#74C2C4"];
palette = ["#BA9A22", "#015F7B", "#43221B", "#AC4A57", "#D0B09B"];
palette = ["#061C29", "#005B76", "#70A9B7", "#C2C6C5", "#604963"];

function setup() {
  canvas = createCanvas(800, 800);
  seed = random(99999);
  // simplexNoise = openSimplexNoise(seed);
}

function draw() {
  randomSeed(seed);
  background("#9E9B94");
  background(255);
  background("#f5f5f5");
  background(10);
  stroke(0);
  noLoop();

  const x1 = 20;
  const x2 = 780;
  const yCount = 20;
  for (let i = 0; i < yCount; i++) {
    const y = ((height - 80) / yCount) * i + 40;
    const cR = int(random(3, 10));
    drawLine(x1, y, x2, y, int(random(20, 40)) * cR, cR);
  }
  // fill("#f5f5f5");
  // textSize(30);
  // noStroke();
  // textFont("Helvetica");
  // text(title, width / 2 - textWidth(title) / 2, 780);
  for (let i = 0; i < 5; i++) {
    const col = color(random(255), 100, 100);
    noFill();
    stroke(col);
    circle(30 * i + width / 2 - 120 / 2, 770, 20);
    noStroke();
    fill(col);
    circle(30 * i + width / 2 - 120 / 2, 770, 5);
  }

  // fill(255, 0, 0);
  // circle(width / 2, 770, 30);
}

const drawLine = (x1, y1, x2, y2, K = 200, colorsCount = 10) => {
  const start = createVector(x1, y1);
  const end = createVector(x2, y2);
  let curStep = 0;
  strokeCap(SQUARE);
  noFill();
  beginShape();
  for (let i = 0; i < K; i++) {
    const p = i / K;
    const cur = p5.Vector.lerp(start, end, p);
    curveVertex(cur.x, cur.y + sin(cur.x) * 10);
    if (curStep === K / colorsCount) {
      stroke(palette[int(random(palette.length))]);
      stroke(random(255), 100, 100);
      endShape();
      beginShape();
      curveVertex(cur.x, cur.y);
      curStep = 0;
    }
    curStep++;
  }
  endShape();
};

function ease(p) {
  return 3 * p * p - 2 * p * p * p;
}

function ease2(p, g) {
  if (p < 0.5) return 0.5 * pow(2 * p, g);
  else return 1 - 0.5 * pow(2 * (1 - p), g);
}

function softplus(q, p) {
  const qq = q + p;
  if (qq <= 0) {
    return 0;
  }
  if (qq >= 2 * p) {
    return qq - p;
  }
  return (1 / (4 * p)) * qq * qq;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `colored_line_1_${seed}`, "png");
}
