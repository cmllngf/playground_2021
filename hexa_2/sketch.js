let canvas;
let seed;
let simplexNoise;
let t = 0;
let palette = [];
const numFrame = 120;

const recording = false;

palette = [255];
// palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
// palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
// palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
// palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
// palette = ["#020224", "#01104F", "#003C81", "#01669E", "#5075A2", "#6F91AD"];
// palette = ["#845ec2", "#2c73d2", "#0081cf", "#0089ba", "#008e9b", "#008f7a"];
// palette = ["#845ec2", "#d65db1", "#ff6f91", "#ff9671", "#ffc75f", "#f9f871"];
// palette = ["#5ec2b5", "#69d1ae", "#81dea2", "#a3e991", "#ccf27f", "#f9f871"];
// palette = ["#c25eb5", "#f6669e", "#ff7f82", "#ffa56a", "#ffcf5f", "#f9f871"];
// palette = ["#3352c6", "#bb4ab6", "#ff5692", "#ff856c", "#ffc058", "#f9f871"];
// palette = ["#091C3C", "#142B57", "#142D5C", "#152D53", "#193E68", "#244F79"];
// palette = ["#572D39", "#6E4D45", "#918B7D", "#A69B9D", "", "#C1C098"];
// palette = ["#231811", "#802A1B", "#CC8328", "#D5AC49", "", "#F4EC9D"]; //sunset

// palette = ["#151412", "#272E1E", "#354A29", "#53734A", "#84B188", "#D5D7D4"];
// palette = ["#220507", "#450A04", "#6A2B02", "#8F5201", "#C98A43", "#E6D19F"];
// palette = ["#5C3C49", "#8D5964", "#AF747F", "#D1617E", "#E788A2", "#EEAEB8"];
// palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
// palette = ["#AF747F", "#D1617E", "#E788A2", "#EEAEB8", "#F9D0C3", "#F6EEE1"];
// palette = [0, 250, 100, 150, 200, 50];s
// palette = [150, 250, 100, 50, 0, 200];
// palette = ["#24130C", "#70381F", "#C04700", "#F07800", "#EFB178", "#CCECEB"];
// palette = ["#6F2B3A", "#DB6578", "#E1B971", "#FBEDC7", "#949971", "#5B5D45"];
// palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
// palette = ["#092A2F", "#652B05", "#B65302", "#E7A700", "#FAE647", "#00B5AE"];
palette = ["#162E21", "#063F39", "#30685B", "#73967E", "#C68C20", "#D8D091"];
// palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
// palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
// palette = ["#021118", "#14383D", "#025F71", "#00ACB3", "#9CDACF", "#EBE3D8"];

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";

let bgColor = lightColor;
let accentColor = darkColor;

const lines = [];

function setup() {
  canvas = createCanvas(1080, 1080);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  if (recording) capturer.start();
}

function draw() {
  randomSeed(seed);
  background(bgColor);
  t = (frameCount / numFrame) % 1;

  const s = 15;
  const h = 1;

  const nx = 25;
  const ny = 30;

  const bx = width / 2 - (nx * s * 3) / 2;
  const by = height / 2 - (ny * s * 0.79) / 2;

  let nh = 0;
  // noLoop();

  for (let y = 0; y < ny; y++) {
    for (let x = 0; x < nx; x++) {
      const hx = bx + x * s * 3 + (y % 2 == 0 ? s * 1.5 : 0);
      const hy = by + y * s * 0.85;

      const r = 0.5;
      // const nv = simplexNoise.noise4D(
      //   hx * 0.01,
      //   hy * 0.01,
      //   cos(t * TWO_PI) * r,
      //   sin(t * TWO_PI) * r
      // );
      const nv = simplexNoise.noise4D(
        hx * 0.01,
        hy * 0.01 + sin(t * TWO_PI) + cos(t * TWO_PI),
        cos(t * TWO_PI) * r,
        sin(t * TWO_PI) * r
      );
      if (nv > 0) {
        // nh = map(sin(ease2(t, 5) * TWO_PI), 0, 1, 0, 100 * nv, true);
        // nh = map(1, 0, 1, 0, 100 * nv, true);
        // nh = periodicFunction(t - offset(x, y));
        // nh = periodicFunction(t - offset(hx, hy));
        nh = nv * 150;
      }

      const col = getColor(nh, 0, 150);
      drawHexagon(hx, hy, s, h + nh, col);
      // drawHexagon(hx, hy, s, h + nh, palette[int(random(palette.length))]);
      nh = 0;
    }
  }

  // noLoop();

  if (recording) {
    capturer.capture(canvas.canvas);
    if (numFrame * 3 === frameCount) {
      capturer.stop();
      capturer.save();
      noLoop();
    }
  }
}

function periodicFunction(p) {
  return map(sin(TWO_PI * p), -1, 1, 0, 100);
}

function offset(x, y) {
  return 0.01 * dist(x, y, width / 2, height / 2);
}

function getColor(h, min, max) {
  const index = int(map(h, min, max, 0, palette.length - 1));
  return palette[index];
}

function drawHexagon(x, y, radius, h, color) {
  noStroke();
  smooth();
  // TOP
  // fill(color(hue,100,80))
  // fill(color);
  fill(color);
  beginShape();
  vertex(x + radius * cos(PI), y + radius * sin(PI) - h);
  vertex(x + radius * cos((2 * PI) / 3), y + radius * sin((2 * PI) / 3) - h);
  vertex(x + radius * cos(PI / 3), y + radius * sin(PI / 3) - h);
  vertex(x + radius * cos(0), y + radius * sin(0) - h);
  vertex(x + radius * cos((5 * PI) / 3), y + radius * sin((5 * PI) / 3) - h);
  vertex(x + radius * cos((4 * PI) / 3), y + radius * sin((4 * PI) / 3) - h);
  endShape(CLOSE);

  // FRONT FACE
  // fill(color);
  fill(palette[2]);
  beginShape();
  vertex(x + radius * cos((2 * PI) / 3), y + radius * sin((2 * PI) / 3) - h);
  vertex(x + radius * cos((2 * PI) / 3), y + radius * sin((2 * PI) / 3));
  vertex(x + radius * cos(PI / 3), y + radius * sin(PI / 3));
  vertex(x + radius * cos(PI / 3), y + radius * sin(PI / 3) - h);
  endShape(CLOSE);

  // FRONT LEFT FACE
  // fill(color);
  fill(palette[1]);
  beginShape();
  vertex(x + radius * cos(PI), y + radius * sin(PI) - h);
  vertex(x + radius * cos(PI), y + radius * sin(PI));
  vertex(x + radius * cos((2 * PI) / 3), y + radius * sin((2 * PI) / 3));
  vertex(x + radius * cos((2 * PI) / 3), y + radius * sin((2 * PI) / 3) - h);
  endShape(CLOSE);

  // FRONT RIGHT FACE
  // fill(color);
  fill(palette[5]);
  beginShape();
  vertex(x + radius * cos(PI / 3), y + radius * sin(PI / 3) - h);
  vertex(x + radius * cos(PI / 3), y + radius * sin(PI / 3));
  vertex(x + radius * cos(0), y + radius * sin(0));
  vertex(x + radius * cos(0), y + radius * sin(0) - h);
  endShape(CLOSE);
}

function ease(p) {
  return 3 * p * p - 2 * p * p * p;
}

function ease2(p, g) {
  if (p < 0.5) return 0.5 * pow(2 * p, g);
  else return 1 - 0.5 * pow(2 * (1 - p), g);
}

function softplus(q, p) {
  const qq = q + p;
  if (qq <= 0) {
    return 0;
  }
  if (qq >= 2 * p) {
    return qq - p;
  }
  return (1 / (4 * p)) * qq * qq;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `hexa_${seed}`, "png");
}
