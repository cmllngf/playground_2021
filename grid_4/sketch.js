// Make a grid of permutations of something.
let canvas,
  seed,
  t = 0;

let simplexNoise;
let tSize = 10;
let words = [];
const ws = [""];
let palette;
palette = ["#264653", "#2a9d8f", "#e9c46a", "#f4a261", "#e76f51"];
palette = ["#D5B5F3", "#75D8EC", "#F3A9C5", "#A9F3D3", "#F3C4A9"];

const numFrame = 60;
const recording = false;

const col = 50;
const row = 50;
const noiseMult = 0.001;
const margin = 25;

function setup() {
  canvas = createCanvas(1080, 1080);
  seed = random(99999);
  randomSeed(seed);
  simplexNoise = openSimplexNoise(seed);
  if (recording) capturer.start();
}

function draw() {
  t = frameCount / numFrame;
  background(255);
  const w = (width - margin * 2) / col;
  const h = (height - margin * 2) / row;
  // drawHash(0, 0, 0, w, h);
  for (let x = margin; x < width - margin; x += w) {
    for (let y = margin; y < height - margin; y += h) {
      // const r = cos(x + y + t) * TWO_PI;
      // const r =
      //   cos(map(dist(x, y, width / 2, height / 2), 0, width / 2, 0, 1)) *
      //   TWO_PI;
      const r =
        sin(
          (map(dist(x, y, width / 2, height / 2), 0, width / 2, 0, 1) + t) % 1
        ) * TWO_PI;
      // const n = random(4, 8)
      const n = map(r, -TWO_PI, TWO_PI, 3, 7);
      drawHash(x, y, r, w, h, n);
    }
  }
  // noLoop();
  if (recording) {
    capturer.capture(canvas.canvas);
    if (t === 3) {
      capturer.stop();
      capturer.save();
      noLoop();
    }
  }
}

function drawHash(x, y, a, w, h, n = 5) {
  push();
  translate(x + w / 2, y + h / 2);
  rotate(a);
  stroke(255);
  strokeWeight(3);
  stroke(palette[int(random(palette.length))]);
  stroke(0);
  // stroke(palette2[int(n % palette2.length)]);
  for (let i = 0; i < n; i++) {
    const xx = (w / n) * i;
    line(xx - w / 2, -h / 2, xx - w / 2, h - h / 2);
  }
  pop();
}

function offset(x, y) {
  return 0.015 * dist(x, y, width / 2, height / 2);
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `grid_4_${seed}`, "png");
}
