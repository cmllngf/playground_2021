let canvas;
let seed;
let simplexNoise;
let t = 0;
let palette = [];
const numFrame = 300;

const recording = true;

palette = [255];
// palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
// palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
// palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
// palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
// palette = ["#020224", "#01104F", "#003C81", "#01669E", "#5075A2", "#6F91AD"];
// palette = ["#845ec2", "#2c73d2", "#0081cf", "#0089ba", "#008e9b", "#008f7a"];
// palette = ["#845ec2", "#d65db1", "#ff6f91", "#ff9671", "#ffc75f", "#f9f871"];
// palette = ["#5ec2b5", "#69d1ae", "#81dea2", "#a3e991", "#ccf27f", "#f9f871"];
// palette = ["#c25eb5", "#f6669e", "#ff7f82", "#ffa56a", "#ffcf5f", "#f9f871"];
// palette = ["#3352c6", "#bb4ab6", "#ff5692", "#ff856c", "#ffc058", "#f9f871"];
// palette = ["#091C3C", "#142B57", "#142D5C", "#152D53", "#193E68", "#244F79"];
// palette = ["#572D39", "#6E4D45", "#918B7D", "#A69B9D", "#C1C098"];
// palette = ["#231811", "#802A1B", "#CC8328", "#D5AC49", "", "#F4EC9D"]; //sunset

// palette = ["#151412", "#272E1E", "#354A29", "#53734A", "#84B188", "#D5D7D4"];
// palette = ["#220507", "#450A04", "#6A2B02", "#8F5201", "#C98A43", "#E6D19F"];
// palette = ["#5C3C49", "#8D5964", "#AF747F", "#D1617E", "#E788A2", "#EEAEB8"];
// palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
// palette = ["#AF747F", "#D1617E", "#E788A2", "#EEAEB8", "#F9D0C3", "#F6EEE1"];
// palette = [0, 250, 100, 150, 200, 50];
palette = [0, 50, 100, 150, 200, 250];
// palette = [150, 250, 100, 50, 0, 200];
// palette = ["#24130C", "#70381F", "#C04700", "#F07800", "#EFB178", "#CCECEB"];
// palette = ["#6F2B3A", "#DB6578", "#E1B971", "#FBEDC7", "#949971", "#5B5D45"];
// palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
// palette = ["#092A2F", "#652B05", "#B65302", "#E7A700", "#FAE647", "#00B5AE"];
// palette = ["#162E21", "#063F39", "#30685B", "#73967E", "#C68C20", "#D8D091"];
// palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
// palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
// palette = ["#021118", "#14383D", "#025F71", "#00ACB3", "#9CDACF", "#EBE3D8"];

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";

let bgColor = lightColor;
let accentColor = darkColor;

const lines = [];

function setup() {
  canvas = createCanvas(1080, 1080);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  if (recording) capturer.start();
}

function draw() {
  randomSeed(seed);
  background(bgColor);
  t = (frameCount / numFrame) % 1;

  const s = 15;
  const h = 1;

  // const nx = 10;
  // const ny = 30;
  const nx = 25;
  const ny = 96;

  const bx = width / 2 - (nx * s * 3) / 2;
  const by = height / 2 - (ny * s * 0.79) / 2;

  let nh = 0;

  for (let y = 0; y < ny; y++) {
    for (let x = 0; x < nx; x++) {
      const hx = bx + x * s * 3 + (y % 2 == 0 ? s * 1.5 : 0);
      const hy = by + y * s * 0.85;

      // const nv = simplexNoise.noise2D(hx * 0.01, hy * 0.01);
      // if (nv > 0) {
      // nh = map(sin(ease2(t, 5) * TWO_PI), 0, 1, 0, 100 * nv, true);
      // nh = map(1, 0, 1, 0, 100 * nv, true);
      // nh = periodicFunction(t - offset(x, y));
      nh = periodicFunction3(t - offset3(hx, hy));
      // }

      // drawHexagon(hx, hy, s, h + nh, palette[int(random(palette.length))]);
      // drawHexagon(hx, hy, s, h + nh, getColor(hx, hy));
      drawHexagon(hx, hy, s, h + nh, getColorFromHeight(nh));
      nh = 0;
    }
  }

  // noLoop();

  if (recording) {
    capturer.capture(canvas.canvas);
    if (numFrame * 1 === frameCount) {
      capturer.stop();
      capturer.save();
      noLoop();
    }
  }
}

const easeOutElastic = x => {
  const c4 = (2 * PI) / 3
  
  return x === 0
    ? 0
    : x === 1
    ? 1
    : pow(2, -10 * x) * sin((x * 10 - 0.75) * c4) + 1;
}

const easeOutBounce = (x) => {
  const n1 = 7.5625;
  const d1 = 2.75;
  
  if (x < 1 / d1) {
      return n1 * x * x;
  } else if (x < 2 / d1) {
      return n1 * (x -= 1.5 / d1) * x + 0.75;
  } else if (x < 2.5 / d1) {
      return n1 * (x -= 2.25 / d1) * x + 0.9375;
  } else {
      return n1 * (x -= 2.625 / d1) * x + 0.984375;
  }
}

const getColor = (x, y) => {
  // const c1 = color(100, random(255), 150);
  // const c2 = color(random(255), 150, 100);
  // const c2 = color("#561705");
  // const c1 = color("#A9F3D3");
  const c2 = color("#204534");
  // const c1 = color("#5E280E");
  // const c2 = color("#FFDBAE");
  const c1 = color("#FF396A");
  // const c2 = color(0);
  // const p = map(x + y, avoidBorder * 2, width + height - avoidBorder * 2, 0, 1);
  const p2 = map(y, 0, height, 0, palette.length - 1);
  // return lerpColor(c1, c2, p);
  // return random() > 0.05 ? c1 : c2;
  return palette[constrain(int(randomGaussian(p2, 1)), 0, palette.length - 1)];
  // return palette[int(random(palette.length))];
};

const getColorFromHeight = (h) => {
  // const c1 = color(100, random(255), 150);
  // const c2 = color(random(255), 150, 100);
  // const c2 = color("#561705");
  // const c1 = color("#A9F3D3");
  const c2 = color("#204534");
  // const c1 = color("#5E280E");
  // const c2 = color("#FFDBAE");
  const c1 = color("#FF396A");
  // const c2 = color(0);
  // const p = map(x + y, avoidBorder * 2, width + height - avoidBorder * 2, 0, 1);
  const p2 = map(h, 0, 100, 0, palette.length - 1);
  // return lerpColor(c1, c2, p);
  // return random() > 0.05 ? c1 : c2;
  return palette[constrain(int(randomGaussian(p2, 1)), 0, palette.length - 1)];
  // return palette[int(random(palette.length))];
};

function periodicFunction(p) {
  return map(cos(TWO_PI * p), -1, 1, 0, 100);
}

function periodicFunction3(p) {
  // return map(easeOutElastic(map(cos(TWO_PI * p), -1, 1, 0, 1)), 0, 1, 100, 0, true);
  return map(easeOutBounce(map(cos(TWO_PI * p), -1, 1, 0, 1)), 0, 1, 100, 0, true);
}

function periodicFunction2(p) {
  return map(sin(TWO_PI * p), -1, 1, 0, 100);
}

function offset(x, y) {
  return 0.0005 * dist(0, y, 0, -height);
}
function offset3(x, y) {
  // return 0.002 * dist(x, y, 0, height * 2);
  return 0.002 * dist(x, y, width/2, height/2);
}

function offset2(x, y) {
  return 0.002 * dist(x * 2, y * 0.01, 0, height * 2);
}

function drawHexagon(x, y, radius, h, color) {
  noStroke();
  smooth();
  // TOP
  // fill(color(hue,100,80))
  // fill(color);
  fill(color);
  beginShape();
  vertex(x + radius * cos(PI), y + radius * sin(PI) - h);
  vertex(x + radius * cos((2 * PI) / 3), y + radius * sin((2 * PI) / 3) - h);
  vertex(x + radius * cos(PI / 3), y + radius * sin(PI / 3) - h);
  vertex(x + radius * cos(0), y + radius * sin(0) - h);
  vertex(x + radius * cos((5 * PI) / 3), y + radius * sin((5 * PI) / 3) - h);
  vertex(x + radius * cos((4 * PI) / 3), y + radius * sin((4 * PI) / 3) - h);
  endShape(CLOSE);

  // FRONT FACE
  // fill(color);
  fill(palette[2]);
  beginShape();
  vertex(x + radius * cos((2 * PI) / 3), y + radius * sin((2 * PI) / 3) - h);
  vertex(x + radius * cos((2 * PI) / 3), y + radius * sin((2 * PI) / 3));
  vertex(x + radius * cos(PI / 3), y + radius * sin(PI / 3));
  vertex(x + radius * cos(PI / 3), y + radius * sin(PI / 3) - h);
  endShape(CLOSE);

  // FRONT LEFT FACE
  // fill(color);
  fill(palette[1]);
  beginShape();
  vertex(x + radius * cos(PI), y + radius * sin(PI) - h);
  vertex(x + radius * cos(PI), y + radius * sin(PI));
  vertex(x + radius * cos((2 * PI) / 3), y + radius * sin((2 * PI) / 3));
  vertex(x + radius * cos((2 * PI) / 3), y + radius * sin((2 * PI) / 3) - h);
  endShape(CLOSE);

  // FRONT RIGHT FACE
  // fill(color);
  fill(palette[5]);
  beginShape();
  vertex(x + radius * cos(PI / 3), y + radius * sin(PI / 3) - h);
  vertex(x + radius * cos(PI / 3), y + radius * sin(PI / 3));
  vertex(x + radius * cos(0), y + radius * sin(0));
  vertex(x + radius * cos(0), y + radius * sin(0) - h);
  endShape(CLOSE);
}

function ease(p) {
  return 3 * p * p - 2 * p * p * p;
}

function ease2(p, g) {
  if (p < 0.5) return 0.5 * pow(2 * p, g);
  else return 1 - 0.5 * pow(2 * (1 - p), g);
}

function softplus(q, p) {
  const qq = q + p;
  if (qq <= 0) {
    return 0;
  }
  if (qq >= 2 * p) {
    return qq - p;
  }
  return (1 / (4 * p)) * qq * qq;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `hexa_${seed}`, "png");
}
