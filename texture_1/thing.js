class Thing {
  constructor(x, y, phi, c = accentColor, c2 = accentColor) {
    this.x = x;
    this.y = y;
    this.phi = phi;
    this.weight = random(2, 4);
    this.c = c;
    this.c2 = c2;
  }

  display(c = this.c) {
    // if (dist(this.x, this.y, 0, 0) < 200) {
    //   stroke(this.c2);
    //   strokeWeight(10);
    // } else {
    stroke(c);
    // stroke(getColor(this.y));
    strokeWeight(this.weight);
    // }

    push();
    translate(this.x, this.y);
    rotate(this.phi);
    point(0, 0);
    pop();
    // point(this.x, this.y);
    // this.edge();
  }

  update(index) {
    this.x += cos(this.phi) * 1;
    this.y += sin(this.phi) * 1;

    // this.v = noise(this.x, this.y);
    const v = (noise(this.x * 0.2, this.y * 0.2) + 0.045 * (index - 6 / 3)) % 1;

    this.phi += 2 * map(v, 0, 1, -1, 1);
    // this.phi += 3 * random(-1, 1);
  }

  edge() {
    if (this.x < -width / 2) {
      this.x = width / 2;
    }
    if (this.x > width / 2) {
      this.x = -width / 2;
    }
    if (this.y < -height / 2) {
      this.y = height / 2;
    }
    if (this.y > height / 2) {
      this.y = height / 2;
    }
  }
}
