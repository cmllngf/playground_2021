let canvas;
let seed;
let simplexNoise;
let t = 0;
let palette = [];

const circleCount = 40;
const circleGap = 10;
const noiseScale = 0.01;
const noiseScale2 = 0.012;
const tRadius = 0.3;
const numFrame = 120;
const maxDisplacement = 40;
const minDisplacement = 1;

const recording = false;

palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
palette = ["#020224", "#01104F", "#003C81", "#01669E", "#5075A2", "#6F91AD"];
palette = [255];

function setup() {
  canvas = createCanvas(1080, 1080);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  if (recording) capturer.start();
}

function draw() {
  randomSeed(seed);
  background(0);
  stroke("#d5d5d5");
  // noStroke();
  t = frameCount / numFrame;
  for (let i = circleCount; i > 0; i--) {
    fill(255);
    fill(palette[int(random(palette.length))]);
    drawCircle(width / 2, height / 2, i * circleGap);
    fill(10);
    drawCircle(width / 2, height / 2, i * circleGap, true, 99);
  }
  // noLoop();
  if (recording) {
    capturer.capture(canvas.canvas);
    if (t === 3) {
      capturer.stop();
      capturer.save();
      noLoop();
    }
  }
}

const drawCircle = (x, y, r, displace = true, seed = 9999) => {
  const n = 100;
  push();
  translate(x, y);
  beginShape();
  // stroke(palette[int(random(palette.length))]);
  for (let i = 0; i < n; i++) {
    const phi = (TWO_PI / n) * i;
    const dx = cos(phi) * r;
    const dy = sin(phi) * r - 50;
    const d = map(
      dist(0, -circleGap * circleCount, 0, dy),
      0,
      circleGap * circleCount * 2,
      minDisplacement,
      maxDisplacement
    );
    const dBase = map(
      dist(0, height / 2, 0, dy),
      circleGap * circleCount,
      0,
      0,
      300,
      true
    );
    if (displace) {
      const noiseY = abs(
        simplexNoise.noise2D(dx * noiseScale2, dy * noiseScale2) * dBase
      );

      const noiseDisplacementX =
        simplexNoise.noise4D(
          dx * noiseScale + seed,
          dy * noiseScale + seed,
          cos(t * TWO_PI) * tRadius + seed,
          sin(t * TWO_PI) * tRadius + seed
        ) * d;
      const noiseDisplacementY =
        simplexNoise.noise4D(
          dx * noiseScale + 1000 + seed,
          dy * noiseScale + 1000 + seed,
          cos(t * TWO_PI) * tRadius + seed,
          sin(t * TWO_PI) * tRadius + seed
        ) * d;
      curveVertex(dx + noiseDisplacementX, dy + noiseY + noiseDisplacementY);
    } else {
      curveVertex(dx, dy);
    }
  }
  endShape(CLOSE);
  pop();
};

function ease(p) {
  return 3 * p * p - 2 * p * p * p;
}

function ease2(p, g) {
  if (p < 0.5) return 0.5 * pow(2 * p, g);
  else return 1 - 0.5 * pow(2 * (1 - p), g);
}

function softplus(q, p) {
  const qq = q + p;
  if (qq <= 0) {
    return 0;
  }
  if (qq >= 2 * p) {
    return qq - p;
  }
  return (1 / (4 * p)) * qq * qq;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `circle_pattern_1_${seed}`, "png");
}
