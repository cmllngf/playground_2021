class Line {
  constructor(x) {
    this.x = x;
    const n = int(map(abs(width / 2 - x), 0, width / 2, 50, 2, true));
    this.K = int(random(n));
    // this.K = 4;
    this.offset = random(1);
  }

  // display() {
  //   stroke(255);
  //   line(this.x, 0, this.x, height);
  // }

  display() {
    push();
    for (let i = 0; i < this.K; i++) {
      let p = map(i + ((t + this.offset) % 1), 0, this.K, 0, 1);
      this.update(p);
    }
    pop();
  }

  update(p) {
    strokeWeight(1);
    stroke(255);
    const y = map(p, 0, 1, 0, height);
    point(this.x, y);
  }
}
