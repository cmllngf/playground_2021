let canvas;
let seed;
let simplexNoise;
let t = 0;
let palette = [];
const numFrame = 120;

const recording = true;

const lines = [];

palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
palette = ["#020224", "#01104F", "#003C81", "#01669E", "#5075A2", "#6F91AD"];
palette = [255];

function setup() {
  canvas = createCanvas(1080, 1080);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  if (recording) capturer.start();

  for (let i = 0; i < 2500; i++) {
    lines.push(new Line(randomGaussian(width / 2, randomGaussian(0, 200))));
  }
}

function draw() {
  randomSeed(seed);
  background(10);
  t = frameCount / numFrame;
  // noLoop();

  for (let i = 0; i < lines.length; i++) {
    const l = lines[i];
    l.display();
  }

  strokeWeight(5);
  stroke(255);
  // line(width / 2, 0, width / 2, height);

  if (recording) {
    capturer.capture(canvas.canvas);
    if (t === 3) {
      capturer.stop();
      capturer.save();
      noLoop();
    }
  }
}

function ease(p) {
  return 3 * p * p - 2 * p * p * p;
}

function ease2(p, g) {
  if (p < 0.5) return 0.5 * pow(2 * p, g);
  else return 1 - 0.5 * pow(2 * (1 - p), g);
}

function softplus(q, p) {
  const qq = q + p;
  if (qq <= 0) {
    return 0;
  }
  if (qq >= 2 * p) {
    return qq - p;
  }
  return (1 / (4 * p)) * qq * qq;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `ball_1_${seed}`, "png");
}
