let visited = [];
let colors = [];
let canvas;
let seed;
let simplexNoise;
const systems = []
let system;
let currentSystemIndex = 0;
let img

function preload() {
  img = loadImage('../assets/athena/portrait_1.jpg')
}

const border = 70;
function setup() {
  canvas = createCanvas(1080, 1350);
  img.loadPixels();
  // canvas = createCanvas(1080, 1080);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  colorMode(HSB);
  system = new System(300, 300, 300, 300, PI/6);
  background('#F4F4F9');
  fill('red');
  const cols = 10;
  const rows = 8;
  const W = (width-border*2)/cols;
  const H = (height-border*2)/rows;
  for (let x = 0; x < cols; x++) {
    for (let y = 0; y < rows; y++) {
      const sx = x * W + border + W/2;
      const sy = y * H + border + H/2;
      circle(sx,sy, 10);
      // systems.push(new System(sx, sy, 20, 90, PI/4.5))
      // systems.push(new System(sx, sy, 50, 150, PI/4.5))
      systems.push(new System(sx, sy, 50, 150, PI/5.8));
      // systems.push(new System(sx, sy, 50, 150))
    }
  }
}

function draw() {
  for (let i = 0; i < 30; i++) {
    if (!systems[currentSystemIndex].update()) {
      currentSystemIndex++;
    }
    if (!systems[currentSystemIndex]) {
      noLoop();
      return;
    }
  }
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `rectangle_color_${seed}`, "png");
}
const k = 300;
const pixel_size = 3;



// const palette = ["#7ab6ff","#7ab6ff","#7ab6ff","#7ab6ff","#ffce7a","#ff8c7a","#7ab6ff","#7ab6ff"]
// const palette = ['#DE8C9E', '#697A77', '#5F7A75', '#CFD4D3', '#364F4A']
// const palette = ['#FEC89A', '#FEC89A', '#FEC89A', '#FEC5BB', '#FCD5CE']
// const palette = ['#FFC857', '#119DA4', '#19647E', '#4B3F72', '#1F2041']
// const palette = ['#DDFFF7', '#93E1D8', '#FFA69E', '#AA4465', '#861657']
// const palette = ["#fec5bb","#fcd5ce","#fae1dd","#f8edeb","#e8e8e4","#d8e2dc","#ece4db","#ffe5d9","#ffd7ba","#fec89a"]
const palette = ["#5f0f40","#9a031e","#fb8b24","#e36414","#0f4c5c"]
// const palette = ["#590d22","#800f2f","#a4133c","#c9184a","#ff4d6d","#ff758f","#ff8fa3","#ffb3c1","#ffccd5","#fff0f3"]
const get1DIndex = (x, y, minX = 0, maxX = width, minY = 0, maxY = height) => {
  const xx = int(map(x, minX, maxX, 0, img.width));
  const yy = int(map(y, minY, maxY, 0, img.height));
  return (yy * img.width + xx) * 4;
};

const getColorPaletteFromImage = (x, y) => {
  const index = get1DIndex(x, y);
  const R = img.pixels[index];
  const G = img.pixels[index + 1];
  const B = img.pixels[index + 2];
  const ll = 0.2126 * R + 0.7152 * G + 0.0722 * B;
  return palette[int(map(ll, 0, 255, 0, palette.length, true))];
};

const getColorImg = (x, y) => {
  let c
  const index = get1DIndex(
    int(x),
    int(y),
  );
  const R = img.pixels[index];
  const G = img.pixels[index + 1];
  const B = img.pixels[index + 2];
  const A = img.pixels[index + 3];
  colorMode(RGB)
  c = color(R,G,B,A)
  colorMode(HSB)
  return c
}

const getColorFromCoord = (x, y) => {
  // vary mean along x axes
  let mappedIndex;
  // if (dist(x,y,width/2,height/2) < 400) {
    mappedIndex = map(y, height, 0, 0, palette.length-1, true);
  // } else {
  //   mappedIndex = map(y, 0, height, 0, palette.length-1, true);
  // }
  // const mappedIndex = map(y, 0, height, 0, palette.length-1, true);
  const noiseMean = .4
  // const noiseMean = noise(x) * 1
  const gaussianIndex = randomGaussian(mappedIndex, noiseMean)
  const index = int(constrain(gaussianIndex, 0, palette.length - 1))
  return palette[index];
}

const getColorRandom = () => {
  return palette[int(random(palette.length))]
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `rectangle_color_${seed}`, "png");
}