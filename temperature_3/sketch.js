let canvas;
let positions;
let seed;
let circles = [];
let is = 0;

function setup() {
  canvas = createCanvas(1080, 1080);
  seed = random(99999);
  randomSeed(seed);
}

function draw() {
  background(0);
  noLoop();
  smooth();

  renderC(random(width), random(height), 70, 8);
  renderC(random(width), random(height), 70, 8);
  // renderC(width / 3, height / 2, 100, 5);
  // renderC(width / 1.5, height / 2, 100, 5);

  renderArcs();
}

const renderC = (x, y, w, s) => {
  noFill();
  strokeWeight(1);
  stroke(255);
  const N = 20000;
  for (let i = 1; i <= s; i++) {
    // circle(x, y, i * w * 2);
    circles.push({ x, y, r: i * w * 2, i });
    if (i > is) is = i;
    for (let n = 0; n < N / i; n++) {
      const r = random((i - 1) * w, i * w);
      const a = random(TWO_PI);
      const px = cos(a) * r + x;
      const py = sin(a) * r + y;
      point(px, py);
    }
  }
};

const renderArcs = () => {
  noFill();
  stroke(100, 150);
  strokeWeight(1);
  for (let i = 0; i < is; i++) {
    // stroke(color(random(255), random(255), random(255)));
    const cs = circles.filter((c) => c.i == i + 1);
    console.log({ cs });
    for (let i = 0; i < cs.length; i++) {
      const c1 = cs[i];
      for (let j = i + 1; j < cs.length; j++) {
        const c2 = cs[j];
        const p = intersect(c1.x, c1.y, c1.r / 2, c2.x, c2.y, c2.r / 2);
        if (p) {
          const { x1, y1, x2, y2 } = p;

          const a11 = atan2(y1 - c1.y, x1 - c1.x);
          const a12 = atan2(y2 - c1.y, x2 - c1.x);

          const a21 = atan2(y1 - c2.y, x1 - c2.x);
          const a22 = atan2(y2 - c2.y, x2 - c2.x);

          push();
          translate(c1.x, c1.y);
          arc(0, 0, c1.r, c1.r, a12, a11);
          pop();
          push();
          translate(c2.x, c2.y);
          arc(0, 0, c1.r, c1.r, a21, a22);
          pop();
        } else {
          circle(c1.x, c1.y, c1.r);
          circle(c2.x, c2.y, c2.r);
        }
      }
    }
  }
};

const intersect = (xx1, yy1, w1, xx2, yy2, w2) => {
  let dx = xx2 - xx1;
  let dy = yy2 - yy1;
  let d2 = dx * dx + dy * dy;
  let d = sqrt(d2);
  let rr2 = w1 * w1;
  let RR2 = w2 * w2;

  if (d < w1 + w2 && d > abs(w1 - w2)) {
    let K = rr2 - RR2 + d2;
    let K2 = K * K;
    let h = sqrt(4 * rr2 * d2 - K2);
    let x1 = xx1 + (dx * K + dy * h) / (2 * d2);
    let x2 = xx1 + (dx * K - dy * h) / (2 * d2);
    let y1 = yy1 + (dy * K - dx * h) / (2 * d2);
    let y2 = yy1 + (dy * K + dx * h) / (2 * d2);
    return { x1, y1, x2, y2 };
  }
};

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `temperature_3_${seed}`, "png");
}
