// palette = ["#9C7B5A", "#CDAF7D", "#7CA98A", "#216C58", "#204534"];
// palette = ["#5E101E", "#732427", "#8BD6C2", "#175450", "#082B2F"];
// palette = ["#742228", "#721821", "#67181D", "#3B1816", "#0F0F0F"];
// palette = ["#760000", "#86502A", "#B87B5E", "#4A6077", "#1F4B3E"];
// palette = ["#00314F", "#6CA5AE", "#E5F1D9", "#B4A387", "#D38C86"];
// palette = ["#D5B5F3", "#75D8EC", "#F3A9C5", "#A9F3D3", "#F3C4A9"];
// palette = ["#082C59", "#0A72A1", "#4183B1", "#526C81", "#DDCEDA"];
// palette = ["#012023", "#1B443C", "#93907D", "#855352", "#6C252D"];
// palette = ["#840000", "#C58402", "#8B929C", "#18434C", "#1F2113"];

palette = ["#151412", "#272E1E", "#354A29", "#53734A", "#84B188", "#D5D7D4"];
// palette = ["#220507", "#450A04", "#6A2B02", "#8F5201", "#C98A43", "#E6D19F"];
// palette = ["#5C3C49", "#8D5964", "#AF747F", "#D1617E", "#E788A2", "#EEAEB8"];
// palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
// palette = ["#AF747F", "#D1617E", "#E788A2", "#EEAEB8", "#F9D0C3", "#F6EEE1"];
// palette = [0, 250, 100, 150, 200, 50];
// palette = [150, 250, 100, 50, 0, 200];
// palette = ["#24130C", "#70381F", "#C04700", "#F07800", "#EFB178", "#CCECEB"];
// palette = ["#6F2B3A", "#DB6578", "#E1B971", "#FBEDC7", "#949971", "#5B5D45"];
// palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
// palette = ["#092A2F", "#652B05", "#B65302", "#E7A700", "#FAE647", "#00B5AE"];
// palette = ["#162E21", "#063F39", "#30685B", "#73967E", "#C68C20", "#D8D091"];
// palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
// palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
// palette = ["#021118", "#14383D", "#025F71", "#00ACB3", "#9CDACF", "#EBE3D8"];
// palette = ["#845ec2", "#2c73d2", "#0081cf", "#0089ba", "#008e9b", "#008f7a"];
// palette = ["#845ec2", "#d65db1", "#ff6f91", "#ff9671", "#ffc75f", "#f9f871"];
// palette = ["#5ec2b5", "#69d1ae", "#81dea2", "#a3e991", "#ccf27f", "#f9f871"];
// palette2 = ["#c25eb5", "#f6669e", "#ff7f82", "#ffa56a", "#ffcf5f", "#f9f871"];
// palette = ["#3352c6", "#bb4ab6", "#ff5692", "#ff856c", "#ffc058", "#f9f871"];
palette = ['#065143', '#129490', '#70b77e', '#e0a890', '#ce1483']

const palettes = [
  // ['#065143', '#129490', '#70b77e', '#e0a890', '#ce1483'],
  // ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"],
  // ["#D5B5F3", "#75D8EC", "#F3A9C5", "#A9F3D3", "#F3C4A9"],
  // ["#3352c6", "#bb4ab6", "#ff5692", "#ff856c", "#ffc058", "#f9f871"],
  ['#ccd5ae', '#e9edc9', '#fefae0', '#faedcd', '#d4a373'],
  ["#606c38", "#283618", "#fefae0", "#dda15e", "#bc6c25"],
  // ["#0d0630", "#18314f", "#384e77", "#8bbeb2", "#e6f9af"],
]

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";

let canvas,
  seed,
  simplexNoise,
  border = 20,
  space = 160,
  recording = false,
  stopRecording = false;
const vehicules = [];

/* TODO
 * - Faire un veritable flowfield plutot que juste un vecteur 1,-1
 * - Faire de vraies bordures ?
 * - Faire d'autres palettes
 * - Shader ?
 */

// flowfield
const scaleX = 20;
const scaleY = 20;
const flowfield = [];
const noiseScaleX = .1;
const noiseScaleY = .1;
const debugFlowfield = false;

function setup() {
  seed = random(999999);
  simplexNoise = openSimplexNoise(seed);
  randomSeed(seed);
  canvas = createCanvas(1080, 1080);
  colorMode(HSB);
  // palette = [color(150,15,100,.1),color(150,15,100,.1),color(150,15,100,.1), color(0, 80, 90)]
  // palette = [
  //   color(150, 30, 90),
  //   color(150, 40, 50),
  //   color(150, 15, 60),
  //   color(150, 10, 10),
  //   color(150, 10, 50),
  //   color(150, 40, 50),
  //   color(150, 40, 10),
  //   // color(150, 8, 100),
  // ];

  complementary = [
    color(0, 100, 80),
  ]
  for (let j = 0; j < 35; j++) {
    const fx = random(space, width - space)
    const fy = random(space, height - space)
    const randomPalette = random(palettes)
    for (let i = 0; i < random(50, 100); i++) {
      // const x = random(500,600);
      // const y = random(500,600);
      const x = randomGaussian(fx,2);
      const y = randomGaussian(fy,2);
      vehicules.push(
        new Vehicule(
          x,y,
          randomPalette[int(random(randomPalette.length))]
          // random() > .05 ? palette[constrain(int(randomGaussian(map(y, 0, height, 0, palette.length)), .1), 0, palette.length-1)] : complementary[int(random(complementary.length))]
        )
      );
    }
  }
  background(darkColor)

  //flowfield
  const rows = height / scaleY;
  const cols = width / scaleX;
  for (let y = 0; y < rows; y++) {
    flowfield.push([]);
    for (let x = 0; x < cols; x++) {
      const a = simplexNoise.noise2D(x * noiseScaleX, y * noiseScaleY) * PI;
      flowfield[y].push({
        force: p5.Vector.fromAngle(a).normalize()
      });
    }
  }

  // circulare flowfield
  // const rows = height / scaleY;
  // const cols = width / scaleX;
  // for (let y = 0; y < rows; y++) {
  //   flowfield.push([]);
  //   for (let x = 0; x < cols; x++) {
  //     const a = atan2(y - rows/2, x - cols/2) - PI/1.5;
  //     flowfield[y].push({
  //       force: p5.Vector.fromAngle(a).normalize()
  //     });
  //   }
  // }
  fill(lightColor);
  noStroke();
  rect(0, 0, width, space);
  rect(0, 0, space, height);
  rect(width - space, 0, space, height);
  rect(0, height - space, width, space);
  if (recording) capturer.start();
}

function draw() {
  // background(color(150,15,100,.1));
  // background(255);

  for (let i = 0; i < vehicules.length; i++) {
    const vehicule = vehicules[i];
    if (!vehicule.isAlive()) {
      continue;
    }
    // vehicule.follow(flowfield)
    vehicule.update();
    // vehicule.applyForce(createVector(1, -1));
    // vehicule.display()
    for (let j = i; j < vehicules.length; j++) {
      if (i == j) continue;
      if (
        vehicule.overlaps(vehicules[j]) &&
        vehicules[j].isAlive() &&
        vehicule.show
      ) {
        const flee = vehicule.flee(vehicules[j].pos);
        vehicule.applyForce(flee);
        vehicule.overlapDisplay(vehicules[j]);
      }
    }
  }
  // fill(lightColor);
  // noStroke();
  // rect(0, 0, width, space);
  // rect(0, 0, space, height);
  // rect(width - space, 0, space, height);
  // rect(0, height - space, width, space);

  if(debugFlowfield) {
    stroke(0)
    for (let y = 0; y < flowfield.length; y++) {
      const row = flowfield[y];
      for (let x = 0; x < row.length; x++) {
        const { force } = row[x];
        push()
        translate(x * scaleX, y * scaleY)
        rotate(force.heading())
        line(0,0,scaleX,scaleY)
        pop()
      }
    }
  }
  if (recording) {
    capturer.capture(canvas.canvas);
    if (stopRecording) {
      capturer.stop();
      capturer.save();
      noLoop();
    }
  }
}

const coordToIndex = (coord) => {
  return {
    x: int(coord.x / scaleX),
    y: int(coord.y / scaleY)
  }
}

function keyPressed(key) {
  if (key.keyCode === 80) {
    stopRecording = true;
    saveCanvas(canvas, `organic_rectangle_${seed}`, "png");
  }
}
