const nodes = 5;
let network = [];
const avoidBorder = 0;
const poisson_radius = 5;
const poisson_k = 10;
let t = 0;

let squares = [];
let squaresShader = [];
let bgPixels = [];

const size = 1;

let canvas;
let imgShader;

const border = 0;
let particles = [];
let flowfield = [];
let scale = 10;
let noiseScale = 0.04;
let rows;
let cols;
let seed;
let currentNetworkIndex = 0;
let currentParticle;

// let palette = ["#D5B5F3"];
// let palette = ["#9C7B5A", "#CDAF7D", "#7CA98A", "#216C58", "#204534"];
// let palette = ["#5E101E", "#732427", "#8BD6C2", "#175450", "#082B2F"];
// let palette = ["#742228", "#721821", "#67181D", "#3B1816", "#0F0F0F"];
// let palette = ["#760000", "#86502A", "#B87B5E", "#4A6077", "#1F4B3E"];
// let palette = ["#00314F", "#6CA5AE", "#E5F1D9", "#B4A387", "#D38C86"];
// let palette = ["#D5B5F3", "#75D8EC", "#F3A9C5", "#A9F3D3", "#F3C4A9"];
// let palette = ["#082C59", "#0A72A1", "#4183B1", "#526C81", "#DDCEDA"];
// let palette = ["#012023", "#1B443C", "#93907D", "#855352", "#6C252D"];
// let palette = ["#840000", "#C58402", "#8B929C", "#18434C", "#1F2113"];
// let palette = [
//   "#151412",
//   "#272E1E",
//   "#354A29",
//   "#53734A",
//   "#84B188",
//   "#D5D7D4",
// ];
// let palette = [
//   "#220507",
//   "#450A04",
//   "#6A2B02",
//   "#8F5201",
//   "#C98A43",
//   "#E6D19F",
// ];
// // let palette = [
//   "#5C3C49",
//   "#8D5964",
//   "#AF747F",
//   "#D1617E",
//   "#E788A2",
//   "#EEAEB8",
// ];
// let palette = [
//   "#AA6A82",
//   "#B69792",
//   "#C7ACA1",
//   "#DDC1BE",
//   "#DBD1C8",
//   "#F6EEE1",
// ];
// let palette = [0, 250, 100, 150, 200, 50];
// let palette = [150, 250, 100, 50, 0, 200];
// let palette = [
//   "#24130C",
//   "#70381F",
//   "#C04700",
//   "#F07800",
//   "#EFB178",
//   "#CCECEB",
// ];
// let palette = [
//   "#6F2B3A",
//   "#DB6578",
//   "#E1B971",
//   "#FBEDC7",
//   "#949971",
//   "#5B5D45",
// ];
// let palette = [
//   "#341C1C",
//   "#5E101E",
//   "#732427",
//   "#8B473E",
//   "#8BD6C2",
//   "#388072",
// ];
// let palette = [
//   "#092A2F",
//   "#652B05",
//   "#B65302",
//   "#E7A700",
//   "#FAE647",
//   "#00B5AE",
// ];
// let palette = [
//   "#162E21",
//   "#063F39",
//   "#30685B",
//   "#73967E",
//   "#C68C20",
//   "#D8D091",
// ];
// let palette = [
//   "#0F0F0F",
//   "#221715",
//   "#3F1715",
//   "#54191B",
//   "#67181D",
//   "#68191E",
// ];
let palette = [
  "#02110C",
  "#162E21",
  "#073E38",
  "#30685B",
  "#73967E",
  "#DACE94",
];

let pp;

function preload() {
  imgShader = loadImage("../assets/chai.png");
}

function setup() {
  pp = shuffle(palette);
  pp = palette;
  colorFn = getColorFromShader2;
  // canvas = createCanvas(800, 800);
  canvas = createCanvas(600, 600);
  seed = random(9999);
  randomSeed(seed);
  background("#f5f5f5");
  imgShader.loadPixels();
  network = poissonDiskSampling(poisson_radius, poisson_k);
  // for (let i = 0; i < 1000; i++) {
  //   particles.push(new Particle());
  // }
  for (let i = 0; i < network.length; i++) {
    const { x, y } = network[i];
    particles.push(
      new Particle(
        x,
        y,
        noise(x * noiseScale * 10, y * noiseScale * 10) > 0.5
          ? color(255)
          : color(0)
      )
    );
  }
  rows = height / scale;
  cols = width / scale;
  for (let x = 0; x < cols; x++) {
    for (let y = 0; y < rows; y++) {
      // const nnn = map(y, 0, rows / 1.8, noiseScale, 0.001, true);
      // const nnn = 0.005;
      const phi = noise(x * noiseScale, y * noiseScale) * TWO_PI;
      const v = p5.Vector.fromAngle(phi);
      // v.setMag(0.1);
      v.normalize();
      flowfield[y * cols + x] = v;
    }
  }
  currentParticle = new Particle(
    network[currentNetworkIndex].x,
    network[currentNetworkIndex].y,
    colorFn(network[currentNetworkIndex].x, network[currentNetworkIndex].y)
  );
  currentParticle.follow(flowfield);
  noFill();
  stroke(0);
  rect(
    avoidBorder,
    avoidBorder,
    width - avoidBorder * 2,
    height - avoidBorder * 2
  );
}

function draw() {
  for (let i = 0; i < 200; i++) {
    noFill();
    // strokeWeight(random(2, 3));
    // fill(255, 20);
    beginShape();
    while (currentParticle.update()) {
      const pos = currentParticle.getPos();
      // stroke(getColorFromShader(pos.x, pos.y));
      stroke(currentParticle.col);
      // stroke(0);
      // curveVertex(pos.x, pos.y);
      // point(pos.x, pos.y);
      vertex(pos.x, pos.y);
      currentParticle.follow(flowfield);
    }
    endShape();
    currentNetworkIndex++;
    if (!!network[currentNetworkIndex]) {
      currentParticle = new Particle(
        network[currentNetworkIndex].x,
        network[currentNetworkIndex].y,
        colorFn(network[currentNetworkIndex].x, network[currentNetworkIndex].y)
      );
    } else {
      // pixelsort();
      noLoop();
      image(imgShader, 0, 0, 600, 600);
      return;
      noStroke();
      // fill("#f5f5f5");
      fill("rgb(248,248,255)");
      // fill(palette[palette.length - 1]);
      const border = 20;
      rect(0, 0, width, border);
      rect(0, 0, border, height);
      rect(width - border, 0, border, height);
      rect(0, height - border, width, border);
    }
  }
}

const getColorFromShader = (x, y) => {
  // if (x > 310 && x < 320 && y > 490 && y < 500) {
  //   return color("red");
  // }
  const xx = int(map(x, 0, width, 0, 225));
  const yy = int(map(y, 0, height, 0, 225));
  const id = (yy * imgShader.width + xx) * 4;
  return color(
    imgShader.pixels[id],
    imgShader.pixels[id + 1],
    imgShader.pixels[id + 2],
    imgShader.pixels[id + 3]
  );
};

const getColorFromShader2 = (x, y) => {
  // if (x > 300 && x < 350 && y > 500 && y < 550) {
  //   return color("red");
  // }
  const xx = int(map(x, 0, width, 0, 1080));
  const yy = int(map(y, 0, height, 0, 1080));
  const id = (yy * imgShader.width + xx) * 4;
  const R = imgShader.pixels[id];
  const G = imgShader.pixels[id + 1];
  const B = imgShader.pixels[id + 2];
  const ll = 0.2126 * R + 0.7152 * G + 0.0722 * B;
  return pp[int(constrain(map(ll, 0, 255, 0, pp.length), 0, pp.length - 1))];
};

const getColorFromShader3 = (x, y) => {
  // if (x > 300 && x < 350 && y > 500 && y < 550) {
  //   return color("red");
  // }
  const xx = int(map(x, 0, width, 0, 225));
  const yy = int(map(y, 0, height, 0, 225));
  const id = (yy * imgShader.width + xx) * 4;
  const R = imgShader.pixels[id];
  const G = imgShader.pixels[id + 1];
  const B = imgShader.pixels[id + 2];
  const ll = 0.2126 * R + 0.7152 * G + 0.0722 * B;
  const iii = map(ll, 0, 255, 0, pp.length);
  return pp[constrain(int(randomGaussian(iii, 0.1)), 0, pp.length - 1)];
};

const getColorFromShader4 = (x, y) => {
  // if (x > 300 && x < 350 && y > 500 && y < 550) {
  //   return color("red");
  // }
  const xx = int(map(x, 0, width, 0, 225));
  const yy = int(map(y, 0, height, 0, 225));
  const id = (yy * imgShader.width + xx) * 4;
  const R = imgShader.pixels[id];
  const G = imgShader.pixels[id + 1];
  const B = imgShader.pixels[id + 2];
  const ll = 0.2126 * R + 0.7152 * G + 0.0722 * B;
  const col =
    pp[int(constrain(map(ll, 0, 255, 0, pp.length), 0, pp.length - 1))];
  return gaussianColor(col);
};

const gaussianColor = (col) => {
  colorMode(HSB);
  const h = randomGaussian(hue(col), 1);
  const s = randomGaussian(saturation(col), 2);
  const b = randomGaussian(brightness(col), 2);
  const newCol = color(h, s, b);
  colorMode(RGB);
  return newCol;
};

const getColorFromShader5 = (x, y) => {
  // if (x > 310 && x < 320 && y > 490 && y < 500) {
  //   return color("red");
  // }
  const xx = int(map(x, 0, width, 0, 225));
  const yy = int(map(y, 0, height, 0, 225));
  const id = (yy * imgShader.width + xx) * 4;
  return gaussianColor(
    color(
      imgShader.pixels[id],
      imgShader.pixels[id + 1],
      imgShader.pixels[id + 2],
      imgShader.pixels[id + 3]
    )
  );
};

const getColorFromPosLerp = (x, y) => {
  // const c1 = color(100, random(255), 150);
  // const c2 = color(random(255), 150, 100);
  // const c2 = color("#561705");
  // const c1 = color("#A9F3D3");
  const c2 = color("#204534");
  // const c1 = color("#5E280E");
  // const c2 = color("#FFDBAE");
  const c1 = color("#FF396A");
  // const c2 = color(0);
  // const p = map(x + y, avoidBorder * 2, width + height - avoidBorder * 2, 0, 1);
  const p2 = map(y, avoidBorder, height - avoidBorder, 0, palette.length - 1);
  // return lerpColor(c1, c2, p);
  // return random() > 0.05 ? c1 : c2;
  return palette[constrain(int(randomGaussian(p2, 1)), 0, palette.length - 1)];
  // return palette[int(random(palette.length))];
};
//////////////////////////////////////////////////////

const pixelsort = () => {
  loadPixels();
  for (let x = 220; x < 800 / size - 180; x++) {
    squares.push([]);
    squaresShader.push([]);
    for (let y = 150; y < 800 / size - 150; y++) {
      let d = pixelDensity();
      const oneD = (y * (width * d) + x) * 4 * d;
      const oneDShader = (y * imgShader.width + x) * 4;
      // const n = noise(x * 0.1, y * 0.1);
      const n = map(noise(x * 0.1, y * 0.1), 0, 1, 0, 0);
      // const n = 0;
      let r = pixels[oneD * size] + n;
      let g = pixels[oneD * size + 1] + n;
      let b = pixels[oneD * size + 2] + n;
      let rs = imgShader.pixels[oneDShader * size] + n;
      let gs = imgShader.pixels[oneDShader * size + 1] + n;
      let bs = imgShader.pixels[oneDShader * size + 2] + n;
      // let c = get(x * size + size / 2, y * size + size / 2);
      squares[x - 220].push([r, g, b]);
      squaresShader[x - 220].push([rs, gs, bs]);
      fill(r, g, b);
      // stroke(0);
      noStroke();
      rect(x * size, y * size, size, size);
    }
  }
  const xs = [10, 11, 12, 20];
  // for (let k = 0; k < xs.length; k++) {
  //   const x = xs[k];
  for (let x = 0; x < squares.length; x++) {
    for (let j = 0; j < squares[x].length / 1; j++) {
      for (let i = 0; i < squares[x].length - 1; i++) {
        const s1 = squares[x][i];
        const s2 = squares[x][i + 1];
        const ss1 = squaresShader[x][i];
        const ss2 = squaresShader[x][i + 1];
        if (isComparable(ss1, ss2)) {
          if (compare1(s1, s2)) {
            squares[x][i] = s2;
            squares[x][i + 1] = s1;
            // squares[x][i] = s2.map((s) => (s * 1.01) % 200);
            // squares[x][i + 1] = s1.map((s) => (s * 1.01) % 200);
          }
        }
        // if (compare3(s1, s2)) {
        //   squares[x][i] = s2;
        //   squares[x][i + 1] = s1;
        // }
      }
    }
  }

  // colorMode(HSB, 255, 255, 100);
  for (let x = 0; x < squares.length; x++) {
    const element = squares[x];
    for (let y = 0; y < element.length; y++) {
      const s = element[y];
      fill(s[0], s[1], s[2]);
      // stroke(0);
      noStroke();
      rect(x * size + 220, y * size + 150, size, size);
    }
  }
};
const isComparable = (s1, s2) => s1[0] > 0 || s2[0] > 0;

const compare1 = (s1, s2) => s1[0] > s2[0];
const compare2 = (s1, s2) => s1[0] - s2[0] < 0; // 10 pour le sable
const compare3 = (s1, s2) => s1[2] - s2[0] < -10;

function neighbors(pos, distMax) {
  const n = [];
  // if (img.pixels[(pos.y * width + pos.x) * 4 + 4] === 0) return [];
  for (let i = 0; i < network.length; i++) {
    if (network[i].x == pos.x && network[i].y == pos.y) continue;

    if (dist(network[i].x, network[i].y, pos.x, pos.y) <= distMax)
      n.push(network[i]);
  }
  return n;
}

function isValidPoint(grid, cellsize, gwidth, gheight, p, radius) {
  const xx = int(map(p.x, 0, width, 0, 1080));
  const yy = int(map(p.y, 0, height, 0, 1080));
  if (imgShader.pixels[(yy * imgShader.width + xx) * 4 + 3] > 0) return false;

  /* Make sure the point is on the screen */
  if (
    p.x < avoidBorder ||
    p.x >= width - avoidBorder ||
    p.y < avoidBorder ||
    p.y >= height - avoidBorder
  )
    return false;

  /* Check neighboring eight cells */
  xindex = floor(p.x / cellsize);
  yindex = floor(p.y / cellsize);
  i0 = max(xindex - 1, 0);
  i1 = min(xindex + 1, gwidth - 1);
  j0 = max(yindex - 1, 0);
  j1 = min(yindex + 1, gheight - 1);

  for (i = i0; i <= i1; i++)
    for (j = j0; j <= j1; j++)
      if (grid[i][j] != null)
        if (dist(grid[i][j].x, grid[i][j].y, p.x, p.y) < radius) return false;

  /* If we get here, return true */
  return true;
}

function insertPoint(grid, cellsize, point) {
  xindex = floor(point.x / cellsize);
  yindex = floor(point.y / cellsize);
  grid[xindex][yindex] = point;
}

function poissonDiskSampling(radius, k) {
  N = 2;
  /* The final set of points to return */
  points = [];
  /* The currently "active" set of points */
  active = [];
  /* Initial point p0 */
  // p0 = createVector(
  //   int(random(avoidBorder, width - avoidBorder)),
  //   int(random(avoidBorder, height - avoidBorder))
  // );
  p0 = createVector(400, 50);
  // p1 = createVector(700, 400);
  grid = [];
  cellsize = floor(radius / sqrt(N));

  /* Figure out no. of cells in the grid for our canvas */
  ncells_width = ceil(width - (avoidBorder * 2) / cellsize) + 1;
  ncells_height = ceil(height - (avoidBorder * 2) / cellsize) + 1;

  /* Allocate the grid an initialize all elements to null */
  for (i = 0; i < ncells_width; i++) {
    grid[i] = [];
    for (j = 0; j < ncells_height; j++) grid[i][j] = null;
  }

  insertPoint(grid, cellsize, p0);
  points.push(p0);
  active.push(p0);
  // insertPoint(grid, cellsize, p1);
  // points.push(p1);
  // active.push(p1);

  while (active.length > 0) {
    random_index = int(random(active.length));
    p = active[random_index];

    found = false;
    for (tries = 0; tries < k; tries++) {
      theta = int(random(360));
      new_radius = int(random(radius, 2 * radius));
      pnewx = int(p.x + new_radius * cos(radians(theta)));
      pnewy = int(p.y + new_radius * sin(radians(theta)));
      pnew = createVector(pnewx, pnewy);

      if (
        !isValidPoint(grid, cellsize, ncells_width, ncells_height, pnew, radius)
      )
        continue;

      points.push(pnew);
      insertPoint(grid, cellsize, pnew);
      active.push(pnew);
      found = true;
      break;
    }

    /* If no point was found after k tries, remove p */
    if (!found) active.splice(random_index, 1);
  }

  return points;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `flowfield_1_${seed}`, "png");
}
