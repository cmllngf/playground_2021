class Thing {
  constructor(x, y, d, r = 0, K = int(random(2, 20)), cP = random(0.3, 0.6)) {
    this.x = x;
    this.y = y;
    this.K = K;
    this.size = 50;
    this.offset = random(1);
    // this.offset = 0;
    this.cutPoint = cP;
    this.direction = d;
    this.col = "";
    this.r = r;
    this.randomOffsetX = random(2, 3);
  }

  display() {
    push();
    for (let i = 0; i < this.K; i++) {
      let p = map(i + 1 - ((t + this.offset) % 1), 0, this.K, 0, 1);
      this.update(p);
    }
    pop();
  }

  update(p) {
    if (p < this.cutPoint) {
      const size = map(p, 0, this.cutPoint, 1, this.size);
      const xx =
        map(p, 0, this.cutPoint, -this.size * this.randomOffsetX, 0) *
        this.direction;
      this.drawSquare(xx, 0, size);
    } else {
      const xx =
        map(p, this.cutPoint, 1, 0, p * this.size + this.size * 10) *
        this.direction;
      this.drawSquare(xx, 0, this.size);
    }
  }

  drawSquare(x, y, s) {
    stroke(accentColor);
    // stroke(this.col);
    strokeWeight(2);
    fill(bgColor);
    // noFill();
    beginShape();
    vertex(x - s / 2, y - s / 2);
    vertex(x + s / 2, y - s / 2);
    vertex(x + s / 2, y + s / 2);
    vertex(x - s / 2, y + s / 2);
    endShape(CLOSE);
  }
}
