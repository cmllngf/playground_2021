let canvas;
let seed;
let simplexNoise;
let t = 0;
let palette = [];

const things = [];

const numFrame = 60;
const recording = false;

palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
palette = ["#020224", "#01104F", "#003C81", "#01669E", "#5075A2", "#6F91AD"];
palette = [255];

palette = ["red", "blue"];

function setup() {
  canvas = createCanvas(1080, 1080);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  const n = 100;
  for (let i = 0; i < n; i++) {
    const phi = (TWO_PI / n) * i;
    things.push(
      // new Thing(
      //   width / 2 + (j % 2 == 0 ? middleOffset : -middleOffset),
      //   y,
      //   j % 2 == 0 ? 1 : -1,
      //   int(map(y, 0, height, 20, 2)),
      //   map(y, 0, height, 0.3, 0.9)
      // )
      new Thing(width / 2, height / 2, 1, phi)
    );
  }
  if (recording) capturer.start();
}

// const bgColor = "#010101";
const bgColor = 20;
const accentColor = "#f5f5ff";

function draw() {
  const middleOffset = 200;
  background(bgColor);
  t = (frameCount / numFrame) % 1;

  for (let i = 0; i < things.length; i++) {
    const thing = things[i];
    push();
    translate(thing.x, thing.y);
    rotate(thing.r);
    translate(middleOffset, 0);
    thing.display();
    pop();
  }

  // if (frameCount == numFrame) noLoop();

  if (recording) {
    capturer.capture(canvas.canvas);
    if (frameCount >= 3 * numFrame) {
      capturer.stop();
      capturer.save();
      noLoop();
    }
  }
}

function ease(p) {
  return 3 * p * p - 2 * p * p * p;
}

function ease2(p, g) {
  if (p < 0.5) return 0.5 * pow(2 * p, g);
  else return 1 - 0.5 * pow(2 * (1 - p), g);
}

function softplus(q, p) {
  const qq = q + p;
  if (qq <= 0) {
    return 0;
  }
  if (qq >= 2 * p) {
    return qq - p;
  }
  return (1 / (4 * p)) * qq * qq;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `grid_3_${seed}`, "png");
}
