const nodes = 5;
let network = [];
const usedNetworkIndex = [];
let polygons = [];
const avoidBorder = 0;
let t = 0;
let palette

let canvas, ctx = { clip: () => {}, save: () => {}, restore: () => {} };
let img;
let simplexNoise;

const border = 0;

const scaleX = 120;
const scaleY = 120;
const noiseField = [];
let maxLength = 0;
let minLength = 1000000;

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";

const poisson_radius = 20;
const poisson_k = 2;

const paletteWhite = [lightColor]
const paletteBlackWhite = [darkColor, lightColor]
const paletteVibrant = ["#c25eb5", "#f6669e", "#ff7f82", "#ffa56a", "#ffcf5f", "#f9f871"];
const paletteVibrant2 = ["#3352c6", "#bb4ab6", "#ff5692", "#ff856c", "#ffc058", "#f9f871"];
const palettePink = ["#AF747F", "#D1617E", "#E788A2", "#EEAEB8", "#F9D0C3", "#F6EEE1"];
const paletteGreen = ["#151412", "#272E1E", "#354A29", "#53734A", "#84B188", "#D5D7D4"]

/**
 * SHAPES
 * - round corners, without padding, little faces (happy, sad, neutral, ...)
 * - 
 */

function setup() {
  seed = random(9999);
  randomSeed(seed);
  simplexNoise = openSimplexNoise(seed);
  canvas = createCanvas(1080, 1080);
  background(darkColor);
  ctx = canvas.drawingContext
  strokeWeight(2)

  const rows = height / scaleY;
  const cols = width / scaleX;

  // const fs = [shape4, shape1];
  const fs = [ shape13 ];
  for (let x = 0; x < cols; x++) {
    noiseField.push([]);
    for (let y = 0; y < rows; y++) {
      // const noiseValue = map(dist(x * scaleX,y*scaleY,width/2, height/2), 0, width/2, -1, 1, false);
      const noiseValue = simplexNoise.noise2D(x * 0.1, y * 0.1);
      noiseField[x].push({
        color: map(noiseValue, -1, 1, 0, 255),
        radius:
          noiseValue > 0
            // ? map(noiseValue, 0, 1, poisson_radius * 1, poisson_radius * .5)
            ? map(noiseValue, 0, 1, poisson_radius * 2, poisson_radius * 2)
            : map(noiseValue, -1, 0, poisson_radius * 2, poisson_radius * 4),
        func: fs[
          constrain(
            int(
              randomGaussian(
                noiseValue > 0
                  ? map(noiseValue, 0.5, 1, ceil(fs.length/2), fs.length - 1)
                  : map(noiseValue, -1, .5, 0, floor(fs.length/2)),
                0.4
              )
            ),
            0,
            fs.length - 1
          )
        ],
      });
    }
  }

  // network = poissonDiskSampling(poisson_radius, poisson_k);
  network = varyingRadiousPoissonDisk(poisson_k);

  const ROWS = 1;
  const COLS = 1;
  const MARGIN = 0;
  const w = width / COLS - MARGIN - MARGIN / COLS;
  const h = height / ROWS - MARGIN - MARGIN / ROWS;

  for (let i = 0; i < COLS; i++) {
    for (let j = 0; j < ROWS; j++) {
      const x0 = MARGIN + (w + MARGIN) * i;
      const y0 = MARGIN + (h + MARGIN) * j;
      const x1 = MARGIN + (w + MARGIN) * i + w;
      const y1 = MARGIN + (h + MARGIN) * j + h;

      const voronoi = d3.voronoi().extent([
        [x0, y0],
        [x1, y1],
      ]);
      polygons = polygons.concat(
        voronoi(
          getPointsInsideBox(network, x0, y0, x1, y1).map(({ p, i }) => {
            usedNetworkIndex.push(i);
            return [p.x, p.y];
          })
        ).polygons()
      );
    }
  }

  maxLength = polygons.reduce((acc, cur) => {
    const v = getAvgSideLengthFromPolygons(cur);
    return v > acc ? v : acc;
  }, 0);

  minLength = polygons.reduce((acc, cur) => {
    const v = getAvgSideLengthFromPolygons(cur);
    return v < acc ? v : acc;
  }, 10000000);

  palette = paletteGreen;
}

function draw() {
  noLoop();
  // translate(-width/2, -height/2);

  // for (let x = 0; x < noiseField.length; x++) {
  //   const col = noiseField[x];
  //   for (let y = 0; y < col.length; y++) {
  //     const entry = col[y];
  //     noStroke();
  //     fill(entry.color);
  //     rect(x * scaleX, y * scaleY, scaleX, scaleY);
  //   }
  // }

  const fs = [shape4, shape5];
  polygons.forEach((polygon, i) => {
    beginShape();
    noFill();
    stroke(darkColor);
    strokeWeight(3)
    // polygon.forEach((point) => {
    //   vertex(point[0], point[1]);
    // });
    endShape(CLOSE);
    getEntryFromPos(network[usedNetworkIndex[i]]).func(
      network[usedNetworkIndex[i]],
      polygon
    );
    // getEntryFromPos(network[i]).func(
    //   network[i],
    //   polygon
    // );
    // shape5(network[usedNetworkIndex[i]], polygon);
    // fs[int(map(getAvgSideLengthFromPolygons(polygon), 0, maxLength, 0, fs.length-1))](network[usedNetworkIndex[i]], polygon);
    // fs[int(map(getAvgSideLengthFromPolygons(polygon), 0, maxLength, 0, fs.length-1))](network[i], polygon);
  });

  // network.forEach((point) => {
  //   noStroke();
  //   fill(lightColor);
  //   circle(point.x, point.y, 2);
  // });
  console.log(count)
  granulate(20)
}

function granulate(gA){
  loadPixels();
  let d = pixelDensity();
  let halfImage = 4 * (width * d) * (height * d);
  for (let ii = 0; ii < halfImage; ii += 4) {
    grainAmount = random(-gA,gA)
    pixels[ii] = pixels[ii]+grainAmount;
    pixels[ii + 1] = pixels[ii+1]+grainAmount;
    pixels[ii + 2] = pixels[ii+2]+grainAmount;
    pixels[ii + 3] = pixels[ii+3]+grainAmount;
  }
  updatePixels();
}

// Art
const shape1 = (center, points) => {
  for (let i = 0; i < points.length; i++) {
    const p = points[i];
    const v1 = createVector(...p);
    const v2 = points[i + 1]
      ? createVector(...points[i + 1])
      : createVector(...points[0]);
    const middle = p5.Vector.lerp(v1, v2, 0.5);

    noFill();
    stroke(lightColor);
    line(middle.x, middle.y, center.x, center.y);
  }
};

const shape2 = (center, points) => {
  const shortestLenFromCenter = points.reduce((acc, cur, i) => {
    const v1 = createVector(...cur);
    const v2 = points[i + 1]
      ? createVector(...points[i + 1])
      : createVector(...points[0]);
    const middle = p5.Vector.lerp(v1, v2, 0.5);
    const d = dist(middle.x, middle.y, center.x, center.y);
    return d > acc ? acc : d;
  }, 1000000);

  noFill();
  stroke(lightColor);
  let r = shortestLenFromCenter;
  while (r >= 0) {
    circle(center.x, center.y, r * 2);
    r -= 8;
  }
};

const shape3 = (center, points) => {
  stroke(lightColor);
  fill(lightColor);
  beginShape();
  for (let i = 0; i < points.length; i++) {
    vertex(...points);
  }
  endShape(CLOSE);
};

const shape4 = (center, points) => {
  stroke(palette[int(random(palette.length))]);
  stroke(getColorFromPaletteAndY(paletteWhite, center.y));
  noFill()
  // fill(darkColor);
  beginShape()
  for (const p of points) {
    vertex(p[0], p[1])
  }
  endShape(CLOSE)
  for (let p = 1; p > 0; p -= 0.1) {
    beginShape();
    for (let i = 0; i < points.length; i++) {
      const v = p5.Vector.lerp(
        createVector(...points[i]),
        center,
        (p)
      );
      vertex(v.x, v.y);
    }
    endShape(CLOSE);
  }
};

const shape5 = (center, points) => {
  // stroke(darkColor);
  // fill(lightColor);
  // beginShape()
  // for (const p of points) {
  //   vertex(p[0], p[1])
  // }
  // endShape(CLOSE)
  noStroke();
  fill(lightColor);
  beginShape();
  for (let i = 0; i <= points.length; i++) {
    const v = points[i]
      ? p5.Vector.lerp(createVector(...points[i]), center, 0.2)
      : p5.Vector.lerp(createVector(...points[0]), center, 0.2);
    curveVertex(v.x, v.y);
  }
  endShape(CLOSE);
};

const shape6 = (center, points) => {
  noStroke();
  fill(palette[int(random(palette.length))]);
  fill(getColorFromPaletteAndY(paletteWhite, center.y));
  const roundedSides = roundCorners(points.map(p => p5.Vector.lerp(createVector(...p), center, 0)), 100)
  beginShape()
  roundedSides.map(v => vertex(v.x, v.y))
  endShape(CLOSE)
}

let count = 0
const shape7 = (center, points) => {
  noStroke();
  fill(palette[int(random(palette.length))]);
  let currentPoint = center;
  for (let i = 0; i < 100; i++) {
    circle(currentPoint.x, currentPoint.y, 4)
    count++;
    const randomPoint = points[int(random(points.length))];
    currentPoint = p5.Vector.lerp(currentPoint, createVector(...randomPoint), .45)
  }
}

const shape8 = (center, points) => {
  noFill();
  stroke(getColorFromPaletteAndY(paletteWhite, center.y));
  strokeWeight(3)
  for (let i = 0; i <= .99; i+= .1) {
    const roundedSides = roundCorners(points.map(p => p5.Vector.lerp(createVector(...p), center, i)), 10 + i * 200)
    beginShape()
    roundedSides.map(v => vertex(v.x, v.y))
    endShape(CLOSE)
  }
}

const shape9 = (center, points) => {
  drawingContext.save();
  stroke(lightColor)
  strokeWeight(1)
  ctx.shadowColor = lightColor;
  ctx.shadowBlur = 1;
  beginShape()
  points.forEach((point) => {
    vertex(point[0], point[1]);
  });
  endShape(CLOSE)
  ctx.shadowBlur = 0;
  ctx.clip()
  const xStep = int(random(30, 10))
  push()
  // rotate(random(TWO_PI))
  // const rotation = map(dist(center.x, center.y, width/2, height/2), width, 0, TWO_PI, 0) 
  const rotation = atan2(center.y - height/2, center.x - width/2) //- PI/1.5;
  rotate(randomGaussian(rotation, .2))
  for (let x = -width*2; x < width*2; x+=xStep) {
    line(x, -height*2, x, height*2)
  }
  pop()
  ctx.restore()
}

const shape10 = (center, points) => {
  drawingContext.save();
  stroke(lightColor)
  strokeWeight(1)
  ctx.shadowColor = lightColor;
  ctx.shadowBlur = 50;
  beginShape()
  points.forEach((point) => {
    vertex(point[0], point[1]);
  });
  endShape(CLOSE)
  ctx.shadowBlur = 0;
  ctx.clip()
  const xStep = int(random(40, 80))
  push()
  rotate(random(TWO_PI))
  noStroke()
  fill(lightColor)
  for (let x = -width*2; x < width*2; x+=xStep) {
    for (let y = -height*2; y < height*2; y+=xStep) {
      circle(x, y, 20)
    }
  }
  pop()
  ctx.restore()
}

const shape11 = (center, points) => {
  drawingContext.save();
  stroke(lightColor)
  strokeWeight(1)
  beginShape()
  points.forEach((point) => {
    vertex(point[0], point[1]);
  });
  endShape(CLOSE)
  ctx.clip()
  const xStep = int(random(20, 40))
  push()
  rotate(random(TWO_PI))
  stroke(lightColor)
  strokeWeight(2)
  noFill()
  for (let r = 0; r < width; r+=xStep) {
    circle(center.x, center.y, r)
  }
  pop()
  ctx.restore()
}

const shape12 = (center, points) => {
  drawingContext.save();
  stroke(lightColor)
  strokeWeight(1)
  beginShape()
  points.forEach((point) => {
    vertex(point[0], point[1]);
  });
  // endShape()
  ctx.restore()
}

const shape13 = (center, points) => {
  // drawingContext.save();
  stroke(lightColor)
  strokeWeight(1)
  // beginShape()
  // points.forEach((point) => {
  //   vertex(point[0], point[1]);
  // });
  // endShape(CLOSE)
  
  const roundedSides = roundCorners(points.map(p => p5.Vector.lerp(createVector(...p), center, 0.05)), 1)
  beginShape()
  roundedSides.map(v => vertex(v.x, v.y))
  endShape(CLOSE)
  for (let i = 0; i < points.length; i++) {
    const p = points[i];
    const v1 = createVector(...p);
    const v2 = points[i + 1]
      ? createVector(...points[i + 1])
      : createVector(...points[0]);
    const middle = p5.Vector.lerp(v1, v2, 0.5);

    fill(darkColor);
    stroke(lightColor)
    strokeWeight(1)
    circle(middle.x, middle.y, dist(v1.x, v1.y, v2.x, v2.y)/3);
  }

  // ctx.restore()
}

// Utils
const getEntryFromPos = (point) => {
  const x = int(map(point.x, 0, width, 0, noiseField.length));
  const y = int(map(point.y, 0, height, 0, noiseField[0].length));
  return noiseField[x][y];
};

const getMedianSideLengthFromPolygons = (points) => {
  const lengths = points.map((p1, i, ps) => {
    const p2 = ps[i + 1] ?? ps[0];
    return dist(p1[0], p1[1], p2[0], p2[1]);
  });
  return lengths.length % 2 === 1
    ? lengths[floor(lengths.length / 2)]
    : (lengths[lengths.length / 2] + lengths[lengths.length / 2 - 1]) / 2;
};

const getAvgSideLengthFromPolygons = (points) => {
  const sum = points.reduce((acc, cur, i, ps) => {
    const p2 = ps[i + 1] ?? ps[0];
    return acc + dist(cur[0], cur[1], p2[0], p2[1]);
  }, 0);
  return sum / points.length;
};

const getPointsInsideBox = (points, x0, y0, x1, y1) =>
  points
    .map((p, i) => ({ p, i }))
    .filter(({ p }) => p.x > x0 && p.y > y0 && p.x < x1 && p.y < y1);

const easeOutBack = (x) => {
  const c1 = 1.70158;
  const c3 = c1 + 1;

  return 1 + c3 * pow(x - 1, 3) + c1 * pow(x - 1, 2);
};

const easeInOutExpo = (x) => {
  return x === 0
    ? 0
    : x === 1
    ? 1
    : x < 0.5
    ? pow(2, 20 * x - 10) / 2
    : (2 - pow(2, -20 * x + 10)) / 2;
};

const getColorFromPaletteAndY = (palette, y) => {
  const index = map(y, 0, height, 0, palette.length - 1);
  const finalIndex = int(constrain(randomGaussian(index, .2) , 0, palette.length - 1));
  return palette[finalIndex]
}

// round corners
function roundCorners(verts, m) {
  
  const P = [];
  const N = verts.length;
  const n = 15;
  
 
  for (let i = 0; i < N; i++) {
    
    sv = verts[(i+N-2)%N];
    pv = verts[(i+N-1)%N];
    cv = verts[i];
    nv = verts[(i+1)%N];
    ev = verts[(i+2)%N];
    
    let pbis = bisector(sv, pv, cv, m);
    let cbis = bisector(pv, cv, nv, m);
    let nbis = bisector(cv, nv, ev, m);
    
    v = cbis[1];    
        
    minDist = 10000;
    nearest = null;
        
    for (let ibis of [pbis, nbis]) {
      
      isec = intersect(cbis[0], cbis[1], ibis[0], ibis[1])
      if (isec) {
        d = cv.dist(isec)
        if (d < minDist) {
          minDist = d
          nearest = isec
        }
      }
    }
    
    if (nearest) v = nearest;
         

                
    d1 = p5.Vector.sub(cv, pv).normalize()
    d2 = p5.Vector.sub(nv, cv).normalize()
        
    //dsum = p5.Vector.add(d1, d2) //sum of directions
        
    //s = dsum.mag() / d1.dot(dsum) //speed  
    //b = dsum.rotate(-HALFPI) //bisector
    //v = b.setMag(s*m).add(cv) //offsetted vertex 
            
    op_prev = orthoProjection(d1, cv, v)
    op_next = orthoProjection(d2, cv, v)
            
    dp = p5.Vector.sub(op_prev, v)
    dn = p5.Vector.sub(op_next, v)
        
    a = dn.angleBetween(dp)
    t = a / n //theta
    p = Math.atan2(op_next.y - v.y, op_next.x - v.x) // phi
    r = op_prev.dist(v) // radius
    
    for (let j = n; j > 0; j--) {
      x = Math.cos(t*j+p) * r * 0.9
      y = Math.sin(t*j+p) * r * 0.9
      P.push(createVector(x+v.x, y+v.y))
    }
  }
  return P
}


function orthoProjection(dir, ep, op) {
  
  let dv = p5.Vector.sub(op, ep);
  dir.mult(dv.dot(dir));
    
  return p5.Vector.add(ep, dir)

}


function intersect(p1, p2, p3, p4) {
                
  uA = ((p4.x-p3.x)*(p1.y-p3.y) - (p4.y-p3.y)*(p1.x-p3.x)) / ((p4.y-p3.y)*(p2.x-p1.x) - (p4.x-p3.x)*(p2.y-p1.y)) 
  uB = ((p2.x-p1.x)*(p1.y-p3.y) - (p2.y-p1.y)*(p1.x-p3.x)) / ((p4.y-p3.y)*(p2.x-p1.x) - (p4.x-p3.x)*(p2.y-p1.y))
    
  if (uA >= 0 && uA <= 1 & uB >= 0 & uB <= 1) {
    
    secX = p1.x + (uA * (p2.x-p1.x))
    secY = p1.y + (uA * (p2.y-p1.y))
        
    return createVector(secX, secY)
  }
}


function bisector(pv, cv, nv, m) {
  
  d1 = p5.Vector.sub(cv, pv).normalize()
  d2 = p5.Vector.sub(nv, cv).normalize()
    
  dsum = p5.Vector.add(d1, d2) //sum of directions
        
  s = dsum.mag() / d1.dot(dsum) //speed  
  v = dsum.rotate(-HALF_PI) //vector of bisector
  p = v.setMag(s*m).add(cv) //offsetted vertex 
  
    
  return [cv, p]
}

// Poisson Disk Sampling
const varyingRadiousPoissonDisk = (k) => {
  points = [];
  actives = [];
  p0 = createVector(width / 2, height / 2);
  grid = [];

  varyingInsertPoint(grid, p0);
  actives.push(p0);
  points.push(p0);

  while (actives.length > 0) {
    // for (let index = 0; index < 100; index++) {
    random_index = int(random(actives.length));
    p = actives[random_index];
    radius = getEntryFromPos(p).radius;

    found = false;
    for (tries = 0; tries < k; tries++) {
      theta = int(random(TWO_PI));
      new_radius = int(random(radius, 2 * radius));
      pnewx = int(p.x + new_radius * cos(theta));
      pnewy = int(p.y + new_radius * sin(theta));
      pnew = createVector(pnewx, pnewy);

      if (!varyingIsValidPoint(grid, pnew)) continue;

      varyingInsertPoint(grid, pnew);
      points.push(pnew);
      actives.push(pnew);
      found = true;
      break;
    }

    if (!found) actives.splice(random_index, 1);
  }

  return points;
};

const varyingIsValidPoint = (grid, point) => {
  for (let i = 0; i < grid.length; i++) {
    if (
      point.x < avoidBorder ||
      point.x >= width - avoidBorder ||
      point.y < avoidBorder ||
      point.y >= height - avoidBorder
    )
      return false;

    if (
      dist(grid[i].point.x, grid[i].point.y, point.x, point.y) < grid[i].radius
    )
      return false;
  }
  return true;
};

const varyingInsertPoint = (grid, point) => {
  grid.push({
    ...getEntryFromPos(point),
    point,
  });
  // console.log(grid)
};

// Good Poisson Disk Sampling
function isValidPoint(grid, cellsize, gwidth, gheight, p, radius) {
  // if (img.pixels[(p.y * width + p.x) * 4 + 4] === 0) return false;
  // if (dist(width / 2, height / 2, p.x, p.y) > 300) return;

  /* Make sure the point is on the screen */
  if (
    p.x < avoidBorder ||
    p.x >= width - avoidBorder ||
    p.y < avoidBorder ||
    p.y >= height - avoidBorder
  )
    return false;

  /* Check neighboring eight cells */
  xindex = floor(p.x / cellsize);
  yindex = floor(p.y / cellsize);
  i0 = max(xindex - 1, 0);
  i1 = min(xindex + 1, gwidth - 1);
  j0 = max(yindex - 1, 0);
  j1 = min(yindex + 1, gheight - 1);

  for (i = i0; i <= i1; i++)
    for (j = j0; j <= j1; j++)
      if (grid[i][j] != null)
        if (dist(grid[i][j].point.x, grid[i][j].point.y, p.x, p.y) < radius)
          return false;

  /* If we get here, return true */
  return true;
}

function insertPoint(grid, cellsize, point) {
  xindex = floor(point.x / cellsize);
  yindex = floor(point.y / cellsize);
  grid[xindex][yindex] = {
    ...getEntryFromPos(point),
    point,
  };
}

function poissonDiskSampling(radius, k) {
  N = 2;
  /* The final set of points to return */
  points = [];
  /* The currently "active" set of points */
  active = [];
  /* Initial point p0 */
  // p0 = createVector(
  //   int(random(avoidBorder, width - avoidBorder)),
  //   int(random(avoidBorder, height - avoidBorder))
  // );
  p0 = createVector(width / 2, height / 2);
  // p1 = createVector(700, 400);
  grid = [];
  cellsize = floor(radius / sqrt(N));

  /* Figure out no. of cells in the grid for our canvas */
  ncells_width = ceil(width - (avoidBorder * 2) / cellsize) + 1;
  ncells_height = ceil(height - (avoidBorder * 2) / cellsize) + 1;

  /* Allocate the grid an initialize all elements to null */
  for (i = 0; i < ncells_width; i++) {
    grid[i] = [];
    for (j = 0; j < ncells_height; j++) grid[i][j] = null;
  }

  insertPoint(grid, cellsize, p0);
  points.push(p0);
  active.push(p0);
  // insertPoint(grid, cellsize, p1);
  // points.push(p1);
  // active.push(p1);

  while (active.length > 0) {
    random_index = int(random(active.length));
    p = active[random_index];

    found = false;
    for (tries = 0; tries < k; tries++) {
      theta = int(random(360));
      new_radius = int(random(radius, 2 * radius));
      pnewx = int(p.x + new_radius * cos(radians(theta)));
      pnewy = int(p.y + new_radius * sin(radians(theta)));
      pnew = createVector(pnewx, pnewy);

      if (
        !isValidPoint(grid, cellsize, ncells_width, ncells_height, pnew, radius)
      )
        continue;

      points.push(pnew);
      insertPoint(grid, cellsize, pnew);
      active.push(pnew);
      found = true;
      break;
    }

    /* If no point was found after k tries, remove p */
    if (!found) active.splice(random_index, 1);
  }

  return points;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `poisson_disk_2_${seed}`, "png");
}
