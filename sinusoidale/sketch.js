let canvas;
let seed;
let simplexNoise;
const recording = false;
const numFrame = 240;
let t = 0;

function setup() {
  canvas = createCanvas(1080, 1080);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
}

function draw() {
  background("black");
  randomSeed(seed);
  smooth(1);
  t = (frameCount / numFrame) % 1;

  const stepX = int(width / 15);
  const stepY = int(height / 30);
  {
    let y = 0;
    for (let x = 0; x <= width; x+=stepX) {
      drawLine(x, 0, 0, y, randomFromLength(dist(x, 0, 0, y)), random(5, 20));
      y += stepY;
    }
  }
  {
    let x = width;
    for (let y = height; y > 0; y-=stepY) {
      drawLine(width, y, x, height, randomFromLength(dist(width, y, x, height)), random(5, 20));
      x -= stepX;
    }
  }
}

const randomFromLength = (l) => {
  const maxLen = dist(width, 0, 0, height);
  const mean = map(l, 100, maxLen, 50, 200);
  return randomGaussian(mean, 10);
};

function periodicFunction(p, K) {
  return map(sin(TWO_PI * p), -1, 1, 0, K);
}

const drawLine = (x1, y1, x2, y2, K = 200, colorsCount = 1) => {
  const start = createVector(x1, y1);
  const end = createVector(x2, y2);
  let curStep = 0;
  strokeCap(SQUARE);
  noFill();
  beginShape();
  const k = periodicFunction(t, K);
  for (let i = 0; i < K; i++) {
    const p = i / K;
    const cur = p5.Vector.lerp(start, end, p);
    // const deviance = sin(p * TWO_PI) * 40;
    const deviance = 0;
    curveVertex(
      cur.x + sin(cur.x) * 10 + deviance,
      cur.y + cos(cur.y) * 10 + deviance
    );
    if (curStep === int(K / colorsCount)) {
      stroke("white");
      endShape();
      beginShape();
      // curveVertex(cur.x, cur.y);
      curStep = 0;
    }
    curStep++;
  }
  endShape();
};

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `sinusoidale_${seed}`, "png");
}
