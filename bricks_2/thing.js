class Thing {
  constructor({
    x,
    y,
    w = 40,
    h = 100,
    reverseW,
    reverseH,
    offsetX = 0,
    offsetY = 0,
    offset = 0,
  }) {
    this.x = x;
    this.y = y;
    this.h = h;
    this.w = w;
    this.offsetX = offsetX;
    this.offsetY = offsetY;
    this.offset = offset;

    this.dx = x;
    this.dy = y;

    this.fromToW = [0, this.w];
    this.fromToH = [0, this.h];

    if (reverseW) {
      this.x += this.w;
      this.fromToW = [0, -this.w];
    }

    if (reverseH) {
      this.y += this.h;
      this.fromToH = [0, -this.h];
    }
  }

  update(p) {
    // const h = map(p, 0, 1, 100, 0);
    // this.h = abs(sin(p * TWO_PI) * 100);

    const cut = 0.75;

    const po = (p + this.offset) % 1;

    if (po < cut) {
      this.w = this.fromToW[1];
      const pp = map(po, 0, cut, 0, 1);
      const h = map(ease2(pp, 5), 0, 1, this.fromToH[1], this.fromToH[0]);
      this.h = h;
      // this.dx = this.x;
      this.dx = this.x + this.offsetX;
      this.dy = this.y + this.offsetY;
    } else {
      this.h = this.fromToH[1];
      const pp = map(po, cut, 1, 0, 1);
      const w = map(ease2(pp, 3), 0, 1, this.fromToW[0], this.fromToW[1]);
      this.w = w;
      this.dx = this.x + this.offsetX;
      this.dy = this.y + this.offsetY;
      // this.dy = this.y;
    }

    this.display();
  }

  display() {
    noStroke();
    fill(accentColor);
    rect(this.dx, this.dy, this.w, this.h);
  }
}
