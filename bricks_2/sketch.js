let canvas;
let seed;
let simplexNoise;
let t = 0;
let palette = [];
const numFrame = 240;

const recording = true;

palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
palette = ["#020224", "#01104F", "#003C81", "#01669E", "#5075A2", "#6F91AD"];
palette = [255];

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";

let bgColor = darkColor;
let accentColor = lightColor;

const things = [];

function setup() {
  canvas = createCanvas(1080, 1080);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  randomSeed(seed);
  // things.push(new Thing(100, 100));

  for (let j = 0; j < 30; j++) {
    let y = 0;
    for (let i = 0; i < 15; i++) {
      const oi = i % 2 == 0 ? -20 : 0;
      const oj = j % 2 == 0 ? -50 : 0;
      // const h = int(random(50, 150));
      // const h = 100;
      things.push(
        new Thing({
          x: 40 * j,
          y: 100 * i,
          // y,
          reverseW: random() > 0.5,
          reverseH: random() > 0.5,
          offsetX: oi,
          offsetY: oj,
          offset: random(),
          // h,
        })
      );
      // y += h;
    }
  }

  if (recording) capturer.start();
}

function draw() {
  background(bgColor);
  t = (frameCount / numFrame) % 1;
  // noLoop();

  for (let i = 0; i < things.length; i++) {
    const thing = things[i];
    thing.update(t);
  }

  if (recording) {
    capturer.capture(canvas.canvas);
    if (frameCount === numFrame) {
      capturer.stop();
      capturer.save();
      noLoop();
    }
  }
}

function ease(p) {
  return 3 * p * p - 2 * p * p * p;
}

function ease2(p, g) {
  if (p < 0.5) return 0.5 * pow(2 * p, g);
  else return 1 - 0.5 * pow(2 * (1 - p), g);
}

function softplus(q, p) {
  const qq = q + p;
  if (qq <= 0) {
    return 0;
  }
  if (qq >= 2 * p) {
    return qq - p;
  }
  return (1 / (4 * p)) * qq * qq;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `ball_1_${seed}`, "png");
}
