let seed, canvas;
let border = 200;
let number_of_particles = 1000;
let number_of_particle_sets = 6;
let particle_sets = [];
let tick = 0;

let palette;

let nzoom = 10;

function setup() {
  canvas = createCanvas(1080, 1350);
  seed = random(99999);
  randomSeed(seed);
  noFill();
  background("#e7e7db");
  stroke(0);
  strokeWeight(2);
  smooth();

  palette = [color(80, 55, 83, 20), color(21, 142, 121, 20)];

  for (var j = 0; j < number_of_particle_sets; j++) {
    let ps = [];
    for (var i = 0; i < number_of_particles; i++) {
      ps.push(
        new Particle(
          random(width),
          random(height),
          // randomGaussian(width / 2, 180),
          //border + random(width - 2 * border),
          //border + random(height - 2 * border),
          // randomGaussian((3 * height) / 5, 180),
          random(TWO_PI)
        )
      );
    }
    particle_sets.push(ps);
  }
}

function draw() {
  for (let i = 0; i < 1; i++) {
    particle_sets.forEach(function (particles, index) {
      particles.forEach(function (particle) {
        particle.update(index);
        particle.display(index);
      });
    });
  }
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `system_${seed}`, "png");
}
