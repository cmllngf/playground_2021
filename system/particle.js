class Particle {
  constructor(x, y, phi) {
    this.pos = createVector(x, y);
    this.angle = phi;
    this.val = 0;
  }

  update(index) {
    this.pos.x += cos(this.angle) * 1;
    this.pos.y += sin(this.angle) * 1;

    let nx =
      map(this.pos.y, 0, height, 3, 0.5) * map(this.pos.x, 0, width, -1, 1);
    let ny =
      2 *
      map(this.pos.y, 0, height, 3, 0.5) *
      map(this.pos.y, 0, height, -1, 1);

    let n = createVector(nx, ny);

    let nval =
      (noise(n.x * 10, n.y * 20) +
        0.045 * (index - number_of_particle_sets / 3)) %
      1;

    this.angle += 3 * map(nval, 0, 1, -1, 1);
    this.val = nval;
  }

  display() {
    if (this.val > 0.482 && this.val < 0.518) {
      //stroke(palette[index % palette.length]);
      // if (index === 2) stroke(255, 25, 20, 20);
      // else stroke(20, 10);
      push();
      translate(this.pos.x, this.pos.y);
      rotate(this.angle);
      point(0, 0);
      pop();
    }
  }
}
