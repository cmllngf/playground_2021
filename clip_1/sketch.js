let canvas;
let seed;
let simplexNoise;
let ctx

function setup() {
  canvas = createCanvas(800, 800);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  ctx = canvas.drawingContext
}

function draw() {
  noLoop()
  randomSeed(seed);
  background(180, 170, 190)
  
  // translate(width/2, height/2)

  // random([drawRectClip, drawCircleClip])(randomGaussian(width/2, 50), randomGaussian(height/2, 50), random(800, 900))

  for (let i = 0; i < 10; i++) {
    random([drawRectClip, drawCircleClip])(random(width), random(height), random(100, 800))
  }

  // drawRectClip(200, 200, 200)
  // drawRectClip(600, 600, 200)
  granulate(10)
}

const drawCircleClip = (x, y, r) => {
  drawingContext.save();
  noStroke()
  fill(180, 170, 190)
  circle(x,y,r+10)
  // noFill()
  stroke(180, 170, 190)
  strokeWeight(0)
  ctx.shadowColor = 'black';
  ctx.shadowBlur = 20;
  circle(x, y, r)
  ctx.clip()
  drawLinePattern()
  ctx.shadowBlur = 0;
  ctx.restore()
}

const drawRectClip = (x, y, r) => {
  drawingContext.save();
  rectMode(CENTER);
  noStroke()
  fill(180, 170, 190)
  rect(x,y,r+10)
  // noFill()
  stroke(180, 170, 190)
  strokeWeight(0)
  ctx.shadowColor = 'black';
  ctx.shadowBlur = 20;
  rect(x, y, r, r)
  ctx.clip()
  drawDotPattern()
  ctx.shadowBlur = 0;
  ctx.restore()
}

const drawLinePattern = () => {
  const xStep = int(random(4, 15))
  push()
  rotate(random(TWO_PI))
  strokeWeight(1)
  stroke(1)
  for (let x = -width*2; x < width*2; x+=xStep) {
    line(x, -height*2, x, height*2)
  }
  pop()
}

const drawDotPattern = () => {
  const xStep = int(random(30, 50))
  push()
  rotate(random(TWO_PI))
  strokeWeight(random(10, 20))
  stroke(1)
  for (let x = -width*2; x < width*2; x+=xStep) {
    for (let y = -height*2; y < height*2; y+=xStep) {
      point(x, y)
    }
  }
  pop()
}

function granulate(gA){
  loadPixels();
  let d = pixelDensity();
  let halfImage = 4 * (width * d) * (height * d);
  for (let ii = 0; ii < halfImage; ii += 4) {
    grainAmount = random(-gA,gA)
    pixels[ii] = pixels[ii]+grainAmount;
    pixels[ii + 1] = pixels[ii+1]+grainAmount;
    pixels[ii + 2] = pixels[ii+2]+grainAmount;
    pixels[ii + 3] = pixels[ii+3]+grainAmount;
  }
  updatePixels();
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `clip_1_${seed}`, "png");
}
