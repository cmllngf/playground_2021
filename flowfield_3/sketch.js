// color palette
let canvas, seed; // let palette = ["#D5B5F3"];
// let palette = ["#9C7B5A", "#CDAF7D", "#7CA98A", "#216C58", "#204534"];
// let palette = ["#5E101E", "#732427", "#8BD6C2", "#175450", "#082B2F"];
// let palette = ["#742228", "#721821", "#67181D", "#3B1816", "#0F0F0F"];
// let palette = ["#760000", "#86502A", "#B87B5E", "#4A6077", "#1F4B3E"];
// let palette = ["#00314F", "#6CA5AE", "#E5F1D9", "#B4A387", "#D38C86"];
// let palette = ["#D5B5F3", "#75D8EC", "#F3A9C5", "#A9F3D3", "#F3C4A9"];
// let palette = ["#082C59", "#0A72A1", "#4183B1", "#526C81", "#DDCEDA"];
// let palette = ["#012023", "#1B443C", "#93907D", "#855352", "#6C252D"];
// let palette = ["#840000", "#C58402", "#8B929C", "#18434C", "#1F2113"];
// let palette = [
//   "#151412",
//   "#272E1E",
//   "#354A29",
//   "#53734A",
//   "#84B188",
//   "#D5D7D4",
// ];
// let palette = [
//   "#220507",
//   "#450A04",
//   "#6A2B02",
//   "#8F5201",
//   "#C98A43",
//   "#E6D19F",
// ];
// // let palette = [
//   "#5C3C49",
//   "#8D5964",
//   "#AF747F",
//   "#D1617E",
//   "#E788A2",
//   "#EEAEB8",
// ];
// let palette = [
//   "#AA6A82",
//   "#B69792",
//   "#C7ACA1",
//   "#DDC1BE",
//   "#DBD1C8",
//   "#F6EEE1",
// ];
// let palette = [0, 250, 100, 150, 200, 50];
// let palette = [150, 250, 100, 50, 0, 200];
// let palette = [
//   "#24130C",
//   "#70381F",
//   "#C04700",
//   "#F07800",
//   "#EFB178",
//   "#CCECEB",
// ];
// let palette = [
//   "#6F2B3A",
//   "#DB6578",
//   "#E1B971",
//   "#FBEDC7",
//   "#949971",
//   "#5B5D45",
// ];
// let palette = [
//   "#341C1C",
//   "#5E101E",
//   "#732427",
//   "#8B473E",
//   "#8BD6C2",
//   "#388072",
// ];
// let palette = [
//   "#092A2F",
//   "#652B05",
//   "#B65302",
//   "#E7A700",
//   "#FAE647",
//   "#00B5AE",
// ];
// let palette = [
//   "#162E21",
//   "#063F39",
//   "#30685B",
//   "#73967E",
//   "#C68C20",
//   "#D8D091",
// ];
// let palette = [
//   "#0F0F0F",
//   "#221715",
//   "#3F1715",
//   "#54191B",
//   "#67181D",
//   "#68191E",
// ];
let palette = [
  "#02110C",
  "#162E21",
  "#073E38",
  "#30685B",
  "#73967E",
  "#DACE94",
];

// set weights for each color

var weights = [1, 1, 1, 1, 1, 2];

// scale of the vector field
// smaller values => bigger structures
// bigger values  ==> smaller structures

var myScale = 3;

// number of drawing agents

var nAgents = 1000;

let agent = [];

// set spinning direction (plus or minus)

var direction = -1;

var par = 0;

function setup() {
  canvas = createCanvas(800, 800);
  seed = random(99999);
  randomSeed(seed);
  colorMode(HSB, 360, 100, 100);
  // strokeCap(SQUARE);

  background(0);

  for (let i = 0; i < nAgents; i++) {
    agent.push(new Agent());
  }
}

function draw() {
  for (let i = 0; i < agent.length; i++) {
    agent[i].update();
  }
}

// select random colors with weights from palette

function myRandom(colors, weights) {
  let tt = 0;
  let sum = 0;

  for (let i = 0; i < colors.length; i++) {
    sum += weights[i];
  }

  let rr = random(0, sum);

  for (let j = 0; j < weights.length; j++) {
    if (weights[j] >= rr) {
      return colors[j];
    }
    rr -= weights[j];
  }

  return tt;
}

// paintining agent

class Agent {
  constructor() {
    this.p = createVector(
      random(-100, width + 100),
      random(-100, height + 100)
    );

    this.pOld = createVector(this.p.x, this.p.y);

    this.step = 1;

    let temp = myRandom(palette, weights);
    this.color = temp;
    // this.color = color(
    //   hue(temp) + randomGaussian() * 10,
    //   saturation(temp) + randomGaussian() * 10,
    //   brightness(temp) - 10,
    //   random(10, 90)
    // );

    // this.color = color(random(360), random(30, 50), random(40, 70));

    this.strokeWidth = random(5, 10);

    this.isOutside = false;
  }

  update() {
    this.p.x += direction * vector_field(this.p.x, this.p.y).x * this.step;
    this.p.y += direction * vector_field(this.p.x, this.p.y).y * this.step;

    // hey...thanks to Danilo Gasques for suggesting the removal of
    // this boundary check.

    // if (this.p.x < 0) this.isOutside = true;
    // else if (this.p.x > width) this.isOutside = true;
    // else if (this.p.y < 0) this.isOutside = true;
    // else if (this.p.y > height) this.isOutside = true;

    // if (this.isOutside) {
    //   this.p.x = random(width);
    //   this.p.y = random(height);
    //   this.pOld.set(this.p);
    // }

    strokeWeight(this.strokeWidth);
    strokeCap(PROJECT);
    stroke(this.color);
    // stroke(255, .1)

    line(this.pOld.x, this.pOld.y, this.p.x, this.p.y);

    this.pOld.set(this.p);

    this.isOutside = false;
  }
}

// vector field function
// the painting agents follow the flow defined
// by this function

function vector_field(x, y) {
  x = map(x, 0, width, -myScale, myScale);
  y = map(y, 0, height, -myScale, myScale);

  let k1 = 1;
  let k2 = 700;

  let u = sin(k1 * y) - atan(k2 * y);
  let v = atan(k2 * x) + cos(k1 * x + 4);

  return createVector(u, v);
}

// function to select

function myRandom(colors, weights) {
  let tt = 0;
  let sum = 0;

  for (let i = 0; i < colors.length; i++) {
    sum += weights[i];
  }

  let rr = random(0, sum);

  for (let j = 0; j < weights.length; j++) {
    if (weights[j] >= rr) {
      return colors[j];
    }
    rr -= weights[j];
  }

  return tt;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `flowfield_3_${seed}`, "png");
}
