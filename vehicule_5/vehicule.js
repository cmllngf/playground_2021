class Vehicule {
  constructor(_x, _y, _color) {
    this.pos = createVector(_x, _y);
    // this.vel = createVector(0, 0);
    this.vel = p5.Vector.random2D();
    // this.vel.mult(random(2,10))
    this.acc = createVector(0, 0);
    this.maxSpeed = random(2, 6);
    // this.maxSpeed = 4;
    this.maxForce = random(0.05, 0.3);
    // this.maxForce = .2;
    this.radar = random(10, 20);
    this.currentRadar = this.radar;
    this.maxLifetime = random(300, 1000);
    this.lifetime = 0;
    this.color = _color;
    // this.color.setAlpha(.05)
    this.show = random() > 0;
    this.neighborDist = 50;
  }

  /* Art */
  display() {
    noStroke();
    fill(0);
    circle(this.pos.x, this.pos.y, 10);
    noFill();
    stroke(0);
    // circle(this.pos.x, this.pos.y, this.radar * 2);
  }

  overlapDisplay(vehicule) {
    // const h = hue(this.color)
    // const s = map(this.lifetime, 0, this.maxLifetime, saturation(this.color), 0)
    // const b = brightness(this.color)
    // const size = map(p5.Vector.dist(this.pos, vehicule.pos), 0, this.currentRadar + vehicule.currentRadar, this.radar, 1)
    // const size = map(
    //   p5.Vector.dist(this.pos, vehicule.pos),
    //   0,
    //   this.currentRadar + vehicule.currentRadar,
    //   this.currentRadar,
    //   1
    // );
    const size = p5.Vector.dist(this.pos, vehicule.pos) / 5;
    // fill(10, .1)
    // fill(color(0, 80, 90))
    // fill(color(h,s,b));
    fill(this.color);
    noStroke();
    const mid = p5.Vector.lerp(this.pos, vehicule.pos, 0.5);
    circle(mid.x, mid.y, size * 2);
  }
  /*************/

  /* Flocking */
  flock(boids) {
    const align = this.align(boids);
    const cohesion = this.cohesion(boids);
    const separation = this.separation(boids);
    
    align.mult(alignSlider.value());
    cohesion.mult(cohesionSlider.value());
    separation.mult(separationSlider.value());
    
    this.repulsewalls(walls);

    this.applyForce(align);
    this.applyForce(cohesion);
    this.applyForce(separation);
  }

  align(boids) {
    const steering = createVector(0, 0);
    let count = 0;
    boids.forEach((other) => {
      const d = dist(other.pos.x, other.pos.y, this.pos.x, this.pos.y);
      if (other !== this && d < this.neighborDist) {
        steering.add(other.vel);
        count++;
      }
    });
    if (count > 0) {
      steering.div(count);
      steering.normalize();
      steering.mult(this.maxSpeed);
      steering.sub(this.vel);
      steering.limit(this.maxForce);
    }
    return steering;
  }

  separation(boids) {
    const steering = createVector();
    const distSeparation = 25;
    let count = 0;
    boids.forEach((other) => {
      const d = p5.Vector.dist(other.pos, this.pos);
      if (other !== this && d < distSeparation) {
        let diff = p5.Vector.sub(this.pos, other.pos);
        diff.normalize();
        diff.div(d);
        steering.add(diff);
        count++;
      }
    });
    if (count > 0) {
      steering.div(count);
    }
    if (steering.mag() > 0) {
      steering.normalize();
      steering.setMag(this.maxSpeed);
      steering.sub(this.vel);
      steering.limit(this.maxForce);
    }
    return steering;
  }

  cohesion(boids) {
    let steering = createVector(0, 0);
    let count = 0;
    boids.forEach((other) => {
      const d = dist(other.pos.x, other.pos.y, this.pos.x, this.pos.y);
      if (other !== this && d < this.neighborDist) {
        steering.add(other.pos);
        count++;
      }
    });
    if (count > 0) {
      steering.div(count);
      steering = this.seek(steering);
    }
    return steering;
  }
  /************/

  /* Lifecycle */
  update() {
    this.vel.add(this.acc);
    this.vel.limit(this.maxSpeed);
    this.pos.add(this.vel);
    this.acc.mult(0);
    // this.edges();
    this.lifetime++;
    this.currentRadar = map(this.lifetime, 0, this.maxLifetime, this.radar, 0);
  }

  overlaps(vehicule) {
    return (
      p5.Vector.dist(this.pos, vehicule.pos) <
      this.currentRadar + vehicule.currentRadar
    );
  }

  applyForce(force) {
    this.acc.add(force);
  }

  isAlive() {
    return this.lifetime < this.maxLifetime;
  }

  edges() {
    if (this.pos.x < border) {
      this.pos.x = width - border;
    }
    if (this.pos.x > width - border) {
      this.pos.x = border;
    }
    if (this.pos.y < border) {
      this.pos.y = height - border;
    }
    if (this.pos.y > height - border) {
      this.pos.y = border;
    }
  }

  repulsewalls(
    walls //reppels walls ie: array with evvery 10th border point added and repelled with EXTRA force that boid-boid repusion
  ) {
    let perceptionRadius = 40;
    let acc = createVector();
    let ctr = 0;
    for (let wall of walls) {
      let distan = dist(this.pos.x, this.pos.y, wall.x, wall.y);
      if (this.pos != wall && distan < perceptionRadius) {
        let d = p5.Vector.sub(this.pos, wall);
        d.div(distan);
        acc.add(d);
        ctr++;
      }
    }

    if (ctr > 0) {
      acc.div(ctr);
      acc.setMag(this.maxSpeed);
      acc.sub(this.vel);
      acc.limit(this.maxForce * 1.5);
    }
    this.acc.add(acc.mult(2));
  }
  /*************/

  /* flowfield */
  follow(flowfield) {
    const { x, y } = coordToIndex(this.pos);
    this.applyForce(flowfield[y][x].force);
    if (debugFlowfield) {
      // console.log(x,y)
    }
  }
  /*************/

  /* Default */
  seek(target) {
    const force = p5.Vector.sub(target, this.pos);
    force.normalize();
    force.mult(this.maxSpeed);
    force.sub(this.vel);
    force.limit(this.maxForce);
    return force;
  }

  flee(target) {
    return this.seek(target).mult(-1);
  }
  /*************/
}
