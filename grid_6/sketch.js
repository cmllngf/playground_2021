let canvas,
  seed,
  t = 0,
  img;

let simplexNoise;
let palette;
palette = ["#264653", "#2a9d8f", "#e9c46a", "#f4a261", "#e76f51"];
palette = ["#D5B5F3", "#75D8EC", "#F3A9C5", "#A9F3D3", "#F3C4A9"];
palette = ["#5e101e", "#753a15", "#796619", "#6a9141", "#39ba87", "#00e0e1"];
palette = ["#AF747F", "#D1617E", "#E788A2", "#EEAEB8", "#F9D0C3", "#F6EEE1"];
palette = ["#3352c6", "#bb4ab6", "#ff5692", "#ff856c", "#ffc058", "#f9f871"];

const numFrame = 180;
const recording = false;
const noiseScale = 0.005;

function preload() {
  img = loadImage("../assets/la_joconde.png");
}

function setup() {
  canvas = createCanvas(1080, 1080);
  seed = random(99999);
  img.loadPixels();
  simplexNoise = openSimplexNoise(seed);
  if (recording) capturer.start();
}

function draw() {
  randomSeed(seed);
  t = frameCount / numFrame;
  background("#f5f8f8");

  const n = 100;
  const space = width / n;
  for (let i = 0; i < n; i++) {
    const x = space * i + space / 2;
    // myLineY(x);
    // myLineY2(x);
    myLineY3(x);
  }

  // noLoop();
  if (recording) {
    capturer.capture(canvas.canvas);
    if (t === 1) {
      capturer.stop();
      capturer.save();
      noLoop();
    }
  }
}

const myLineY3 = (x, steps = 1000) => {
  stroke("#010303");
  fill(palette[int(random(palette.length))]);
  strokeWeight(2);
  noFill();
  beginShape();
  for (let step = 0; step <= steps; step++) {
    const y = (height / steps) * step;
    let amp = getBrightnessAmp(x, y);
    // if (amp < 2) {
    //   amp = simplexNoise.noise2D(step * 0.01, x) * 10;
    // }
    const freq = 0.8;
    // vertex(x + sin(step * freq) * amp, y);
    vertex(x + sin(step * freq + t * 1000) * amp, y);
  }
  endShape();
};

const getAmpForTime = () => {
  // return abs(20 * sin(t * TWO_PI));
  return abs(20 * sin(ease2(t, 1) * PI));
};

const get1DIndex = (x, y, minX = 0, maxX = width, minY = 0, maxY = height) => {
  const xx = int(map(x, minX, maxX, 0, img.width));
  const yy = int(map(y, minY, maxY, 0, img.height));
  return (yy * img.width + xx) * 4;
};

const getBrightnessAmp = (x, y) => {
  const index = get1DIndex(int(x), int(y));
  const R = img.pixels[index];
  const G = img.pixels[index + 1];
  const B = img.pixels[index + 2];
  const A = img.pixels[index + 3];
  if (A === 0) {
    return 0;
  }
  const ll = 0.2126 * R + 0.7152 * G + 0.0722 * B;
  // return map(ll, 0, 255, 100, 1);
  return map(ll, 0, 255, getAmpForTime(), 0);
};

const myLineY2 = (x, steps = 1000) => {
  stroke("#010303");
  fill(palette[0]);
  noFill();
  beginShape();
  for (let step = 0; step <= steps; step++) {
    const y = (height / steps) * step;
    const dd = t * 300;
    const amp = map(dist(x, y, width / 2, height / 2), 0, 350, 10, 0, true);
    const freq = 0.4;
    vertex(x + sin(step * freq) * amp, y);
  }
  endShape();
};

const myLineY = (x, steps = 1000) => {
  stroke("#010303");
  fill(palette[0]);
  noFill();
  beginShape();
  for (let step = 0; step <= steps; step++) {
    const y = (height / steps) * step;
    const amp = simplexNoise.noise2D(step * 0.01, x) * 10;
    // const amp = 10;
    // const freq = simplexNoise.noise2D(step * 0.01, 0) * 0.5;
    const freq = 0.5;
    vertex(x + sin(step * freq) * amp, y);
  }
  endShape();
};

function offset(x, y) {
  return 0.015 * dist(x, y, width / 2, height / 2);
}
function ease2(p, g) {
  if (p < 0.5) return 0.5 * pow(2 * p, g);
  else return 1 - 0.5 * pow(2 * (1 - p), g);
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `grid_6_${seed}`, "png");
}
