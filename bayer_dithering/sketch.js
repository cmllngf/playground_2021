let canvas;
let simplexNoise;

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";

let bayer;

const palette = ["#845ec2", "#d65db1", "#ff6f91", "#ff9671", "#ffc75f", "#f9f871"];

function setup() {
  seed = random(9999);
  randomSeed(seed);
  simplexNoise = openSimplexNoise(seed);
  canvas = createCanvas(1080, 1080, WEBGL);
  background(darkColor);

  bayer = getBayer(1);
  colorMode(HSB)
}

const scale = 10;
function draw() {
  noLoop();
  translate(-width / 2, -height / 2);

  for (let x = 0; x < width; x += scale) {
    for (let y = 0; y < height; y += scale) {
      noStroke();
      // const dithered = getClosestColor(getColor2(x, y), getValueAt(bayer, int(x / scale), int(y / scale)))
      // const dithered = getClosestColor(getColor(x, y), getColorIndex(x, y), getValueAt(bayer, int(x / scale), int(y / scale)))
      const dithered =
      getColor3(x, y) >
        getValueAt(bayer, int(x / scale), int(y / scale))
          ? color(darkColor)
          : color(lightColor);
      // const dithered = color(map(getValueAt(bayer, int(x / scale), int(y/scale)), 0, 15, 0, 360), 50, 50)
      fill(dithered);
      square(x, y, scale);
    }
  }
}

const getClosestColor = (gradientValue, origColorIndex, bayerValue) => {
  return gradientValue > bayerValue ? palette[origColorIndex] : palette[(origColorIndex ) % palette.length]
}

const getColor = (x,y) => {
  const border = 250
  if (x > border && x < width - border && y > border && y < height - border) {
    // return map(x+y, border+border, width - border + height-border, 20, 0)
    return map(x+y, 0, width + height, 20, 0)
  }
  return map(x+y, 0, width + height, 0, 20)
}

const getColor3 = (x,y) => {
  const w = width/10
  const xTo2W = x % (w*2)
  const max = 15
  if (xTo2W > w) {
    // return map(y, 0, height, 15, 0)
    return map(x+y, 0, width + height, max, 0)
  }
  // return map(y, 0, height, 0, 15)
  return map(x+y, 0, width + height, 0, max)
}

const getColor2 = (x,y) => {
  const border = 250
  let index
  if (x > border && x < width - border && y > border && y < height - border) {
    // return map(x+y, border+border, width - border + height-border, 20, 0)
    index = map(x + y, 0,  width + height, 0, palette.length )
  } else {
    index = map(x + y, 0,  width + height, palette.length , 0)
  }
  // return palette[constrain(int(randomGaussian(index, 1)), 0, palette.length - 1)]
  return palette[int(index)]
}

const getColorIndex = (x,y) => {
  const border = 250
  let index
  if (x > border && x < width - border && y > border && y < height - border) {
    // return map(x+y, border+border, width - border + height-border, 20, 0)
    index = int(map(x+y, 0,  width + height, 0, palette.length - 1))
  } else {
    index = int(map(x+y, 0,  width + height, palette.length - 1, 0))
  }
  return constrain(int(randomGaussian(index, 1)), 0, palette.length - 1)
}

// bayer[x][y]
const getBayer = (level) => {
  if (level === 0) {
    return [
      [0, 3],
      [2, 1],
    ];
  }
  if (level === 1) {
    return [
      [0, 12, 3, 15],
      [8, 4, 11, 7],
      [2, 14, 1, 13],
      [10, 6, 9, 5],
    ];
  }
  return [
    [0  , 48, 12, 60, 3 , 51, 15, 63],
    [32 , 16, 44, 28, 35, 19, 47, 31],
    [8  , 56, 4 , 52, 11, 59, 7 , 55],
    [40 , 24, 36, 20, 43, 27, 39, 23],
    [2  , 50, 14, 62, 1 , 49, 13, 61],
    [34 , 18, 46, 30, 33, 17, 45, 29],
    [10 , 58, 6 , 54, 9 , 57, 5 , 53],
    [42 , 26, 38, 22, 41, 25, 37, 21],
  ];
};

const getValueAt = (matrix, x, y) => {
  return matrix[x % matrix.length][y % matrix.length];
};

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `bayer_dithering_${seed}`, "png");
}
