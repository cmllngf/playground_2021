let canvas;
let seed;
let simplexNoise;
let space;

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";
let t = 0

function ease(p) {
  return 3*p*p - 2*p*p*p;
}

function ease(p, g) {
  if (p < 0.5) 
    return 0.5 * pow(2*p, g);
  else
    return 1 - 0.5 * pow(2*(1 - p), g);
}

let n1 = 20;

let n2 = 20;
const R = 800;
const N = 15;

const K = n2/N;

function fx(p){
  return 1-cos(TWO_PI*p);
}

function position2(p,q){ 
  const pp = map(p,0,1,-1.3*width,0.7*width);

  const theta = map(p,0,1,-0.85*HALF_PI,0.45*HALF_PI);
  const r = map(q,0,1,0.6*R,1.4*R);
  const y = r*sin(theta);
  const x = r*cos(theta)-0.87*R;
  const z = -1*softplus(-pp,300)+200*pow(map(p,0,1,2.8,1)*2*max(0,abs(q-0.5)-map(p,0,1,-0.3,0.17)),2.0)-10;

  return createVector(x,y,z);
}

class Thing{
  seed = random(10,1000);
  
  constructor(i_,j_){
    this.i = i_;
    this.j = j_;
  }
  
  col = 300*pow(random(1),5.0);

  coladd(p){
    return 500*pow(map(noise(2*seed+7.5*p,0),-1,1,0,1),7.0);
  }
  
  displacementI = 100;
  
  displacement(p){
    return new createVector(this.dx(p),this.dy(p),this.dz(p));
  }
  
  dx(p){
    return this.displacementI*noise(3*this.seed+4.0*p,0);
  }
  
  dy(p){
    return this.displacementI*noise(4*this.seed+4.0*p,0);
  }
  
  dz(p){
    return this.displacementI*noise(5*this.seed+4.0*p,0)/4;
  }
  
  dr(p){
    return pow((noise(6*this.seed+13.0*p,0)+1)/2,5.0);
  }
  
  show(){
    for(let k=0;k<=K;k++){
      const q = 1.0*this.i/n1;
      const ind = this.j+(k+t)*N;
      
      const p0 = map(ind+0.5,0,n2,0,1);
      const p1 = map(ind,0,n2,0,1);
      const p2 = map(ind+1,0,n2,0,1);
      
      const q0 = (this.i+0.5)/n1;
      const q1 = (this.i)/n1;
      const q2 = (this.i+1)/n1;

      const v0 = position2(p0,q0).add(this.displacement(p0).mult(0.8));
      const v1 = position2(p1,q1);
      const v2 = position2(p2,q1);
      const v3 = position2(p2,q2);
      const v4 = position2(p1,q2);

      const f = constrain(1.3*p0,0,1);
      
      stroke(f*(10+this.coladd(p0)));
      // stroke(255)
      fill(0);
      strokeWeight(1.3);
      
      beginShape();
      vertex(v0.x,v0.y,v0.z);
      vertex(v1.x,v1.y,v1.z);
      vertex(v2.x,v2.y,v2.z);
      endShape(CLOSE);
      
      beginShape();
      vertex(v0.x,v0.y,v0.z);
      vertex(v3.x,v3.y,v3.z);
      vertex(v2.x,v2.y,v2.z);
      endShape(CLOSE);
      
      beginShape();
      vertex(v0.x,v0.y,v0.z);
      vertex(v3.x,v3.y,v3.z);
      vertex(v4.x,v4.y,v4.z);
      endShape(CLOSE);
      
      beginShape();
      vertex(v0.x,v0.y,v0.z);
      vertex(v1.x,v1.y,v1.z);
      vertex(v4.x,v4.y,v4.z);
      endShape(CLOSE);
      

      noStroke();
      fill(255);
      push();
      translate(v0.x,v0.y,v0.z);
      sphere(this.dr(p0)*3.2);
      pop();
    }
  }
}

const array = [];

function setup() {
  canvas = createCanvas(...SETTINGS.DIM, WEBGL);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  space = SETTINGS.border;
  colorMode(HSB)
  // ortho(-width/2, width/2, -height/2, height/2, -8000, 10000);

  
  let k = 0;
  for(let i=0;i<n1;i++){
    for(let j=0;j<N;j++){
      array[k] = new Thing(i,j);
      k++;
    }
  }
}

function draw() {
  t = mouseX/width;
  // t=-.5
  // t +=.1
  // t = t % 1
  console.log(t)
  // randomSeed(seed)
  // t+=.001
  orbitControl()
  // noLoop()
  background(10);

  // noFill()
  // stroke(darkColor)
  push()
  
  // translate(width/2,height/2,0);
  
  rotateZ(0.15*HALF_PI);
  
  rotateX(1.53);
  
  // translate(0,-1000,-20);
  translate(0,0,-20);

  // beginShape(TRIANGLE_STRIP);
  // for (let x = 0; x < 10; x++) {
  //   for (let y = 0; y < 2; y++) {
  //     vertex(x * 100, 0, y * 100)
  //   }
  // }
  // endShape();

  // draw_surface(0)

  for(let i=0;i<n1*N;i++)
  {
    array[i].show();
  }
  pop()

  if (SETTINGS.drawBorder) {
    translate(-width/2, -height/2, 8000)
    fill(lightColor);
    noStroke();
    strokeWeight(4)
    rect(0, 0, space, height);
    rect(0, 0, width, space);
    rect(0, height - space, width, space);
    rect(width - space, 0, space, height);
    noFill()
    stroke(lightColor)
    line(space, space, space, height - space + 2)
    line(space - 2, space, width - space + 2, space)
    line(width - space + 2, height - space, space, height - space)
    line(width - space, height - space, width - space, space)
  }
}

function softplus(q,p){
  const qq = q+p;
  if(qq<=0){
    return 0;
  }
  if(qq>=2*p){
    return qq-p;
  }
  return 1/(4*p)*qq*qq;
}

function surface(p, theta,off)
{
  const p2 = map(p,0,1,-750,1350);
  const z = softplus(-p2,w)-off;
  let r = 550 + softplus(p2,w) - off;
  r *= pow(p,2.0);
  
  const x = r*cos(theta);
  const y = r*sin(theta);
  
  return createVector(x,y,z);
}

function draw_surface(off = 0)
{
  push();
  
  let m1 = 60;
  let m2 = 60;
  
  stroke(darkColor);
  fill(lightColor);
  // noFill()
  //strokeWeight(2.0);
  // noStroke();
  
  for(let i=0;i<m1;i++){
    beginShape(TRIANGLE_STRIP);
    for(let j=0;j<=m2;j++){
      const theta1 = map(i+t,0,m1,0,TWO_PI);
      const theta2 = map(i+1+t,0,m1,0,TWO_PI);
      
      // const p = map(j+3*t+0.05*random(0),0,m2,0,1);
      const p = map(j,0,m2,0,1);
      
      const v1 = surface(p,theta1,off);
      const v2 = surface(p,theta2,off);
      
      vertex(v1.x,v1.y,v1.z);
      vertex(v2.x,v2.y,v2.z);

      // push()
      // fill('red')
      // translate(v1.x,v1.y,v1.z)
      // sphere(10, 10, 10)
      // pop()
      // push()
      // fill('green')
      // translate(v2.x,v2.y,v2.z)
      // sphere(15, 10, 10)
      // pop()
      // fill(lightColor)
    }
    endShape();
  }
  pop();
}

/**
 * { value: '', weight: 4 }
 */
const myWeightedRandom = (weightedRandoms) => random(weightedRandoms.reduce((acc, cur) => [...acc, ...new Array(cur.weight).fill(cur.value)],[]))

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `architecture_2_${seed}`, "png");
}
