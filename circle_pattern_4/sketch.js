let canvas;
let seed;
let simplexNoise;
let t = 0;
let palette = [];

const circleCount = 25;
const circleGap = 15;
const noiseScale = 0.01;
const noiseScale2 = 0.012;
const tRadius = 0.3;
const numFrame = 120;
const maxDisplacement = 40;
const minDisplacement = 1;

const recording = false;

let img;

// palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
// palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
// palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
// palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
// palette = ["#020224", "#01104F", "#003C81", "#01669E", "#5075A2", "#6F91AD"];
// palette = [255];
palette = ["#572D39", "#6E4D45", "#918B7D", "#A69B9D", "#C1C098"];

function preload() {
  // img = loadImage("../assets/axe1.png");
  // img = loadImage("../assets/lucifer.jpg");
  // img = loadImage("../assets/la_joconde.png");
  img = loadImage("../assets/lady_earring.png");
}

function setup() {
  canvas = createCanvas(1080, 1080);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  if (recording) capturer.start();
}

function draw() {
  translate(width / 2, height / 2 - 20);
  randomSeed(seed);
  background(10);
  // noStroke();
  // noLoop();
  strokeWeight(2);
  img.loadPixels();
  t = frameCount / numFrame;
  noLoop();

  const n = 100;
  for (let i = 1; i <= n; i++) {
    push();
    rotate(random(PI / 24));
    drawCircle3(0, 0, i * 2 + 2.5 * i);
    pop();
  }
  // drawCircle3(0, 0, 200);
  // circle(0, height / 2, 20);

  if (recording) {
    capturer.capture(canvas.canvas);
    if (t === 3) {
      capturer.stop();
      capturer.save();
      noLoop();
    }
  }
}

const drawCircle = (x, y, r, steps = r) => {
  stroke("#d5d5d5");
  noFill();
  const stepPhi = TWO_PI / steps;
  for (let i = 0; i < steps; i++) {
    stroke(random(100, 255));
    arc(x, y, r, r, stepPhi * i, stepPhi * (i + 1));
  }
};

const drawCircle3 = (x, y, r, steps = r * 2) => {
  stroke("#d5d5d5");
  noFill();
  const stepPhi = TWO_PI / steps;
  const stepPerArc = 10;
  const noiseScale = 7;
  for (let i = 0; i < steps; i++) {
    stroke(random(100, 255));

    const dBase = map(
      dist(
        cos(stepPhi * i) * r,
        height / 2,
        cos(stepPhi * i) * r,
        sin(stepPhi * i) * r - 20
      ),
      0,
      height / 2,
      180,
      0,
      true
    );

    beginShape();
    for (let j = 0; j <= stepPerArc + 2; j++) {
      const phi = stepPhi * i + (stepPhi / stepPerArc) * j;
      const xa = cos(phi) * r;
      const ya = sin(phi) * r;
      stroke(getBrightnessAmp(xa, ya));
      // const noiseV =
      //   simplexNoise.noise2D(xa * noiseScale, ya * noiseScale) *
      //   map(dist(x, y, r, r), 0, width / 2, 1, dBase);
      // const noiseV =
      //   simplexNoise.noise2D(cos(phi) * noiseScale, sin(phi) * noiseScale) *
      //   map(dist(x, y, r, r), 0, width / 2, 1, dBase);
      const noiseV =
        simplexNoise.noise2D(cos(phi) * noiseScale, 0) *
        map(dist(x, y, x, r), 0, width / 2, 1, dBase);
      curveVertex(xa, ya);
    }
    endShape();
  }
};

const drawCircle2 = (x, y, r, steps = r * 2) => {
  stroke("#d5d5d5");
  noFill();
  const stepPhi = TWO_PI / steps;
  const noiseRadius = 6;
  for (let i = 0; i < steps; i++) {
    stroke(random(100, 255));

    const dBase = map(
      dist(0, height / 2, 0, sin(stepPhi * i) * r),
      0,
      height / 2,
      30,
      0,
      true
    );

    const noiseV =
      simplexNoise.noise2D(
        cos(stepPhi * i) * noiseRadius,
        sin(stepPhi * i) * noiseRadius
      ) * map(dist(x, y, r, r), 0, width / 2, 1, dBase);
    // const noiseV =
    //   simplexNoise.noise2D(
    //     cos(stepPhi * i) * noiseRadius,
    //     sin(stepPhi * i) * noiseRadius
    //   ) * dBase;
    arc(x, y, r, r + noiseV, stepPhi * i, stepPhi * (i + 1));
  }
};

const getAmpForTime = () => {
  // return abs(20 * sin(t * TWO_PI));
  return abs(20 * sin(ease2(t, 1) * PI));
};

const get1DIndex = (x, y, minX = 0, maxX = width, minY = 0, maxY = height) => {
  const xx = int(map(x, minX, maxX, 0, img.width));
  const yy = int(map(y, minY, maxY, 0, img.height));
  return (yy * img.width + xx) * 4;
};

const getBrightnessAmp = (x, y) => {
  const index = get1DIndex(
    int(x),
    int(y),
    -width / 2,
    width / 2,
    -height / 2,
    height / 2
  );
  const R = img.pixels[index];
  const G = img.pixels[index + 1];
  const B = img.pixels[index + 2];
  const A = img.pixels[index + 3];
  if (A === 0) {
    return random(20, 150);
    // return palette[int(random(palette.length))];
  }
  return color(R, G, B);
  const ll = 0.2126 * R + 0.7152 * G + 0.0722 * B;
  // return map(ll, 0, 255, 100, 1);
  return map(ll, 0, 255, 50, 255);
};

function ease(p) {
  return 3 * p * p - 2 * p * p * p;
}

function ease2(p, g) {
  if (p < 0.5) return 0.5 * pow(2 * p, g);
  else return 1 - 0.5 * pow(2 * (1 - p), g);
}

function softplus(q, p) {
  const qq = q + p;
  if (qq <= 0) {
    return 0;
  }
  if (qq >= 2 * p) {
    return qq - p;
  }
  return (1 / (4 * p)) * qq * qq;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `circle_pattern_4_${seed}`, "png");
}
