let canvas;
let seed;
let simplexNoise;
let ctx;

// color fill
let visited = [];
let colors = [];
let pixel_size = 2;

const MARGIN = 20;
const curves = [];
const systems = [];
const N = 700;

const palette = [
  "#845ec2",
  "#d65db1",
  "#ff6f91",
  "#ff9671",
  "#ffc75f",
  "#f9f871",
];

const lightColor = "#f5f8f8";
const darkColor = "#080F0F";

const presets = [
  // {
  //   noiseOffset: 1,
  //   color: 50,
  // },
  // {
  //   noiseOffset: 100,
  //   color: 100,
  // }
];

// todo
// Prendre plusieurs points d'une curve pour en faire une rectangle courbé

function setup() {
  colorMode(HSB);
  canvas = createCanvas(1080, 1350);
  seed = random(99999);
  randomSeed(seed);
  simplexNoise = openSimplexNoise(seed);

  for (let i = 0; i < 1; i++) {
    presets.push({
      color: random(palette),
      noiseOffset: random(10000),
    });
  }

  for (let i = 0; i < N; i++) {
    const curve = random() > .99 ? new CurveSwirl() : new Curve();
    curve.create(curves);
    if (curve.points.length > 0) {
      curves.push(curve);
    }
  }

  for (let i = 0; i < curves.length; i++) {
    const curve = curves[i];
    for (let p = 0; p < curve.points.length; p++) {
      const point = curve.points[p];
      systems.push(
        new System(
          point,
          [0, PI / 2, PI, -PI / 2].map((a) =>
            createVector(point.x + cos(a) * 5, point.y + sin(a) * 5)
          )
        )
      );
    }
  }
  background(random(palette));
  background(darkColor);
}

function draw() {
  //! Show flowfield
  noLoop();
  stroke(250);
  strokeWeight(7);
  strokeCap(PROJECT);
  noFill();
  for (let i = 0; i < curves.length; i++) {
    const curve = curves[i];
    beginShape();
    stroke(curve.color);
    for (let p = 0; p < curve.points.length; p++) {
      const point = curve.points[p];
      // circle(point.x, point.y, 4)
      curveVertex(point.x, point.y);
    }
    endShape();
  }

  // for (let i = 0; i < 20; i++) {
  //   if (!!systems[0] && !systems[0].update()) {
  //     systems.shift();
  //     if(!systems[0]) {
  //       noLoop();
  //       // fill("rgb(248,248,255)");
  //       // noStroke();
  //       // rect(0, 0, width, border);
  //       // rect(0, 0, border, height);
  //       // rect(width - border, 0, border, height);
  //       // rect(0, height - border, width, border);
  //       // granulate(15)
  //       return;
  //     }
  //   }
  // }
  granulate(17);
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `curve_test_2_${seed}`, "png");
}
