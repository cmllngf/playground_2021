let seed;
let xoff = 0;
let yoff = 0;
let canvas;
const numberOfParticles = 100;
let flowfield;
const scl = 100;
let cols, rows;
const particles = [];
const border = 150;
let t = 0;

function setup() {
  canvas = createCanvas(800, 800);
  seed = random(99999);
  background("black");
  cols = width / scl;
  rows = height / scl;
  flowfield = new Array(cols * rows);
  const anglesPos = [
    { a: 0, x: -width / 2 + border, y: -height / 2 + border },
    { a: PI / 2, x: -width / 2 + border, y: -height / 2 + border },
    { a: PI, x: width / 2 - border, y: height / 2 - border },
    { a: -PI / 2, x: width / 2 - border, y: height / 2 - border },
  ];

  for (let i = 0; i < numberOfParticles; i++) {
    const aP = anglesPos[int(random(anglesPos.length))];
    particles.push(
      new Particle(
        random() < 0.5,
        p5.Vector.fromAngle(aP.a),
        createVector(aP.x, aP.y)
      )
    );
  }

  yoff = 0;
  for (let y = 0; y < rows; y++) {
    xoff = 0;
    for (let x = 0; x < cols; x++) {
      const phi = noise(xoff, yoff) * TWO_PI;
      const v = p5.Vector.fromAngle(phi);
      v.setMag(0.1);
      flowfield[y * cols + x] = v;
      xoff += 0.5;
    }
    yoff += 0.5;
  }
}

function draw() {
  translate(width / 2, height / 2);
  randomSeed(seed);
  // background(10, 120);
  for (let i = 0; i < numberOfParticles; i++) {
    particles[i].follow(flowfield);
    particles[i].update();
    const n = random(2, 2);
    for (let j = 0; j < n; j++) {
      push();
      const a = (TWO_PI / n) * j;
      rotate(a);
      if (n % 2 == 0) scale(-1, -1);
      particles[i].display();
      pop();
    }
  }
}

function keyPressed(key) {
  console.log(key);
  if (key.keyCode === 80) saveCanvas(canvas, `flowfield_1_${seed}`, "png");
}
