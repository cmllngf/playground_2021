let canvas;
let seed;
let simplexNoise;
let space;

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";
let t = 0, w = 300

function ease(p) {
  return 3*p*p - 2*p*p*p;
}

function ease(p, g) {
  if (p < 0.5) 
    return 0.5 * pow(2*p, g);
  else
    return 1 - 0.5 * pow(2*(1 - p), g);
}

function drawBlock(p, pos, v1, v2,angle,swf, L)
{
  push();
  translate(pos.x,pos.y,pos.z);
  angle = 0
  
  let l1 = L;
  let l2 = 40.0*pow(p,3.2);
  let h = 10.0;
  
  let v3 = v1.copy().cross(v2);
  
  let V1 = v1.copy().mult(cos(angle)).add(v3.copy().mult(sin(angle)));
  let V2 = v2;
  let V3 = v1.copy().mult(-sin(angle)).add(v3.copy().mult(cos(angle)));
  
  let v000 = V1.copy().mult(l1/2).add(V2.copy().mult(l2/2)).add(V3.copy().mult(h/2));
  let v100 = V1.copy().mult(-l1/2).add(V2.copy().mult(l2/2)).add(V3.copy().mult(h/2));
  let v010 = V1.copy().mult(l1/2).add(V2.copy().mult(-l2/2)).add(V3.copy().mult(h/2));
  let v110 = V1.copy().mult(-l1/2).add(V2.copy().mult(-l2/2)).add(V3.copy().mult(h/2));
  let v001 = V1.copy().mult(l1/2).add(V2.copy().mult(l2/2)).add(V3.copy().mult(-h/2));
  let v101 = V1.copy().mult(-l1/2).add(V2.copy().mult(l2/2)).add(V3.copy().mult(-h/2));
  let v011 = V1.copy().mult(l1/2).add(V2.copy().mult(-l2/2)).add(V3.copy().mult(-h/2));
  let v111 = V1.copy().mult(-l1/2).add(V2.copy().mult(-l2/2)).add(V3.copy().mult(-h/2));
  
  stroke(255);
  strokeWeight(1.7*swf);
  fill(lightColor);
  stroke(lightColor)
  // noStroke()
  
  // beginShape();
  // vertex(v000.x,v000.y,v000.z);
  // vertex(v100.x,v100.y,v100.z);
  // vertex(v110.x,v110.y,v110.z);
  // vertex(v010.x,v010.y,v010.z);
  // endShape(CLOSE);
  
  // beginShape();
  // vertex(v001.x,v001.y,v001.z);
  // vertex(v101.x,v101.y,v101.z);
  // vertex(v111.x,v111.y,v111.z);
  // vertex(v011.x,v011.y,v011.z);
  // endShape(CLOSE);
  
  // beginShape();
  // vertex(v000.x,v000.y,v000.z);
  // vertex(v100.x,v100.y,v100.z);
  // vertex(v101.x,v101.y,v101.z);
  // vertex(v001.x,v001.y,v001.z);
  // endShape(CLOSE);
  
  // beginShape();
  // vertex(v000.x,v000.y,v000.z);
  // vertex(v010.x,v010.y,v010.z);
  // vertex(v011.x,v011.y,v011.z);
  // vertex(v001.x,v001.y,v001.z);
  // endShape(CLOSE);
  
  // beginShape();
  // vertex(v010.x,v010.y,v010.z);
  // vertex(v110.x,v110.y,v110.z);
  // vertex(v111.x,v111.y,v111.z);
  // vertex(v011.x,v011.y,v011.z);
  // endShape(CLOSE);
  
  beginShape();
  vertex(v100.x,v100.y,v100.z);
  vertex(v110.x,v110.y,v110.z);
  vertex(v111.x,v111.y,v111.z);
  vertex(v101.x,v101.y,v101.z);
  endShape(CLOSE);
  
  pop();
}

function drawBlock2(p, pos, v1, v2,angle,swf, L)
{
  push();
  translate(pos.x,pos.y,pos.z);
  // rotate(angle)
  stroke(darkColor)
  // fill(lightColor)
  box(10,10,10)
  pop()
  return;
  
  let l1 = L;
  let l2 = 10.0*pow(p,3.2);
  let h = 4.0;
  
  let v3 = v1.copy().cross(v2);
  
  let V1 = v1.copy().mult(cos(angle)).add(v3.copy().mult(sin(angle)));
  let V2 = v2;
  let V3 = v1.copy().mult(-sin(angle)).add(v3.copy().mult(cos(angle)));
  
  let v000 = V1.copy().mult(l1/2).add(V2.copy().mult(l2/2)).add(V3.copy().mult(h/2));
  let v100 = V1.copy().mult(-l1/2).add(V2.copy().mult(l2/2)).add(V3.copy().mult(h/2));
  let v010 = V1.copy().mult(l1/2).add(V2.copy().mult(-l2/2)).add(V3.copy().mult(h/2));
  let v110 = V1.copy().mult(-l1/2).add(V2.copy().mult(-l2/2)).add(V3.copy().mult(h/2));
  let v001 = V1.copy().mult(l1/2).add(V2.copy().mult(l2/2)).add(V3.copy().mult(-h/2));
  let v101 = V1.copy().mult(-l1/2).add(V2.copy().mult(l2/2)).add(V3.copy().mult(-h/2));
  let v011 = V1.copy().mult(l1/2).add(V2.copy().mult(-l2/2)).add(V3.copy().mult(-h/2));
  let v111 = V1.copy().mult(-l1/2).add(V2.copy().mult(-l2/2)).add(V3.copy().mult(-h/2));
  
  stroke(255);
  strokeWeight(1.7*swf);
  fill(0);
  
  beginShape();
  vertex(v000.x,v000.y,v000.z);
  vertex(v100.x,v100.y,v100.z);
  vertex(v110.x,v110.y,v110.z);
  vertex(v010.x,v010.y,v010.z);
  endShape(CLOSE);
  
  // beginShape();
  // vertex(v001.x,v001.y,v001.z);
  // vertex(v101.x,v101.y,v101.z);
  // vertex(v111.x,v111.y,v111.z);
  // vertex(v011.x,v011.y,v011.z);
  // endShape(CLOSE);
  
  // beginShape();
  // vertex(v000.x,v000.y,v000.z);
  // vertex(v100.x,v100.y,v100.z);
  // vertex(v101.x,v101.y,v101.z);
  // vertex(v001.x,v001.y,v001.z);
  // endShape(CLOSE);
  
  // beginShape();
  // vertex(v000.x,v000.y,v000.z);
  // vertex(v010.x,v010.y,v010.z);
  // vertex(v011.x,v011.y,v011.z);
  // vertex(v001.x,v001.y,v001.z);
  // endShape(CLOSE);
  
  // beginShape();
  // vertex(v010.x,v010.y,v010.z);
  // vertex(v110.x,v110.y,v110.z);
  // vertex(v111.x,v111.y,v111.z);
  // vertex(v011.x,v011.y,v011.z);
  // endShape(CLOSE);
  
  // beginShape();
  // vertex(v100.x,v100.y,v100.z);
  // vertex(v110.x,v110.y,v110.z);
  // vertex(v111.x,v111.y,v111.z);
  // vertex(v101.x,v101.y,v101.z);
  // endShape(CLOSE);
  
  pop();
}

const N1 = 50;
const N2 = 1;

class Thing
{
  K = 50;
  m = 30;
  
  sz = random(2,10);
  
  offset = random(1);
  
  theta0 = random(TWO_PI);
  
  col = random(170,300);
  
  swf = pow(random(1),2.0)+0.5; // unused
  
  constructor(ind1,ind2)
  {
    this.theta0 = map(ind1,0,N1,0,TWO_PI);
    this.offset = 1.0*ind2/this.K/N2;
  }

  showP(p)
  {
    let offset2 = -t + 100 + 11.5*p - 0*4*this.theta0/TWO_PI + 1.3*sin(3*this.theta0+0.75);
    let aux = offset2 - floor(offset2);
    offset2 = floor(offset2) + ease(aux,3.0);
    
    let theta = this.theta0 + 0.8*p;
    
    let pos = surface(p,theta,0);
    let v1 = (surface(p+0.001,theta,0).sub(pos)).normalize();
    let v2 = (surface(p,theta+0.01,0).sub(pos)).normalize();
    
    let u = surface(p+0.98/N2/this.K,theta,0).sub(pos);
    let L = u.mag();
    // let L = 30
    
    drawBlock(p,pos,v1,v2,PI*offset2,1.0,L);
    // push()
    // translate(pos.x, pos.y, pos.z)
    // box(10,20,5);
    // pop()
  }
  
  show()
  {
    for(let i=0;i<this.K;i++)
    {
      let p = map(i+1-t,0,this.K,0,1);
      p = (p+this.offset)%1;
      this.showP(p);
    }
  }
}

const array = [];

function setup() {
  canvas = createCanvas(...SETTINGS.DIM, WEBGL);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  space = SETTINGS.border;
  colorMode(HSB)
  // ortho(-width/4, width/4, -height/4, height/4, -8000, 10000);
  perspective(PI/7, 1, .5)
  
  let k = 0;
  for(let i=0;i<N1;i++){
    for(let j=0;j<N2;j++){
      array[k] = new Thing(i,j);
      k++;
    }
  }
}

function draw() {
  randomSeed(seed)
  t+=.05
  t = t % 1
  orbitControl()
  // noLoop()
  background(darkColor);

  // noFill()
  // stroke(darkColor)

  rotateY(-PI/1.2)
  rotateX(-PI/20)
  // translate(700, 0, -10)
  // rotateY(-.2)
  // rotateX(0.5)
  // rotateX(0.1)
  // translate(300,0,-400)

  // beginShape(TRIANGLE_STRIP);
  // for (let x = 0; x < 10; x++) {
  //   for (let y = 0; y < 2; y++) {
  //     vertex(x * 100, 0, y * 100)
  //   }
  // }
  // endShape();

  draw_surface(-10)

  for(let i=0;i<N1*N2;i++)
  {
    array[i].show();
  }

  if (SETTINGS.drawBorder) {
    translate(-width/2, -height/2, 8000)
    fill(lightColor);
    noStroke();
    strokeWeight(4)
    rect(0, 0, space, height);
    rect(0, 0, width, space);
    rect(0, height - space, width, space);
    rect(width - space, 0, space, height);
    noFill()
    stroke(lightColor)
    line(space, space, space, height - space + 2)
    line(space - 2, space, width - space + 2, space)
    line(width - space + 2, height - space, space, height - space)
    line(width - space, height - space, width - space, space)
  }
}

function softplus(q,p){
  const qq = q+p;
  if(qq<=0){
    return 0;
  }
  if(qq>=2*p){
    return qq-p;
  }
  return 1/(4*p)*qq*qq;
}

function surface(p, theta,off)
{
  // const p2 = map(p,0,1,1350, -750);
  // const p2 = map(p,0,1,-750,1350);
  const p2 = map(p,0,1,-1300,700);
  const z = softplus(-p2,500)-off;
  let r = 100 + softplus(p2,500) - off;
  // r *= pow(p,2);

  const x = r*cos(theta);
  const y = r*sin(theta);

  return createVector(x,y,z);
}

function 
draw_surface(off = 0)
{
  push();
  
  let m1 = 60;
  let m2 = 60;
  
  stroke(lightColor);
  fill(lightColor);
  strokeWeight(2)
  // fill(darkColor);
  // noFill()
  // strokeWeight(2.5);
  noStroke();
  
  for(let i=0;i<m1;i++){
    // beginShape();
    beginShape(TRIANGLE_STRIP);
    for(let j=0;j<=m2;j++){
      const theta1 = map(i+t,0,m1,0,TWO_PI);
      const theta2 = map(i+1+t,0,m1,0,TWO_PI);
      
      // const p = map(j+3*t+0.05*random(100),0,m2,0,1);
      // const p = map(j,0,m2,0,1);
      const rand = random(0)
      const p1 = map(j+t,0,m2,0,1);
      const p2 = map(j+1+t,0,m2,0,1);
      
      // const v1 = surface(p,theta1,off);
      // const v2 = surface(p,theta2,off);
      const v1 = surface(p1,theta1,off);
      const v2 = surface(p1,theta2,off);
      const v3 = surface(p2,theta1,off);
      const v4 = surface(p2,theta2,off);
      fill(random(random(SETTINGS.palettes)))
      
      vertex(v1.x,v1.y,v1.z);
      vertex(v2.x,v2.y,v2.z);
      vertex(v3.x,v3.y,v3.z);
      vertex(v4.x,v4.y,v4.z);
    }
    endShape();
  }
  pop();
}

/**
 * { value: '', weight: 4 }
 */
const myWeightedRandom = (weightedRandoms) => random(weightedRandoms.reduce((acc, cur) => [...acc, ...new Array(cur.weight).fill(cur.value)],[]))

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `plane_1_${seed}`, "png");
}
