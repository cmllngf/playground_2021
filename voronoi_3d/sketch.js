let canvas;
let seed;
let simplexNoise;
let space;

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";
let t = 0, numFrame = 240
const recording = true;

const W = 1080
const H = 1080

let voronoi, polygons = [], centers = [], tCenters = []

const K = 4
const L = 100

class Thing {
  constructor(i, j, options = { x: 0, y: 0, r: 0, phi: 0}) {
    this.xIndex = i;
    this.yIndex = j;
    this.options = options;
    this.col = random(['green', 'red', 'blue', 'yellow', 'brown'])
    this.h = random(-400, -100)
    // this.h = -400
  }

  show() {
    for (let p = -K; p < K; p++) {
      push()
      fill(this.col)
      const [ x, y ] = this.getCoordP(p + t)
      // translate(x, y)
      // box(10)
      tCenters.push({h: this.h, c: this.col, pos: [x,y]})
      pop()
    }
  }

  // get coord() {
  //   for (let p = 0; p < K; p++) {
  //     return this.getCoordP(p)
  //   // const x = map(this.xIndex, 0, this.options.N, this.options.x, this.options.x + (this.options.xdir) * (W/5))
  //   // const y = map(this.xIndex, 0, this.options.N, this.options.y, this.options.y + (this.options.ydir) * (H/5))
  //   // return [x, y]
  // }

  getCoordP(p) {
    const r = map(this.yIndex, 0, M, p * this.options.r, this.options.r + p * this.options.r)
    const x = cos(this.options.phi) * r + this.options.x
    const y = sin(this.options.phi) * r + this.options.y
    // const x = map(this.yIndex, 0, M, this.options.x, this.options.x + (this.options.xdir *p) * L)
    // const y = map(this.yIndex, 0, M, this.options.y, this.options.y + (this.options.ydir *p) * L)
    return [x, y]
  }
}

const things = []
const N = 25
const M = 6

function setup() {
  canvas = createCanvas(...SETTINGS.DIM, WEBGL);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  space = SETTINGS.border;
  colorMode(HSB)
  // lights()
  // ortho()
  // ortho(-width/2, width/2, -height/2, height/2, -8000, 10000);
  // perspective(PI/5, 1, .5)
  perspective(PI/5)
  voronoi = d3.voronoi().extent([
    [0, 0],
    [W, H],
  ]);

  centers = new Array(320).fill(null).map(() => ( {h: random(-100, 100), pos:[int(random(W)), int(random(H))], sat: random(100, 100)}))
  polygons = voronoi(centers.map(c => c.pos)).polygons();

  for (let i = 0; i < N; i++) {
    // const x = W/2
    // const y = H/2
    const x = random(W)
    const y = random(H)
    // const x = random([random(W, W + 10), random(0, 10)])
    // const y = random([random(H, H + 10), random(0, 10)])
    const r = 300
    const phi = random(TWO_PI)
    // const xdir = random([-1, 0, 1])
    // const ydir = random([-1, 1])
    for (let j = 0; j < M; j++) {
      things.push(new Thing(i, j, { x, y, r, phi }));
    }
  }

	requestAnimationFrame(draw);
  if (recording) capturer.start();
}

const drawPolygon = (points = [[]], center = {h: 0, pos: [], sat:0, c: 'white' }) => {
  stroke(darkColor)
  strokeWeight(3)
  fill(center.c)
  fill(darkColor)
  fill(lightColor)
  // fill(center.sat)
  noStroke()

  const margin = 8
  const depth = 5500
  // const rheight = random(-300,300)
  const rheight = center.h
  
  points.forEach((p, i) => {
    const nextPoint = points[(i + 1) % points.length]
    const ratioP = margin / dist(p[0], p[1], center.pos[0], center.pos[1])
    const ratioP2 = margin / dist(nextPoint[0], nextPoint[1], center.pos[0], center.pos[1])
    beginShape()
    const newP = p5.Vector.lerp(createVector(p[0], p[1]),createVector(center.pos[0], center.pos[1]), ratioP)
    const nextP = p5.Vector.lerp(createVector(nextPoint[0], nextPoint[1]),createVector(center.pos[0], center.pos[1]), ratioP2)
    vertex(newP.x, -rheight, newP.y)
    vertex(nextP.x, -rheight, nextP.y)
    vertex(nextP.x, depth, nextP.y)
    vertex(newP.x, depth, newP.y)
    endShape()
  })

  beginShape()
  points.forEach((p, i) => {
    const ratioP = margin / dist(p[0], p[1], center.pos[0], center.pos[1])
    const newP = p5.Vector.lerp(createVector(p[0], p[1]),createVector(center.pos[0], center.pos[1]), ratioP)
    vertex(newP.x, -rheight, newP.y)
    // push()
    // translate(center.pos[0], 0, center.pos[1])
    // box(10)
    // pop()
  })
  endShape(CLOSE)

  beginShape()
  points.forEach((p, i) => {
    const ratioP = margin / dist(p[0], p[1], center.pos[0], center.pos[1])
    const newP = p5.Vector.lerp(createVector(p[0], p[1]),createVector(center.pos[0], center.pos[1]), ratioP)
    vertex(newP.x, depth, newP.y)
    // push()
    // translate(center.pos[0], 0, center.pos[1])
    // box(10)
    // pop()
  })
  endShape(CLOSE)
}

function draw() {
  t = frameCount / numFrame;
  t = t % 1
  // noLoop()
  randomSeed(seed)
  background(lightColor);
  orbitControl()
  push();
  // debugMode()
  // normalMaterial();

  //move your mouse to change light direction
  // let dirX = (mouseX / width) * 2;
  // let dirY = (mouseY / height) * 2;
  // directionalLight(250, 0, 100,-dirX, -dirY, 10);
  // translate(-W/2, -H/2, -500)
  translate(-W/2, -H/2, 250)
  rotateX(-PI/2)

  tCenters = []
  for (let i = 0; i < things.length; i++) {
    const thing = things[i];
    thing.show()
  }
  // pop()

  // return;
  pointLight(250, 10, 250, 0, height, 500);
  spotLight(0, 30, 100, 0, height, 800, 0, -1, -1, PI*1.9, 130);
  // spotLight(0, 30, 100, 0, -800, height, 0, -1, -1, PI*1.9, 130);

  let locX = mouseX;
  let locY = mouseY;
  // pointLight(250, 0, 250, locX, locY, -100);
  // spotLight(250, 0, 100, 0, 0, 200, 0, -1, -1, PI, 2);
  // spotLight(20, 20, 100, locX, locY, 230, 0, -1, -1, PI*1.9, 130);
  // spotLight(0, 30, 100, 0, height, 500, 0, -1, -1, PI*1.9, 130);
  ambientLight(0,100,0)
  // push()
  // translate(locX, locY, 0)
  // box(20)
  // pop()
  centers = centers.map((c) => ({...c, pos: [(c.pos[0] + random(-5, 5)) % W, (c.pos[1] + random(-5, 5)) % H]}))
  polygons = voronoi(tCenters.map(c => c.pos)).polygons();
  // polygons = voronoi(tCenters).polygons();
  
  polygons.forEach((polygon, i) => {
    drawPolygon(polygon, tCenters[i])
  })

  // centers = centers.map((c) => ({...c, pos: [(c.pos[0] + random(-5, 5)) % W, (c.pos[1] + random(-5, 5)) % H]}))

  pop();
  if (SETTINGS.drawBorder) {
    const heightShift = 0
    const widthtShift = 0
    // translate(-width/2, -height/2, 0)
    // translate(-width/2, -height/2, -600)
    translate(-width/2 - widthtShift/2, -height/2 - heightShift/2, 10)
    fill(darkColor);
    noStroke();
    rect(0, 0, space, height + heightShift);
    rect(0, 0, width + widthtShift, space);
    rect(0, height - space+ heightShift, width + widthtShift, space);
    rect(width - space + widthtShift, 0, space, height+ heightShift);
    noFill()
    strokeWeight(4)
    // stroke(lightColor)
    // line(space, space, space, height - space + 2)
    // line(space - 2, space, width - space + 2, space)
    // line(width - space + 2, height - space, space, height - space)
    // line(width - space, height - space, width - space, space)
  }
  if (recording) {
    capturer.capture(canvas.canvas);
    if (frameCount === numFrame * 3) {
      capturer.stop();
      capturer.save();
      noLoop();
    }
  }
}

/**
 * { value: '', weight: 4 }
 */
const myWeightedRandom = (weightedRandoms) => random(weightedRandoms.reduce((acc, cur) => [...acc, ...new Array(cur.weight).fill(cur.value)],[]))

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `voronoi_3D_${seed}`, "png");
}
