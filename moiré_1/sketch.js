let canvas;
let seed;
let simplexNoise;
let t = 0;
const numFrame = 120;

const recording = false;

const noiseScale = 0.01;
const radiusNoise = 1;

let img;

function preload() {
  img = loadImage("../assets/adam_hand.png");
}

function setup() {
  canvas = createCanvas(img.width, img.height);
  img.loadPixels();
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  if (recording) capturer.start();
}

function draw() {
  background(10);
  // image(img, 0, 0, width, height);
  randomSeed(seed);
  t = frameCount / numFrame;

  noLoop();

  // for (let x = 0; x < width; x += 10) {
  //   for (let y = 0; y < height; y += 10) {
  //     const id = get1DIndexFromImg(x, y);
  //     if (img.pixels[id] > 0) console.log(x, y);

  //     stroke(255);
  //     noFill();
  //     circle(x, y, 10);
  //   }
  // }

  // return;
  // console.log(img.pixels);

  noFill();
  stroke(230);
  strokeWeight(1);

  translate(width / 2, height / 2);
  const n = 70;
  for (let j = 1; j < 2; j++) {
    for (let i = 1; i <= n; i++) {
      const side = (width / n) * i;
      const xy = -side / 2;
      // drawSquareNoise(xy, xy, side, j * 1000);
      drawSquareShader(xy, xy, side, j * 10);
    }
  }

  if (recording) {
    capturer.capture(canvas.canvas);
    if (t === 3) {
      capturer.stop();
      capturer.save();
      noLoop();
    }
  }
}

const drawSquareShader = (x, y, side, d = 0) => {
  // rect(x, y, side, side);
  const divider = 10;
  beginShape();
  for (let i = 0; i < side; i += divider) {
    const index = get1DIndexFromImg(x + i, y);
    // console.log(img.pixels[index]);
    const displacement =
      img.pixels[index + 3] > 0 ? getBrightness(x + i, y) * d : 0;
    // console.log(displacement);
    curveVertex(x + i, y - displacement);
  }
  for (let i = 0; i < side; i += divider) {
    const index = get1DIndexFromImg(x + side, y + i);
    // console.log(img.pixels[index]);
    const displacement =
      img.pixels[index + 3] > 0 ? getBrightness(x + side, y + i) * d : 0;
    // console.log(displacement);
    curveVertex(x + side + displacement, y + i);
  }
  for (let i = side; i > 0; i -= divider) {
    const index = get1DIndexFromImg(x + i, y + side);
    // console.log(img.pixels[index]);
    const displacement =
      img.pixels[index + 3] > 0 ? getBrightness(x + i, y + side) * d : 0;
    // console.log(displacement);
    curveVertex(x + i, y + side + displacement);
  }
  for (let i = side; i > 0; i -= divider) {
    const index = get1DIndexFromImg(x, y + i);
    // console.log(img.pixels[index]);
    const displacement =
      img.pixels[index + 3] > 0 ? getBrightness(x, y + i) * d : 0;
    // console.log(displacement);
    curveVertex(x - displacement, y + i);
    // circle(x + displacement, y + i, divider);
  }
  endShape(CLOSE);
};

const get1DIndexFromImg = (x, y) => {
  // const xx = int(map(x, 0, width, 0, img.width));
  // const yy = int(map(y, 0, height, 0, img.height));
  const xx = int(map(x, -width / 2, width / 2, 0, img.width));
  const yy = int(map(y, -height / 2, height / 2, 0, img.height));
  return (yy * img.width + xx) * 4;
};

const getBrightness = (x, y) => {
  const id = get1DIndexFromImg(x, y);
  const R = img.pixels[id];
  const G = img.pixels[id + 1];
  const B = img.pixels[id + 2];
  return map(0.2126 * R + 0.7152 * G + 0.0722 * B, 0, 255, 0, 1);
  // return pp[int(constrain(map(ll, 0, 255, 0, pp.length), 0, pp.length - 1))];
};

const drawSquareNoise = (x, y, side, seed = 0) => {
  // rect(x, y, side, side);
  const divider = 10;
  beginShape();
  for (let i = 0; i <= side; i += divider) {
    const noiseValue =
      simplexNoise.noise4D(
        (x + i) * noiseScale + seed,
        y * noiseScale + seed,
        radiusNoise * cos(t * TWO_PI),
        radiusNoise * sin(t * TWO_PI)
      ) * 5;
    vertex(x + i + noiseValue, y + noiseValue);
  }
  endShape(CLOSE);
  beginShape();
  for (let i = 0; i <= side; i += divider) {
    const noiseValue =
      simplexNoise.noise4D(
        (x + i) * noiseScale + seed,
        (y + side) * noiseScale + seed,
        radiusNoise * cos(t * TWO_PI),
        radiusNoise * sin(t * TWO_PI)
      ) * 5;
    vertex(x + i + noiseValue, y + side + noiseValue);
  }
  endShape(CLOSE);
  beginShape();
  for (let i = 0; i <= side; i += divider) {
    const noiseValue =
      simplexNoise.noise4D(
        x * noiseScale + seed,
        (y + seed + i) * noiseScale,
        radiusNoise * cos(t * TWO_PI),
        radiusNoise * sin(t * TWO_PI)
      ) * 5;
    vertex(x + noiseValue, y + i + noiseValue);
  }
  endShape(CLOSE);
  beginShape();
  for (let i = 0; i <= side; i += divider) {
    const noiseValue =
      simplexNoise.noise4D(
        (x + side) * noiseScale + seed,
        (y + i) * noiseScale + seed,
        radiusNoise * cos(t * TWO_PI),
        radiusNoise * sin(t * TWO_PI)
      ) * 5;
    vertex(x + noiseValue + side, y + i + noiseValue);
  }
  endShape(CLOSE);
};

function ease(p) {
  return 3 * p * p - 2 * p * p * p;
}

function ease2(p, g) {
  if (p < 0.5) return 0.5 * pow(2 * p, g);
  else return 1 - 0.5 * pow(2 * (1 - p), g);
}

function softplus(q, p) {
  const qq = q + p;
  if (qq <= 0) {
    return 0;
  }
  if (qq >= 2 * p) {
    return qq - p;
  }
  return (1 / (4 * p)) * qq * qq;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `moiré_1_${seed}`, "png");
}
