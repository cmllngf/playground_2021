let canvas;
let seed;
const numFrame = 20;
let t = 0;
let simplexNoise;
const border = 70;
let borders;

let particles = [];

function setup() {
  canvas = createCanvas(600, 600);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  // capturer.start();
  borders = [
    [border, border, border, height - border],
    [border, border, width - border, border],
    [width - border, height - border, border, height - border],
    [width - border, height - border, width - border, border],
  ];

  for (let i = 0; i < 400; i++) {
    const r = random();
    particles.push(new Particle(r > 0.5, r > 0.1));
  }
}

function draw() {
  randomSeed(seed);
  background(10, 80);

  t = (frameCount / numFrame) % 1;

  for (let i = 0; i < particles.length; i++) {
    const p = particles[i];
    p.display();
  }

  fill(255);
  text(frameRate(), 100, 100);
  // capturer.capture(canvas.canvas);
  // if (frameCount == numFrame * 2) {
  //   console.log(frameCount);
  //   capturer.stop();
  //   capturer.save();
  //   noLoop();
  // }
}

function ease(p) {
  return 3 * p * p - 2 * p * p * p;
}

function ease2(p, g) {
  if (p < 0.5) return 0.5 * pow(2 * p, g);
  else return 1 - 0.5 * pow(2 * (1 - p), g);
}

function softplus(q, p) {
  const qq = q + p;
  if (qq <= 0) {
    return 0;
  }
  if (qq >= 2 * p) {
    return qq - p;
  }
  return (1 / (4 * p)) * qq * qq;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `rain_1_${seed}`, "png");
}
