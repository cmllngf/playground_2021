let canvas;
let seed;
let t = 0;
let recording = false;
let network = [];
let simplexNoise;

const avoidBorder = 20;

const poisson_radius = 10;
const poisson_k = 2;

function setup() {
  createCanvas(700, 700);
  background(0);

  network = poissonDiskSampling(poisson_radius, poisson_k);
  simplexNoise = openSimplexNoise(seed);
}

function draw() {
  background(10);
  t = frameCount / 60;
  beginShape();
  const scale = 0.02;
  const radius = 1;
  for (let i = 0; i < network.length; i++) {
    const { x, y } = network[i];
    noStroke();
    fill(map(x + y, 0, 1600, 0, 255));

    const nColor = map(
      simplexNoise.noise4D(
        x * scale,
        y * scale,
        radius * cos(TWO_PI * t),
        radius * sin(TWO_PI * t)
      ),
      -1,
      1,
      0,
      255
    );

    fill(nColor);

    const displacement = map(
      dist(x, y, width / 2, height / 2),
      0,
      300,
      60,
      0,
      true
    );
    const nx = map(
      simplexNoise.noise4D(
        x * scale,
        y * scale,
        100 + radius * cos(TWO_PI * t),
        1200 + radius * sin(TWO_PI * t)
      ),
      -1,
      1,
      -displacement,
      displacement
    );
    const ny = map(
      simplexNoise.noise4D(
        x * scale + 2000,
        y * scale + 2000,
        100 + radius * cos(TWO_PI * t),
        1090 + radius * sin(TWO_PI * t)
      ),
      -1,
      1,
      -displacement,
      displacement
    );

    circle(x + nx, y + ny, 4);
  }
  endShape();
}

function neighbors(pos, distMax) {
  const n = [];
  for (let i = 0; i < network.length; i++) {
    if (network[i].x == pos.x && network[i].y == pos.y) continue;

    if (dist(network[i].x, network[i].y, pos.x, pos.y) <= distMax)
      n.push(network[i]);
  }
  return n;
}

function isValidPoint(grid, cellsize, gwidth, gheight, p, radius) {
  /* Make sure the point is on the screen */
  if (
    p.x < avoidBorder ||
    p.x >= width - avoidBorder ||
    p.y < avoidBorder ||
    p.y >= height - avoidBorder
  )
    return false;

  /* Check neighboring eight cells */
  xindex = floor(p.x / cellsize);
  yindex = floor(p.y / cellsize);
  i0 = max(xindex - 1, 0);
  i1 = min(xindex + 1, gwidth - 1);
  j0 = max(yindex - 1, 0);
  j1 = min(yindex + 1, gheight - 1);

  for (i = i0; i <= i1; i++)
    for (j = j0; j <= j1; j++)
      if (grid[i][j] != null)
        if (dist(grid[i][j].x, grid[i][j].y, p.x, p.y) < radius) return false;

  /* If we get here, return true */
  return true;
}

function insertPoint(grid, cellsize, point) {
  xindex = floor(point.x / cellsize);
  yindex = floor(point.y / cellsize);
  grid[xindex][yindex] = point;
}

function poissonDiskSampling(radius, k) {
  N = 2;
  /* The final set of points to return */
  points = [];
  /* The currently "active" set of points */
  active = [];
  /* Initial point p0 */
  p0 = createVector(
    int(random(avoidBorder, width - avoidBorder)),
    int(random(avoidBorder, height - avoidBorder))
  );
  grid = [];
  cellsize = floor(radius / sqrt(N));

  /* Figure out no. of cells in the grid for our canvas */
  ncells_width = ceil(width - (avoidBorder * 2) / cellsize) + 1;
  ncells_height = ceil(height - (avoidBorder * 2) / cellsize) + 1;

  /* Allocate the grid an initialize all elements to null */
  for (i = 0; i < ncells_width; i++) {
    grid[i] = [];
    for (j = 0; j < ncells_height; j++) grid[i][j] = null;
  }

  insertPoint(grid, cellsize, p0);
  points.push(p0);
  active.push(p0);

  while (active.length > 0) {
    random_index = int(random(active.length));
    p = active[random_index];

    found = false;
    for (tries = 0; tries < k; tries++) {
      theta = int(random(360));
      new_radius = int(random(radius, 2 * radius));
      pnewx = int(p.x + new_radius * cos(radians(theta)));
      pnewy = int(p.y + new_radius * sin(radians(theta)));
      pnew = createVector(pnewx, pnewy);

      if (
        !isValidPoint(grid, cellsize, ncells_width, ncells_height, pnew, radius)
      )
        continue;

      points.push(pnew);
      insertPoint(grid, cellsize, pnew);
      active.push(pnew);
      found = true;
      break;
    }

    /* If no point was found after k tries, remove p */
    if (!found) active.splice(random_index, 1);
  }

  return points;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `loop_cube_${seed}`, "png");
}
