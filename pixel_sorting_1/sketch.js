let canvas;
let seed;
let img;

let squares = [];
let squaresShader = [];
let bgPixels = [];

const size = 2;

function preload() {
  img = loadImage("../assets/me.jpg");
  imgShader = loadImage("../assets/face.png");
}

function setup() {
  canvas = createCanvas(800, 800);
  seed = random(99999);
  img.loadPixels();
  imgShader.loadPixels();
}

function draw() {
  randomSeed(seed);
  background(10);
  // drawBg();
  image(img, 0, 0, 800, 800);
  drawBg2();
  // drawBg3();
  loadPixels();
  console.log(pixels);
  for (let x = 0; x < 800 / size; x++) {
    squares.push([]);
    squaresShader.push([]);
    for (let y = 0; y < 800 / size; y++) {
      let d = pixelDensity();
      const oneD = (y * (width * d) + x) * 4 * d;
      const oneDShader = (y * imgShader.width + x) * 4;
      // const n = noise(x * 0.1, y * 0.1);
      const n = map(noise(x * 0.1, y * 0.1), 0, 1, -5, 5);
      // const n = 0;
      let r = pixels[oneD * size] + n;
      let g = pixels[oneD * size + 1] + n;
      let b = pixels[oneD * size + 2] + n;
      let rs = imgShader.pixels[oneDShader * size] + n;
      let gs = imgShader.pixels[oneDShader * size + 1] + n;
      let bs = imgShader.pixels[oneDShader * size + 2] + n;
      // let c = get(x * size + size / 2, y * size + size / 2);
      squares[x].push([r, g, b]);
      squaresShader[x].push([rs, gs, bs]);
      fill(r, g, b);
      // stroke(0);
      noStroke();
      rect(x * size, y * size, size, size);
    }
  }
  const xs = [10, 11, 12, 20];
  // for (let k = 0; k < xs.length; k++) {
  //   const x = xs[k];
  for (let x = 0; x < squares.length; x++) {
    for (let j = 0; j < squares[x].length / 1; j++) {
      for (let i = 0; i < squares[x].length - 1; i++) {
        const s1 = squares[x][i];
        const s2 = squares[x][i + 1];
        const ss1 = squaresShader[x][i];
        const ss2 = squaresShader[x][i + 1];
        if (isComparable(ss1, ss2)) {
          if (compare2(s1, s2)) {
            squares[x][i] = s2;
            squares[x][i + 1] = s1;
          }
        }
        // if (compare3(s1, s2)) {
        //   squares[x][i] = s2;
        //   squares[x][i + 1] = s1;
        // }
      }
    }
  }

  for (let x = 0; x < squares.length; x++) {
    const element = squares[x];
    for (let y = 0; y < element.length; y++) {
      const s = element[y];
      fill(s[0], s[1], s[2]);
      // stroke(0);
      noStroke();
      rect(x * size, y * size, size, size);
    }
  }
  // updatePixels();
  noLoop();
}

const drawBg = () => {
  const c1 = color("#E6D29F");
  const c2 = color("#004C59");
  const n = 50;
  for (let x = 0; x < n; x++) {
    const col = lerpColor(c1, c2, x / n);
    stroke(col);
    fill(col);
    // rect(x * (width / n), 0, width / n, height);
    rect(0, x * (height / n), width, height / n);
  }
};

const drawBg2 = () => {
  const c2 = color(0);
  const c1 = color(255);
  // const c2 = color("#74004D");
  // const c1 = color("#C68D0C");
  // const c2 = color("#E6D29F");
  // const c1 = color("#004C59");
  const n = 100;
  for (let x = 0; x < n; x++) {
    const col = lerpColor(c1, c2, x / n);
    stroke(col);
    fill(col);
    // rect(x * (width / n), 0, width / n, height);
    // rect(0, x * (height / n), width, height / n);
    circle(width / 2, height / 2, map(x, 0, n, 1200, 50));
  }
};

const drawBg3 = () => {
  const c1 = color("#E6D29F");
  const c2 = color("#004C59");
  const nx = 10;
  const ny = 10;
  for (let x = 0; x < nx; x++) {
    for (let y = 0; y < ny; y++) {
      const col = lerpColor(c1, c2, (x + y) / (nx + ny));
      stroke(col);
      fill(col);
      rect(x * (width / nx), y * (height / ny), width / nx, height / ny);
      // rect(0, x * (height / n), width, height / n);
      // circle(width / 2, height / 2, map(x, 0, n, 1500, 50));
    }
  }
};

const isComparable = (s1, s2) => s1[0] > 0 || s2[0] > 0;

const compare1 = (s1, s2) => s1[3] > s2[3];
const compare2 = (s1, s2) => s1[2] - s2[2] < 20; // 10 pour le sable
const compare3 = (s1, s2) => s1[2] - s2[0] < -10;

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `pixel_sorting_1_${seed}`, "png");
}
