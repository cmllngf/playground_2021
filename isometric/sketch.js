let canvas;
let simplexNoise;
const space = 160;


let lightColor = "#f5f8f8";
let darkColor = "#080F0F";


function setup() {
  seed = random(9999);
  randomSeed(seed);
  simplexNoise = openSimplexNoise(seed);
  smooth()
  canvas = createCanvas(1080, 1080, WEBGL);
  pixelDensity(1)
  ortho(-width / 2, width / 2, height / 2, -height / 2, -1000, 5000);
  background(darkColor);
}

function draw() {
  background(lightColor)
  noLoop()
  
  // noFill()
  // strokeWeight(4)
  // stroke(darkColor)
  // fill(lightColor)

  const noiseScaleX = 0.05
  const noiseScaleZ = 0.05
  const boxSize = 20

  push()
  rotateX(PI/4)
  rotateY(-PI/4)
  translate(-950, 0, -950)
  for (let x = 0; x < 80; x++) {
    push()
    translate(x * boxSize,0,0)
    for (let z = 0; z < 80; z++) {
      push()
      translate(0, 0, z * boxSize)
      const noiseValue = simplexNoise.noise2D(x * noiseScaleX,z * noiseScaleZ)
      for (let y = 0; y < 30; y++) {
        push()
        translate(0, - y * boxSize, 0)
        if(noiseValue > .5 || y > map(noiseValue, .5, -1, 0, 30, true)) {
          // drawFaceBox(boxSize, boxSize, boxSize, color('red'), color('blue'), color('green'), lightColor, color('orange'), color('pink'))
          // drawFaceBox(boxSize, boxSize, boxSize, lightColor, color('blue'), lightColor, lightColor, color('orange'), color('pink'))
          // drawFaceBox(boxSize, boxSize, boxSize, '#363636', color('blue'), '#282828', '#fbfbfb', color('orange'), color('pink'))
          if (x > 53 || x < 43 || z < 43 || z > 53)
            drawFaceBox(boxSize, boxSize, boxSize, '#52796f', color('blue'), '#201a1b', '#fefcf7', color('orange'), color('pink'))
        }
        pop()
      }
      pop()
    }
    pop()
  }
  pop()

  translate(-width/2, -height/2, 1000)
  fill(darkColor);
  noStroke();
  strokeWeight(4)
  rect(0, 0, space, height);
  rect(0, 0, width, space);
  rect(0, height - space, width, space);
  rect(width - space, 0, space, height);
  noFill()
  stroke(lightColor)
  line(space, space, space, height - space + 2)
  line(space - 2, space, width - space + 2, space)
  line(width - space + 2, height - space, space, height - space)
  line(width - space, height - space, width - space, space)
}

function drawFaceBox(boxWidth, boxHeight, boxDepth,
  front, bottom, right, top, left, back) {
  angleMode(DEGREES); 
  let w = boxWidth;
  let h = boxHeight;
  let d = boxDepth;
  stroke(darkColor)
  strokeWeight(5)
  // noStroke()

  // Center the box.
  translate(-w / 2, -h / 2);

  fill(front);
  quad(0, 0, w, 0, w, h, 0, h, 1000, 1000);

  push();
  fill(left);
  translate(0, 0, -d);
  rotateY(-90);
  quad(0, 0, d, 0, d, h, 0, h, 1000, 1000);

  pop();
  push();
  fill(bottom);
  translate(0, 0, -d);
  rotateX(90);
  quad(0, 0, w, 0, w, d, 0, d, 1000, 1000);

  pop();
  push();
  fill(right);
  translate(w, 0, 0);
  rotateY(90);
  quad(0, 0, d, 0, d, h, 0, h, 1000, 1000);

  pop();
  push();
  fill(top);
  translate(0, h, 0);
  rotateX(-90);
  quad(0, 0, w, 0, w, d, 0, d, 1000, 1000);

  pop();
  push();
  fill(back);
  rotateY(180);
  translate(-w, 0, d);
  quad(0, 0, w, 0, w, h, 0, h, 1000, 1000);
  pop()
  angleMode(RADIANS);
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `isometric_${seed}`, "png");
}
