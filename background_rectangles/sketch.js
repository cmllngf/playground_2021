let seedId;
let x = 0;
let y = 0;
let palette;
let img;
const border = 0
const avoidBorder = 0;

let simplexNoise;

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";

function preload() {
  img = loadImage("../assets/pompei.png");
}

function setup() {
  seedId = random(999999);
  img.loadPixels();
  randomSeed(seedId);
  simplexNoise = openSimplexNoise(seedId);
  // createCanvas(img.width * 2, img.height * 2);
  createCanvas(1080, 1350);
  colorMode(HSB);
  background("#f8f8ff");
  // blendMode(HARD_LIGHT)
  // blendMode(MULTIPLY)
}

function draw() {
  for (let index = 0; index < 3; index++) {
    const x = random(-300, width + 300)
    const y = random(-300, height + 300)
    // const w = randomGaussian(300, 20)
    // const h = randomGaussian(100, 50)
    const w = randomGaussian(300, 80)
    const h = randomGaussian(300, 100)
    noStroke()
    const col = color(getColorFromCoord(x, y))
    col.setAlpha(.015)
    fill(col)
    rectMode(RADIUS)
    // rect(x, y, w, h, 20)
    ellipse(x, y, w, h)
  }
}

function granulate(gA){
  loadPixels();
  let d = pixelDensity();
  let halfImage = 4 * (width * d) * (height * d);
  for (let ii = 0; ii < halfImage; ii += 4) {
    grainAmount = random(-gA,gA)
    pixels[ii] = pixels[ii]+grainAmount;
    pixels[ii + 1] = pixels[ii+1]+grainAmount;
    pixels[ii + 2] = pixels[ii+2]+grainAmount;
    pixels[ii + 3] = pixels[ii+3]+grainAmount;
  }
  updatePixels();
}

const get1DIndex = (x, y, minX = 0, maxX = width, minY = 0, maxY = height) => {
  const xx = int(map(x, minX, maxX, 0, img.width));
  const yy = int(map(y, minY, maxY, 0, img.height));
  return (yy * img.width + xx) * 4;
};

const getColorPaletteFromImage = (x, y) => {
  const index = get1DIndex(x, y);
  const R = img.pixels[index];
  const G = img.pixels[index + 1];
  const B = img.pixels[index + 2];
  const ll = 0.2126 * R + 0.7152 * G + 0.0722 * B;
  return palette[int(map(ll, 0, 255, 0, palette.length, true))];
};

const getColorFromCoord = (x, y) => {
  // vary mean along x axes
  let mappedIndex;
  // if (dist(x,y,width/2,height/2) < 400) {
    mappedIndex = map(y, height, 0, 0, palette.length-1, true);
  // } else {
  //   mappedIndex = map(y, 0, height, 0, palette.length-1, true);
  // }
  // const mappedIndex = map(y, 0, height, 0, palette.length-1, true);
  const noiseMean = 1.8
  // const noiseMean = noise(x) * 1
  const gaussianIndex = randomGaussian(mappedIndex, noiseMean)
  const index = int(constrain(gaussianIndex, 0, palette.length - 1))
  return palette[index];
}

const getColorRandom = () => {
  return palette[int(random(palette.length))]
}

function keyPressed(key) {
  if (key.keyCode === 80)
    saveCanvas(canvas, `background_rectangle_${seedId}`, "png");
  if (key.keyCode === 83) {
    isLooping() ? noLoop() : loop()
  }
  if (key.keyCode === 71) {
    granulate(10)
  }
}

palette = ["#151412", "#272E1E", "#354A29", "#53734A", "#84B188", "#D5D7D4"];
// palette = ["#220507", "#450A04", "#6A2B02", "#8F5201", "#C98A43", "#E6D19F"];
// palette = ["#5C3C49", "#8D5964", "#AF747F", "#D1617E", "#E788A2", "#EEAEB8"];
// palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
// palette = ["#AF747F", "#D1617E", "#E788A2", "#EEAEB8", "#F9D0C3", "#F6EEE1"];
// palette = [0, 250, 100, 150, 200, 50];
// palette = [150, 250, 100, 50, 0, 200];
palette = ["#24130C", "#70381F", "#C04700", "#F07800", "#EFB178", "#CCECEB"];
// palette = ["#6F2B3A", "#DB6578", "#E1B971", "#FBEDC7", "#949971", "#5B5D45"];
// palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
// palette = ["#092A2F", "#652B05", "#B65302", "#E7A700", "#FAE647", "#00B5AE"];
// palette = ["#162E21", "#063F39", "#30685B", "#73967E", "#C68C20", "#D8D091"];
// palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
// palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
// palette = ["#021118", "#14383D", "#025F71", "#00ACB3", "#9CDACF", "#EBE3D8"];
// palette = ["#845ec2", "#2c73d2", "#0081cf", "#0089ba", "#008e9b", "#008f7a"];
// palette = ["#845ec2", "#d65db1", "#ff6f91", "#ff9671", "#ffc75f", "#f9f871"];
// palette = ["#5ec2b5", "#69d1ae", "#81dea2", "#a3e991", "#ccf27f", "#f9f871"];
// palette = ["#c25eb5", "#f6669e", "#ff7f82", "#ffa56a", "#ffcf5f", "#f9f871"];
// palette = ["#3352c6", "#bb4ab6", "#ff5692", "#ff856c", "#ffc058", "#f9f871"];
// palette = ["#7ab6ff","#7ab6ff","#7ab6ff","#7ab6ff","#ffce7a","#ff8c7a","#7ab6ff","#7ab6ff"]
// palette = [darkColor, lightColor]

// palette =  [
//   "#393232",
//   "#4D4545",
//   "#8D6262",
//   "#ED8D8D"
// ]