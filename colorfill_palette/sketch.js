const nodes = 5;
let network = [];
const avoidBorder = 0;
const poisson_radius = 3;
const poisson_radius2 = 100;
const poisson_k = 5;
let t = 0;

let canvas;
let img;

const border = 20;
let noiseScale = 0.1;
let seed;
const pointWeight = 1;
// let palette = ["#9C7B5A", "#CDAF7D", "#7CA98A", "#216C58", "#204534"];
// let palette = ["#5E101E", "#732427", "#8BD6C2", "#175450", "#082B2F"];
// let palette = ["#742228", "#721821", "#67181D", "#3B1816", "#0F0F0F"];
// let palette = ["#760000", "#86502A", "#B87B5E", "#4A6077", "#1F4B3E"];
// let palette = ["#00314F", "#6CA5AE", "#E5F1D9", "#B4A387", "#D38C86"];
// let palette = ["#D5B5F3", "#75D8EC", "#F3A9C5", "#A9F3D3", "#F3C4A9"];
// let palette = ["#082C59", "#0A72A1", "#4183B1", "#526C81", "#DDCEDA"];
// let palette = ["#012023", "#1B443C", "#93907D", "#855352", "#6C252D"];
// let palette = ["#840000", "#C58402", "#8B929C", "#18434C", "#1F2113"];
// let palette = [
//   "#151412",
//   "#272E1E",
//   "#354A29",
//   "#53734A",
//   "#84B188",
//   "#D5D7D4",
// ];
// let palette = [
//   "#220507",
//   "#450A04",
//   "#6A2B02",
//   "#8F5201",
//   "#C98A43",
//   "#E6D19F",
// ];
// // let palette = [
//   "#5C3C49",
//   "#8D5964",
//   "#AF747F",
//   "#D1617E",
//   "#E788A2",
//   "#EEAEB8",
// ];
// let palette = [
//   "#AA6A82",
//   "#B69792",
//   "#C7ACA1",
//   "#DDC1BE",
//   "#DBD1C8",
//   "#F6EEE1",
// ];
// let palette = [0, 250, 100, 150, 200, 50];
// let palette = [0, 50, 100, 150, 200, 250];
// let palette = [150, 250, 100, 50, 0, 200];
// let palette = [
//   "#24130C",
//   "#70381F",
//   "#C04700",
//   "#F07800",
//   "#EFB178",
//   "#CCECEB",
// ];
// let palette = [
//   "#5B1B2C",
//   "#7E5881",
//   "#8B7896",
//   "#9CA1B7",
//   "#99A6C6",
//   "#6A7799",
// ];
// let palette = [
//   "#6F2B3A",
//   "#DB6578",
//   "#E1B971",
//   "#FBEDC7",
//   "#949971",
//   "#5B5D45",
// ];
// let palette = [
//   "#341C1C",
//   "#5E101E",
//   "#732427",
//   "#8B473E",
//   "#8BD6C2",
//   "#388072",
// ];
// let palette = [
//   "#092A2F",
//   "#652B05",
//   "#B65302",
//   "#E7A700",
//   "#FAE647",
//   "#00B5AE",
// ];
// let palette = [
//   "#162E21",
//   "#063F39",
//   "#30685B",
//   "#73967E",
//   "#C68C20",
//   "#D8D091",
// ];
// let palette = [
//   "#0F0F0F",
//   "#221715",
//   "#3F1715",
//   "#54191B",
//   "#67181D",
//   "#68191E",
// ];
// let palette = [
//   "#02110C",
//   "#162E21",
//   "#073E38",
//   "#30685B",
//   "#73967E",
//   "#DACE94",
// ];
// let palette = [
//   "#04000E",
//   "#07144B",
//   "#0C1862",
//   "#14228D",
//   "#1A2FAF",
//   "#2846E2",
// ];
let palette = [
  "#020224",
  "#01104F",
  "#003C81",
  "#01669E",
  "#5075A2",
  "#6F91AD",
];

const visited = [];
const colors = [];
let unprocessedPixels = [];

function preload() {
  // img = loadImage("../assets/head1.png");
  img = loadImage("../assets/la_joconde.png");
}

function setup() {
  canvas = createCanvas(800, 800);
  seed = random(9999);
  img.loadPixels();
  randomSeed(seed);
  network = poissonDiskSampling(poisson_radius, poisson_k);
  background("#f5f5f5");
  strokeWeight(pointWeight);
  for (let i = 0; i < network.length; i++) {
    const { x, y } = network[i];
    seedPoint(x, y, getColorFromShader3(x, y));
  }
}

const getIndex = (x, y) => y * width + x;

function draw() {
  for (let iii = 0; iii < 1000; iii++) {
    if (unprocessedPixels.length === 0) {
      noLoop();
      noStroke();
      fill("#141414");
      fill("rgb(248,248,255)");
      rect(0, 0, width, border);
      rect(0, 0, border, height);
      rect(width - border, 0, border, height);
      rect(0, height - border, width, border);
      return;
    }
    const randomIndex = int(random(unprocessedPixels.length));
    const { x, y } = unprocessedPixels[randomIndex];
    unprocessedPixels.splice(randomIndex, 1);
    for (let xx = -1; xx <= 1; xx++) {
      for (let yy = -1; yy <= 1; yy++) {
        if (xx == 0 && yy == 0) continue;

        if (x + xx > width || x + xx < 0 || y + yy > height || y + yy < 0)
          continue;

        const i = getIndex(x + xx, y + yy);
        if (visited[i]) continue;

        colors[i] = colors[y * width + x];
        colors[i] = gaussianColorHSB2(colors[i], x, y);
        visited[i] = true;
        unprocessedPixels.push({ x: x + xx, y: y + yy });
        stroke(colors[i]);
        point(x + xx, y + yy);
      }
    }
  }
}

const gaussianColorHSB2 = (col, x, y) => {
  const shader = img.pixels[(y * width + x) * 4 + 4] > 0;

  if (shader) return gaussianColorHSB(col);

  let h, s, b;
  let colorToReturn;
  colorMode(HSB);
  // let colAim = color(240, 0, 100);
  let colAim = palette[0];
  // let colAim = "#4C0027";
  // let colAim = "#4D2750";
  // let colAim = "#AEB3B6";
  const step = 0.5;
  h = randomGaussian(hue(col), 1) + (hue(col) > hue(colAim) ? -step : step);
  s =
    randomGaussian(saturation(col), 1) +
    (saturation(col) > saturation(colAim) ? -step : step);
  b =
    randomGaussian(brightness(col), 1) +
    (brightness(col) > brightness(colAim) ? -step : step);
  colorToReturn = color(h, s, b);
  colorMode(RGB);
  return colorToReturn;
};

const gaussianColorHSB4 = (col, x, y) => {
  const shader = img.pixels[(y * width + x) * 4 + 4] > 0;

  if (shader) return gaussianColorHSB(col);

  let h, s, b;
  let colorToReturn;
  colorMode(HSB);
  let colAim = color(255);
  const step = 1;
  h = randomGaussian(hue(col), 0.5) + 1;
  s = randomGaussian(saturation(col), 0.5) + 0.25;
  b = randomGaussian(brightness(col), 0.5);
  colorToReturn = color(h, s, b);
  colorMode(RGB);
  return colorToReturn;
};

const gaussianColorHSB = (col) => {
  let h, s, b;
  let colorToReturn;
  colorMode(HSB);
  h = randomGaussian(hue(col), 0.5);
  s = randomGaussian(saturation(col), 0.5);
  b = randomGaussian(brightness(col), 0.5);
  colorToReturn = color(h, s, b);
  colorMode(RGB);
  return colorToReturn;
};

const seedPoint = (x, y, c) => {
  const i = getIndex(x, y);
  visited[i] = true;
  colors[i] = c;
  unprocessedPixels.push({ x, y });
  stroke(c);
  point(x, y);
};

const getColorFromShader3 = (x, y) => {
  // if (x > 300 && x < 350 && y > 500 && y < 550) {
  //   return color("red");
  // }
  const xx = int(map(x, 0, width, 0, width));
  const yy = int(map(y, 0, height, 0, height));
  const id = (yy * img.width + xx) * 4;
  const R = img.pixels[id];
  const G = img.pixels[id + 1];
  const B = img.pixels[id + 2];
  const ll = 0.2126 * R + 0.7152 * G + 0.0722 * B;
  const iii = map(ll, 0, 255, 0, palette.length);
  return palette[
    constrain(int(randomGaussian(iii, 0.1)), 0, palette.length - 1)
  ];
};

const getColorFromShader = (x, y) => {
  const id = getIndex(x, y);
  const R = img.pixels[id * 4];
  const G = img.pixels[id * 4 + 1];
  const B = img.pixels[id * 4 + 2];
  return color(R, G, B);
};

///////////////////////

const getColorFromPosLerp = (x, y) => {
  // const c1 = color(100, random(255), 150);
  // const c2 = color(random(255), 150, 100);
  // const c2 = color("#561705");
  // const c1 = color("#A9F3D3");
  const c2 = color("#204534");
  // const c1 = color("#5E280E");
  // const c2 = color("#FFDBAE");
  const c1 = color("#FF396A");
  // const c2 = color(0);
  // const p = map(x + y, avoidBorder * 2, width + height - avoidBorder * 2, 0, 1);
  const p2 = map(y, avoidBorder, height - avoidBorder, 0, palette.length - 1);
  // return lerpColor(c1, c2, p);
  // return random() > 0.05 ? c1 : c2;
  return palette[constrain(int(randomGaussian(p2, 1)), 0, palette.length - 1)];
  // return palette[int(random(palette.length))];
};

function neighbors(pos, distMax) {
  const n = [];
  // if (img.pixels[(pos.y * width + pos.x) * 4 + 4] === 0) return [];
  for (let i = 0; i < network.length; i++) {
    if (network[i].x == pos.x && network[i].y == pos.y) continue;

    if (dist(network[i].x, network[i].y, pos.x, pos.y) <= distMax)
      n.push(network[i]);
  }
  return n;
}

function isValidPoint(grid, cellsize, gwidth, gheight, p, radius) {
  if (img.pixels[(p.y * width + p.x) * 4 + 4] === 0) return false;
  // let r =
  //   img.pixels[(p.y * width + p.x) * 4 + 4] === 0
  //     ? poisson_radius2
  //     : poisson_radius;

  /* Make sure the point is on the screen */
  if (
    p.x < avoidBorder ||
    p.x >= width - avoidBorder ||
    p.y < avoidBorder ||
    p.y >= height - avoidBorder
  )
    return false;

  /* Check neighboring eight cells */
  xindex = floor(p.x / cellsize);
  yindex = floor(p.y / cellsize);
  i0 = max(xindex - 1, 0);
  i1 = min(xindex + 1, gwidth - 1);
  j0 = max(yindex - 1, 0);
  j1 = min(yindex + 1, gheight - 1);

  for (i = i0; i <= i1; i++)
    for (j = j0; j <= j1; j++)
      if (grid[i][j] != null)
        if (dist(grid[i][j].x, grid[i][j].y, p.x, p.y) < radius) return false;

  /* If we get here, return true */
  return true;
}

function insertPoint(grid, cellsize, point) {
  xindex = floor(point.x / cellsize);
  yindex = floor(point.y / cellsize);
  grid[xindex][yindex] = point;
}

function poissonDiskSampling(radius, k) {
  N = 2;
  /* The final set of points to return */
  points = [];
  /* The currently "active" set of points */
  active = [];
  /* Initial point p0 */
  // p0 = createVector(
  //   int(random(avoidBorder, width - avoidBorder)),
  //   int(random(avoidBorder, height - avoidBorder))
  // );
  p0 = createVector(width / 2, height / 2);
  // p1 = createVector(700, 400);
  grid = [];
  cellsize = floor(radius / sqrt(N));

  /* Figure out no. of cells in the grid for our canvas */
  ncells_width = ceil(width - (avoidBorder * 2) / cellsize) + 1;
  ncells_height = ceil(height - (avoidBorder * 2) / cellsize) + 1;

  /* Allocate the grid an initialize all elements to null */
  for (i = 0; i < ncells_width; i++) {
    grid[i] = [];
    for (j = 0; j < ncells_height; j++) grid[i][j] = null;
  }

  insertPoint(grid, cellsize, p0);
  points.push(p0);
  active.push(p0);
  // insertPoint(grid, cellsize, p1);
  // points.push(p1);
  // active.push(p1);

  while (active.length > 0) {
    random_index = int(random(active.length));
    p = active[random_index];
    // let r =
    //   img.pixels[(p.y * width + p.x) * 4 + 4] === 0
    //     ? poisson_radius2
    //     : poisson_radius;

    found = false;
    for (tries = 0; tries < k; tries++) {
      theta = int(random(360));
      new_radius = int(random(radius, 2 * radius));
      pnewx = int(p.x + new_radius * cos(radians(theta)));
      pnewy = int(p.y + new_radius * sin(radians(theta)));
      pnew = createVector(pnewx, pnewy);

      if (
        !isValidPoint(grid, cellsize, ncells_width, ncells_height, pnew, radius)
      )
        continue;

      points.push(pnew);
      insertPoint(grid, cellsize, pnew);
      active.push(pnew);
      found = true;
      break;
    }

    /* If no point was found after k tries, remove p */
    if (!found) active.splice(random_index, 1);
  }

  return points;
}

function keyPressed(key) {
  if (key.keyCode === 80)
    saveCanvas(canvas, `colorfill_palette_1_${seed}`, "png");
}
