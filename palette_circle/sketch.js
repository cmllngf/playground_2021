let canvas;
let seed;
let simplexNoise;
const border = 160;

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";
// const palette = ["#2E294E", "#541388", "#F1E9DA", "#FFD400", "#D90368"]
const palette = ["#541388","#D90368", "#FFD400", "#F1E9DA",  "#2E294E"]

function setup() {
  canvas = createCanvas(1080, 1080);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
}
const shuffleArray = (array) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    const temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
};

function draw() {
  // shuffleArray(palette)
  background(darkColor);
  noLoop();
  push()
  translate(width/2, height/2)

  noFill()
  stroke(lightColor)
  strokeWeight(3)
  strokeCap(PROJECT)
  const noiseScale = .01
  const spaceBetweenCircle = 10
  for (let i = 0; i <= 113; i++) {
    const steps = i * random(3, 2);
    const step = TWO_PI / steps;
    const theta0 = random(TWO_PI)
    rotate(theta0)
    for (let a = 0; a <= steps; a++) {
      const r = i * spaceBetweenCircle
      const theta1 = a * step
      const x = cos(theta1) * r
      const y = sin(theta1) * r
      // const noiseValue = simplexNoise.noise2D(x * noiseScale, y * noiseScale)
      // stroke(random(palette))
      // stroke(palette[int(map(noiseValue, -1, 1, 0, palette.length))])
      stroke(getColorFromCenter(x, y))
      // stroke(getColorFromY(y))
      arc(0, 0, r, r, theta1, step)
    }
  }
  pop()

  rectMode(CORNER);
  fill(lightColor);
  noStroke();
  rect(0, 0, width, border);
  rect(0, 0, border, height);
  rect(width - border, 0, border, height);
  rect(0, height - border, width, border);
}

const getColorFromCenter = (x, y) => {
  const i = map(dist(x,y,0,0), 0, width, 0, palette.length)
  const gaussianI = constrain(int(randomGaussian(i, .4)), 0, palette.length-1)
  return palette[gaussianI]
}

const getColorFromBottom = (x, y) => {
  const i = map(dist(x,y,x,-height/2), 0, width, 0, palette.length)
  const gaussianI = constrain(int(randomGaussian(i, .1)), 0, palette.length-1)
  return palette[gaussianI]
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `palette_${seed}`, "png");
}
