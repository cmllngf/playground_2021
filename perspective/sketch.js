let canvas;
let simplexNoise;

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";

const space = 160

function setup() {
  seed = random(9999);
  randomSeed(seed);
  simplexNoise = openSimplexNoise(seed);
  canvas = createCanvas(1080, 1080, WEBGL);
  colorMode(HSB);
  randomSeed(seed);
  perspective(undefined, undefined, undefined, 100000);
}

function draw() {
  orbitControl();
  background(lightColor)
  strokeWeight(6)
  stroke(lightColor)
  fill(darkColor)

  const bSize = 360
  const step = 600
  const zSteps = 30

  push()
  // rotateY(-PI/12)
  rotateZ(PI/5)
  translate(-8500, -7500, 100)
  for (let i = 0; i < zSteps; i++) {
    translate(0, 0, -step)
    const t = map(i, 0, zSteps, 0, 1)
    const p = easeInCirc(t) * 1000
    push()
    for (let j = 0; j < 20; j++) {
      translate(0, step)
      push()
      for (let k = 0; k < 20; k++) {
        translate(step + p, 0)
        box(bSize)
      }
      pop()
    }
    pop()
  }
  pop()

  // push()
  // translate(0, 0, -200)
  // rotateX(-PI/6)
  // rotateY(-PI/8)
  // const noiseScaleX = .0018
  // const noiseScaleY = .002
  // const step = 100s
  // for (let x = -5000; x < 600; x += step) {
  //   for (let z = -300; z < 6900; z += step) {

  //     const noiseValue = simplexNoise.noise3D(x * noiseScaleX, z * noiseScaleY, frameCount * 0) * 100

  //     push()
  //     translate(x, 500 + noiseValue, -z);
  //     box(80)
  //     pop()
  //   }
  // }
  // pop()

  translate(-width/2, -height/2, 0)
  fill(darkColor);
  noStroke();
  strokeWeight(4)
  rect(0, 0, space, height);
  rect(0, 0, width, space);
  rect(0, height - space, width, space);
  rect(width - space, 0, space, height);
  noFill()
  stroke(lightColor)
  line(space, space, space, height - space + 2)
  line(space - 2, space, width - space + 2, space)
  line(width - space + 2, height - space, space, height - space)
  line(width - space, height - space, width - space, space)
}

const easeInCirc = (x) => {
  return 1 - sqrt(1 - pow(x, 2));
};

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `perspective_${seed}`, "png");
}
