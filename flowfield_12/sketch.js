const nodes = 5;
let network = [];
const avoidBorder = 0;
let t = 0;

let canvas;
let img;

const border = 20;
let particles = [];
let flowfield = [];
let scale = 10;
// let noiseScale = 0.03;
let noiseScale = 3;
let rows;
let cols;
let seed;
let currentNetworkIndex = 0;
let currentParticle;

const offsetPoints = 10;

// let palette = ["#D5B5F3"];
let palette = ["#9C7B5A", "#CDAF7D", "#7CA98A", "#216C58", "#204534"];
// let palette = ["#5E101E", "#732427", "#8BD6C2", "#175450", "#082B2F"];
// let palette = ["#742228", "#721821", "#67181D", "#3B1816", "#0F0F0F"];
// let palette = ["#760000", "#86502A", "#B87B5E", "#4A6077", "#1F4B3E"];
// let palette = ["#00314F", "#6CA5AE", "#E5F1D9", "#B4A387", "#D38C86"];
// let palette = ["#D5B5F3", "#75D8EC", "#F3A9C5", "#A9F3D3", "#F3C4A9"];
// let palette = ["#082C59", "#0A72A1", "#4183B1", "#526C81", "#DDCEDA"];
// let palette = ["#012023", "#1B443C", "#93907D", "#855352", "#6C252D"];
// let palette = ["#840000", "#C58402", "#8B929C", "#18434C", "#1F2113"];

function preload() {
  img = loadImage("../assets/god_adam_hands.png");
}

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";

// let bgColor = lightColor;
// let accentColor = darkColor;
let bgColor = darkColor;
let accentColor = lightColor;

const poisson_radius = 5;
const poisson_k = 3;
function setup() {
  // canvas = createCanvas(1080, 1350);
  canvas = createCanvas(1080, 1080);
  seed = random(9999);
  randomSeed(seed);
  background(bgColor);
  img.loadPixels();
  network = poissonDiskSampling(poisson_radius, poisson_k);
  // for (let i = 0; i < 1000; i++) {
  //   particles.push(new Particle());
  // }
  for (let i = 0; i < min(network.length, 100); i++) {
    const { x, y } = network[i];
    particles.push(
      new Particle(
        x,
        y
        // noise(x * noiseScale * 10, y * noiseScale * 10) > 0.5
        //   ? color(255)
        //   : color(0)
      )
    );
  }
  rows = (height + offsetPoints) / scale;
  cols = (width + offsetPoints) / scale;
  for (let x = 0; x < cols; x++) {
    for (let y = 0; y < rows; y++) {
      // const nnn = map(y, 0, rows, noiseScale, 0.001);
      const nnn = map(y, 0, rows, noiseScale, noiseScale);
      // const nnn = 0.05;
      // const phi = noise(x * nnn, y * nnn) * TWO_PI;
      const phi = map(
        dist(x * scale + scale / 2, y * scale + scale / 2, width / 2, height/2),
        0,
        width / 2,
        -PI*5 +cos(x),
        // PI *10
        PI *5 + x*y*.1
      );
      const v = p5.Vector.fromAngle(phi);
      // v.setMag(0.1);
      v.normalize();
      flowfield[y * cols + x] = v;
    }
  }
  currentParticle = new Particle(
    network[currentNetworkIndex].x,
    network[currentNetworkIndex].y,
    getColorFromPosLerp(
      network[currentNetworkIndex].x,
      network[currentNetworkIndex].y
    )
    // random(0, 150)
  );
  currentParticle.follow(flowfield);
  noFill();
  stroke(0);
  // rect(
  //   avoidBorder,
  //   avoidBorder,
  //   width - avoidBorder * 2,
  //   height - avoidBorder * 2
  // );
}

function draw() {
  for (let i = 0; i < 300; i++) {
    noFill();
    strokeWeight(1);
    // fill(255, 20);
    beginShape();
    while (currentParticle.update()) {
      const pos = currentParticle.getPos();
      // stroke(getColorFromShader(pos.x, pos.y));
      stroke(currentParticle.col);
      strokeWeight(currentParticle.weight);
      // curveVertex(pos.x, pos.y);
      // point(pos.x, pos.y);
      vertex(pos.x - offsetPoints / 2, pos.y - offsetPoints / 2);
      currentParticle.follow(flowfield);
    }
    endShape();
    currentNetworkIndex++;
    if (!!network[currentNetworkIndex]) {
      currentParticle = new Particle(
        network[currentNetworkIndex].x,
        network[currentNetworkIndex].y,
        getColorFromPosLerp(
          network[currentNetworkIndex].x,
          network[currentNetworkIndex].y
        )
      );
      // strokeWeight(random(5));
    } else {
      noLoop();
      // background(255);
      // currentNetworkIndex = 0;
      // noStroke();
      // fill(255);
      // fill("#f5f5f5");
      // const border = 30;
      // rect(0, 0, width, border);
      // rect(0, 0, border, height);
      // rect(width - border, 0, border, height);
      // rect(0, height - border, width, border);
      break;
    }
  }
  fill("rgb(248,248,255)");
  noStroke();
  rect(0, 0, width, border);
  rect(0, 0, border, height);
  rect(width - border, 0, border, height);
  rect(0, height - border, width, border);
}

const getColorFromShader = (x, y) => {
  return img.pixels[(y * width + x) * 4 + 4] > 0
    ? color(255, 100, 100)
    : color(0);
};

const getColorFromPosLerp = (x, y) => {
  // const c1 = color(100, random(255), 150);
  // const c2 = color(random(255), 150, 100);
  // const c2 = color("#561705");
  // const c1 = color("#A9F3D3");
  const c2 = color("#204534");
  // const c1 = color("#5E280E");
  // const c2 = color("#FFDBAE");
  const c1 = color("#FF396A");
  // const c2 = color(0);
  // const p = map(x + y, avoidBorder * 2, width + height - avoidBorder * 2, 0, 1);
  const p2 = map(y, avoidBorder, height - avoidBorder, 0, palette.length - 1);
  // return lerpColor(c1, c2, p);
  // return random() > 0.05 ? c1 : c2;
  // return palette[constrain(int(randomGaussian(p2, 1)), 0, palette.length - 1)];
  return palette[int(random(palette.length))];
};

function neighbors(pos, distMax) {
  const n = [];
  // if (img.pixels[(pos.y * width + pos.x) * 4 + 4] === 0) return [];
  for (let i = 0; i < network.length; i++) {
    if (network[i].x == pos.x && network[i].y == pos.y) continue;

    if (dist(network[i].x, network[i].y, pos.x, pos.y) <= distMax)
      n.push(network[i]);
  }
  return n;
}

function isValidPoint(grid, cellsize, gwidth, gheight, p, radius) {
  // if (img.pixels[(p.y * width + p.x) * 4 + 4] === 0) return false;
  // if (dist(width / 2, height / 2, p.x, p.y) > 300) return;

  /* Make sure the point is on the screen */
  if (
    p.x < avoidBorder ||
    p.x >= width - avoidBorder + offsetPoints ||
    p.y < avoidBorder ||
    p.y >= height - avoidBorder + offsetPoints
  )
    return false;

  /* Check neighboring eight cells */
  xindex = floor(p.x / cellsize);
  yindex = floor(p.y / cellsize);
  i0 = max(xindex - 1, 0);
  i1 = min(xindex + 1, gwidth - 1);
  j0 = max(yindex - 1, 0);
  j1 = min(yindex + 1, gheight - 1);

  for (i = i0; i <= i1; i++)
    for (j = j0; j <= j1; j++)
      if (grid[i][j] != null)
        if (dist(grid[i][j].x, grid[i][j].y, p.x, p.y) < radius) return false;

  /* If we get here, return true */
  return true;
}

function insertPoint(grid, cellsize, point) {
  xindex = floor(point.x / cellsize);
  yindex = floor(point.y / cellsize);
  grid[xindex][yindex] = point;
}

function poissonDiskSampling(radius, k) {
  N = 2;
  /* The final set of points to return */
  points = [];
  /* The currently "active" set of points */
  active = [];
  /* Initial point p0 */
  // p0 = createVector(
  //   int(random(avoidBorder, width - avoidBorder)),
  //   int(random(avoidBorder, height - avoidBorder))
  // );
  p0 = createVector(width / 2, height / 2);
  // p1 = createVector(700, 400);
  grid = [];
  cellsize = floor(radius / sqrt(N));

  /* Figure out no. of cells in the grid for our canvas */
  ncells_width = ceil(width + offsetPoints - (avoidBorder * 2) / cellsize) + 1;
  ncells_height =
    ceil(height + offsetPoints - (avoidBorder * 2) / cellsize) + 1;

  /* Allocate the grid an initialize all elements to null */
  for (i = 0; i < ncells_width; i++) {
    grid[i] = [];
    for (j = 0; j < ncells_height; j++) grid[i][j] = null;
  }

  insertPoint(grid, cellsize, p0);
  points.push(p0);
  active.push(p0);
  // insertPoint(grid, cellsize, p1);
  // points.push(p1);
  // active.push(p1);

  while (active.length > 0) {
    random_index = int(random(active.length));
    p = active[random_index];

    found = false;
    for (tries = 0; tries < k; tries++) {
      theta = int(random(360));
      new_radius = int(random(radius, 2 * radius));
      pnewx = int(p.x + new_radius * cos(radians(theta)));
      pnewy = int(p.y + new_radius * sin(radians(theta)));
      pnew = createVector(pnewx, pnewy);

      if (
        !isValidPoint(grid, cellsize, ncells_width, ncells_height, pnew, radius)
      )
        continue;

      points.push(pnew);
      insertPoint(grid, cellsize, pnew);
      active.push(pnew);
      found = true;
      break;
    }

    /* If no point was found after k tries, remove p */
    if (!found) active.splice(random_index, 1);
  }

  return points;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `flowfield_12_${seed}`, "png");
}
