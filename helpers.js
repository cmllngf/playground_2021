

function pointInPoly(verts, pt) {
    let intersections = [];
    let c = false;
    // for each edge of the polygon
    for (let i = 0, j = verts.length - 1; i < verts.length; j = i++) {
      // Compute the slope of the edge
      let slope = (verts[j].y - verts[i].y) / (verts[j].x - verts[i].x);
      
      // If the mouse is positioned within the vertical bounds of the edge
      if (((verts[i].y > pt.y) != (verts[j].y > pt.y)) &&
          // And it is far enough to the right that a horizontal line from the
          // left edge of the screen to the mouse would cross the edge
          (pt.x > (pt.y - verts[i].y) / slope + verts[i].x)) {
        
        // To help visualize the algorithm.
        intersections.push({ x: (pt.y - verts[i].y) / slope + verts[i].x, y: mouseY });
        // Flip the flag
        c = !c;
      }
    }
    return c;
  }

  function granulate(gA){
    loadPixels();
    let d = pixelDensity();
    let halfImage = 4 * (width * d) * (height * d);
    for (let ii = 0; ii < halfImage; ii += 4) {
      grainAmount = random(-gA,gA)
      pixels[ii] = pixels[ii]+grainAmount;
      pixels[ii + 1] = pixels[ii+1]+grainAmount;
      pixels[ii + 2] = pixels[ii+2]+grainAmount;
      pixels[ii + 3] = pixels[ii+3]+grainAmount;
    }
    updatePixels();
  }