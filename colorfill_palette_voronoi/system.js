class System {
  constructor(middlePoint, vertices) {
    this.middlePoint = middlePoint;
    this.vertices = vertices;
    this.walkers = [];
    const c = int(random(4, 10))
    for (let i = 0; i < c; i++) {
      const offset = 10;
      this.walkers.push(this.createWalker(createVector(this.middlePoint.x + int(random(-offset,offset)), this.middlePoint.y + int(random(-offset,offset)))));
    }
  }

  //returns false if finished
  update() {
    for (let i = 0; i < 100; i++) {
      this.walkers.forEach((walker) => {
        walker.update(visited, colors, pixel_size);
      });
    }
    this.walkers = this.walkers.filter((walker) => walker.alive);
    return this.walkers.length > 0 ? true : false;
  }

  createWalker(p, lifespan = -1) {
    // const c = palette[int(random(palette.length))];
    // const c = getColorFromCoord(p.x,p.y);
    const c = getColorFromNoise(p.x,p.y);
    // const c = getColorRandom(x,y);

    // const imgIndex = get1DIndex(x, y);
    // colorMode(RGB);

    // const R = img.pixels[imgIndex];
    // const G = img.pixels[imgIndex + 1];
    // const B = img.pixels[imgIndex + 2];
    // const c = color(R, G, B);
    // colorMode(HSB);

    // const c = getColorPaletteFromImage(x, y);

    seed(p, c);

    return new Walker(
      p,
      lifespan,
      this.vertices
    );
  }
}
