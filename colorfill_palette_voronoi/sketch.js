let visited = [];
let colors = [];
let seedId;
let x = 0;
let y = 0;
let palette;
let img;
const border = 0
const avoidBorder = 0;

const k = 300;
const pixel_size = 3;
let simplexNoise;
const scaleX = 80;
const scaleY = 80;

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";

let currentSystemIndex = 0;
const systems = [];
const noiseField = [];

const poisson_radius = 40;
const poisson_k = 2;

let rows, cols

function seed(s, c) {
  visited[s.y * width + s.x] = true;
  colors[s.y * width + s.x] = c;
}

function preload() {
  img = loadImage("../assets/pompei.png");
}

function setup() {
  seedId = random(999999);
  img.loadPixels();
  randomSeed(seedId);
  simplexNoise = openSimplexNoise(seedId);
  // createCanvas(img.width * 2, img.height * 2);
  createCanvas(1080, 1350);
  colorMode(HSB);
  background("#f8f8ff");

  rows = int(height / scaleY);
  cols = int(width / scaleX);

  for (let x = 0; x < cols; x++) {
    noiseField.push([]);
    for (let y = 0; y < rows; y++) {
      // const noiseValue = map(dist(x * scaleX,y*scaleY,width/2, height/2), 0, width/2, -1, 1, false);
      const noiseValue = simplexNoise.noise2D(x * 0.1, y * 0.1);
      noiseField[x].push({
        // color: map(noiseValue, -1, 1, 0, 255),
        color: palette[int(map(noiseValue, -1, 1, 0, palette.length))],
        colorIndex: int(map(noiseValue, -1, 1, 0, palette.length)),
        palette: noiseValue > 0 ? ["#845ec2", "#d65db1", "#ff6f91", "#ff9671", "#ffc75f", "#f9f871"] : ["#5ec2b5", "#69d1ae", "#81dea2", "#a3e991", "#ccf27f", "#f9f871"],
        radius:
          noiseValue > 0
            ? map(noiseValue, 0, 1, poisson_radius * .6, poisson_radius * .3)
            : map(noiseValue, -1, 0, poisson_radius * .8, poisson_radius * 1.2),
      });
    }
  }
  network = varyingRadiousPoissonDisk(poisson_k);

  const voronoi = d3.voronoi().extent([
    [border, border],
    [width - border, height - border],
  ]);

  // const positions = new Array(140).fill(0).map(() => [int(random(border, width - border)), int(random(border, height - border))])
  const positions = network.map((p) => [p.x, p.y])

  const polygons = voronoi(positions).polygons();

  polygons.forEach((vertices, i) => {
    systems.push(new System(createVector(positions[i][0], positions[i][1]), vertices.map(([x,y]) => createVector(int(x), int(y)))));
  })

  // systems = [new System(border, border, width - border * 2, height - border * 2)];
}

function draw() {
  for (let i = 0; i < 20; i++) {
    if (!!systems[0] && !systems[0].update()) {
      systems.shift();
      if(!systems[0]) {
        noLoop();
        fill("rgb(248,248,255)");
        noStroke();
        rect(0, 0, width, border);
        rect(0, 0, border, height);
        rect(width - border, 0, border, height);
        rect(0, height - border, width, border);
        granulate(15)
        return;
      }
    }
  }
}

function granulate(gA){
  loadPixels();
  let d = pixelDensity();
  let halfImage = 4 * (width * d) * (height * d);
  for (let ii = 0; ii < halfImage; ii += 4) {
    grainAmount = random(-gA,gA)
    pixels[ii] = pixels[ii]+grainAmount;
    pixels[ii + 1] = pixels[ii+1]+grainAmount;
    pixels[ii + 2] = pixels[ii+2]+grainAmount;
    pixels[ii + 3] = pixels[ii+3]+grainAmount;
  }
  updatePixels();
}

const get1DIndex = (x, y, minX = 0, maxX = width, minY = 0, maxY = height) => {
  const xx = int(map(x, minX, maxX, 0, img.width));
  const yy = int(map(y, minY, maxY, 0, img.height));
  return (yy * img.width + xx) * 4;
};

const getColorPaletteFromImage = (x, y) => {
  const index = get1DIndex(x, y);
  const R = img.pixels[index];
  const G = img.pixels[index + 1];
  const B = img.pixels[index + 2];
  const ll = 0.2126 * R + 0.7152 * G + 0.0722 * B;
  return palette[int(map(ll, 0, 255, 0, palette.length, true))];
};

const getColorFromCoord = (x, y) => {
  // vary mean along x axes
  let mappedIndex;
  // if (dist(x,y,width/2,height/2) < 400) {
    mappedIndex = map(y, height, 0, 0, palette.length-1, true);
  // } else {
  //   mappedIndex = map(y, 0, height, 0, palette.length-1, true);
  // }
  // const mappedIndex = map(y, 0, height, 0, palette.length-1, true);
  const noiseMean = .4
  // const noiseMean = noise(x) * 1
  const gaussianIndex = randomGaussian(mappedIndex, noiseMean)
  const index = int(constrain(gaussianIndex, 0, palette.length - 1))
  return palette[index];
}

const getColorFromNoise = (x, y) => {
  // console.log(noiseField, [constrain(floor(x/scaleX), 0, rows-1)],[constrain(floor(y/scaleY), 0,cols-1)], rows, cols)
  const noiseFielddCell = noiseField[constrain(floor(x/scaleX), 0, cols-1)][constrain(floor(y/scaleY), 0,rows-1)]
  return noiseFielddCell ? noiseFielddCell.palette[constrain(int(randomGaussian(noiseFielddCell.colorIndex, 0.0001)), 0, palette.length)] : 0;
}

const getColorRandom = () => {
  return palette[int(random(palette.length))]
}

function keyPressed(key) {
  if (key.keyCode === 80)
    saveCanvas(canvas, `colorfill_voronoi_${seedId}`, "png");
}

const getEntryFromPos = (point) => {
  const x = int(map(point.x, 0, width, 0, noiseField.length));
  const y = int(map(point.y, 0, height, 0, noiseField[0].length));
  return noiseField[x][y];
};

const varyingRadiousPoissonDisk = (k) => {
  points = [];
  actives = [];
  p0 = createVector(width / 2, height / 2);
  grid = [];

  varyingInsertPoint(grid, p0);
  actives.push(p0);
  points.push(p0);

  while (actives.length > 0) {
    // for (let index = 0; index < 100; index++) {
    random_index = int(random(actives.length));
    p = actives[random_index];
    radius = getEntryFromPos(p).radius;

    found = false;
    for (tries = 0; tries < k; tries++) {
      theta = int(random(TWO_PI));
      new_radius = int(random(radius, 2 * radius));
      pnewx = int(p.x + new_radius * cos(theta));
      pnewy = int(p.y + new_radius * sin(theta));
      pnew = createVector(pnewx, pnewy);

      if (!varyingIsValidPoint(grid, pnew)) continue;

      varyingInsertPoint(grid, pnew);
      points.push(pnew);
      actives.push(pnew);
      found = true;
      break;
    }

    if (!found) actives.splice(random_index, 1);
  }

  return points;
};

const varyingIsValidPoint = (grid, point) => {
  for (let i = 0; i < grid.length; i++) {
    if (
      point.x < avoidBorder ||
      point.x >= width - avoidBorder ||
      point.y < avoidBorder ||
      point.y >= height - avoidBorder
    )
      return false;

    if (
      dist(grid[i].point.x, grid[i].point.y, point.x, point.y) < grid[i].radius
    )
      return false;
  }
  return true;
};

const varyingInsertPoint = (grid, point) => {
  grid.push({
    ...getEntryFromPos(point),
    point,
  });
  // console.log(grid)
};

palette = ["#151412", "#272E1E", "#354A29", "#53734A", "#84B188", "#D5D7D4"];
// palette = ["#220507", "#450A04", "#6A2B02", "#8F5201", "#C98A43", "#E6D19F"];
// palette = ["#5C3C49", "#8D5964", "#AF747F", "#D1617E", "#E788A2", "#EEAEB8"];
// palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
// palette = ["#AF747F", "#D1617E", "#E788A2", "#EEAEB8", "#F9D0C3", "#F6EEE1"];
// palette = [0, 250, 100, 150, 200, 50];
// palette = [150, 250, 100, 50, 0, 200];
// palette = ["#24130C", "#70381F", "#C04700", "#F07800", "#EFB178", "#CCECEB"];
// palette = ["#6F2B3A", "#DB6578", "#E1B971", "#FBEDC7", "#949971", "#5B5D45"];
// palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
// palette = ["#092A2F", "#652B05", "#B65302", "#E7A700", "#FAE647", "#00B5AE"];
// palette = ["#162E21", "#063F39", "#30685B", "#73967E", "#C68C20", "#D8D091"];
// palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
// palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
// palette = ["#021118", "#14383D", "#025F71", "#00ACB3", "#9CDACF", "#EBE3D8"];
// palette = ["#845ec2", "#2c73d2", "#0081cf", "#0089ba", "#008e9b", "#008f7a"];
palette = ["#845ec2", "#d65db1", "#ff6f91", "#ff9671", "#ffc75f", "#f9f871"];
// palette = ["#5ec2b5", "#69d1ae", "#81dea2", "#a3e991", "#ccf27f", "#f9f871"];
// palette = ["#c25eb5", "#f6669e", "#ff7f82", "#ffa56a", "#ffcf5f", "#f9f871"];
// palette = ["#3352c6", "#bb4ab6", "#ff5692", "#ff856c", "#ffc058", "#f9f871"];
// palette = ["#7ab6ff","#7ab6ff","#7ab6ff","#7ab6ff","#ffce7a","#ff8c7a","#7ab6ff","#7ab6ff"]
// palette = [darkColor, lightColor]
