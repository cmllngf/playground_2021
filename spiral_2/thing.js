class Thing {
  constructor() {
    const phis = [PI, TWO_PI, PI / 2, (3 * PI) / 2];
    // this.scalar = int(random(width, width + 300));
    this.scalar = width / 3;
    // this.phi = random(TWO_PI, 2 * TWO_PI);
    this.phi = phis[int(random(phis.length))];
    // this.randomMult = int(random(2, 4));
    this.randomMult = 5;
    this.K = 50;
    // this.offset = random(1);
    this.offset = 0;
    // this.randomEndScalar = int(random(20, 40));
    this.randomEndScalar = 40;
    // this.randomEndPhi = random(TWO_PI, 2 * TWO_PI);
    this.randomEndPhi = 0;
    // this.randomEaseValue = int(random(2, 4));
    this.randomEaseValue = 1;
  }

  display() {
    for (let i = 0; i < this.K; i++) {
      let p = map(i + 1 - ((t + this.offset) % 1), 0, this.K, 0, 1);
      p = p % 1;
      this.update(p);
    }
  }

  update(p) {
    const pE = ease2(p, this.randomEaseValue);
    // const pE = p;

    const mult = map(pE, 0, 1, this.randomMult * 2, this.randomMult);
    const scalar = map(pE, 0, 1, this.randomEndScalar, this.scalar);
    const phi = map(pE, 0, 1, this.randomEndPhi, mult * this.phi);

    const x = -cos(phi) * scalar + width / 2;
    const y = sin(phi) * scalar * 0.4 + height / 2;

    let yy = map(dist(x, y, width / 2, height / 2), 0, 300, 40, 0, true);

    stroke(255);
    // stroke(map(pE, 0, 1, 255, 150, true));
    // stroke(this.periodColor((p - t) % 1));
    // stroke(this.periodColor(t - this.offsetFunc(x, y)));
    strokeWeight(
      // map(dist(x, y, width / 2, height / 2), 0, this.scalar, 10, 0, true)
      map(scalar, 0, this.scalar, 5, 0, true)
    );
    stroke(
      // map(dist(x, y, width / 2, height / 2), 0, this.scalar, 10, 0, true)
      map(scalar, 0, this.scalar, 255, 50, true)
    );
    if (!this.isHidden(x, y + yy)) point(x, y + yy);
  }

  isHidden(x, y) {
    return y < height / 2 + 40 && dist(x, y, width / 2, height / 2 + 40) <= 40;
  }

  periodColor(p) {
    return map(sin(TWO_PI * p), 0, 1, 255, 155);
  }

  offsetFunc(x, y) {
    return 0.001 * dist(x, y, width / 2, height / 2);
  }
}
