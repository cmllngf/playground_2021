let canvas;
let seed;
let simplexNoise;
let ctx

const MARGIN = 20
const curves = []
const N = 1020

const presets = [
{
  noiseOffset: 1,
  color: 100,
},
{
  noiseOffset: 100,
  color: 200,
}
];

function setup() {
  canvas = createCanvas(1080, 1350);
  seed = random(99999);
  randomSeed(seed);
  simplexNoise = openSimplexNoise(seed);
  for (let i = 0; i < N; i++) {
    const curve = random() > 1 ? new CurveSwirl() : new Curve();
    curve.create(curves);
    if (curve.points.length > 0) {
      curves.push(curve);
    }
  }
}

function draw() {
  background(20)
  stroke(250)
  strokeWeight(4)
  noFill()
  noLoop()
  for (let i = 0; i < curves.length; i++) {
    const curve = curves[i];
    beginShape()
    stroke(curve.color)
    for (let p = 0; p < curve.points.length; p++) {
      const point = curve.points[p];
      // circle(point.x, point.y, 4)
      curveVertex(point.x, point.y)
    }
    endShape()
  }
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `curve_test_${seed}`, "png");
}
