class Curve {
  constructor() {
    this.x = random(width)
    this.y = random(height)
    this.points = [];
    this.a = random(TWO_PI)
    const preset = random(presets)
    this.color = preset.color;
    this.noiseOffset = preset.noiseOffset;
  }

  create(list) {
    const N_POINTS = 60
    for (let i = 0; i < N_POINTS; i++) {
      this.setNextPoint()
      if (!this.isValidPoint(list)) {
        break;
      }

      this.points.push(createVector(this.x, this.y))
    }
  }

  setNextPoint() {
    this.a = simplexNoise.noise3D(this.x * .001, this.y * .001, this.noiseOffset) * TWO_PI
    // a *= 1.008
    this.x += cos(this.a) * 5
    this.y += sin(this.a) * 5
  }

  isValidPoint(list) {
    if (this.x > width - MARGIN || this.x < MARGIN || this.y > height - MARGIN || this.y < MARGIN) {
      return false
    }

    if (
      list.some(
        item => item.points.some((p) => dist(p.x, p.y, this.x, this.y) < 10))
    ) {
      return false
    }
    return true;
  }
}

class CurveSwirl extends Curve {
  constructor() {
    super();
    this.a = random(TWO_PI)
    this.a = simplexNoise.noise2D(this.x * .001, this.y * .001) * TWO_PI
  }

  setNextPoint() {
    // const a = simplexNoise.noise2D(this.x * .001, this.y * .001) * TWO_PI
    this.a *= 1.012
    this.x += cos(this.a) * 5
    this.y += sin(this.a) * 5
  }
}