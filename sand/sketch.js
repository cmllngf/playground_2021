const nodes = 5;
let network = [];
const avoidBorder = 0;
const poisson_radius = 20;
const poisson_k = 2;
let t = 0;
let palette;

let canvas;
let img;

const border = 160;
let particles = [];

// palette = ["#D5B5F3"];
// palette = ["#9C7B5A", "#CDAF7D", "#7CA98A", "#216C58", "#204534"];
// palette = ["#5E101E", "#732427", "#8BD6C2", "#175450", "#082B2F"];
// palette = ["#742228", "#721821", "#67181D", "#3B1816", "#0F0F0F"];
// palette = ["#760000", "#86502A", "#B87B5E", "#4A6077", "#1F4B3E"];
// palette = ["#00314F", "#6CA5AE", "#E5F1D9", "#B4A387", "#D38C86"];
// palette = ["#D5B5F3", "#75D8EC", "#F3A9C5", "#A9F3D3", "#F3C4A9"];
// palette = ["#082C59", "#0A72A1", "#4183B1", "#526C81", "#DDCEDA"];
// palette = ["#012023", "#1B443C", "#93907D", "#855352", "#6C252D"];
// palette = ["#840000", "#C58402", "#8B929C", "#18434C", "#1F2113"];

// palette = ["#151412", "#272E1E", "#354A29", "#53734A", "#84B188", "#D5D7D4"];
// palette = ["#220507", "#450A04", "#6A2B02", "#8F5201", "#C98A43", "#E6D19F"];
// palette = ["#5C3C49", "#8D5964", "#AF747F", "#D1617E", "#E788A2", "#EEAEB8"];
// palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
palette = ["#AF747F", "#D1617E", "#E788A2", "#EEAEB8", "#F9D0C3", "#F6EEE1"];
// palette = [0, 250, 100, 150, 200, 50];
// palette = [150, 250, 100, 50, 0, 200];
// palette = ["#24130C", "#70381F", "#C04700", "#F07800", "#EFB178", "#CCECEB"];
// palette = ["#6F2B3A", "#DB6578", "#E1B971", "#FBEDC7", "#949971", "#5B5D45"];
// palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
// palette = ["#092A2F", "#652B05", "#B65302", "#E7A700", "#FAE647", "#00B5AE"];
palette = ["#162E21", "#063F39", "#30685B", "#73967E", "#C68C20", "#D8D091"];
// palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
// palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
// palette = ["#021118", "#14383D", "#025F71", "#00ACB3", "#9CDACF", "#EBE3D8"];
// palette = ["#845ec2", "#2c73d2", "#0081cf", "#0089ba", "#008e9b", "#008f7a"];
// palette = ["#845ec2", "#d65db1", "#ff6f91", "#ff9671", "#ffc75f", "#f9f871"];
// palette = ["#5ec2b5", "#69d1ae", "#81dea2", "#a3e991", "#ccf27f", "#f9f871"];
// palette = ["#c25eb5", "#f6669e", "#ff7f82", "#ffa56a", "#ffcf5f", "#f9f871"];
// palette = ["#3352c6", "#bb4ab6", "#ff5692", "#ff856c", "#ffc058", "#f9f871"];


let lightColor = "#f5f8f8";
let darkColor = "#080F0F";

function setup() {
  canvas = createCanvas(1080, 1080);
  // background("#f5f8f8");
  background(lightColor);
  seed = random(9999);
  randomSeed(seed);
  network = poissonDiskSampling(poisson_radius, poisson_k);
  // for (let i = 0; i < min(network.length, 3500); i++) {
  //   const { x, y } = network[i];
  //   particles.push(new Particle(x, y));
  // }
  for (let i = 0; i < 3000; i++) {
    particles.push(new Particle());
  }

  // particles.push(new Particle());
}

function draw() {
  //
  for (let j = 0; j < 1; j++) {
    for (let i = 0; i < particles.length; i++) {
      const p = particles[i];
      p.update();
    }
  }
  fill(lightColor);
  noStroke();
  rect(0, 0, width, border);
  rect(0, 0, border, height);
  rect(width - border, 0, border, height);
  rect(0, height - border, width, border);
  // noLoop();
}

const getColor = (x, y) => {
  const indexMapped = map(y, 0, height, 0, palette.length);
  const index = constrain(
    int(randomGaussian(indexMapped, 0.5)),
    0,
    palette.length - 1
  );
  return color(palette[index]);
};

const getColorFromShader = (x, y) => {
  const xx = int(map(x, 0, width, 0, img.width));
  const yy = int(map(y, 0, height, 0, img.height));
  const index = (yy * img.width + xx) * 4;

  if (img.pixels[index + 3] > 0) {
    const R = img.pixels[index];
    const G = img.pixels[index + 1];
    const B = img.pixels[index + 2];
    return color(R, G, B);
  }

  return color(255);
};

function neighbors(pos, distMax) {
  const n = [];
  // if (img.pixels[(pos.y * width + pos.x) * 4 + 4] === 0) return [];
  for (let i = 0; i < network.length; i++) {
    if (network[i].x == pos.x && network[i].y == pos.y) continue;

    if (dist(network[i].x, network[i].y, pos.x, pos.y) <= distMax)
      n.push(network[i]);
  }
  return n;
}

function isValidPoint(grid, cellsize, gwidth, gheight, p, radius) {
  // if (img.pixels[(p.y * width + p.x) * 4 + 4] === 0) return false;
  if (dist(width / 2, height / 2, p.x, p.y) > 300) return;

  /* Make sure the point is on the screen */
  if (
    p.x < avoidBorder ||
    p.x >= width - avoidBorder ||
    p.y < avoidBorder ||
    p.y >= height - avoidBorder
  )
    return false;

  /* Check neighboring eight cells */
  xindex = floor(p.x / cellsize);
  yindex = floor(p.y / cellsize);
  i0 = max(xindex - 1, 0);
  i1 = min(xindex + 1, gwidth - 1);
  j0 = max(yindex - 1, 0);
  j1 = min(yindex + 1, gheight - 1);

  for (i = i0; i <= i1; i++)
    for (j = j0; j <= j1; j++)
      if (grid[i][j] != null)
        if (dist(grid[i][j].x, grid[i][j].y, p.x, p.y) < radius) return false;

  /* If we get here, return true */
  return true;
}

function insertPoint(grid, cellsize, point) {
  xindex = floor(point.x / cellsize);
  yindex = floor(point.y / cellsize);
  grid[xindex][yindex] = point;
}

function poissonDiskSampling(radius, k) {
  N = 2;
  /* The final set of points to return */
  points = [];
  /* The currently "active" set of points */
  active = [];
  /* Initial point p0 */
  // p0 = createVector(
  //   int(random(avoidBorder, width - avoidBorder)),
  //   int(random(avoidBorder, height - avoidBorder))
  // );
  p0 = createVector(width / 2, height / 2);
  // p1 = createVector(700, 400);
  grid = [];
  cellsize = floor(radius / sqrt(N));

  /* Figure out no. of cells in the grid for our canvas */
  ncells_width = ceil(width - (avoidBorder * 2) / cellsize) + 1;
  ncells_height = ceil(height - (avoidBorder * 2) / cellsize) + 1;

  /* Allocate the grid an initialize all elements to null */
  for (i = 0; i < ncells_width; i++) {
    grid[i] = [];
    for (j = 0; j < ncells_height; j++) grid[i][j] = null;
  }

  insertPoint(grid, cellsize, p0);
  points.push(p0);
  active.push(p0);
  // insertPoint(grid, cellsize, p1);
  // points.push(p1);
  // active.push(p1);

  while (active.length > 0) {
    random_index = int(random(active.length));
    p = active[random_index];

    found = false;
    for (tries = 0; tries < k; tries++) {
      theta = int(random(360));
      new_radius = int(random(radius, 2 * radius));
      pnewx = int(p.x + new_radius * cos(radians(theta)));
      pnewy = int(p.y + new_radius * sin(radians(theta)));
      pnew = createVector(pnewx, pnewy);

      if (
        !isValidPoint(grid, cellsize, ncells_width, ncells_height, pnew, radius)
      )
        continue;

      points.push(pnew);
      insertPoint(grid, cellsize, pnew);
      active.push(pnew);
      found = true;
      break;
    }

    /* If no point was found after k tries, remove p */
    if (!found) active.splice(random_index, 1);
  }

  return points;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `sand_${seed}`, "png");
}
