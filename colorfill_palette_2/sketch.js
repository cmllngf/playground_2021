let visited = [];
let colors = [];
let seedId;
let x = 0;
let y = 0;
let palette;
let img;

const k = 300;
const pixel_size = 3;

let currentSystemIndex = 0;
let system;

function seed(s, c) {
  visited[s.y * width + s.x] = true;
  colors[s.y * width + s.x] = c;
}

function preload() {
  img = loadImage("../assets/adam_hand.png");
}

function setup() {
  seedId = random(999999);
  img.loadPixels();
  randomSeed(seedId);
  createCanvas(1080, 1080);
  colorMode(HSB);
  background("#f8f8ff");
  system = new System(10, 10, width - 20);
}

function draw() {
  for (let i = 0; i < 10; i++) {
    if (!system.update()) {
      noLoop();
      return;
    }
  }
}

function keyPressed(key) {
  console.log(key);
  if (key.keyCode === 80)
    saveCanvas(canvas, `colorfill_palette_2_${seedId}`, "png");
}

// palette = ["#D5B5F3"];
// palette = ["#9C7B5A", "#CDAF7D", "#7CA98A", "#216C58", "#204534"];
// palette = ["#5E101E", "#732427", "#8BD6C2", "#175450", "#082B2F"];
palette = ["#742228", "#721821", "#67181D", "#3B1816", "#0F0F0F"];
// palette = ["#760000", "#86502A", "#B87B5E", "#4A6077", "#1F4B3E"];
// palette = ["#00314F", "#6CA5AE", "#E5F1D9", "#B4A387", "#D38C86"];
// palette = ["#D5B5F3", "#75D8EC", "#F3A9C5", "#A9F3D3", "#F3C4A9"];
// palette = ["#082C59", "#0A72A1", "#4183B1", "#526C81", "#DDCEDA"];
// palette = ["#012023", "#1B443C", "#93907D", "#855352", "#6C252D"];
// palette = ["#840000", "#C58402", "#8B929C", "#18434C", "#1F2113"];
// palette = [
//   "#151412",
//   "#272E1E",
//   "#354A29",
//   "#53734A",
//   "#84B188",
//   "#D5D7D4",
// ];
// palette = [
//   "#220507",
//   "#450A04",
//   "#6A2B02",
//   "#8F5201",
//   "#C98A43",
//   "#E6D19F",
// ];
// palette = ["#5C3C49", "#8D5964", "#AF747F", "#D1617E", "#E788A2", "#EEAEB8"];
// palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
// palette = ["#AF747F", "#D1617E", "#E788A2", "#EEAEB8", "#F9D0C3", "#F6EEE1"];
// palette = [0, 250, 100, 150, 200, 50];
// palette = [150, 250, 100, 50, 0, 200];
// palette = [
//   "#24130C",
//   "#70381F",
//   "#C04700",
//   "#F07800",
//   "#EFB178",
//   "#CCECEB",
// ];
// palette = ["#6F2B3A", "#DB6578", "#E1B971", "#FBEDC7", "#949971", "#5B5D45"];
// palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
// palette = [
//   "#092A2F",
//   "#652B05",
//   "#B65302",
//   "#E7A700",
//   "#FAE647",
//   "#00B5AE",
// ];
// palette = [
//   "#162E21",
//   "#063F39",
//   "#30685B",
//   "#73967E",
//   "#C68C20",
//   "#D8D091",
// ];
// palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
// palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
// palette = ["#845ec2", "#2c73d2", "#0081cf", "#0089ba", "#008e9b", "#008f7a"];
// palette = ["#845ec2", "#d65db1", "#ff6f91", "#ff9671", "#ffc75f", "#f9f871"];
// palette = ["#5ec2b5", "#69d1ae", "#81dea2", "#a3e991", "#ccf27f", "#f9f871"];
// palette = ["#c25eb5", "#f6669e", "#ff7f82", "#ffa56a", "#ffcf5f", "#f9f871"];
// palette = ["#3352c6", "#bb4ab6", "#ff5692", "#ff856c", "#ffc058", "#f9f871"];
// palette = ["#C1C098", "#918B7D", "#572D39", "#6E4D45", "#A69B9D"];
// palette = ["#572D39", "#6E4D45", "#918B7D", "#A69B9D", "#C1C098"];
// palette = ["#5e101e", "#753a15", "#796619", "#6a9141", "#39ba87", "#00e0e1"];
