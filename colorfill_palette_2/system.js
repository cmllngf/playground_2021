class System {
  constructor(x, y, size) {
    this.x = x;
    this.y = y;
    this.size = size;
    this.walkers = [];
    this.randomHue = random(360);

    // for (let i = 0; i < 1; i++) {
    //   const wx = int(random(x, x + size));
    //   const wy = int(random(y, y + size));
    //   this.walkers.push(this.createWalker(wx, wy));
    // }
    for (let i = 0; i < 1; i++) {
      const wx = int(width / 2);
      const wy = int(height / 2);
      this.walkers.push(this.createWalker(wx, wy));
    }

    // let wx = int(width / 2 - 250);
    // let wy = int(height / 2 - 150);
    // this.walkers.push(
    //   this.createWalker(wx, wy, palette[palette.length - 1], 10000, 1)
    // );

    // wx = int(width / 2 + 250);
    // wy = int(height / 2 - 150);
    // this.walkers.push(
    //   this.createWalker(wx, wy, palette[palette.length - 1], 10000, 1)
    // );

    // const network = poissonDiskSampling(poisson_radius, poisson_k);
    // for (let i = 0; i < network.length; i++) {
    //   const { x, y } = network[i];
    //   this.walkers.push(this.createWalker(x, y));
    // }
  }

  //returns false if finished
  update() {
    for (let i = 0; i < 100; i++) {
      this.walkers.forEach((walker) => {
        walker.update(visited, colors, pixel_size);
      });
    }
    this.walkers = this.walkers.filter((walker) => walker.alive);
    return this.walkers.length > 0 ? true : false;
  }

  createWalker(
    x = int(random(width)),
    y = int(random(height)),
    color = palette[int(random(palette.length))],
    lifespan = -1,
    chanceChange = 0.992
  ) {
    const p = createVector(x, y);
    seed(p, color);

    return new Walker(
      p,
      lifespan,
      this.x,
      this.x + this.size,
      this.y,
      this.y + this.size,
      chanceChange
    );
  }
}

const poisson_radius = 30;
const poisson_k = 5;
const avoidBorder = 0;

function isValidPoint(grid, cellsize, gwidth, gheight, p, radius) {
  // if (img.pixels[(p.y * width + p.x) * 4 + 4] === 0) return false;

  /* Make sure the point is on the screen */
  if (
    p.x < avoidBorder ||
    p.x >= width - avoidBorder ||
    p.y < avoidBorder ||
    p.y >= height - avoidBorder
  )
    return false;

  /* Check neighboring eight cells */
  xindex = floor(p.x / cellsize);
  yindex = floor(p.y / cellsize);
  i0 = max(xindex - 1, 0);
  i1 = min(xindex + 1, gwidth - 1);
  j0 = max(yindex - 1, 0);
  j1 = min(yindex + 1, gheight - 1);

  for (i = i0; i <= i1; i++)
    for (j = j0; j <= j1; j++)
      if (grid[i][j] != null)
        if (dist(grid[i][j].x, grid[i][j].y, p.x, p.y) < radius) return false;

  /* If we get here, return true */
  return true;
}

function insertPoint(grid, cellsize, point) {
  xindex = floor(point.x / cellsize);
  yindex = floor(point.y / cellsize);
  grid[xindex][yindex] = point;
}

function poissonDiskSampling(radius, k) {
  N = 2;
  /* The final set of points to return */
  points = [];
  /* The currently "active" set of points */
  active = [];
  /* Initial point p0 */
  p0 = createVector(
    int(random(avoidBorder, width - avoidBorder)),
    int(random(avoidBorder, height - avoidBorder))
  );
  // p0 = createVector(100, 400);
  // p1 = createVector(700, 400);
  grid = [];
  cellsize = floor(radius / sqrt(N));

  /* Figure out no. of cells in the grid for our canvas */
  ncells_width = ceil(width - (avoidBorder * 2) / cellsize) + 1;
  ncells_height = ceil(height - (avoidBorder * 2) / cellsize) + 1;

  /* Allocate the grid an initialize all elements to null */
  for (i = 0; i < ncells_width; i++) {
    grid[i] = [];
    for (j = 0; j < ncells_height; j++) grid[i][j] = null;
  }

  insertPoint(grid, cellsize, p0);
  points.push(p0);
  active.push(p0);
  // insertPoint(grid, cellsize, p1);
  // points.push(p1);
  // active.push(p1);

  while (active.length > 0) {
    random_index = int(random(active.length));
    p = active[random_index];

    found = false;
    for (tries = 0; tries < k; tries++) {
      theta = int(random(360));
      new_radius = int(random(radius, 2 * radius));
      pnewx = int(p.x + new_radius * cos(radians(theta)));
      pnewy = int(p.y + new_radius * sin(radians(theta)));
      pnew = createVector(pnewx, pnewy);

      if (
        !isValidPoint(grid, cellsize, ncells_width, ncells_height, pnew, radius)
      )
        continue;

      points.push(pnew);
      insertPoint(grid, cellsize, pnew);
      active.push(pnew);
      found = true;
      break;
    }

    /* If no point was found after k tries, remove p */
    if (!found) active.splice(random_index, 1);
  }

  return points;
}
