class System {
  constructor(_x, _y, _w, _h, _a = 0) {
    this.x = _x;
    this.y = _y;
    this.width = _w;
    this.height = _h;
    this.a = _a;
    this.walkers = [];
    this.visited = []
    this.colors = []

    // for (let i = 0; i < 1; i++) {
    //   const wx = int(random(x, x + size));
    //   const wy = int(random(y, y + size));
    //   this.walkers.push(this.createWalker(wx, wy));
    // }
    // for (let i = 0; i < 1; i++) {
    // const wx = int(this.width / 2) + this.x;
    // const wy = int(this.height / 2) + this.y;
    // this.walkers.push(this.createWalker(wx, wy));
    // }

    // const poisson_radius = 20;
    // const poisson_radius = randomGaussian(20);
    // const poisson_radius = map(_y, border, height-border, 60, 10);
    const poisson_radius = 20;
    // const poisson_radius = min(5, int(abs(randomGaussian(map(_y, border, height-border, 80, 10), 10))));
    const poisson_k = 1;
    const network = this.poissonDiskSampling(poisson_radius, poisson_k);
    for (let i = 0; i < network.length; i++) {
      const { x, y } = network[i];
      this.walkers.push(this.createWalker(x - this.width/2, y - this.height/2));
    }
  }

  //returns false if finished
  update(visited = this.visited, colors = this.colors) {
    push()
    translate(this.x, this.y)
    rotate(this.a)
    for (let i = 0; i < 100; i++) {
      this.walkers.forEach((walker) => {
        walker.update(visited, colors, pixel_size);
      });
    }
    this.walkers = this.walkers.filter((walker) => walker.alive);
    pop()
    return this.walkers.length > 0;
  }

  seed(s, c) {
    this.visited[s.y * width + s.x] = true;
    this.colors[s.y * width + s.x] = c;
  }

  createWalker(x = int(random(width)), y = int(random(height)), lifespan = -1) {
    const p = createVector(x, y);
    // const c = palette[int(random(palette.length))];
    const c = getColorFromCoord(x+this.x,y+this.y);
    // const c = getColorFromCoord2(x+this.x,y+this.y);
    // const c = getColorRandom();
    // const c = random(250);
    // const c = getColorImg(x+this.x,y+this.y)

    // const imgIndex = get1DIndex(x, y);
    // colorMode(RGB);

    // const R = img.pixels[imgIndex];
    // const G = img.pixels[imgIndex + 1];
    // const B = img.pixels[imgIndex + 2];
    // const c = color(R, G, B);
    // colorMode(HSB);

    // const c = getColorPaletteFromImage(x, y);

    this.seed(p, c);

    return new Walker(
      p,
      lifespan,
      -this.width/2,
      this.width/2,
      -this.height/2,
      this.height/2
      // this.x,
      // this.x + this.width,
      // this.y,
      // this.y + this.height
    );
  }

  isValidPoint(grid, cellsize, gwidth, gheight, p, radius) {
    // if (img.pixels[(p.y * width + p.x) * 4 + 4] === 0) return false;

    /* Make sure the point is on the screen */
    if (
      p.x < 0 ||
      p.x >= this.width ||
      p.y < 0 ||
      p.y >= this.height
    )
      return false;

    /* Check neighboring eight cells */
    let xindex = floor(p.x / cellsize);
    let yindex = floor(p.y / cellsize);
    let i0 = max(xindex - 1, 0);
    let i1 = min(xindex + 1, gwidth - 1);
    let j0 = max(yindex - 1, 0);
    let j1 = min(yindex + 1, gheight - 1);

    // if (!grid[p.x]) return false;

    for (let i = i0; i <= i1; i++)
      for (let j = j0; j <= j1; j++)
        if (grid[i][j] != null)
          if (dist(grid[i][j].x, grid[i][j].y, p.x, p.y) < radius) return false;

    /* If we get here, return true */
    return true;
  }

  insertPoint(grid, cellsize, point) {
    let xindex = floor((point.x) / cellsize);
    let yindex = floor((point.y) / cellsize);
    try {
      grid[xindex][yindex] = point;
    } catch(e) {
      console.log(point.x, xindex, yindex)
    }
  }

  poissonDiskSampling(radius, k) {
    let N = 2;
    /* The final set of points to return */
    let points = [];
    /* The currently "active" set of points */
    let active = [];
    /* Initial point p0 */
    let p0 = createVector(
      int(random(0, this.width)),
      int(random(0, this.height))
    );
    // p0 = createVector(100, 400);
    // p1 = createVector(700, 400);
    let grid = [];
    let cellsize = floor(radius / sqrt(N));

    /* Figure out no. of cells in the grid for our canvas */
    let ncells_width = ceil(this.width / cellsize) + 1;
    let ncells_height = ceil(this.height / cellsize) + 1;
    /* Allocate the grid an initialize all elements to null */
    for (let i = 0; i <= ncells_width; i++) {
      grid[i] = [];
      for (let j = 0; j <= ncells_height; j++) grid[i][j] = null;
    }

    this.insertPoint(grid, cellsize, p0);
    points.push(p0);
    active.push(p0);

    while (active.length > 0) {
      let random_index = int(random(active.length));
      let p = active[random_index];

      let found = false;
      for (let tries = 0; tries < k; tries++) {
        let theta = int(random(360));
        let new_radius = int(random(radius, 2 * radius));
        let pnewx = int(p.x + new_radius * cos(radians(theta)));
        let pnewy = int(p.y + new_radius * sin(radians(theta)));
        let pnew = createVector(pnewx, pnewy);

        if (
          !this.isValidPoint(
            grid,
            cellsize,
            ncells_width,
            ncells_height,
            pnew,
            radius
          )
        )
          continue;

        points.push(pnew);
        this.insertPoint(grid, cellsize, pnew);
        active.push(pnew);
        found = true;
        break;
      }

      /* If no point was found after k tries, remove p */
      if (!found) active.splice(random_index, 1);
    }

    return points;
  }
}
