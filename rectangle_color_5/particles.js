class Particle {
  constructor(_x, _y, _color, _flowfieldIndex) {
    this.pos = createVector(_x, _y);
    this.vel = createVector(0, 0);
    this.acc = createVector(0, 0);
    this.maxSpeed = random(2, 3);
    // this.maxSpeed = abs(randomGaussian(2, 3));
    // this.maxSpeed = 1;
    this.maxForce = random(4.5, 2.5);
    this.radar = random(10, 30);
    this.currentRadar = this.radar;
    this.maxLifetime = random(100, 500);
    this.lifetime = 0;
    this.color = _color;
    this.show = random() > 0.5;
    this.flowfieldIndex = _flowfieldIndex;
  }

  display() {
    noStroke();
    fill(0);
    circle(this.pos.x, this.pos.y, 10);
    // noFill();
    // stroke(0);
    // circle(this.pos.x, this.pos.y, this.radar * 2);
  }

  isAlive() {
    return this.lifetime < this.maxLifetime;
  }

  flee(target) {
    return this.seek(target).mult(-1);
  }

  follow(flowfield) {
    const { x, y } = coordToIndex(this.pos);
    if (flowfield[y] && flowfield[y][x]) {
      this.applyForce(flowfield[y][x].force);
    }
    // console.log(x,y)
  }

  update() {
    this.vel.add(this.acc);
    this.vel.limit(this.maxSpeed);
    this.pos.add(this.vel);
    this.acc.mult(0);
    this.edges();
    // this.center();
    this.lifetime++;
    this.currentRadar = map(this.lifetime, 0, this.maxLifetime, this.radar, 0);
  }

  applyForce(force) {
    this.acc.add(force);
  }

  followOneOf(flowfields) {
    const { x, y } = coordToIndex(this.pos);
    if (
      flowfields[this.flowfieldIndex][y] &&
      flowfields[this.flowfieldIndex][y][x]
    )
      this.applyForce(flowfields[this.flowfieldIndex][y][x].force);
    if (debugFlowfield) {
      // console.log(x,y)
    }
  }

  seek(target) {
    const force = p5.Vector.sub(target, this.pos);
    force.setMag(this.maxSpeed);
    force.sub(this.vel);
    force.limit(this.maxForce);
    return force;
  }

  overlaps(vehicule) {
    return (
      p5.Vector.dist(this.pos, vehicule.pos) <
      this.currentRadar + vehicule.currentRadar
    );
  }

  overlapDisplay(vehicule, fColor) {
    // const size = map(p5.Vector.dist(this.pos, vehicule.pos), 0, this.currentRadar + vehicule.currentRadar, this.radar, 1)
    const size = map(
      p5.Vector.dist(this.pos, vehicule.pos),
      0,
      this.currentRadar + vehicule.currentRadar,
      this.currentRadar,
      5
    );
    // fill(10, .1)
    // fill(color(0, 80, 90))
    fill(fColor ? fColor(this.pos.x, this.pos.y) : this.color);
    noStroke();
    const mid = p5.Vector.lerp(this.pos, vehicule.pos, 0.5);
    circle(mid.x, mid.y, size * 2);
  }

  edges() {
    if (this.pos.x < -outbound) {
      this.pos.x = width + outbound;
    }
    if (this.pos.x > width + outbound) {
      this.pos.x = -outbound;
    }
    if (this.pos.y < -outbound) {
      this.pos.y = height + outbound;
    }
    if (this.pos.y > height + outbound) {
      this.pos.y = -outbound;
    }
  }

  center() {
    if(dist(width/2, height/2, this.pos.x, this.pos.y) < 30) {
      this.pos = createVector(random(-outbound, width+outbound), random(-outbound, height+outbound));
    }
  }
}
