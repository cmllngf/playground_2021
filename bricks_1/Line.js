class Line {
  constructor(x, y, K = 10, speed = random(4)) {
    this.x = x;
    this.y = y;
    this.K = K;
    this.offset = random(1);
    this.speed = speed;
  }

  display() {
    // noStroke();
    // fill(255);
    // circle(this.x, this.y, 10);

    for (let i = 0; i < this.K; i++) {
      let p = map(i - t, 0, this.K - this.speed, 0, 1);
      // p = (p + this.offset) % 1;
      this.update(p);
    }
  }

  update(p) {
    const x = map(p, 0, 1, 0, this.K * (w + gapCol));
    const r = new Rectangle(x + this.x, this.y, w, h);
    r.display();
  }
}
