let canvas;
let seed;
let simplexNoise;
const border = 160;

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";
const palette = ['#065143', '#129490', '#70b77e', '#e0a890', '#ce1483']
// const palette = ["#273c2c", "#626868", "#939196", "#d3c1d2", "#ffe2fe"]

function setup() {
  canvas = createCanvas(1080, 1080);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
}

function draw() {
  background(darkColor);
  noLoop();

  const quad = new Quad({ x: width / 2, y: height / 2, w: width - border*2, h: height - border*2 });
  for (let i = 0; i < 50; i++) {
    quad.subdivide()
  }
  rectMode(CENTER);
  quad.show();

  rectMode(CORNER);
  fill(darkColor);
  noStroke();
  rect(0, 0, width, border);
  rect(0, 0, border, height);
  rect(width - border, 0, border, height);
  rect(0, height - border, width, border);
}

const fill1 = (boundary) => {
  const { x, y, w, h } = boundary;
  const step = w / 4
  for (let i = 0; i < w; i+=step) {
    const lx = x - w/2 + i
    const ly1 = y - h/2
    // const ly2 = y + h/2
    const ly2 = x + h/2
    line(lx, ly1, lx, ly2)
  }
}

const fill2 = (boundary) => {
  const { x, y, w, h } = boundary;
  const step = h / 4
  for (let i = 0; i < h; i+=step) {
    const ly = y - h/2 + i
    const lx1 = x - w/2
    // const lx2 = x + w/2
    const lx2 = y + w/2
    line(lx1, ly, lx2, ly)
  }
}

const fill3 = (boundary) => {
  const { x, y, w, h } = boundary;
  noStroke()
  fill(random(palette))
  rect(x,y,w,h)
}

const fill4 = (boundary) => {
  const { x, y, w, h } = boundary;
  const midx1 = random(x, x+w)
  const midx2 = random(x, x+w)
  fill(random(palette))
  stroke(lightColor)
  beginShape()
  vertex(midx1, y)
  vertex(x + w, y)
  vertex(x + w, y + h)
  vertex(midx2, y + h)
  endShape(CLOSE)
  fill(random(palette))
  beginShape()
  vertex(midx1, y)
  vertex(x, y)
  vertex(x, y + h)
  vertex(midx2, y + h)
  endShape(CLOSE)
}

const isInBox = (x, y) => {
  const multiplier = 1;
  return (
    x > border * multiplier &&
    x < width - border * multiplier &&
    y > border * multiplier &&
    y < height - border * multiplier
  );
};

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `airport_carpet_${seed}`, "png");
}
