class Quad {
  
  constructor(_boundary, _color = lightColor, _orientation = 'column', _cutCount = 2) {
    this.ORIENTATIONS = ['row', 'column']
    this.boundary = _boundary
    this.orientation = _orientation;
    this.subdivisions = [];
    this.color = _color;
    this.cutCount = _cutCount;
    this.shouldSub = true
    this.FUNCTIONS = [fill1, fill2, fill3, fill3, fill3, fill4, fill4, fill4, fill4, fill4, fill4, fill4]
    this.func = this.FUNCTIONS[int(random(this.FUNCTIONS.length))]
  }

  subdivide() {
    if (this.subdivisions.length == 0 && this.shouldSub) {
      // const cut = this.orientation === 'row' ? randomGaussian(this.boundary.w/2, this.boundary.w/20) : randomGaussian(this.boundary.h/2, this.boundary.h/20)
      const newSize = this.orientation === 'row' ? this.boundary.w/this.cutCount : this.boundary.h/this.cutCount
      for (let i = 0; i < this.cutCount; i++) {
        const randomOrientation = this.ORIENTATIONS[int(random(this.ORIENTATIONS.length))]
        // const randomOrientation = this.ORIENTATIONS[1]
        if(this.orientation == 'row') {
          this.subdivisions.push(new Quad({
            ...this.boundary,
            x: i * newSize + this.boundary.x - newSize/this.cutCount,
            w: newSize
          }, undefined, randomOrientation, int(random(2, 3))))
        } else {
          this.subdivisions.push(new Quad({
            ...this.boundary,
            y: i * newSize + this.boundary.y - newSize/this.cutCount,
            h: newSize
          }, undefined, randomOrientation, int(random(2, 3))))
        }
      }
    } else {
      this.subdivisions.forEach(sub => {
        if (random() > .5) {
          sub.subdivide()
        } else {
          // sub.stop()
        }
      })
    }
  }

  stop() {
    this.shouldSub = false
  }

  show () {
    noFill()
    strokeWeight(4)
    stroke(this.color)
    if(this.subdivisions == 0) {
    //   stroke(random(100, 255))
      // circle(this.boundary.x, this.boundary.y, 10)
    //   rect(this.boundary.x, this.boundary.y, this.boundary.w, this.boundary.h)
      this.func(this.boundary)
    } //else {
      rect(this.boundary.x, this.boundary.y, this.boundary.w, this.boundary.h)
    // }
    this.subdivisions.forEach(sub => sub.show());
  }
}