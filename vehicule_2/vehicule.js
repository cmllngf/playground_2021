class Vehicule {
  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.phi = random(TWO_PI);
    this.weight = random(5, 15);
    this.maxLifetime = 1000;
    this.lifetime = 0;
    
    const isPrimary = random() > .01
    this.c = isPrimary ? primary[int(random(primary.length))] : complementary[int(random(complementary.length))];
    this.mult = isPrimary ? .05 : 0
    this.drawShape = !isPrimary || random() > 1
  }

  display() {
    stroke(255);
    // stroke(this.c);
    stroke(!this.drawShape ? this.c : (getColor(this.x, this.y) ?? this.c));
    strokeWeight(this.weight);

    push();
    translate(this.x, this.y);
    rotate(this.phi);
    // point(this.offsetX, this.offsetY);
    point(0, 0);
    pop();
    // point(this.x, this.y);
  }

  isAlive() {
    return this.lifetime < this.maxLifetime;
  }

  update(index = 0) {
    this.x += cos(this.phi) * 2;
    this.y += sin(this.phi) * 2;

    // this.v = noise(this.x, this.y);
    const v = (noise(this.x * 1, this.y * 1) + this.mult * (index - 6 / 3)) % 1;
    // const v = (noise(this.x * 0.1, this.y * 0.1) + randomGaussian(.02, .005) * (index - 6 / 3)) % 1;
    // const v = (noise(this.x * 0.1, this.y * 0.1) + 0.045 * (index - 6 / 3)) % 1;

    this.phi += 2 * map(v, 0, 1, -1, 1);
    this.lifetime++;
    // this.phi += 3 * random(-1, 1);
  }

  edge() {
    if (this.x < -border) {
      this.x = border;
    }
    if (this.x > border) {
      this.x = -border;
    }
    if (this.y < -border) {
      this.y = border;
    }
    if (this.y > border) {
      this.y = border;
    }
  }
}
