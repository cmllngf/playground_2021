let canvas;
let seed;
let simplexNoise;
let t = 0;
let palette = [];
const numFrame = 30;

const recording = true;

// palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
// palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
// palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
// palette = ["#020224", "#01104F", "#003C81", "#01669E", "#5075A2", "#6F91AD"];
// palette = [255];
// palette = ["#845ec2", "#2c73d2", "#0081cf", "#0089ba", "#008e9b", "#008f7a"];
// palette = ["#845ec2", "#d65db1", "#ff6f91", "#ff9671", "#ffc75f", "#f9f871"];
// palette = ["#5ec2b5", "#69d1ae", "#81dea2", "#a3e991", "#ccf27f", "#f9f871"];
// palette = ["#c25eb5", "#f6669e", "#ff7f82", "#ffa56a", "#ffcf5f", "#f9f871"];
// palette = ["#3352c6", "#bb4ab6", "#ff5692", "#ff856c", "#ffc058", "#f9f871"];
// palette = ["#091C3C", "#142B57", "#142D5C", "#152D53", "#193E68", "#244F79"];

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";

const rows = 82;
const cols = 28;
const gapRow = 10;
const gapCol = 10;

let widthBorder;
let heightBorder;

let bgColor = darkColor;
const border = 10;

const W = 100;
const things = [];
let h = 0;
function setup() {
  canvas = createCanvas(1000, 1000);
  widthBorder = width / 1.25;
  heightBorder = height / 1.25;
  seed = random(99999);
  randomSeed(seed);
  simplexNoise = openSimplexNoise(seed);
  if (recording) capturer.start();
  background(bgColor);
  for (let i = 1; i <= 18; i++) {
    // things.push(new Line(200, height - 50 * i - 100, random(3, 20)));
    // things.push(new Line(400, height - 50 * i - 100, random(3, 20)));
    // things.push(new Line(600, height - 50 * i - 100, random(3, 20)));
    // things.push(new Line(800, height - 50 * i - 100, random(3, 20)));
    things.push(new Line(150, height - 50 * i - 50, i * 3));
    things.push(new Line(350, height - 50 * i - 50, i * 3));
    things.push(new Line(550, height - 50 * i - 50, i * 3));
    things.push(new Line(750, height - 50 * i - 50, i * 3));
    // things.push(new Line(900, height - 50 * i - 50, i * 3));
    h = height - 50 * (i - 1) - 100;
  }
}

function draw() {
  background(lightColor);

  t = frameCount / numFrame;
  things.forEach((thing) => {
    thing.display();
  });
  noFill();
  // rect(0, h, W, 20 * 50);
  rect(150, h, W, 18 * 50);
  rect(350, h, W, 18 * 50);
  rect(550, h, W, 18 * 50);
  rect(750, h, W, 18 * 50);
  fill(darkColor);
  noStroke();
  rect(0, 0, width, border);
  rect(0, 0, border, height);
  rect(width - border, 0, border, height);
  rect(0, height - border, width, border);

  if (recording) {
    capturer.capture(canvas.canvas);
    if (frameCount === numFrame * 16) {
      capturer.stop();
      capturer.save();
      noLoop();
    }
  }
}

function ease(p) {
  return 3 * p * p - 2 * p * p * p;
}

function ease2(p, g) {
  if (p < 0.5) return 0.5 * pow(2 * p, g);
  else return 1 - 0.5 * pow(2 * (1 - p), g);
}

function softplus(q, p) {
  const qq = q + p;
  if (qq <= 0) {
    return 0;
  }
  if (qq >= 2 * p) {
    return qq - p;
  }
  return (1 / (4 * p)) * qq * qq;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `fade_${seed}`, "png");
}
