class Line {
  constructor(x, y, n) {
    this.x = x;
    this.y = y;
    this.K = n;
    this.offset = random(1);
  }

  display() {
    for (let i = 0; i < this.K; i++) {
      let p = map((i + t + this.offset) % this.K, 0, this.K, 0, 1);
      // let p = (i + t) / this.K;
      // p = (p + this.offset) % 1;
      const { x, y } = this.update(p);
      stroke(darkColor);
      line(x, y, x, y + 50);
    }
  }

  update(p) {
    const x = map(p, 0, 1, this.x, this.x + W);
    return { x, y: this.y };
  }
}
