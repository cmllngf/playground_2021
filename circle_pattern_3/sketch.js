let canvas;
let seed;
let simplexNoise;
let t = 0;
let palette = [];

const circleCount = 25;
const circleGap = 15;
const noiseScale = 0.01;
const noiseScale2 = 0.012;
const tRadius = 0.3;
const numFrame = 120;
const maxDisplacement = 40;
const minDisplacement = 1;

const recording = false;

let img;

palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
palette = ["#020224", "#01104F", "#003C81", "#01669E", "#5075A2", "#6F91AD"];
palette = [255];

function preload() {
  img = loadImage("../assets/axe1.png");
}

function setup() {
  canvas = createCanvas(1080, 1080);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  if (recording) capturer.start();
}

function draw() {
  randomSeed(seed);
  img.loadPixels();
  background(10);
  image(img, 0, 0);
  stroke("#d5d5d5");
  // noStroke();
  // noLoop();
  strokeWeight(2);
  t = frameCount / numFrame;
  for (let i = circleCount; i > 0; i--) {
    fill(255);
    fill(palette[int(random(palette.length))]);
    // drawCircle(width / 2, height / 2, i * circleGap, true);
    fill(10);
    drawCircle(width / 2, height / 2, i * circleGap, true);
  }
  // noLoop();
  // image(img, 0, 0);
  if (recording) {
    capturer.capture(canvas.canvas);
    if (t === 3) {
      capturer.stop();
      capturer.save();
      noLoop();
    }
  }
}

const drawCircle = (x, y, r, followShader = false) => {
  img.loadPixels();
  const n = 200;
  const nn = int(map(r, circleGap, width / 2, 20, n));
  push();
  translate(x, y);
  beginShape();
  // stroke(palette[int(random(palette.length))]);
  for (let i = 0; i < n; i++) {
    const phi = (TWO_PI / n) * i;
    const dx = int(cos(phi) * r);
    const dy = int(sin(phi) * r);
    rotate(t * 0.01);
    if (followShader) {
      push();
      // translate(dx, dy);
      const id = ((dy + height / 2) * img.width + (dx + width / 2)) * 4;
      const R = img.pixels[id] || 0;
      const G = img.pixels[id + 1] || 0;
      const B = img.pixels[id + 2] || 0;
      const bri = 0.2126 * R + 0.7152 * G + 0.0722 * B;
      // const rand = map(R, 0, 255, 1, 100);
      const rand = R > 0 ? 10 : 0;
      // console.log(R);
      // const rand = 0;
      // console.log(rand);

      curveVertex(dx + rand, dy - rand);
      pop();
    } else {
      curveVertex(dx, dy);
    }
  }
  endShape(CLOSE);
  pop();
};

function ease(p) {
  return 3 * p * p - 2 * p * p * p;
}

function ease2(p, g) {
  if (p < 0.5) return 0.5 * pow(2 * p, g);
  else return 1 - 0.5 * pow(2 * (1 - p), g);
}

function softplus(q, p) {
  const qq = q + p;
  if (qq <= 0) {
    return 0;
  }
  if (qq >= 2 * p) {
    return qq - p;
  }
  return (1 / (4 * p)) * qq * qq;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `circle_pattern_3_${seed}`, "png");
}
