let canvas;
let simplexNoise;

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";
const border = 160;

const DEBUG = false

const N = 10

function setup() {
  seed = random(9999);
  randomSeed(seed);
  simplexNoise = openSimplexNoise(seed);
  canvas = createCanvas(1080, 1080);
  background(darkColor);

}

function draw() {
  noLoop();

  rectMode(CORNER);
  fill(lightColor);
  noStroke();
  rect(0, 0, width, border);
  rect(0, 0, border, height);
  rect(width - border, 0, border, height);
  rect(0, height - border, width, border);

  // branch(random(width*.4, width*.6), height, randomGaussian(-PI/2, PI/70), 250, 5)
  branch(random(width*.4, width*.6), height, 0, 230, N)

}

const branch = (x0, y0, phi, l, deepness) => {
  const x = x0 + sin(phi) * l
  const y = y0 - cos(-phi) * l
  
  if (DEBUG) {
    stroke('red')
    line(x0, y0, x, y)
  }
  deadBranch(x0, y0, phi, l, deepness)
  
  if (deepness == 0) return;

  const luck = map(deepness, 0, N, .2, .1)

  if(random() > luck)
  branch(x,y,phi + randomGaussian(PI/10, PI/30), l*randomGaussian(.8, .08), deepness - 1)
  if(random() > luck)
  branch(x,y,phi - randomGaussian(PI/10, PI/30), l*randomGaussian(.8, .08), deepness - 1)
}

const deadBranch = (x, y, a, l, d) => {
  const b = d * 2
  const w = map(d, 0, N, 2, .2)
  const sW = map(d, 0, N, 1, 7)
  const c = lerpColor(color(lightColor), color(darkColor), map(d, 0, N, .4, 0))

  stroke(c)
  strokeWeight(sW)
  strokeCap(SQUARE)
  push()
  translate(x, y)
  rotate(a)
	for (let i = 0; i < b; i++) {
		const x0 = random(-b / w, b / w);
		const y0 = random(-l * 1.3, -l * 0.01);
		const y1 = random(y0, l * 0.1);
		line(x0, y0, x0, y1);
		// line(0,0,0,-l)
	}
  pop()
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `never_make_${seed}`, "png");
}
