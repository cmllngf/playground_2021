// palette = ["#9C7B5A", "#CDAF7D", "#7CA98A", "#216C58", "#204534"];
// palette = ["#5E101E", "#732427", "#8BD6C2", "#175450", "#082B2F"];
// palette = ["#742228", "#721821", "#67181D", "#3B1816", "#0F0F0F"];
// palette = ["#760000", "#86502A", "#B87B5E", "#4A6077", "#1F4B3E"];
// palette = ["#00314F", "#6CA5AE", "#E5F1D9", "#B4A387", "#D38C86"];
// palette = ["#D5B5F3", "#75D8EC", "#F3A9C5", "#A9F3D3", "#F3C4A9"];
// palette = ["#082C59", "#0A72A1", "#4183B1", "#526C81", "#DDCEDA"];
// palette = ["#012023", "#1B443C", "#93907D", "#855352", "#6C252D"];
// palette = ["#840000", "#C58402", "#8B929C", "#18434C", "#1F2113"];

// palette = ["#151412", "#272E1E", "#354A29", "#53734A", "#84B188", "#D5D7D4"];
// palette = ["#220507", "#450A04", "#6A2B02", "#8F5201", "#C98A43", "#E6D19F"];
// palette = ["#5C3C49", "#8D5964", "#AF747F", "#D1617E", "#E788A2", "#EEAEB8"];
// palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
// palette = ["#AF747F", "#D1617E", "#E788A2", "#EEAEB8", "#F9D0C3", "#F6EEE1"];
// palette = [0, 250, 100, 150, 200, 50];
// palette = [150, 250, 100, 50, 0, 200];
// palette = ["#24130C", "#70381F", "#C04700", "#F07800", "#EFB178", "#CCECEB"];
// palette = ["#6F2B3A", "#DB6578", "#E1B971", "#FBEDC7", "#949971", "#5B5D45"];
// palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
// palette = ["#092A2F", "#652B05", "#B65302", "#E7A700", "#FAE647", "#00B5AE"];
// palette = ["#162E21", "#063F39", "#30685B", "#73967E", "#C68C20", "#D8D091"];
// palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
// palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
// palette = ["#021118", "#14383D", "#025F71", "#00ACB3", "#9CDACF", "#EBE3D8"];
// palette = ["#845ec2", "#2c73d2", "#0081cf", "#0089ba", "#008e9b", "#008f7a"];
// palette = ["#845ec2", "#d65db1", "#ff6f91", "#ff9671", "#ffc75f", "#f9f871"];
// palette = ["#5ec2b5", "#69d1ae", "#81dea2", "#a3e991", "#ccf27f", "#f9f871"];
// palette2 = ["#c25eb5", "#f6669e", "#ff7f82", "#ffa56a", "#ffcf5f", "#f9f871"];
// palette = ["#3352c6", "#bb4ab6", "#ff5692", "#ff856c", "#ffc058", "#f9f871"];

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";

let canvas,
  seed,
  simplexNoise,
  border = -20;
const vehicules = [],
  spaces = [];

/* TODO
 * - Faire un veritable flowfield plutot que juste un vecteur 1,-1
 * - Faire d'autres palettes
 * - Shader ?
 */

// flowfield
const scaleX = 20;
const scaleY = 20;
const flowfield = [];
const noiseScaleX = 0.01;
const noiseScaleY = 0.01;
const debugFlowfield = false;

function setup() {
  seed = random(999999);
  simplexNoise = openSimplexNoise(seed);
  randomSeed(seed);
  canvas = createCanvas(1080, 1080);
  colorMode(HSB);
  // palette = [color(150,15,100,.1),color(150,15,100,.1),color(150,15,100,.1), color(0, 80, 90)]
  palette = [
    color(150, 30, 90, 0.3),
    color(150, 40, 50, 0.3),
    color(150, 15, 60, 0.3),
    color(150, 10, 10, 0.3),
    color(150, 10, 50, 0.3),
    color(150, 40, 50, 0.3),
    color(150, 40, 10, 0.3),
    // color(150, 8, 100),
  ];
  palette = {
    primary: [
      color("#5f0f40"),
      color("#9a031e"),
      color("#fb8b24"),
      color("#e36414"),
      color("#0f4c5c"),
      color("#fed8b1"),
    ],
    background: color("#fed8b1"),
  };
  // palette = {
  //   primary: [
  //     color("#855743"),
  //     color("#5E4F4A"),
  //     color("#776D72"),
  //     color("#BEA68E"),
  //     color("#D86866"),
  //   ],
  //   background: color('#BEA68E'),
  // };

  // palette = {
  //   primary: new Array(5).fill(null).map(() => color(random(360), random(100), random(100))),
  //   background: color('white')
  // }

  palette = {
    primary: [color("#c9cebd"),color("#b2bcaa"),color("#838e83"),color("#6c6061"),color("#64403e")],
    background: color("#c9cebd")
  }

  // palette = {
  //   primary: [color("#d8e2dc"),color("#ffe5d9"),color("#ffcad4"),color("#f4acb7"),color("#9d8189")],
  //   background: color("#ffe5d9")
  // }

  // palette = {
  //   primary: [color(lightColor),color(darkColor)],
  //   background: color(lightColor)
  // }

  // palette.background = palette.primary[int(random(palette.primary.length))]

  // blendMode(OVERLAY);
  // blendMode(EXCLUSION);
  shuffleArray(palette.primary)

  for (let i = 0; i < 2000; i++) {
    const x = random(width);
    const y = random(height);
    vehicules.push(
      new Vehicule(
        x,
        y,
        palette.primary[int(random(palette.primary.length))]
        // getColorFromCoord(x,y)
        // random() > .05 ? palette[constrain(int(randomGaussian(map(y, 0, height, 0, palette.length)), .1), 0, palette.length-1)] : complementary[int(random(complementary.length))]
      )
    );
  }
  // background(color(150, 8, 100));
  // background(color("#fed8b1"));
  background(lightColor);

  //flowfield
  const rows = int(height / scaleY);
  const cols = int(width / scaleX);
  for (let y = 0; y < rows; y++) {
    flowfield.push([]);
    for (let x = 0; x < cols; x++) {
      const a = simplexNoise.noise2D(x * noiseScaleX, y * noiseScaleY) * PI;
      flowfield[y].push({
        force: p5.Vector.fromAngle(a).normalize(),
      });
    }
  }

  // circulare flowfield
  // const rows = height / scaleY;
  // const cols = width / scaleX;
  // for (let y = 0; y < rows; y++) {
  //   flowfield.push([]);
  //   for (let x = 0; x < cols; x++) {
  //     const a = atan2(y - rows/2, x - cols/2) - PI/1.5;
  //     flowfield[y].push({
  //       force: p5.Vector.fromAngle(a).normalize()
  //     });
  //   }
  // }     

  // spaces
  const n = 1;
  const m = 1;
  const sw = width / n;
  const sh = height / m;
  const margin = 160;
  // for (let i = 0; i < n; i++) {
  //   spaces.push(new Space(i * sw + margin, 0, sw - 2*margin, height, palette[int(random(palette.length))]))
  //   spaces[i].display();
  // }

  for (let i = 0; i < n; i++) {
    for (let j = 0; j < m; j++) {
      spaces.push(
        new Space(
          i * sw + margin,
          j * sh + margin,
          sw - 2 * margin,
          sh - 2 * margin,
          palette.primary[int(random(palette.primary.length))]
        )
      );
      spaces[spaces.length - 1].display();
    }
  }
}

function draw() {
  // background(color(150,15,100,.1));
  // background(color("#fed8b1"));

  // for (let i = 0; i < spaces.length; i++) {
  //   const space = spaces[i];
  //   space.display()
  // }

  for (let i = 0; i < vehicules.length; i++) {
    const vehicule = vehicules[i];
    if (!vehicule.isAlive()) {
      continue;
    }
    vehicule.follow(flowfield)
    vehicule.update();
    // vehicule.applyForce(createVector(1, 1));
    for (let j = i; j < vehicules.length; j++) {
      if (i == j) continue;
      if (
        vehicule.overlaps(vehicules[j]) &&
        vehicules[j].isAlive() &&
        vehicule.show
      ) {
        const flee = vehicule.flee(vehicules[j].pos);
        vehicule.applyForce(flee);
        const show = vehicule.showOutsideOnly
          ? spaces.every((s) => !s.isIn(vehicule.pos.x, vehicule.pos.y))
          : vehicule.showAllTime ||
            spaces.some((s) => s.isIn(vehicule.pos.x, vehicule.pos.y));
        if (show) vehicule.overlapDisplay(vehicules[j]);
      }
    }
  }
  fill(color(150, 8, 100));
  noStroke();
  rect(0, 0, width, border);
  rect(0, 0, border, height);
  rect(width - border, 0, border, height);
  rect(0, height - border, width, border);

  if (debugFlowfield) {
    stroke(0);
    for (let y = 0; y < flowfield.length; y++) {
      const row = flowfield[y];
      for (let x = 0; x < row.length; x++) {
        const { force } = row[x];
        push();
        translate(x * scaleX, y * scaleY);
        rotate(force.heading());
        line(0, 0, scaleX, scaleY);
        pop();
      }
    }
  }
}

const getColorFromCoord = (x, y) => {
  // vary mean along x axes
  let mappedIndex;
  // if (dist(x,y,width/2,height/2) < 400) {
  mappedIndex = map(y, height - border, border, 0, palette.primary.length, true);
  // } else {
  //   mappedIndex = map(y, 0, height, 0, palette.length-1, true);
  // }
  // const mappedIndex = map(y, 0, height, 0, palette.length-1, true);
  const noiseMean = .3;
  // const noiseMean = noise(x) * 1
  const gaussianIndex = randomGaussian(mappedIndex, noiseMean);
  const index = int(constrain(gaussianIndex, 0, palette.primary.length - 1));
  return palette.primary[index];
};

const shuffleArray = (array) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    const temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
};


const coordToIndex = (coord) => {
  return {
    x: constrain(floor(coord.x / scaleX), 0, width/scaleX - 1),
    y: constrain(floor(coord.y / scaleY), 0, height/scaleY - 1),
  };
};

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `vehicule_8_${seed}`, "png");
}
