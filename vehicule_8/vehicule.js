class Vehicule {
  constructor(_x, _y, _color) {
    this.pos = createVector(_x, _y);
    this.vel = createVector(0, 0);
    this.acc = createVector(0, 0);
    this.maxSpeed = random(2, 7);
    this.maxForce = random(0.5, 5);
    this.currentRadar = this.radar;
    // this.maxLifetime = random(100, 300);
    this.maxLifetime = randomGaussian(700, 200);
    this.lifetime = 0;
    this.color = _color;
    this.show = random() > 0.86;
    this.showAllTime = random() > .97
    this.showOutsideOnly = this.showAllTime ? random() > .2 : false
    this.radar = !this.showAllTime ? random(20, 20) : random(30, 10);
  }
 
  display() {
    noStroke();
    fill(0);
    circle(this.pos.x, this.pos.y, 10);
    noFill();
    stroke(0);
    circle(this.pos.x, this.pos.y, this.radar * 2);
  }

  isAlive() {
    return this.lifetime < this.maxLifetime;
  }

  flee(target) {
    return this.seek(target).mult(-1);
  }

  follow(flowfield) {
    const { x, y } = coordToIndex(this.pos);
    try {
      this.applyForce(flowfield[y][x].force);
    } catch {
      console.log(x, y)
    }
    if (debugFlowfield) {
      // console.log(x,y)
    }
  }

  seek(target) {
    const force = p5.Vector.sub(target, this.pos);
    force.setMag(this.maxSpeed);
    force.sub(this.vel);
    force.limit(this.maxForce);
    return force;
  }

  update() {
    this.vel.add(this.acc);
    this.vel.limit(this.maxSpeed);
    this.pos.add(this.vel);
    this.acc.mult(0);
    this.edges();
    this.lifetime++;
    this.currentRadar = map(this.lifetime, 0, this.maxLifetime, this.radar, 0);
  }

  applyForce(force) {
    this.acc.add(force);
  }

  overlaps(vehicule) {
    return (
      p5.Vector.dist(this.pos, vehicule.pos) <
      this.currentRadar + vehicule.currentRadar
    );
  }

  overlapDisplay(vehicule) {
    // const size = map(p5.Vector.dist(this.pos, vehicule.pos), 0, this.currentRadar + vehicule.currentRadar, this.radar, 1)
    const size = map(
      p5.Vector.dist(this.pos, vehicule.pos),
      0,
      this.currentRadar + vehicule.currentRadar,
      this.currentRadar,
      2
    );
    // fill(10, .1)
    // fill(color(0, 80, 90))
    fill(this.color);
    noStroke();
    const mid = p5.Vector.lerp(this.pos, vehicule.pos, 0.5);
    circle(mid.x, mid.y, size * 2);
  }

  edges() {
    if (this.pos.x < border) {
      this.pos.x = width - border;
    }
    if (this.pos.x > width - border) {
      this.pos.x = border;
    }
    if (this.pos.y < border) {
      this.pos.y = height - border;
    }
    if (this.pos.y > height - border) {
      this.pos.y = border;
    }
  }
}
