let canvas;
let seed;
let simplexNoise;
let space;

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";
let t = 0
const recording = true;

function ease(p) {
  return 3*p*p - 2*p*p*p;
}

function ease(p, g) {
  if (p < 0.5) 
    return 0.5 * pow(2*p, g);
  else
    return 1 - 0.5 * pow(2*(1 - p), g);
}

const r = 300;
const R = 3000;
// const r = 500;
// const R = 1000;

function drawRectangle(theta, alpha, sz){
  push();
  const x = R-R*cos(alpha)
  const y = R*sin(alpha)
  translate(0,x,y);

  const phiT = t * TWO_PI

  // const noiseValue = map(simplexNoise.noise4D(x * .1 * theta, y * .1 * theta, cos(phiT), sin(phiT)), -1, 1, 10, 200)
  
  push();
  rotateX(-alpha);
  rotateZ(theta);
  rotateX(HALF_PI);
  // translate(0,0,random(r/2, r));
  translate(0,0,r);
  rotateX(-PI/2)
  
  fill(random(random(SETTINGS.palettes)));
  stroke(lightColor);
  strokeWeight(3)
  // noStroke()
  // rect(0,0,sz/3,2*sz);
  // rect(0,0,10, 30);
  // box(30, noiseValue, 30)
  // box(30, random(10, 100), 30)
  // box(40, randomGaussian(50, 30), randomGaussian(50, 30))
  // box(randomGaussian(60, 10), randomGaussian(50, 30), randomGaussian(60, 10))
  if(random() < .4)
      box(40, randomGaussian(50, 30), randomGaussian(50, 30))
  //   box(randomGaussian(60, 30), randomGaussian(30, 30), randomGaussian(60, 30))
  
  rotateX(PI/2)
  rotateY(PI/10)
  translate(0,0,45);
  
  fill(0);
  stroke(0);
  // rect(0,0,110,80);
  
  pop();
  
  pop();
}

function setup() {
  canvas = createCanvas(...SETTINGS.DIM, WEBGL);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  space = SETTINGS.border;
  colorMode(HSB)
  // lights()
  // ortho()
  // ortho(-width/2, width/2, -height/2, height/2, -8000, 10000);
  // perspective(PI/5, 1, .5)
  // perspective(PI/10)

	requestAnimationFrame(draw);
  if (recording) capturer.start();
}

// const N = 10;
// const K = 10;
// const n = 10;
const N = 15;
const K = 5;
const n = 50;

/**
 * TODO
 *  - Bord trop blanc
 *  - autre palette
 */

function draw() {
  randomSeed(seed)
  let dirX = (mouseX / width - 0.5) * 2;
  let dirY = (mouseY / height - 0.5) * 2;
  // directionalLight(color(255), -dirX, -dirY, -1);
  // t = mouseX*1.0/width;
  t += .005
  // t = t % 1
  background(darkColor);
  orbitControl()
  push();

  // inside
  // translate(0,-height/4);

  // on top, looking down
  // translate(0,-height/2, -500);

  // Halo
  // rotateZ(PI/3)
  // rotateY(PI/10)
  // translate(0, -700, -300)

  // coming tube (R 3000)
  rotateZ(PI/3)
  rotateY(PI/20)
  translate(0, -600, -100)

  
  for(let k=0;k<K;k++){
    const sz = map(k,0,K,12/K,12);
    for(let i=0;i<N;i++){
      for(let j=0;j<n;j++){
        // Fait le circle (petit)
        const theta = TWO_PI*j/n;
        // const alpha = map(i+1-5*lerp(ease(t,2.5),t,0.6)-j/n-k/K,0,N,PI,-PI);
        // const alpha = map(i+1-5*lerp(t,t,0.6)-j/n-k/K,0,N,PI,-PI);
        // const alpha = map(i+1-5*t-j/n-k/K,0,N,PI,-PI);
        // const alpha = map(i+1-5*t-j/n,0,N,PI,-PI);
        // const alpha = map(i+1-5*t-j/n,10,N,PI,-PI); // test
        // const alpha = map(i+1-5*t-j/n,0,N,TWO_PI, 0);
        // const alpha = map(i+t-k/K,0,N,PI,-PI);
        // const alpha = map(i-k/K,0,N,PI,-PI);
        const alpha = map(i-k/K,0,N,PI/10,-PI/1.5);
        // const alpha = map(i-t+k/K,0,N,PI/10,-PI/1.5);
        drawRectangle(theta + map(t, 0, 1, 0, TWO_PI - .02),alpha,sz);
        // drawRectangle(theta,alpha,sz);
      }
    }
  }
  // coming tube (R 3000)
  // rotateY(-PI)
  translate(400, 1000, 0)
  rotateZ(-PI/1.5)
  fill('red')
  // box(100,100,100)

  
  for(let k=0;k<K;k++){
    const sz = map(k,0,K,12/K,12);
    for(let i=0;i<N;i++){
      for(let j=0;j<n;j++){
        // Fait le circle (petit)
        const theta = TWO_PI*j/n;
        // const alpha = map(i+1-5*lerp(ease(t,2.5),t,0.6)-j/n-k/K,0,N,PI,-PI);
        // const alpha = map(i+1-5*lerp(t,t,0.6)-j/n-k/K,0,N,PI,-PI);
        // const alpha = map(i+1-5*t-j/n-k/K,0,N,PI,-PI);
        // const alpha = map(i+1-5*t-j/n,0,N,PI,-PI);
        // const alpha = map(i+1-5*t-j/n,10,N,PI,-PI); // test
        // const alpha = map(i+1-5*t-j/n,0,N,TWO_PI, 0);
        // const alpha = map(i+t-k/K,0,N,PI,-PI);
        // const alpha = map(i-k/K,0,N,PI,-PI);
        const alpha = map(i-k/K,0,N,PI/10,-PI/1.5);
        // const alpha = map(i-t+k/K,0,N,PI/10,-PI/1.5);
        drawRectangle(theta + map(t, 0, 1, 0, TWO_PI - .02),alpha,sz);
        // drawRectangle(theta,alpha,sz);
      }
    }
  }
  
  // stroke(lightColor)
  // beginShape(TRIANGLE_STRIP)
  // for(let k=0;k<K;k++){
  //   const sz = map(k,0,K,12/K,12);
  //   for(let i=0;i<N;i++){
  //     for(let j=0;j<n;j++){
  //       // Fait le circle (petit)
  //       const theta = TWO_PI*j/n;
  //       // const alpha = map(i+1-5*lerp(ease(t,2.5),t,0.6)-j/n-k/K,0,N,PI,-PI);
  //       // const alpha = map(i+1-5*lerp(t,t,0.6)-j/n-k/K,0,N,PI,-PI);
  //       // const alpha = map(i+1-5*t-j/n-k/K,0,N,PI,-PI);
  //       // const alpha = map(i+1-5*t-j/n,0,N,PI,-PI);
  //       // const alpha = map(i+1-5*t-j/n,10,N,PI,-PI); // test
  //       // const alpha = map(i+1-5*t-j/n,0,N,TWO_PI, 0);
  //       const alpha = map(i+t-k/K,0,N,PI,-PI);
  //       // drawRectangle(theta,alpha,sz);
  //       push();
  //       translate(0,R-R*cos(alpha),R*sin(alpha))
  //       rotateX(-alpha);
  //       rotateZ(theta);
  //       rotateX(HALF_PI);
  //       translate(0,0,r);
  //       point(0,0,0)
  //       pop()
  //     }
  //   }
  // }
  // endShape()
  
  pop();

  // push()
  // fill(lightColor)
  // rectMode(CENTER)
  // translate(0,0, -4000)
  // rect(0,0, 4000, 4000)
  // pop()

  if (SETTINGS.drawBorder) {
    const heightShift = 800
    const widthtShift = 800
    // translate(-width/2, -height/2, 0)
    // translate(-width/2, -height/2, -600)
    translate(-width/2 - widthtShift/2, -height/2 - heightShift/2, -1000)
    fill(lightColor);
    noStroke();
    rect(0, 0, space, height + heightShift);
    rect(0, 0, width + widthtShift, space);
    rect(0, height - space+ heightShift, width + widthtShift, space);
    rect(width - space + widthtShift, 0, space, height+ heightShift);
    noFill()
    strokeWeight(4)
    // stroke(lightColor)
    // line(space, space, space, height - space + 2)
    // line(space - 2, space, width - space + 2, space)
    // line(width - space + 2, height - space, space, height - space)
    // line(width - space, height - space, width - space, space)
  }
  if (recording) {
    capturer.capture(canvas.canvas);
    if (t >= 3) {
      capturer.stop();
      capturer.save();
      noLoop();
    }
  }
}

/**
 * { value: '', weight: 4 }
 */
const myWeightedRandom = (weightedRandoms) => random(weightedRandoms.reduce((acc, cur) => [...acc, ...new Array(cur.weight).fill(cur.value)],[]))

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `tube_study_1_${seed}`, "png");
}
