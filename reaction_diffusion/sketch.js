p5.disableFriendlyErrors = true;
let seed;
// palette = ["#9C7B5A", "#CDAF7D", "#7CA98A", "#216C58", "#204534"];
// palette = ["#5E101E", "#732427", "#8BD6C2", "#175450", "#082B2F"];
// palette = ["#742228", "#721821", "#67181D", "#3B1816", "#0F0F0F"];
// palette = ["#760000", "#86502A", "#B87B5E", "#4A6077", "#1F4B3E"];
// palette = ["#00314F", "#6CA5AE", "#E5F1D9", "#B4A387", "#D38C86"];
// palette = ["#D5B5F3", "#75D8EC", "#F3A9C5", "#A9F3D3", "#F3C4A9"];
// palette = ["#082C59", "#0A72A1", "#4183B1", "#526C81", "#DDCEDA"];
// palette = ["#012023", "#1B443C", "#93907D", "#855352", "#6C252D"];
// palette = ["#840000", "#C58402", "#8B929C", "#18434C", "#1F2113"];

// palette = ["#151412", "#272E1E", "#354A29", "#53734A", "#84B188", "#D5D7D4"];
// palette = ["#220507", "#450A04", "#6A2B02", "#8F5201", "#C98A43", "#E6D19F"];
// palette = ["#5C3C49", "#8D5964", "#AF747F", "#D1617E", "#E788A2", "#EEAEB8"];
// palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
palette = ["#AF747F", "#D1617E", "#E788A2", "#EEAEB8", "#F9D0C3", "#F6EEE1"];
// palette = [0, 250, 100, 150, 200, 50];
// palette = [150, 250, 100, 50, 0, 200];
// palette = ["#24130C", "#70381F", "#C04700", "#F07800", "#EFB178", "#CCECEB"];
// palette = ["#6F2B3A", "#DB6578", "#E1B971", "#FBEDC7", "#949971", "#5B5D45"];
// palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
// palette = ["#092A2F", "#652B05", "#B65302", "#E7A700", "#FAE647", "#00B5AE"];
// palette = ["#162E21", "#063F39", "#30685B", "#73967E", "#C68C20", "#D8D091"];
// palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
// palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
palette = ["#021118", "#14383D", "#025F71", "#00ACB3", "#9CDACF", "#EBE3D8"];
// palette = ["#845ec2", "#2c73d2", "#0081cf", "#0089ba", "#008e9b", "#008f7a"];
// palette = ["#845ec2", "#d65db1", "#ff6f91", "#ff9671", "#ffc75f", "#f9f871"];
// palette = ["#5ec2b5", "#69d1ae", "#81dea2", "#a3e991", "#ccf27f", "#f9f871"];
// palette2 = ["#c25eb5", "#f6669e", "#ff7f82", "#ffa56a", "#ffcf5f", "#f9f871"];
// palette = ["#3352c6", "#bb4ab6", "#ff5692", "#ff856c", "#ffc058", "#f9f871"];

const scale = 2;

let grid = [];

function setup() {
  seed = random(999999);
  randomSeed(seed);
  createCanvas(200, 200);
  pixelDensity(1);

  const cols = width / scale;
  const rows = height / scale;

  for (let x = 0; x < cols; x++) {
    grid[x] = [];
    for (let y = 0; y < rows; y++) {
      grid[x][y] = {
        a: 1,
        b: 0,
      };
    }
  }

  for (let x = 100 / scale; x < 110 / scale; x++) {
    for (let y = 100 / scale; y < 110 / scale; y++) {
      grid[x][y].b = 1;
    }
  }
}

function draw() {
  background("white");

  const feedRate = 0.055;
  const killRate = 0.062;

  const diffusionRateA = 1;
  const diffusionRateB = 0.2;

  grid = updateGrid(
    grid,
    feedRate,
    killRate,
    diffusionRateA,
    diffusionRateB,
    1
  );
  drawGrid(grid);
}

const drawGrid = (grid) => {
  for (let x = 0; x < grid.length; x++) {
    for (let y = 0; y < grid[x].length; y++) {
      const xx = x * scale;
      const yy = y * scale;
      const { a, b } = grid[x][y];
      const col = color(floor((a - b) * 255));
      // const col = color(a * 255, 0, b * 255);
      // console.log(col);
      noStroke();
      fill(col);
      rect(xx, yy, scale, scale);
      if (a > 1 || b < 0) {
        noLoop();
        console.log("grosse merde");
        console.log(grid[x][y]);
        return;
      }
    }
  }
};

const updateGrid = (grid, fRate, kRate, dRA, dRB, dT) => {
  const newGrid = [];
  for (let x = 0; x < grid.length; x++) {
    newGrid[x] = [];
    for (let y = 0; y < grid[x].length; y++) {
      const { a: oldA, b: oldB } = grid[x][y];
      const a =
        oldA +
        (dRA * laplacian(grid, x, y, "a") -
          ABB(oldA, oldB) +
          fRate * (1 - oldA)) *
          dT;
      const b =
        oldB +
        (dRB * laplacian(grid, x, y, "b") +
          ABB(oldA, oldB) -
          (kRate + fRate) * oldB) *
          dT;
      newGrid[x][y] = {
        a: constrain(a, 0, 1),
        b: constrain(b, 0, 1),
      };
    }
  }
  return newGrid;
};

const convolutionMatrix = [
  [0.05, 0.2, 0.05],
  [0.2, -1, 0.2],
  [0.05, 0.2, 0.05],
];

const laplacian = (grid, x, y, value) => {
  let v = 0;
  for (let cx = -1; cx < 2; cx++) {
    for (let cy = -1; cy < 2; cy++) {
      if (!!grid[x + cx] && !!grid[x + cx][y + cy]) {
        // console.log(convolutionMatrix[cy + 1][cx + 1]);
        v += convolutionMatrix[cy + 1][cx + 1] * grid[x + cx][y + cy][value];
      }
    }
  }
  return v;
};

const ABB = (a, b) => a * b * b;

function keyPressed(key) {
  if (key.keyCode === 80)
    saveCanvas(canvas, `reaction_diffusion_1_${seed}`, "png");
}
