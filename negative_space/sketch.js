let canvas;
let simplexNoise;

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";

const border = 160

function setup() {
  seed = random(9999);
  randomSeed(seed);
  simplexNoise = openSimplexNoise(seed);
  canvas = createCanvas(1080, 1080);
  colorMode(HSB);
  randomSeed(seed);
}

function draw() {
  background(lightColor);
  noLoop();
  
  const radius = 550;
  noStroke()
  fill(darkColor)
  beginShape();
  for (let i = 0; i < 300; i++) {
    let a = random(PI);
    let y = random(-1, 1);
    let r = sqrt(1 - y * y);
    // let z = sin(a * 3);
    let z = sin(a);

    const xx = cos(a) * radius * r + width / 2;
    const yy = y * radius + z * r * 5 + height / 2;
    // const col =
    //   map(noise(xx * 0.01, yy * 0.05), 0, 1, 155, 255) + random(-20, 20);
    // stroke(col);

    // point(xx + random(-2, 2), yy + random(-2, 2));
    vertex(xx, yy);
  }
  endShape();

  rectMode(CORNER);
  fill(lightColor);
  noStroke();
  rect(0, 0, width, border);
  rect(0, 0, border, height);
  rect(width - border, 0, border, height);
  rect(0, height - border, width, border);
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `negative_space_${seed}`, "png");
}
