let canvas;
let seed;
let simplexNoise;
const border = 160
let img

function preload() {
  img = loadImage('../assets/portrait_4 (1).jpg')
}

function setup() {
  canvas = createCanvas(1080, 1080);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  img.loadPixels();
}

const palette = ["#845ec2", "#d65db1", "#ff6f91", "#ff9671", "#ffc75f", "#f9f871"];

function draw() {
  let lightColor = color("#f5f8f8");
  let darkColor = color("#080F0F");

  background(darkColor)
  noLoop()
  
  noStroke()
  fill(darkColor)

  strokeWeight(2)
  noFill()
  const step = .5
  const maxLoop = 100*TWO_PI
  const maxRadius = 1080
  for (let i = 0; i < maxLoop; i+=step) {
    beginShape()
    for (let j = i; j < i + step; j+=step/5) {
      const r = map(j, maxLoop, 0, 0, maxRadius)
      const x = cos(j) * r + width/2
      const y = sin(j) * r + height/2
      // stroke(isInBox(x,y) ? lerpColor(lightColor, darkColor, random()) : lightColor)
      // stroke(isInBox(x,y) ? palette[int(random(palette.length))] : lightColor)
      // stroke(lerpColor(darkColor, lightColor, getIMGValue(x,y)))
      stroke(lightColor)
      vertex(x, y)
    }
    endShape()
  }
  
  rectMode(CORNER)
  fill(lightColor);
  noStroke();
  rect(0, 0, width, border);
  rect(0, 0, border, height);
  rect(width - border, 0, border, height);
  rect(0, height - border, width, border);
}

const getIMGValue = (x, y) => {
  const ix = int(map(x, 0, width, 0, img.width))
  const iy = int(map(y, 0, width, 0, img.height))
  const id = (iy * img.width + ix) * 4
  const r = img.pixels[id];
  const g = img.pixels[id + 1];
  const b = img.pixels[id + 2];
  return map(0.2126 * r + 0.7152 * g + 0.0722 * b, 0, 255, 0, 1);
}

const isInBox = (x,y) => {
  const multiplier = 6.5
  return x > border * multiplier && x < width - border * multiplier && y > border * multiplier && y < height - border * multiplier
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `one_curve_${seed}`, "png");
}
