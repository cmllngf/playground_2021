const nodes = 5;
let network = [];
let voronoi;
let polygons = [];
const avoidBorder = 0;
let t = 0;

let canvas;
let img;
let simplexNoise;

const border = 20;

const scaleX = 150;
const scaleY = 150;
const noiseField = [];

const poisson_radius = 20;
const poisson_k = 5;

let maxLength = 0;
let minLength = 1000000;
function setup() {
  seed = random(9999);
  randomSeed(seed);
  simplexNoise = openSimplexNoise(seed);
  canvas = createCanvas(1080, 1080);
  background(20);

  // const fs = [shape2, shape1, shape4];
  const fs = [shape4, shape5];
  const rows = height / scaleY;
  const cols = width / scaleX;
  for (let x = 0; x < cols; x++) {
    noiseField.push([]);
    for (let y = 0; y < rows; y++) {
      const noiseValue = simplexNoise.noise2D(x * 0.2, y * 0.2);
      noiseField[x].push({
        color: map(noiseValue, -1, 1, 0, 255),
        radius:
          noiseValue > 0
            ? map(noiseValue, 0, 1, poisson_radius * 6, poisson_radius * 8)
            : map(noiseValue, -1, 0, poisson_radius, poisson_radius * 1.5),
        func: fs[
          constrain(
            int(
              randomGaussian(
                noiseValue > 0
                  ? map(noiseValue, 0, 1, 2, 3)
                  : map(noiseValue, -1, 0, 0, 1),
                0.4
              )
            ),
            0,
            fs.length - 1
          )
        ],
        // radius: map(noiseValue, -1, 1, poisson_radius, poisson_radius*4),
      });
    }
  }

  // network = poissonDiskSampling(poisson_radius, poisson_k);
  network = varyingRadiousPoissonDisk(poisson_k);

  // TODO Plusieurs voronoi pas superposés. Côte à Côte avec un margin
  voronoi = d3.voronoi().extent([
    [avoidBorder, avoidBorder],
    [width - avoidBorder, height - avoidBorder],
  ]);

  polygons = voronoi(network.map((p) => [p.x, p.y])).polygons();

  maxLength = polygons.reduce((acc, cur) => {
    const v = getAvgSideLengthFromPolygons(cur);
    return v > acc ? v : acc;
  }, 0);

  minLength = polygons.reduce((acc, cur) => {
    const v = getAvgSideLengthFromPolygons(cur);
    return v < acc ? v : acc;
  }, 10000000);
}

function draw() {
  noLoop();

  // for (let x = 0; x < noiseField.length; x++) {
  //   const col = noiseField[x];
  //   for (let y = 0; y < col.length; y++) {
  //     const entry = col[y];
  //     noStroke();
  //     fill(entry.color);
  //     rect(x * scaleX, y * scaleY, scaleX, scaleY);
  //   }
  // }

  // network.forEach((point) => {
  //   noStroke();
  //   fill("white");
  //   circle(point.x, point.y, 5);
  // });

  polygons.forEach((polygon, i) => {
    beginShape();
    noFill();
    stroke("white");
    polygon.forEach((point) => {
      vertex(point[0], point[1]);
    });
    endShape(CLOSE);
    // fs[int(random(fs.length))](network[i], polygon);
    // fs[int(map(getAvgSideLengthFromPolygons(polygon), 0, maxLength, 0, fs.length-1))](network[i], polygon);
    getEntryFromPos(network[i]).func(network[i], polygon);
  });

  // fill('red')
  // stroke('red')
  // arc(width/2, height/2, 500,450, PI, PI * 1.1, PIE)
}

// Art
const shape1 = (center, points) => {
  for (let i = 0; i < points.length; i++) {
    const p = points[i];
    const v1 = createVector(...p);
    const v2 = points[i + 1]
      ? createVector(...points[i + 1])
      : createVector(...points[0]);
    const middle = p5.Vector.lerp(v1, v2, 0.5);

    noFill();
    stroke("white");
    line(middle.x, middle.y, center.x, center.y);
  }
};

const shape2 = (center, points) => {
  const shortestLenFromCenter = points.reduce((acc, cur, i) => {
    const v1 = createVector(...cur);
    const v2 = points[i + 1]
      ? createVector(...points[i + 1])
      : createVector(...points[0]);
    const middle = p5.Vector.lerp(v1, v2, 0.5);
    const d = dist(middle.x, middle.y, center.x, center.y);
    return d > acc ? acc : d;
  }, 1000000);

  noFill();
  stroke("white");
  let r = shortestLenFromCenter;
  while (r >= 0) {
    circle(center.x, center.y, r * 2);
    r -= 8;
  }
};

const shape3 = (center, points) => {
  stroke("white");
  fill("white");
  beginShape();
  for (let i = 0; i < points.length; i++) {
    vertex(...points);
  }
  endShape(CLOSE);
};

const shape4 = (center, points) => {
  stroke("white");
  noFill();
  for (let p = 1; p > 0; p -= 0.1) {
    beginShape();
    for (let i = 0; i < points.length; i++) {
      const v = p5.Vector.lerp(createVector(...points[i]), center, p);
      vertex(v.x, v.y);
    }
    endShape(CLOSE);
  }
};

const shape5 = (center, points) => {
  noStroke();
  fill("white");
  beginShape();
  for (let i = 0; i <= points.length; i++) {
    const v = points[i]
      ? p5.Vector.lerp(createVector(...points[i]), center, 0.8)
      : p5.Vector.lerp(createVector(...points[0]), center, 0.8);
    curveVertex(v.x, v.y);
  }
  endShape(CLOSE);
};

// Utils
const getEntryFromPos = (point) => {
  const x = int(map(point.x, 0, width, 0, noiseField.length));
  const y = int(map(point.y, 0, height, 0, noiseField[0].length));
  return noiseField[x][y];
};

const getMedianSideLengthFromPolygons = (points) => {
  const lengths = points.map((p1, i, ps) => {
    const p2 = ps[i + 1] ?? ps[0];
    return dist(p1[0], p1[1], p2[0], p2[1]);
  });
  return lengths.length % 2 === 1
    ? lengths[floor(lengths.length / 2)]
    : (lengths[lengths.length / 2] + lengths[lengths.length / 2 - 1]) / 2;
};

const getAvgSideLengthFromPolygons = (points) => {
  const sum = points.reduce((acc, cur, i, ps) => {
    const p2 = ps[i + 1] ?? ps[0];
    return acc + dist(cur[0], cur[1], p2[0], p2[1]);
  }, 0);
  return sum / points.length;
};

// Poisson Disk Sampling
const varyingRadiousPoissonDisk = (k) => {
  points = [];
  actives = [];
  p0 = createVector(width / 2, height / 2);
  grid = [];

  varyingInsertPoint(grid, p0);
  actives.push(p0);
  points.push(p0);

  while (actives.length > 0) {
    // for (let index = 0; index < 100; index++) {
    random_index = int(random(actives.length));
    p = actives[random_index];
    radius = getEntryFromPos(p).radius;

    found = false;
    for (tries = 0; tries < k; tries++) {
      theta = int(random(TWO_PI));
      new_radius = int(random(radius, 2 * radius));
      pnewx = int(p.x + new_radius * cos(theta));
      pnewy = int(p.y + new_radius * sin(theta));
      pnew = createVector(pnewx, pnewy);

      if (!varyingIsValidPoint(grid, pnew)) continue;

      varyingInsertPoint(grid, pnew);
      points.push(pnew);
      actives.push(pnew);
      found = true;
      break;
    }

    if (!found) actives.splice(random_index, 1);
  }

  return points;
};

const varyingIsValidPoint = (grid, point) => {
  for (let i = 0; i < grid.length; i++) {
    if (
      point.x < avoidBorder ||
      point.x >= width - avoidBorder ||
      point.y < avoidBorder ||
      point.y >= height - avoidBorder
    )
      return false;

    if (
      dist(grid[i].point.x, grid[i].point.y, point.x, point.y) < grid[i].radius
    )
      return false;
  }
  return true;
};

const varyingInsertPoint = (grid, point) => {
  grid.push({
    ...getEntryFromPos(point),
    point,
  });
  // console.log(grid)
};

// Good Poisson Disk Sampling
function isValidPoint(grid, cellsize, gwidth, gheight, p, radius) {
  // if (img.pixels[(p.y * width + p.x) * 4 + 4] === 0) return false;
  // if (dist(width / 2, height / 2, p.x, p.y) > 300) return;

  /* Make sure the point is on the screen */
  if (
    p.x < avoidBorder ||
    p.x >= width - avoidBorder ||
    p.y < avoidBorder ||
    p.y >= height - avoidBorder
  )
    return false;

  /* Check neighboring eight cells */
  xindex = floor(p.x / cellsize);
  yindex = floor(p.y / cellsize);
  i0 = max(xindex - 1, 0);
  i1 = min(xindex + 1, gwidth - 1);
  j0 = max(yindex - 1, 0);
  j1 = min(yindex + 1, gheight - 1);

  for (i = i0; i <= i1; i++)
    for (j = j0; j <= j1; j++)
      if (grid[i][j] != null)
        if (dist(grid[i][j].point.x, grid[i][j].point.y, p.x, p.y) < radius)
          return false;

  /* If we get here, return true */
  return true;
}

function insertPoint(grid, cellsize, point) {
  xindex = floor(point.x / cellsize);
  yindex = floor(point.y / cellsize);
  grid[xindex][yindex] = {
    ...getEntryFromPos(point),
    point,
  };
}

function poissonDiskSampling(radius, k) {
  N = 2;
  /* The final set of points to return */
  points = [];
  /* The currently "active" set of points */
  active = [];
  /* Initial point p0 */
  // p0 = createVector(
  //   int(random(avoidBorder, width - avoidBorder)),
  //   int(random(avoidBorder, height - avoidBorder))
  // );
  p0 = createVector(width / 2, height / 2);
  // p1 = createVector(700, 400);
  grid = [];
  cellsize = floor(radius / sqrt(N));

  /* Figure out no. of cells in the grid for our canvas */
  ncells_width = ceil(width - (avoidBorder * 2) / cellsize) + 1;
  ncells_height = ceil(height - (avoidBorder * 2) / cellsize) + 1;

  /* Allocate the grid an initialize all elements to null */
  for (i = 0; i < ncells_width; i++) {
    grid[i] = [];
    for (j = 0; j < ncells_height; j++) grid[i][j] = null;
  }

  insertPoint(grid, cellsize, p0);
  points.push(p0);
  active.push(p0);
  // insertPoint(grid, cellsize, p1);
  // points.push(p1);
  // active.push(p1);

  while (active.length > 0) {
    random_index = int(random(active.length));
    p = active[random_index];

    found = false;
    for (tries = 0; tries < k; tries++) {
      theta = int(random(360));
      new_radius = int(random(radius, 2 * radius));
      pnewx = int(p.x + new_radius * cos(radians(theta)));
      pnewy = int(p.y + new_radius * sin(radians(theta)));
      pnew = createVector(pnewx, pnewy);

      if (
        !isValidPoint(grid, cellsize, ncells_width, ncells_height, pnew, radius)
      )
        continue;

      points.push(pnew);
      insertPoint(grid, cellsize, pnew);
      active.push(pnew);
      found = true;
      break;
    }

    /* If no point was found after k tries, remove p */
    if (!found) active.splice(random_index, 1);
  }

  return points;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `poisson_disk_${seed}`, "png");
}
