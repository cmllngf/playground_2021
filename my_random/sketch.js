let canvas;
let seed;
let simplexNoise;
const border = 160;
// const palette = ['#065143', '#129490', '#70b77e', '#e0a890', '#ce1483']
// const palette = ["#f6e8ea", "#ef626c", "#22181c", "#312f2f", "#84dccf"]
// const palette = ["#393d3f", "#d5bbb1", "#9cc4b2", "#c98ca7", "#e76d83"]
// const palette = ["#087e8b", "#ff5a5f", "#3c3c3c", "#f5f5f5", "#c1839f"]
// const palette = ["#ff74d4", "#ffb8de", "#ffdde1", "#b8e1ff", "#b6174b"]
// const palette = ["#7180ac", "#d81e5b", "#f0f7ee", "#28231c", "#f4bfdb", "#009ddc", "#e94f37", "#30292f", "#f8ad9d", "#053225"]
const palette = ["#053c5e", "#1d3958", "#353652", "#4c334d", "#643047", "#7c2e41", "#942b3b", "#ab2836", "#c32530", "#db222a"]

function setup() {
  canvas = createCanvas(1080, 1080);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
}

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";

const scaleX = 35
const scaleY = 35
const margin = 2

let w,h

function draw() {
  background(darkColor);
  noLoop();

  w = width / (scaleX + margin)
  h = height / (scaleY + margin)
  for (let x = 0; x < width; x+=w) {
    for (let y = 0; y < height; y+=h) {
      noStroke()
      fill(getColorFromY(y))
      // rect(x,y,w - margin,h - margin)
      const noiseScale = .01
      const noiseValueX = simplexNoise.noise2D(x * noiseScale, y * noiseScale) * 10
      const noiseValueY = simplexNoise.noise2D(x * noiseScale, y * noiseScale, 10) * 10
      getShapeFromX(x + noiseValueX,y + noiseValueY)
    }
  }


  rectMode(CORNER);
  fill(darkColor);
  noStroke();
  rect(0, 0, width, border);
  rect(0, 0, border, height);
  rect(width - border, 0, border, height);
  rect(0, height - border, width, border);
}

const getColorFromY = (y) => {
  const mapI = map(y, 0, height, 0, palette.length)
  const normalI = constrain(int(randomGaussian(mapI, .7)), 0, palette.length - 1)
  return palette[normalI];
}

const getShapeFromX = (x, y) => {
  const p = randomGaussian(map(x*y,0, width*height, 4, 10), 1)
  const phi = TWO_PI / p;
  push()
  translate(x, y)
  rotate(random(TWO_PI * 2))
  beginShape()
  for (let i = 0; i < p; i++) {
    const sx = cos(phi * i) * ((w - margin)/2)
    const sy = sin(phi * i) * ((h - margin)/2)
    vertex(sx, sy)
  }
  endShape(CLOSE)
  pop()
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `my_random_${seed}`, "png");
}
