// palette = ["#9C7B5A", "#CDAF7D", "#7CA98A", "#216C58", "#204534"];
// palette = ["#5E101E", "#732427", "#8BD6C2", "#175450", "#082B2F"];
// palette = ["#742228", "#721821", "#67181D", "#3B1816", "#0F0F0F"];
// palette = ["#760000", "#86502A", "#B87B5E", "#4A6077", "#1F4B3E"];
// palette = ["#00314F", "#6CA5AE", "#E5F1D9", "#B4A387", "#D38C86"];
// palette = ["#D5B5F3", "#75D8EC", "#F3A9C5", "#A9F3D3", "#F3C4A9"];
// palette = ["#082C59", "#0A72A1", "#4183B1", "#526C81", "#DDCEDA"];
// palette = ["#012023", "#1B443C", "#93907D", "#855352", "#6C252D"];
// palette = ["#840000", "#C58402", "#8B929C", "#18434C", "#1F2113"];

// palette = ["#151412", "#272E1E", "#354A29", "#53734A", "#84B188", "#D5D7D4"];
// palette = ["#220507", "#450A04", "#6A2B02", "#8F5201", "#C98A43", "#E6D19F"];
// palette = ["#5C3C49", "#8D5964", "#AF747F", "#D1617E", "#E788A2", "#EEAEB8"];
// palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
// palette = ["#AF747F", "#D1617E", "#E788A2", "#EEAEB8", "#F9D0C3", "#F6EEE1"];
// palette = [0, 250, 100, 150, 200, 50];
// palette = [150, 250, 100, 50, 0, 200];
// palette = ["#24130C", "#70381F", "#C04700", "#F07800", "#EFB178", "#CCECEB"];
// palette = ["#6F2B3A", "#DB6578", "#E1B971", "#FBEDC7", "#949971", "#5B5D45"];
// palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
// palette = ["#092A2F", "#652B05", "#B65302", "#E7A700", "#FAE647", "#00B5AE"];
// palette = ["#162E21", "#063F39", "#30685B", "#73967E", "#C68C20", "#D8D091"];
// palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
palette = ["#0F0F0F", "#3F1715", "#68191E"];
// palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
// palette = ["#021118", "#14383D", "#025F71", "#00ACB3", "#9CDACF", "#EBE3D8"];
// palette = ["#845ec2", "#2c73d2", "#0081cf", "#0089ba", "#008e9b", "#008f7a"];
// palette = ["#845ec2", "#d65db1", "#ff6f91", "#ff9671", "#ffc75f", "#f9f871"];
// palette = ["#5ec2b5", "#69d1ae", "#81dea2", "#a3e991", "#ccf27f", "#f9f871"];
// palette2 = ["#c25eb5", "#f6669e", "#ff7f82", "#ffa56a", "#ffcf5f", "#f9f871"];
// palette = ["#3352c6", "#bb4ab6", "#ff5692", "#ff856c", "#ffc058", "#f9f871"];

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";

let canvas,
  seed,
  simplexNoise,
  border = 160,
  img;
const vehicules = [];

/* TODO
 * - Shader ?
 */

// flowfield
const scaleX = 5;
const scaleY = 5;
const flowfield = [];
const flowfields = [];
const noiseScaleX = .01;
const noiseScaleY = .01;
const debugFlowfield = false;

function preload() {
  img = loadImage("../assets/la_joconde.png")
}

function setup() {
  img.loadPixels();
  seed = random(999999);
  simplexNoise = openSimplexNoise(seed);
  randomSeed(seed);

  const W = 1080
  const H = 1080

  canvas = createCanvas(W, H);
  // canvas = createCanvas(1080, 1080);
  colorMode(HSB);
  // palette = [color(150,15,100,.1),color(150,15,100,.1),color(150,15,100,.1), color(0, 80, 90)]
  // palette = {
  //   primary: [
  //     color('#855743'),
  //     color('#5E4F4A'),
  //     color('#776D72'),
  //     color('#BEA68E'),
  //   ],
  //   complementary: [
  //     color('#D86866'),
  //   ]
  // };
  // palette = {
  //   primary: [
  //     color('#E1E2D0'),
  //     color('#B1C2A9'),
  //     color('#485557'),
  //     color('#B1A051'),
  //     color('#3A3240'),
  //   ],
  //   complementary: [
  //     color('#B1A051'),
  //   ]
  // };
  // palette = {
  //   primary: [
  //     color('#3A3D45'),
  //     color('#84A895'),
  //     // color('#FAF7EC'),
  //     color('#D0C8AD'),
  //   ],
  //   complementary: [
  //     color('#735A69'),
  //   ]
  // };
  // palette = {
  //   primary: [
  //     color('#B06F39'),
  //     color('#CFCC9C'),
  //     color('#BB9F76'),
  //     color('#4E6F6E'),
  //   ],
  //   complementary: [
  //     color('#5E2576'),
  //   ]
  // };
  // palette = {
  //   primary: [
  //     color(150, 30, 90, .2),
  //     color(150, 40, 50, .2),
  //     color(150, 15, 60, .2),
  //     color(150, 10, 10, .2),
  //     color(150, 10, 50, .2),
  //     color(150, 40, 50, .2),
  //     color(150, 40, 10, .2),
  //     color(150, 8, 100),
  //   ],
  //   complementary: [
  //     color(0, 100, 80, 0.6),
  //   ]
  // };

  for (let i = 0; i < 5; i++) {
    const rows = height / scaleY;
    const cols = width / scaleX;
    flowfields.push([])
    for (let y = 0; y < rows; y++) {
      flowfields[i].push([]);
      for (let x = 0; x < cols; x++) {
        const a = simplexNoise.noise3D(x * noiseScaleX, y * noiseScaleY, i*.1) * TWO_PI;
        flowfields[i][y].push({
          force: p5.Vector.fromAngle(a).normalize()
        });
      }
    }
  }

  for (let i = 0; i < 1900; i++) {
    const x = random(width);
    const y = random(height);
    vehicules.push(
      new Vehicule(
        x,
        y,
        // random() > .05 ? palette.primary[int(random(palette.primary.length))] : palette.complementary[int(random(palette.complementary.length))],
        // random() > 1 ? getColorImg(x, y, height, palette.primary) : palette.complementary[int(random(palette.complementary.length))],
        // random() > .0 ? getCoordToColorPalette(x, y, height, palette.primary) : palette.complementary[int(random(palette.complementary.length))],
        getColorFromPaletteAndY(palette, y),
        int(random(flowfields.length))
      )
    );
  }
  background(color(150, 8, 100));
  background(darkColor);

  //flowfield
  const rows = height / scaleY;
  const cols = width / scaleX;
  for (let y = 0; y < rows; y++) {
    flowfield.push([]);
    for (let x = 0; x < cols; x++) {
      const a = simplexNoise.noise2D(x * noiseScaleX, y * noiseScaleY) * PI;
      flowfield[y].push({
        force: p5.Vector.fromAngle(a).normalize()
      });
    }
  }

  // circulare flowfield
  // const rows = height / scaleY;
  // const cols = width / scaleX;
  // for (let y = 0; y < rows; y++) {
  //   flowfield.push([]);
  //   for (let x = 0; x < cols; x++) {
  //     const a = atan2(y - rows/2, x - cols/2) - PI/1.5;
  //     flowfield[y].push({
  //       force: p5.Vector.fromAngle(a).normalize()
  //     });
  //   }
  // }
}

function draw() {
  // background(color(150,15,100,.1));

  for (let i = 0; i < vehicules.length; i++) {
    const vehicule = vehicules[i];
    if (!vehicule.isAlive()) {
      continue;
    }
    // vehicule.follow(flowfield)
    // vehicule.followOneOf(flowfields)
    vehicule.update();
    // vehicule.applyForce(createVector(1, -1));
    for (let j = i; j < vehicules.length; j++) {
      if (i == j) continue;
      if (
        vehicule.overlaps(vehicules[j]) &&
        vehicules[j].isAlive() &&
        vehicule.show
      ) {
        const flee = vehicule.flee(vehicules[j].pos);
        vehicule.applyForce(flee);
        // vehicule.overlapDisplay(vehicules[j], getColorImg);
        vehicule.overlapDisplay(vehicules[j], getCoordToColorPalette);
        // vehicule.overlapDisplay(vehicules[j]);
      }
    }
  }
  fill(color(150, 8, 100));
  fill(darkColor);
  noStroke();
  rect(0, 0, width, border);
  rect(0, 0, border, height);
  rect(width - border, 0, border, height);
  rect(0, height - border, width, border);

  if(debugFlowfield) {
    stroke(0)
    for (let y = 0; y < flowfield.length; y++) {
      const row = flowfield[y];
      for (let x = 0; x < row.length; x++) {
        const { force } = row[x];
        push()
        translate(x * scaleX, y * scaleY)
        rotate(force.heading())
        line(0,0,scaleX,scaleY)
        pop()
      }
    }
  }
}

const getColor = (v, min, max, colors) => {
  const i = map(v, min, max, 0, colors.length)
  return colors[constrain(int(randomGaussian(i, .2)), 0, colors.length -1)]
}

const get1DIndex = (x, y, minX = 0, maxX = width, minY = 0, maxY = height) => {
  const xx = int(map(x, minX, maxX, 0, img.width));
  const yy = int(map(y, minY, maxY, 0, img.height));
  return (yy * img.width + xx) * 4;
};

const getColorImg = (x, y) => {
  let c
  const index = get1DIndex(
    int(x),
    int(y),
  );
  const R = img.pixels[index];
  const G = img.pixels[index + 1];
  const B = img.pixels[index + 2];
  const A = img.pixels[index + 3];
  colorMode(RGB)
  c = color(R,G,B,A)
  colorMode(HSB)
  return c
}

const getBrightness = (x, y) => {
  const index = get1DIndex(
    int(x),
    int(y),
    // -width / 2 + 100,
    // width / 2 - 100,
    // -height / 2 + 100,
    // height / 2 - 100
  );
  const R = img.pixels[index];
  const G = img.pixels[index + 1];
  const B = img.pixels[index + 2];
  const A = img.pixels[index + 3];
  if (A === 0) {
    return null;
  }
  const ll = 0.2126 * R + 0.7152 * G + 0.0722 * B;
  // return map(ll, 0, 255, 20, 0);
  return ll;
};

const getCoordToColorPalette = (x, y) => {
  const brightness = getBrightness(x, y);
  if(!brightness) return null
  return palette[int(map(brightness, 0, 255, 0, palette.length))]
}

const getColorFromPaletteAndY = (palette, y) => {
  const index = map(y, border, height - border * 2, 0, palette.length - 1);
  const finalIndex = int(constrain(randomGaussian(index, .8) , 0, palette.length - 1));
  return palette[finalIndex]
}

const coordToIndex = (coord) => {
  return {
    x: int(coord.x / scaleX),
    y: int(coord.y / scaleY)
  }
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `vehicule_6_${seed}`, "png");
}
