let canvas;
let seed;
let t = 0;
let recording = false;
let img;

function preload() {
  img = loadImage("../assets/me.jpg");
}

function setup() {
  canvas = createCanvas(450, 450);
  seed = random(99999);
}

function draw() {
  randomSeed(seed);
  background(10, 10);
  background(0);
  // image(img, 0, 0, 500, 500);

  t = frameCount / 60;
  // if (t >= PI && !recording) {
  //   recording = true;
  //   capturer.start();
  // }

  // for (let i = 0; i < 900; i++) {
  //   let a = random(PI);
  //   let y = random(-1, 1);
  //   let r = sqrt(1 - y * y);
  //   a += t;
  //   // let z = sin(a);
  //   let z = 0;

  //   const px = cos(a % PI) * 350 * r + width / 2;
  //   const py = y * 350 + z * r * 50 + width / 2;
  //   // const py = cos(a % 0.5) * y * 200 + z * r * 50 + width / 2;

  //   const cr = map(z, 0, 1, 0, 255);
  //   // const cg = map(z, -1, 1, 0, 100);

  //   stroke(cr, 0, 0);
  //   stroke(cr);
  //   stroke(180);
  //   strokeWeight(2);
  //   point(px, py);
  // }

  // noStroke();
  // fill(0);
  // circle(width / 2, width / 2, 299);

  beginShape();
  for (let i = 0; i < 100; i++) {
    let a = random(TWO_PI);
    let y = random(-1, 1);
    let r = sqrt(1 - y * y);
    // a += t;
    // let z = sin(a);
    let z = 0;

    const px = cos(a) * 500 * r + width / 2;
    const py = y * 500 + z * r * 50 + width / 2;
    // const py = cos(a % 0.5) * y * 200 + z * r * 50 + width / 2;

    const cr = map(z, 0, 1, 0, 255);
    // const cg = map(z, -1, 1, 0, 100);

    stroke(cr);
    stroke(cr, 0, 0);
    fill(255);
    noStroke();
    strokeWeight(2);
    // point(px, py);
    vertex(px, py);
  }
  endShape();
  // if (recording) {
  //   capturer.capture(canvas.canvas);
  //   if (t >= TWO_PI) {
  //     // if (frameCount == numFrame * 2) {
  //     console.log(frameCount);
  //     capturer.stop();
  //     capturer.save();
  //     noLoop();
  //   }
  // }
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `loop_sphere_2_${seed}`, "png");
}
