let canvas;
let simplexNoise;

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";
const border = 160;

let font;

function preload() {
  font = loadFont('../assets/VCR_OSD_MONO_1.001.ttf')
}

function setup() {
  seed = random(9999);
  randomSeed(seed);
  simplexNoise = openSimplexNoise(seed);
  canvas = createCanvas(1080, 1080);
}

const dateFN = [
  new Date().toLocaleTimeString(),
  new Date().toDateString(),
  new Date().toISOString(),
  new Date().toTimeString(),
  new Date().toLocaleDateString(),
  new Date().toUTCString(),
  new Date().toLocaleString(),
  new Date().toJSON().toString(),
]

const scale = 10;
function draw() {
  noLoop();
  background(darkColor);

  textFont(font)
  textAlign(CENTER)
  fill(lightColor)
  noStroke()
  textSize(25)
  
  for (let i = border; i < height - border; i+= 30) {
    text(random(dateFN), width/2, i)
  }

  rectMode(CORNER);
  fill(lightColor);
  noStroke();
  rect(0, 0, width, border);
  rect(0, 0, border, height);
  rect(width - border, 0, border, height);
  rect(0, height - border, width, border);
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `different_year_${seed}`, "png");
}
