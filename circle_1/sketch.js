let canvas;
let seed;
let simplexNoise;
let t = 0;
let palette = [];
const numFrame = 120;

const recording = false;

palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
palette = ["#020224", "#01104F", "#003C81", "#01669E", "#5075A2", "#6F91AD"];
palette = [255];

function setup() {
  canvas = createCanvas(800, 800);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  if (recording) capturer.start();
}

function draw() {
  translate(width / 2, height / 2);
  randomSeed(seed);
  background(10);
  // noStroke();
  stroke(240);
  t = frameCount / numFrame;
  // noLoop();
  strokeWeight(2);
  noFill();

  const radiusNoise = 1;

  const n1 = randomValue(
    cos(t * TWO_PI) * radiusNoise,
    sin(t * TWO_PI) * radiusNoise,
    100
  );
  const n2 = randomValue(
    cos(t * TWO_PI) * radiusNoise,
    sin(t * TWO_PI) * radiusNoise,
    200
  );
  const n3 = randomValue(
    cos(t * TWO_PI) * radiusNoise,
    sin(t * TWO_PI) * radiusNoise,
    300
  );
  const sumV = n1 + n2 + n3;

  // const t0 = map((n1 + n2 + n3) / sum, 0, 1, 0, TWO_PI);
  // const t0 = 0;
  // const t1 = map(n1 / sumV, -1, 1, 0, TWO_PI);
  // const t2 = map((n1 + n2) / sumV, -1, 1, 0, TWO_PI);
  // const t3 = TWO_PI;

  const t0 = n3 + map(n1, 0, sumV, 0, TWO_PI);
  const t1 = n3 + map(n1 + n2, 0, sumV, 0, TWO_PI);
  const t2 = n3 + map(n1 + n2 + n3, 0, sumV, 0, TWO_PI);

  stroke(255);
  arc(0, 0, 300, 300, t0, t1);
  stroke(155);
  arc(0, 0, 300, 300, t1, t2);
  stroke(55);
  arc(0, 0, 300, 300, t2, t0);

  if (recording) {
    capturer.capture(canvas.canvas);
    if (t === 3) {
      capturer.stop();
      capturer.save();
      noLoop();
    }
  }
}

function randomValue(p, p2, seed) {
  // const scl = 120;
  const scl = 1;
  return pow((noise(scl * p, p2 * scl, seed) + 1) / 2.0, 2.0);
}

function ease(p) {
  return 3 * p * p - 2 * p * p * p;
}

function ease2(p, g) {
  if (p < 0.5) return 0.5 * pow(2 * p, g);
  else return 1 - 0.5 * pow(2 * (1 - p), g);
}

function softplus(q, p) {
  const qq = q + p;
  if (qq <= 0) {
    return 0;
  }
  if (qq >= 2 * p) {
    return qq - p;
  }
  return (1 / (4 * p)) * qq * qq;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `circle_1_${seed}`, "png");
}
