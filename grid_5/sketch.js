let canvas,
  seed,
  t = 0;

let simplexNoise;
let palette;
palette = ["#264653", "#2a9d8f", "#e9c46a", "#f4a261", "#e76f51"];
palette = ["#D5B5F3", "#75D8EC", "#F3A9C5", "#A9F3D3", "#F3C4A9"];

const numFrame = 180;
const recording = true;
const noiseScale = 0.005;

function setup() {
  canvas = createCanvas(1080, 1080);
  seed = random(99999);
  randomSeed(seed);
  simplexNoise = openSimplexNoise(seed);
  if (recording) capturer.start();
}

function draw() {
  t = frameCount / numFrame;
  background(10);

  // circles();
  lines();

  // noLoop();
  if (recording) {
    capturer.capture(canvas.canvas);
    if (t === 3) {
      capturer.stop();
      capturer.save();
      noLoop();
    }
  }
}

const lines = () => {
  const nLines = 30;
  const nHeight = 100;
  const space = width / nLines;
  stroke(255);
  for (let i = 0; i < nLines; i++) {
    beginShape();
    for (let j = 0; j <= nHeight; j++) {
      const y = ((height + 200) / nHeight) * j - 100;
      const x = space * i + space / 2;
      // vertex(x, y);
      push();
      translate(x, y);

      const noiseR = 0.7;
      const noiseValue = simplexNoise.noise4D(
        x * noiseScale,
        y * noiseScale,
        cos(TWO_PI * t) * noiseR,
        sin(TWO_PI * t) * noiseR
      );

      vertex(0, 0);
      rotate(noiseValue * PI);
      line(0, 0, noiseValue * 120, noiseValue * 120);
      line(0, 0, -noiseValue * 120, -noiseValue * 120);
      pop();
    }
    endShape();
  }
};

const circles = () => {
  translate(width / 2, height / 2);
  const ns = [1500, 1000, 1000, 500];
  const rs = [400, 300, 200, 100];
  for (let j = 0; j < 4; j++) {
    const n = ns[j];
    const r = rs[j];
    noFill();
    stroke(255);
    beginShape();
    for (let i = 0; i < n; i++) {
      const phi = (TWO_PI / n) * i;
      const x = cos(phi) * r;
      const y = sin(phi) * r;
      vertex(x, y);
      push();
      translate(x, y);
      rotate(phi * (180 / PI) + t);
      const noiseR = 0.25;
      const noiseValue =
        simplexNoise.noise4D(
          x * noiseScale,
          y * noiseScale,
          cos(TWO_PI * t) * noiseR,
          sin(TWO_PI * t) * noiseR
        ) * 80;
      line(0, 0, noiseValue, noiseValue);
      // line(0, 0, -20, -20);
      pop();
    }
    endShape(CLOSE);
  }
};

function offset(x, y) {
  return 0.015 * dist(x, y, width / 2, height / 2);
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `grid_4_${seed}`, "png");
}
