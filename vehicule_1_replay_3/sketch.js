// palette = ["#9C7B5A", "#CDAF7D", "#7CA98A", "#216C58", "#204534"];
// palette = ["#5E101E", "#732427", "#8BD6C2", "#175450", "#082B2F"];
// palette = ["#742228", "#721821", "#67181D", "#3B1816", "#0F0F0F"];
// palette = ["#760000", "#86502A", "#B87B5E", "#4A6077", "#1F4B3E"];
// palette = ["#00314F", "#6CA5AE", "#E5F1D9", "#B4A387", "#D38C86"];
// palette = ["#D5B5F3", "#75D8EC", "#F3A9C5", "#A9F3D3", "#F3C4A9"];
// palette = ["#082C59", "#0A72A1", "#4183B1", "#526C81", "#DDCEDA"];
// palette = ["#012023", "#1B443C", "#93907D", "#855352", "#6C252D"];
// palette = ["#840000", "#C58402", "#8B929C", "#18434C", "#1F2113"];

// palette = ["#151412", "#272E1E", "#354A29", "#53734A", "#84B188", "#D5D7D4"];
// palette = ["#220507", "#450A04", "#6A2B02", "#8F5201", "#C98A43", "#E6D19F"];
// palette = ["#5C3C49", "#8D5964", "#AF747F", "#D1617E", "#E788A2", "#EEAEB8"];
// palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
// palette = ["#AF747F", "#D1617E", "#E788A2", "#EEAEB8", "#F9D0C3", "#F6EEE1"];
// palette = [0, 250, 100, 150, 200, 50];
// palette = [150, 250, 100, 50, 0, 200];
// palette = ["#24130C", "#70381F", "#C04700", "#F07800", "#EFB178", "#CCECEB"];
// palette = ["#6F2B3A", "#DB6578", "#E1B971", "#FBEDC7", "#949971", "#5B5D45"];
// palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
// palette = ["#092A2F", "#652B05", "#B65302", "#E7A700", "#FAE647", "#00B5AE"];
// palette = ["#162E21", "#063F39", "#30685B", "#73967E", "#C68C20", "#D8D091"];
// palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
// palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
// palette = ["#021118", "#14383D", "#025F71", "#00ACB3", "#9CDACF", "#EBE3D8"];
// palette = ["#845ec2", "#2c73d2", "#0081cf", "#0089ba", "#008e9b", "#008f7a"];
// palette = ["#845ec2", "#d65db1", "#ff6f91", "#ff9671", "#ffc75f", "#f9f871"];
// palette = ["#5ec2b5", "#69d1ae", "#81dea2", "#a3e991", "#ccf27f", "#f9f871"];
// palette2 = ["#c25eb5", "#f6669e", "#ff7f82", "#ffa56a", "#ffcf5f", "#f9f871"];
// palette = ["#3352c6", "#bb4ab6", "#ff5692", "#ff856c", "#ffc058", "#f9f871"];

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";

let canvas,
  seed,
  simplexNoise,
  border = 20,
  img,
  palette;

/* DOABLE
 * - More Palettes
 * - One or more flowfield
 * - Color according to position/randomness/shader/following flowfield
 * - flee or not flee
 * - change speed/lifetime/edges/radar/noiseScale/scale
 * - if shader, do KMeansClustering
 * - less colors
 * - change following flwfield according to certain rules
 * - Modify values from a menu in the browser
 * - black/white (["000000","2f4550","586f7c","b8dbd9","f4f4f9"])
 * - hide small dots
 * - thin white lines over black background
 */

// flowfield
const scaleX = 19;
const scaleY = 19;
let flowfield = [];
let flowfields = [];
const noiseScaleX = .01;
const noiseScaleY = .01;
const ffScale = 0.001;
const showRatio = 0.9;
const primaryColorRatio = 0;
const obeyShaderRatio = 1;
const debugFlowfield = false;
const flee = true

// batch
let batches = [];
let currentBatch = -1;

const spaces = [];

function preload() {
  // img = loadImage("../assets/god_adam_hands.png")
  img = loadImage("../assets/la_joconde.png");
  // img = loadImage("../assets/lady_earring.png");
}

function setup() {
  img.loadPixels();
  frameRate(220);
  seed = random(999999);
  simplexNoise = openSimplexNoise(seed);
  randomSeed(seed);
  currentBatch = -1;
  // canvas = createCanvas(1080, 1080);
  canvas = createCanvas(1080, 1350);
  // canvas = createCanvas(img.width*2.5, img.height*2.5);
  colorMode(HSB);
  // palette = [color(150,15,100,.1),color(150,15,100,.1),color(150,15,100,.1), color(0, 80, 90)]
  // palette = {
  //   primary: [
  //     color('#855743'),
  //     color('#5E4F4A'),
  //     color('#776D72'),
  //     color('#BEA68E'),
  //   ],
  //   complementary: [
  //     color('#D86866'),
  //   ]
  // };
  // palette = {
  //   primary: [
  //     color('#E1E2D0'),
  //     color('#B1C2A9'),
  //     color('#485557'),
  //     color('#3A3240'),
  //   ],
  //   complementary: [
  //     color('#B1A051'),
  //   ]
  // };
  // palette = {
  //   primary: [
  //     color('#3A3D45'),
  //     color('#84A895'),
  //     color('#FAF7EC'),
  //     color('#D0C8AD'),
  //   ],
  //   complementary: [
  //     color('#735A69'),
  //   ]
  // };
  // palette = {
  //   primary: [
  //     color('#B06F39'),
  //     color('#CFCC9C'),
  //     color('#BB9F76'),
  //     color('#4E6F6E'),
  //   ],
  //   complementary: [
  //     color('#5E2576'),
  //   ]
  // };
  // palette = {
  //   primary: [
  //     color(150, 30, 90, .2),
  //     color(150, 40, 50, .2),
  //     color(150, 15, 60, .2),
  //     color(150, 10, 10, .2),
  //     color(150, 10, 50, .2),
  //     color(150, 40, 50, .2),
  //     color(150, 40, 10, .2),
  //     color(150, 8, 100),
  //   ],
  //   complementary: [
  //     color(0, 100, 80, 0.6),
  //   ]
  // };
  palette = {
    primary: [
      color("#5f0f40"),
      color("#9a031e"),
      color("#fb8b24"),
      color("#e36414"),
      color("#0f4c5c"),
      // color("#fed8b1"),
    ],
    complementary: [color("#fed8b1")],
  };
  // blendMode(EXCLUSION);
  // palette = {
  //   primary: [
  //     color('#F8F8F2'),
  //     color('#080F0F'),
  //   ],
  //   complementary: [color('#F8F8F2')],
  // };
  // palette = {
  //   primary: [lightColor],
  //   complementary: [color("#fed8b1")],
  // };
  // palette = {
  //   primary: [0,0],
  //   complementary: [
  //     color('#F8F8F2'),
  //   ]
  // }
  // palette = {
  //   primary: [0, 50, 100, 150, 200, 250],
  //   complementary: [color("#9a031e")],
  // };
  // palette = {
  //   primary: [
  //     "#393232",
  //     "#4D4545",
  //     "#8D6262",
  //     "#ED8D8D"
  //   ],
  //   complementary: ["#ED8D8D"],
  // };
  // palette = {
  //   primary: ["#C9D6DF", "#52616B", "#1E2022"],
  //   complementary: ["#F0F5F9"],
  // };
  // shuffleArray(palette.primary);

  for (let i = 0; i < 6; i++) {
    const rows = height / scaleY;
    const cols = width / scaleX;
    const fx = int(randomGaussian(cols/2, 20))
    const fy = int(randomGaussian(rows/2, 20))
    // const fx = int(randomGaussian(cols/2, 20))
    // const fy = int(randomGaussian(rows/2, 20))
    // const fx = int(random(1,cols-1))
    // const fy = int(random(1,rows-1))
    flowfields.push({
      x: fx,
      y: fy,
      forces: []
    });
    for (let y = 0; y < rows; y++) {
      flowfields[i].forces.push([]);
      for (let x = 0; x < cols; x++) {
        // const a =
        //   simplexNoise.noise3D(x * noiseScaleX, y * noiseScaleY, i * ffScale) *
        //   TWO_PI *
        //   1;
        // const a = map(dist(x,y,cols/2, rows/2), 0, rows/2, PI/6, 2*PI/3 * (i%2==0?-1:1));
        // const a = atan2(y - fx, x - fy) - PI/1.5;
        const a = atan2(y - rows/2, x - cols/2) - PI/1.5;
        // const a = atan2(y - rows/2, x - cols/2) - PI/2;
        flowfields[i].forces[y].push({
          force: p5.Vector.fromAngle(a).normalize(),
          color: palette.primary[i],
        });
      }
    }
  }

  const n = 1;
  const m = 1.5;
  const minMaxLifetime = random(180, 180)
  const maxMaxLifetime = random(480, 480)
  for (let c = 0; c < palette.primary.length * 2 * n; c++) {
    batches.push({
      vehicules: [],
    });
    for (let i = 0; i < floor(1400 / (palette.primary.length * m)); i++) {
      const ffIndex = c % flowfields.length;
      // const x = constrain(randomGaussian(width, width/10), 0, width);
      // const y = constrain(randomGaussian(height, height/10), 0, height);
      // const x = random(width);
      // const y = random(height);
      // const x = randomGaussian(width/2,100);
      // const y = randomGaussian(height/2,100);
      // const x = map(c, 0, palette.primary.length * n, 0, width);
      // const y = map(c, 0, palette.primary.length * n, 0, height);
      const x = constrain(randomGaussian(flowfields[ffIndex].x * scaleX, 100), 0, width);
      const y = constrain(randomGaussian(flowfields[ffIndex].y * scaleY, 100), 0, height);
      // palette.primary[c%palette.primary.length].setAlpha(.7)
      batches[c].vehicules.push(
        new Vehicule(
          x,
          y,
          // getHeightFromColorIndex(c%palette.primary.length),
          // random() > .2 ? palette.primary[int(random(palette.primary.length))] : palette.complementary[int(random(palette.complementary.length))],
          // random() > primaryColorRatio ? getColor(y, 0, height, palette.primary) : palette.complementary[int(random(palette.complementary.length))],
          random() > primaryColorRatio
            ? palette.primary[c % palette.primary.length]
            : palette.complementary[int(random(palette.complementary.length))],
          ffIndex,
          random() > obeyShaderRatio, {
            // _maxSpeed: map(c, 0, palette.primary.length -1, 5, 1.1)
            minMaxLifetime,
            maxMaxLifetime,
          }
        )
      );
    }
  }
  background(color(150, 8, 100));
  // shuffleArray(batches);

  //flowfield
  const rows = height / scaleY;
  const cols = width / scaleX;
  for (let y = 0; y < rows; y++) {
    flowfield.push([]);
    for (let x = 0; x < cols; x++) {
      const a = simplexNoise.noise2D(x * noiseScaleX, y * noiseScaleY) * TWO_PI;
      flowfield[y].push({
        force: p5.Vector.fromAngle(a).normalize(),
      });
    }
  }

  spaces.push(new Space(300, 300, 200, 600));

  // background("#ED8D8D");
  // background("#F0F5F9");
  // background('#F8F8F2');
  // background("#fed8b1");
  // background(darkColor);
  currentBatch = 0;
}

const shuffleArray = (array) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    const temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
};

const isBatchDone = () => {
  return batches[currentBatch]?.vehicules.reduce(
    (acc, vehicule) => !vehicule.isAlive() || acc,
    false
  );
};

const resetAll = () => {
  for (let b = 0; b < batches.length; b++) {
    const vehicules = batches[b].vehicules;
    for (let i = 0; i < vehicules.length; i++) {
      vehicules[i].reset();
    }
  }
};

function draw() {
  // background(color(150,15,100,.1));
  // background("#fed8b1");

  // flowfields.forEach(ff => {
  //   fill('red')
  //   circle(ff.x * scaleX, ff.y * scaleY, 50)
  // })

  for (let b = 0; b < batches.length; b++) {
    const vehicules = batches[b].vehicules;
    for (let i = 0; i < vehicules.length; i++) {
      const vehicule = vehicules[i];
      if (!vehicule.isAlive()) {
        continue;
      }
      // vehicule.follow(flowfield)
      // vehicule.followOneOf(flowfields);
      vehicule.applyForce(createVector(1,-1))
      vehicule.update();
      if (vehicule.show) {
        // vehicule.display();
      }
      // vehicule.applyForce(createVector(1, -1));
      for (let j = i; j < vehicules.length; j++) {
        if (i == j) continue;
        if (
          vehicule.overlaps(vehicules[j]) &&
          vehicules[j].isAlive() &&
          vehicule.show
        ) {
          if (flee) {
            const fleeF = vehicule.flee(vehicules[j].pos);
            vehicule.applyForce(fleeF);
          }
          // vehicule.overlapDisplay(vehicules[j], getCoordToColorPalette);
          for (let s = 0; s < spaces.length; s++) {
            const space = spaces[s];
            // if (b === currentBatch && !space.isIn(vehicule.pos.x, vehicule.pos.y)) {
            //   vehicule.overlapDisplay(vehicules[j]);
            // }
            if (space.isIn(vehicule.pos.x, vehicule.pos.y)) blendMode(EXCLUSION);
            if (b === currentBatch) {
              vehicule.overlapDisplay(vehicules[j]);
            }
            blendMode(BLEND)
          }
        }
      }
    }
  }
  // fill(color(150, 8, 100));
  // fill(color("#ED8D8D"));
  // fill(color("#F0F5F9"));
  // fill(color("#4E6F6E"));
  // fill(color("#fed8b1"));
  fill(darkColor);
  // fill(color("#F8F8F2"));
  noStroke();
  rect(0, 0, width, border);
  rect(0, 0, border, height);
  rect(width - border, 0, border, height);
  rect(0, height - border, width, border);
  // for (let s = 0; s < spaces.length; s++) {
  //   const space = spaces[s];
  //   noStroke()
  //   fill(darkColor)
  //   rect(space.x, space.y, space.width, space.height);
  // }
  if (isBatchDone()) {
    currentBatch++;
    if (!batches[currentBatch]) {
      // noLoop();
      saveCanvas(canvas, `vehicule_1_replay_3_${seed}`, "png");
      batches = []
      flowfields = []
      flowfield = []
      // setup();
      console.log("done");
    } else {
      resetAll();
    }
  }

  if (debugFlowfield) {
    stroke(0);
    for (let y = 0; y < flowfield.length; y++) {
      const row = flowfield[y];
      for (let x = 0; x < row.length; x++) {
        const { force } = row[x];
        push();
        translate(x * scaleX, y * scaleY);
        rotate(force.heading());
        line(0, 0, scaleX, scaleY);
        pop();
      }
    }
  }
}

const getColor = (v, min, max, colors) => {
  const i = map(v, min, max, 0, colors.length);
  return colors[constrain(int(randomGaussian(i, 1)), 0, colors.length - 1)];
};

const getHeightFromColorIndex = (colorIndex) => {
  const y = map(colorIndex, 0, palette.primary.length, 0, height);
  return constrain(randomGaussian(y, 150), 0, height - 1);
};

const get1DIndex = (x, y, minX = 0, maxX = width, minY = 0, maxY = height) => {
  const xx = int(map(x, minX, maxX, 0, img.width));
  const yy = int(map(y, minY, maxY, 0, img.height));
  return (yy * img.width + xx) * 4;
};

const getBrightness = (x, y, col) => {
  const index = get1DIndex(
    int(x),
    int(y)
    // -width / 2 + 100,
    // width / 2 - 100,
    // -height / 2 + 100,
    // height / 2 - 100
  );
  const R = img.pixels[index];
  const G = img.pixels[index + 1];
  const B = img.pixels[index + 2];
  const A = img.pixels[index + 3];
  if (A === 0) {
    return col;
  }
  const ll = 0.2126 * R + 0.7152 * G + 0.0722 * B;
  // return map(ll, 0, 255, 20, 0);
  return ll;
};

const getCoordToColorPalette = (x, y) => {
  const brightness = getBrightness(x, y);
  if (!brightness) return color(0, 0);
  return palette.primary[
    int(map(brightness, 0, 255, 0, palette.primary.length))
  ];
};

const getColorPaletteFromImage = (x, y) => {
  const index = get1DIndex(
    x,
    y,
    border,
    width - border,
    border,
    height - border
  );
  const R = img.pixels[index];
  const G = img.pixels[index + 1];
  const B = img.pixels[index + 2];
  const A = img.pixels[index + 3];
  if (A === 0) {
    return null;
  }
  const ll = 0.2126 * R + 0.7152 * G + 0.0722 * B;
  return palette.primary[int(map(ll, 0, 255, 0, palette.primary.length, true))];
};

const coordToIndex = (coord) => {
  return {
    x: floor(constrain(floor(coord.x / scaleX), 0, width/scaleX)),
    y: floor(constrain(floor(coord.y / scaleY), 0, height/scaleY)),
  };
};

function keyPressed(key) {
  if (key.keyCode === 80)
    saveCanvas(canvas, `vehicule_1_replay_2_${seed}`, "png");
}
