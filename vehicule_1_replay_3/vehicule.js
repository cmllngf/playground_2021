class Vehicule {
  constructor(_x, _y, _color, _flowfieldIndex, _obeyShader, {
    _maxSpeed = abs(randomGaussian(5, .1)),
    minMaxLifetime = 140,
    maxMaxLifetime = 180,
  }) {

    // inital values
    this.initPos = createVector(_x, _y);
    this.radar = random(50, 20);

    // values
    this.pos = createVector(_x, _y);
    this.vel = createVector(0, 0);
    this.acc = createVector(0, 0);
    // this.maxSpeed = random(2, 7);
    this.maxSpeed = _maxSpeed;
    this.maxForce = random(4, 10);
    this.currentRadar = this.radar;
    this.maxLifetime = random(minMaxLifetime, maxMaxLifetime);
    this.lifetime = 0;
    this.color = _color;
    this.show = random() > showRatio;
    this.flowfieldIndex = _flowfieldIndex;
    this.obeyShader = _obeyShader;

    // color
    this.color = color(hue(this.color), saturation(this.color), brightness(this.color) + random(-10, 10))
  }

  reset() {
    this.pos = this.initPos.copy();
    this.currentRadar = this.radar;
    this.lifetime = 0;
  }

  display(fColor) {
    // noStroke();
    // fill(0);
    // circle(this.pos.x, this.pos.y, 10);
    // noFill();
    // stroke(0);
    // circle(this.pos.x, this.pos.y, this.radar * 2);
    const col = this.obeyShader ? getColorPaletteFromImage(this.pos.x, this.pos.y) : this.color
    fill(fColor ? fColor(this.pos.x, this.pos.y) : col);
    noStroke();
    circle(this.pos.x, this.pos.y, this.radar * 2);
  }

  isAlive() {
    return this.lifetime < this.maxLifetime;
  }

  flee(target) {
    return this.seek(target).mult(-1);
  }

  follow(flowfield) {
    const { x, y } = coordToIndex(this.pos);
    if(flowfield[y] && flowfield[y][x]) {
      this.applyForce(flowfield[y][x].force);
      if (debugFlowfield) {
        // console.log(x,y)
      }
    }
  }

  followOneOf(flowfields) {
    const { x, y } = coordToIndex(this.pos);
    try {
      // console.log({x,y, pos: this.pos, ff: flowfields[this.flowfieldIndex]})
      this.applyForce(flowfields[this.flowfieldIndex].forces[y][x].force);
      if (debugFlowfield) {
        // console.log(x,y)
      }
    } catch(e) {
      console.log({e, flowfields,ffindex: this.flowfieldIndex, x, y})
      stop()
    }
  }

  seek(target) {
    const force = p5.Vector.sub(target, this.pos);
    force.setMag(this.maxSpeed);
    force.sub(this.vel);
    force.limit(this.maxForce);
    return force;
  }

  update() {
    this.vel.add(this.acc);
    this.vel.limit(this.maxSpeed);
    this.pos.add(this.vel);
    this.acc.mult(0);
    this.edges();
    this.lifetime++;
    this.currentRadar = map(this.lifetime, 0, this.maxLifetime, this.radar, 3);
  }

  applyForce(force) {
    this.acc.add(force);
  }

  overlaps(vehicule) {
    return (
      p5.Vector.dist(this.pos, vehicule.pos) <
      this.currentRadar + vehicule.currentRadar
    );
  }

  overlapDisplay(vehicule, fColor) {
    // const size = map(p5.Vector.dist(this.pos, vehicule.pos), 0, this.currentRadar + vehicule.currentRadar, this.radar, 1)
    const size = map(
      p5.Vector.dist(this.pos, vehicule.pos),
      0,
      this.currentRadar + vehicule.currentRadar,
      this.currentRadar,
      3
    );
    // fill(10, .1)
    // fill(color(0, 80, 90))
    // this.color.setAlpha(.5)
    const col = this.obeyShader ? getColorPaletteFromImage(this.pos.x, this.pos.y) : this.color
    fill(fColor ? fColor(this.pos.x, this.pos.y) : col || this.color);
    noStroke();
    const mid = p5.Vector.lerp(this.pos, vehicule.pos, 0.5);
    if(size >= 5)
      circle(mid.x, mid.y, size*1.2);
      // circle(mid.x, mid.y, 3);
  }

  edges() {
    if (this.pos.x < border) {
      this.pos.x = width - border;
    }
    if (this.pos.x > width - border) {
      this.pos.x = border;
    }
    if (this.pos.y < border) {
      this.pos.y = height - border;
    }
    if (this.pos.y > height - border) {
      this.pos.y = border;
    }
  }
}
