let visited = [];
let colors = [];
let canvas;
let seed;
let simplexNoise;
const systems = [];
let anchors = [];
let system;
let currentSystemIndex = 0;
let img;
const flowfield = [];
let particles = [];

function preload() {
  img = loadImage("../assets/athena/portrait_1.jpg");
}

const cols = 20;
const rows = 20;
let scaleW;
let scaleH;
const border = 0;
const outbound = 100;
function setup() {
  // canvas = createCanvas(img.width*2, img.height*2);
  canvas = createCanvas(1080, 1080);
  img.loadPixels();
  // canvas = createCanvas(1080, 1080);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  colorMode(HSB);
  background("#F4F4F9");
  for (let y = 0; y < rows; y++) {
    flowfield.push([]);
    for (let x = 0; x < cols; x++) {
      const a = simplexNoise.noise2D(x * 0.01, y * 0.01)
      // const a = (PI/2) + simplexNoise.noise2D(x * 0.01, y * 0.01) * (PI/2)
      const force = p5.Vector.fromAngle(a);
      force.normalize();
      flowfield[y].push({
        force,
      });
    }
  }
  scaleW = (width+outbound*2) / cols;
  scaleH = (height+outbound*2) / rows;
  for (let i = 0; i < 300; i++) {
    particles.push(new Particle(random(width), random(height)))
  }
  // for (let y = 0; y < flowfield.length; y++) {
  //   for (let x = 0; x < flowfield[y].length; x++) {
  //     const { force } = flowfield[y][x];
  //     push();
  //     strokeWeight(1);
  //     translate(x * scaleW, y * scaleH);
  //     rotate(force.heading());
  //     line(0, scaleH/2, scaleW, scaleH/2);
  //     strokeWeight(10);
  //     point(scaleW, scaleH/2)
  //     pop();
  //   }
  // }
  systems.push(new System(width/2, height/2, width-40, height-40, 0))
}

let y = 0;
function draw() {
  for (let j = 0; j < 3; j++) {
    for (let i = 0; i < 1; i++) {
      if (!!systems[0] && !systems[0].update()) {
        systems.shift();
      }
    }
    // const x = width/2
    // for (let i = 0; i < 10; i++) {
    //   point(x + simplexNoise.noise2D(x, y*.001) * 140, y)
    //   y++;
    // }
    // particles.forEach(particle => {
    //   if(particle.pos.y < height+100 && frameCount%5 == 0)
    //   // systems.push(new System(particle.pos.x, particle.pos.y, 100, 20, particle.vel.heading()))
    //   // systems.push(new System(particle.pos.x, particle.pos.y, abs(randomGaussian(100, 50)), abs(randomGaussian(30, 10)), particle.vel.heading()))
    //   for (let i = 0; i < 10; i++) {
    //     particle.follow(flowfield);
    //     particle.update();
    //     // particle.display();
    //   }
    // });
  }
}
const coordToIndex = (coord) => {
  return {
    x: int((coord.x + outbound) / scaleW),
    y: int((coord.y + outbound) / scaleH),
  };
};

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `rectangle_color_${seed}`, "png");
}
const k = 300;
const pixel_size = 3;

// const palette = ["#7ab6ff","#7ab6ff","#7ab6ff","#7ab6ff","#ffce7a","#ff8c7a","#7ab6ff","#7ab6ff"]
// const palette = ['#DE8C9E', '#697A77', '#5F7A75', '#CFD4D3', '#364F4A']
// const palette = ['#FEC89A', '#FEC89A', '#FEC89A', '#FEC5BB', '#FCD5CE']
// const palette = ['#FFC857', '#119DA4', '#19647E', '#4B3F72', '#1F2041']
// const palette = ['#DDFFF7', '#93E1D8', '#FFA69E', '#AA4465', '#861657']
// const palette = ["#fec5bb","#fcd5ce","#fae1dd","#f8edeb","#e8e8e4","#d8e2dc","#ece4db","#ffe5d9","#ffd7ba","#fec89a"]
// const palette = ["#5f0f40","#9a031e","#fb8b24","#e36414","#0f4c5c"]
// const palette = ["#590d22","#800f2f","#a4133c","#c9184a","#ff4d6d","#ff758f","#ff8fa3","#ffb3c1","#ffccd5","#fff0f3"]
// const palette = ["#0e47e3","#24408c","#6273a1","#6c6d70","#ced1d9"]
// const palette = [
//   "#03045e",
//   "#023e8a",
//   "#0077b6",
//   "#0096c7",
//   "#00b4d8",
//   "#48cae4",
//   "#90e0ef",
//   "#ade8f4",
//   "#caf0f8",
// ];
// const palette = ["#E3E8EE","#E29743","#B579A1","#AF8D63","#DE829B","#594330"]
const palette = [0, 50, 100, 150, 200, 250]
const get1DIndex = (x, y, minX = 0, maxX = width, minY = 0, maxY = height) => {
  const xx = int(map(x, minX, maxX, 0, img.width));
  const yy = int(map(y, minY, maxY, 0, img.height));
  return (yy * img.width + xx) * 4;
};

const getColorPaletteFromImage = (x, y) => {
  const index = get1DIndex(
    x,
    y,
    border,
    width - border,
    border,
    height - border
  );
  const R = img.pixels[index];
  const G = img.pixels[index + 1];
  const B = img.pixels[index + 2];
  const ll = 0.2126 * R + 0.7152 * G + 0.0722 * B;
  return palette[int(map(ll, 0, 255, 0, palette.length, true))];
};

const getColorImg = (x, y) => {
  let c;
  const index = get1DIndex(int(x), int(y));
  const R = img.pixels[index];
  const G = img.pixels[index + 1];
  const B = img.pixels[index + 2];
  const A = img.pixels[index + 3];
  c = color(`rgba(${R}, ${G}, ${B}, ${A})`);
  colorMode(HSB);
  return c;
};

const getColorFromCoord = (x, y) => {
  // vary mean along x axes
  let mappedIndex;
  // if (dist(x,y,width/2,height/2) < 400) {
  mappedIndex = map(y, height - border, border, 0, palette.length, true);
  // } else {
  //   mappedIndex = map(y, 0, height, 0, palette.length-1, true);
  // }
  // const mappedIndex = map(y, 0, height, 0, palette.length-1, true);
  const noiseMean = 2;
  // const noiseMean = noise(x) * 1
  const gaussianIndex = randomGaussian(mappedIndex, noiseMean);
  const index = int(constrain(gaussianIndex, 0, palette.length - 1));
  return palette[index];
};

const getColorRandom = () => {
  return palette[int(random(palette.length))];
};

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `rectangle_color_3_${seed}`, "png");
}
