let canvas;
let seed;
let simplexNoise;
let space;

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";
let t = 0, w = 300

function ease(p) {
  return 3*p*p - 2*p*p*p;
}

function ease2(p, g) {
  if (p < 0.5) 
    return 0.5 * pow(2*p, g);
  else
    return 1 - 0.5 * pow(2*(1 - p), g);
}

const array = [];
let cam

function setup() {
  canvas = createCanvas(...SETTINGS.DIM, WEBGL);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  space = SETTINGS.border;
  colorMode(HSB)
  // ortho(-width/4, width/4, -height/4, height/4, -8000, 10000);
  perspective(PI/7, 1, .5)
  cam = createCamera();
  lights()
  // cam.move(300,-500,750)
  // cam.tilt(.70)
  // cam.pan(-.7)

  holes = new Array(6).fill(null).map(() => [random(.15, .55), random(.45, .85)])
  // holes = new Array(40).fill(null).map(() => [random(), random()])
  lines = new Array(350).fill(null).map((_, i, l) => ({q: map(i, 0, l.length, 0, 1), p: [random(0, .01), random(.99, 1)].sort()}))
}

function draw() {
  randomSeed(seed)
  cam.move(300,-500,750)
  cam.tilt(.70)
  cam.pan(-.7)
  perspective(PI/7, 1, .5)

  // t+=.05
  // t = t % 1
  orbitControl()
  noLoop()
  background(lightColor);

  push()
  // translate(-width/2, -100, -100)
  rotateX(PI/2)
  // rotateZ(-PI/2)

  draw_surface(-1)

  lines.map(draw_line)
  pop()
  
  cam.pan(.7)
  cam.tilt(-.70)
  cam.move(-300,500,-750)
  perspective()
  // box(100)

  if (SETTINGS.drawBorder) {
    translate(width/2, height/2, 10)
    fill(darkColor);
    noStroke();
    strokeWeight(4)
    rect(0, 0, space, height);
    rect(0, 0, width, space);
    rect(0, height - space, width, space);
    rect(width - space, 0, space, height);
    noFill()
    stroke(lightColor)
    line(space, space, space, height - space + 2)
    line(space - 2, space, width - space + 2, space)
    line(width - space + 2, height - space, space, height - space)
    line(width - space, height - space, width - space, space)
  }
}

let holes = []
let lines = []

function softplus(q,p){
  const qq = q+p;
  if(qq<=0){
    return 0;
  }
  if(qq>=2*p){
    return qq-p;
  }
  return 1/(4*p)*qq*qq;
}

function surface(p, q, off = 0)
{
  const x = map(p,0,1,580,2130);
  const y = map(q,0,1,250,1900);

  const distFromHole = holes.reduce((acc, [x, y]) => {
    const d = map(dist(p, q, x, y), 0, .05, 0, 1, true)
    return acc > d ? d : acc
  },1)

  // const distFromHole = map(dist(p, q, .23, .5), 0, .05, 0, 1, true)

  const z = map(ease2(distFromHole, 1), 0, 1, -1000, 0, true) + off;
  // const z = map((distFromHole), 0, 1, -4000, 0, true) + off;
  // const z = distFromHole == 1 ? 0 : -40

  return createVector(x,y,z);
}

function draw_line(lineData)
{
  const {q, p} = lineData
  push();
  
  let m1 = 200;
  let m2 = 2;
  
  stroke(lightColor);
  fill(darkColor);
  strokeWeight(1)
  // fill(darkColor);
  // noFill()
  // strokeWeight(2.5);
  noStroke();
  
  for(let i=0;i<m1;i++){
    // beginShape();
    beginShape(TRIANGLE_STRIP);
    for(let j=0;j<=m2;j++){
      const p1 = map(i,0,m1,p[0],p[1]);
      const p2 = map(i+1,0,m1,p[0],p[1]);

      // const noiseValue = simplexNoise.noise3D(p1 * 10, p2 * 10, q* 100) * .01
      // const noiseValue = simplexNoise.noise3D(p1 * 10, p2 * 10, q* 100) * .005
      const noiseValue = abs(simplexNoise.noise3D(p1 * 10, p2 * 10, q* 100)) * .008
      
      const q1 = map(j,0,m2,q,q+noiseValue);
      const q2 = map(j+1,0,m2,q,q+noiseValue);
      
      const v1 = surface(p1,q1);
      const v2 = surface(p1,q2);
      const v3 = surface(p2,q1);
      const v4 = surface(p2,q2);
      // fill(random(random(SETTINGS.palettes)))
      
      vertex(v1.x,v1.y,v1.z);
      vertex(v2.x,v2.y,v2.z);
      vertex(v3.x,v3.y,v3.z);
      vertex(v4.x,v4.y,v4.z);
    }
    endShape();
  }
  pop();
}

function draw_surface(off = 0)
{
  push();
  
  let m1 = 180;
  let m2 = 180;
  
  stroke(darkColor);
  fill(lightColor);
  strokeWeight(1)
  // fill(darkColor);
  // noFill()
  // strokeWeight(2.5);
  noStroke();
  
  for(let i=0;i<m1;i++){
    // beginShape();
    beginShape(TRIANGLE_STRIP);
    for(let j=0;j<=m2;j++){
      const p1 = map(i,0,m1,0,1);
      const p2 = map(i+1,0,m1,0,1);
      
      const q1 = map(j,0,m2,0,1);
      const q2 = map(j+1,0,m2,0,1);
      
      const v1 = surface(p1,q1, off);
      const v2 = surface(p1,q2, off);
      const v3 = surface(p2,q1, off);
      const v4 = surface(p2,q2, off);
      // fill(random(random(SETTINGS.palettes)))
      
      vertex(v1.x,v1.y,v1.z);
      vertex(v2.x,v2.y,v2.z);
      vertex(v3.x,v3.y,v3.z);
      vertex(v4.x,v4.y,v4.z);
    }
    endShape();
  }
  pop();
}

/**
 * { value: '', weight: 4 }
 */
const myWeightedRandom = (weightedRandoms) => random(weightedRandoms.reduce((acc, cur) => [...acc, ...new Array(cur.weight).fill(cur.value)],[]))

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `plane_2_${seed}`, "png");
}
