let canvas;
let seed;
let simplexNoise;
const border = 160;

function setup() {
  canvas = createCanvas(1080, 1080);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
}

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";

function draw() {
  background(darkColor);
  noLoop();

  const COLS = 7
  const ROWS = 7
  
  const STEPS = 100
  
  const size = (width - border * 2) / ROWS
  const margin = size * .15
  const d = size - margin * 2

  noFill()
  stroke(lightColor)
  strokeWeight(3)

  let m = 0

  for (let j = 0; j < ROWS; j++) {
    for (let i = 0; i < COLS; i++) {
      const step = TWO_PI / STEPS;

      const p = m / (COLS * ROWS)

      const noisiness = map(easeInCirc(p), 0, 1, 5, -1)
      const noiseMult = map(easeInCirc(p), 0, 1, 25, -1)

      const x = border + i * size + d/2 + margin;
      const y = border + j * size + d/2 + margin;
      
      for (let k = 0; k < 3; k++) {
        beginShape()
        for (let a = 0; a < TWO_PI; a+= step) {
          const noiseValueX = simplexNoise.noise3D((i + j) * noisiness, sin(a) * noisiness, k * noisiness) * noiseMult
          const noiseValueY = simplexNoise.noise3D((j - j) * noisiness, cos(a) * noisiness, k * noisiness) * noiseMult
          const cx = cos(a) * d/2 + x + noiseValueX;
          const cy = sin(a) * d/2 + y + noiseValueY;
          vertex(cx, cy);
        }
        endShape(CLOSE)
      }
      m++;
    }
  }

  rectMode(CORNER);
  fill(lightColor);
  noStroke();
  rect(0, 0, width, border);
  rect(0, 0, border, height);
  rect(width - border, 0, border, height);
  rect(0, height - border, width, border);
}

function easeInCirc(x) {
  return 1 - sqrt(1 - pow(x, 2));
}


function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `machin_learning_${seed}`, "png");
}
