let canvas;
let seed;
const numFrame = 30;
let t = 0;
let simplexNoise;
const border = 20;
let borders;

let particles = [];
let palette = ["#E977BC", "#9571C9", "#3F0D48"];

let bg;

function setup() {
  canvas = createCanvas(600, 600);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  // capturer.start();
  borders = [
    [border, border, border, height - border],
    [border, border, width - border, border],
    [width - border, height - border, border, height - border],
    [width - border, height - border, width - border, border],
  ];

  for (let i = 0; i < 400; i++) {
    const r = random();
    particles.push(new Particle(r > 0.5, r > 0.1));
  }
  bg = color("#486B7F");
  background(bg);
}

function draw() {
  randomSeed(seed);
  bg.setAlpha(150);
  // background(bg);
  // background(bg);
  drawBg1();
  drawBorders();
  // background(10);

  // t = (frameCount / numFrame) % 1;
  t = frameCount / numFrame;
  // t = 1;
  // if (t >= TWO_PI * 2) capturer.start();

  // for (let i = 0; i < particles.length; i++) {
  //   const p = particles[i];
  //   p.display();
  // }
  let pcount = 500;
  let nn = 3;

  beginShape();
  for (let i = 0; i < pcount; i++) {
    let a = random(PI / 2);
    // let a = (PI / nn) * int(random(nn * 2));
    // let a = random(PI);
    let y = random(-1);
    let r = sqrt(1 - y * y);
    // let r = sqrt(y);
    // let c = noise(y) * 360
    a += t;
    // let z = sin(a) + t;
    let z = sin(a * 3);
    // stroke(palette[int(random(palette.length))]);
    stroke(255);
    // if (z>0)
    strokeWeight(map(z, -1, 1, 2, 4));
    strokeWeight(3);
    point(
      // cos(a + t) * 100 * r + width / 2,
      // cos(a + t) * t * 100 * r + width / 2,
      cos(a) * 200 * r * z + width / 2,
      // cos(a + a) * 200 * r * z + width / 2,
      sin(a) * 1 * y * 200 + z * r * 80 + width / 2
      // sin(a) * 1 * y * 200 + z * r * 5 + width / 2
      // sin(a) * t * y * 100 + z * r * 5 + height / 2
      // sin(a + t) * y * 100 + z * r * 5 + height / 2
      // sin(a) * y * 100 + z * r * 5 + height / 2
    );
    // vertex(
    //   // cos(a + t) * 100 * r + width / 2,
    //   // cos(a + t) * t * 100 * r + width / 2,
    //   cos(a) * 200 * r + width / 2,
    //   sin(a) * 1 * y * 200 + z * r * 5 + width / 2
    //   // sin(a) * t * y * 100 + z * r * 5 + height / 2
    //   // sin(a + t) * y * 100 + z * r * 5 + height / 2
    //   // sin(a) * y * 100 + z * r * 5 + height / 2
    // );
  }
  endShape();

  // for (let ii = 0; ii < 3; ii++) {
  //   for (let jj = 0; jj < 3; jj++) {
  //     const xoff = ii * 220 + 80;
  //     const yoff = jj * 220 + 80;
  //     // beginShape();
  //     for (let i = 0; i < pcount; i++) {
  //       // let a = random(TWO_PI);
  //       let a = (PI / nn) * int(random(nn * 2));
  //       // let a = random(PI);
  //       let y = random(-1, 1);
  //       let r = sqrt(1 - y * y);
  //       // let r = sqrt(y);
  //       // let c = noise(y) * 360
  //       a += t / 2;
  //       // let z = sin(a) + t;
  //       let z = sin(a);
  //       stroke(255);
  //       // if (z>0)
  //       strokeWeight(map(z, -1, 1, 1, 3));
  //       // vertex(
  //       //   // cos(a + t) * 100 * r + width / 2,
  //       //   // cos(a + t) * t * 100 * r + width / 2,
  //       //   cos(a) * 100 * r + xoff,
  //       //   sin(a) * 1 * y * 100 + z * r * 5 + yoff
  //       //   // sin(a) * t * y * 100 + z * r * 5 + height / 2
  //       //   // sin(a + t) * y * 100 + z * r * 5 + height / 2
  //       //   // sin(a) * y * 100 + z * r * 5 + height / 2
  //       // );
  //       point(
  //         // cos(a + t) * 100 * r + width / 2,
  //         // cos(a + t) * t * 100 * r + width / 2,
  //         cos(a) * 100 * r + xoff,
  //         sin(a) * 1 * y * 100 + z * r * 5 + yoff
  //         // sin(a) * t * y * 100 + z * r * 5 + height / 2
  //         // sin(a + t) * y * 100 + z * r * 5 + height / 2
  //         // sin(a) * y * 100 + z * r * 5 + height / 2
  //       );
  //     }
  //     // endShape();
  //   }
  // }
  // nn = 5000;
  // pcount = 100;
  // for (let ii = 0; ii < 4; ii++) {
  //   for (let jj = 0; jj < 4; jj++) {
  //     const xoff = ii * 220 - 30;
  //     const yoff = jj * 220 - 30;
  //     for (let i = 0; i < pcount; i++) {
  //       // let a = random(TWO_PI);
  //       let a = (PI / nn) * int(random(nn * 2));
  //       // let a = random(PI);
  //       let y = random(-1, 1);
  //       let r = sqrt(1 - y * y);
  //       // let r = sqrt(y);
  //       // let c = noise(y) * 360
  //       a += t / 2;
  //       // let z = sin(a) + t;
  //       let z = sin(a);
  //       stroke(255);
  //       // if (z>0)
  //       strokeWeight(map(z, -1, 1, 1, 3));
  //       point(
  //         // cos(a + t) * 100 * r + 150,
  //         // cos(a + t) * t * 100 * r + 150,
  //         cos(a) * 100 * r + xoff,
  //         sin(a) * 1 * y * 100 + z * r * 5 + yoff
  //         // sin(a) * t * y * 100 + z * r * 5 + height / 2
  //         // sin(a + t) * y * 100 + z * r * 5 + height / 2
  //         // sin(a) * y * 100 + z * r * 5 + height / 2
  //       );
  //     }
  //   }
  // }

  fill(255);
  // text(frameRate(), 100, 100);
  // capturer.capture(canvas.canvas);
  // if (t >= TWO_PI) {
  //   // if (frameCount == numFrame * 2) {
  //   console.log(frameCount);
  //   capturer.stop();
  //   capturer.save();
  //   noLoop();
  // }
}

function drawBg1() {
  const n = 100;
  for (let i = 0; i < n; i++) {
    const p = i / n;
    const c1 = color("#4E6262");
    const c2 = color("#7D9087");
    const c = lerpColor(c1, c2, p);
    // if (frameCount > 0) c.setAlpha(150);
    noStroke();
    fill(c);
    rect(i * (width / n), 0, width / n, height);
  }
}

function drawBorders() {
  noStroke();
  fill(255);
  rect(0, 0, border, height);
  rect(0, 0, width, border);
  rect(width - border, 0, border, height);
  rect(0, height - border, width, border);
}

function ease(p) {
  return 3 * p * p - 2 * p * p * p;
}

function ease2(p, g) {
  if (p < 0.5) return 0.5 * pow(2 * p, g);
  else return 1 - 0.5 * pow(2 * (1 - p), g);
}

function softplus(q, p) {
  const qq = q + p;
  if (qq <= 0) {
    return 0;
  }
  if (qq >= 2 * p) {
    return qq - p;
  }
  return (1 / (4 * p)) * qq * qq;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `rain_1_${seed}`, "png");
}
