class Particle {
  constructor(border = true, rain = false) {
    this.offset = random(1);
    this.border = border;
    this.rain = rain;
    this.borderCoord = borders[int(random(borders.length))];
    this.K = border ? random(2, 6) : random(10, 30);
    this.minWidth = random(-width * 1.5, 0);
    this.maxWidth = random(width / 3, width * 1.5, 3);

    this.amplitude = random(7, 12);
    // this.minheight =
  }

  display() {
    // noStroke();
    // fill(255);
    // circle(this.x, this.y, 10);

    for (let i = 0; i < this.K; i++) {
      let p = map(i + t, 0, this.K, 0, 1);
      // p = (p + this.offset) % 1;
      this.update(p);
    }
  }

  update(p) {
    let pos;
    if (this.border) {
      pos = p5.Vector.lerp(
        createVector(this.borderCoord[0], this.borderCoord[1]),
        createVector(this.borderCoord[2], this.borderCoord[3]),
        (p + this.offset) % 1
      );
    } else if (this.rain) {
      pos = createVector(
        map(p, 0, 1, 0, width),
        sin(p * this.amplitude) * 10 + 500
        // map(log(p * 0.7) * -1, 1, 0, 0, height * 1.4, true)
      );
    } else {
      pos = createVector(
        map(p, 0, 1, this.minWidth, this.maxWidth, true),
        map(log(p * 0.7) * -1, 1, 0, 0, height * 1.4, true)
        // map(1 - cos(p * p * 2), 1, 0, 0, height)
      );
    }
    stroke(255);
    if (
      pos.x >= border &&
      pos.x <= width - border &&
      pos.y >= border &&
      pos.y <= height - border
    )
      point(pos.x, pos.y);
  }
}
