const nodes = 5;
let networks = [];
let voronois = [];
let seed

const avoidBorder = 20;

let poisson_radius = 30;
let poisson_k = 1;
const scaleX = 150;
const scaleY = 150;
const noiseField = [];

let t = 0,
simplexNoise,
  polygons = [];

function setup() {
  seed = random(9999)
  randomSeed(seed)  
  simplexNoise = openSimplexNoise(seed);
  createCanvas(700, 700);
  background(10);

  let noiseField = generateNoiseField(5, 60);
  networks.push(varyingRadiousPoissonDisk(4, noiseField));
  noiseField = generateNoiseField(10, 15);
  networks.push(varyingRadiousPoissonDisk(2, noiseField));
  // noiseField = generateNoiseField(20, 10);
  // networks.push(varyingRadiousPoissonDisk(2, noiseField));
  // poisson_radius -= 5
  // networks.push(varyingRadiousPoissonDisk(poisson_k, noiseField));
  // poisson_radius -= 10
  // poisson_k += 1
  // networks.push(poissonDiskSampling(poisson_radius, poisson_k));
  // poisson_radius -= 5

  for (let i = 0; i < networks.length; i++) {
    voronois.push(
      d3.voronoi().extent([
        [avoidBorder, avoidBorder],
        [width - avoidBorder, height - avoidBorder],
      ])
    );
  }

  networks.forEach((n, i) => {
    polygons.push(voronois[i](n.map((p) => [p.x, p.y])).polygons());
  });
}

const generateNoiseField = (from = poisson_radius, to = poisson_radius) => {
  const scaleX = 150;
  const scaleY = 150;
  const rows = height / scaleY;
  const cols = width / scaleX;
  const noiseField = []
  for (let x = 0; x < cols; x++) {
    noiseField.push([]);
    for (let y = 0; y < rows; y++) {
      const noiseValue = simplexNoise.noise2D(x * 0.5, y * 0.5);
      noiseField[x].push({
        // radius:
        //   noiseValue > 0
        //     ? map(noiseValue, 0, 1, poisson_radius * 6, poisson_radius * 8)
        //     : map(noiseValue, -1, 0, poisson_radius, poisson_radius * 1.5),
        radius:  map(noiseValue, -1, 1, from, to)
      });
    }
  }
  return noiseField;
}

function draw() {
  colorMode(HSB)
  background("#080F0F");
  // noFill()
  // stroke('red')
  // circle(width/2, height/2, 200)
  // circle(width/2, height/2, 205)
  // circle(width/2, height/2, 210)
  // fill('red')
  // noStroke()
  // circle(width/2, height/2, 50)
  // for (let i = 0; i < network.length; i++) {
  //   noStroke()
  //   fill(155)
  //   circle(network[i].x - 5, network[i].y - 5, 40)
  //   const n = neighbors(network[i], 200)
  //   shuffle(n);
  //   for (let j = 0; j < min(n.length, 1); j++) {
  //     stroke(155)
  //     strokeWeight(10)
  //     line(network[i].x - 5, network[i].y - 5, n[j].x - 5, n[j].y - 5)
  //   }
  // }
  fill(255);
  noStroke();
  // for (let i = 0; i < networks[0].length; i++) {
  //   circle(networks[0][i].x, networks[0][i].y, 5)
  //   const n = neighbors(networks[0][i], 200, networks[0])
  //   shuffle(n);
  //   for (let j = 0; j < min(n.length, 1); j++) {
  //     stroke(255)
  //     strokeWeight(1)
  //     line(networks[0][i].x, networks[0][i].y, n[j].x, n[j].y)
  //   }
  // }
  noFill();
  strokeWeight(1);
  stroke(155);
  for (let j = 0; j < polygons.length; j++) {
    for (let i = 0; i < polygons[j].length; i++) {
      vertices = polygons[j][i].map((v) => createVector(v[0], v[1]));
      beginShape();
      vertices.map((v) => vertex(v.x, v.y));
      endShape(CLOSE);
    }
    // stroke(random(360), 10, 10);
    // stroke("#080F0F")
    strokeWeight(1);
    stroke(random(360), 60, 60);
    for (let i = 0; i < polygons[j].length; i++) {
      // vertices = polygons[j][i].map((v) => createVector(v[0] + simplexNoise.noise2D(v[0] * .1, v[1] * .1) * 10, v[1] + simplexNoise.noise2D(v[0] * .1, v[1] * .1) * 10));
      vertices = polygons[j][i].map((v) => createVector(v[0] + randomGaussian(0,2), v[1] + randomGaussian(0,2)));
      beginShape();
      vertices.map((v) => vertex(v.x, v.y));
      endShape(CLOSE);
    }
  }
  // filter(THRESHOLD);
  noLoop();
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `network_${seed}`, "png");
}
const getEntryFromPos = (point, noiseField) => {
  const x = int(map(point.x, 0, width, 0, noiseField.length));
  const y = int(map(point.y, 0, height, 0, noiseField[0].length));
  return noiseField[x][y];
};

function neighbors(pos, distMax, network) {
  const n = [];
  for (let i = 0; i < network.length; i++) {
    if (network[i].x == pos.x && network[i].y == pos.y) continue;

    if (dist(network[i].x, network[i].y, pos.x, pos.y) <= distMax)
      n.push(network[i]);
  }
  return n;
}

const varyingRadiousPoissonDisk = (k, noiseField) => {
  points = [];
  actives = [];
  // p0 = createVector(random(width), random(height));
  p0 = createVector(width/2, height/2);
  grid = [];

  varyingInsertPoint(grid, p0, noiseField);
  actives.push(p0);
  points.push(p0);

  while (actives.length > 0) {
    // for (let index = 0; index < 100; index++) {
    random_index = int(random(actives.length));
    p = actives[random_index];
    radius = getEntryFromPos(p, noiseField).radius;

    found = false;
    for (tries = 0; tries < k; tries++) {
      theta = int(random(TWO_PI));
      new_radius = int(random(radius, 2 * radius));
      pnewx = int(p.x + new_radius * cos(theta));
      pnewy = int(p.y + new_radius * sin(theta));
      pnew = createVector(pnewx, pnewy);

      if (!varyingIsValidPoint(grid, pnew)) continue;

      varyingInsertPoint(grid, pnew, noiseField);
      points.push(pnew);
      actives.push(pnew);
      found = true;
      break;
    }

    if (!found) actives.splice(random_index, 1);
  }

  return points;
};

const varyingIsValidPoint = (grid, point) => {
  for (let i = 0; i < grid.length; i++) {
    if (
      point.x < avoidBorder ||
      point.x >= width - avoidBorder ||
      point.y < avoidBorder ||
      point.y >= height - avoidBorder
    )
      return false;

    if (
      dist(grid[i].point.x, grid[i].point.y, point.x, point.y) < grid[i].radius
    )
      return false;
  }
  return true;
};

const varyingInsertPoint = (grid, point, noiseField) => {
  grid.push({
    ...getEntryFromPos(point, noiseField),
    point,
  });
  // console.log(grid)
};

function isValidPoint(grid, cellsize, gwidth, gheight, p, radius) {
  /* Make sure the point is on the screen */
  if (
    p.x < avoidBorder ||
    p.x >= width - avoidBorder ||
    p.y < avoidBorder ||
    p.y >= height - avoidBorder
  )
    return false;

  /* Check neighboring eight cells */
  xindex = floor(p.x / cellsize);
  yindex = floor(p.y / cellsize);
  i0 = max(xindex - 1, 0);
  i1 = min(xindex + 1, gwidth - 1);
  j0 = max(yindex - 1, 0);
  j1 = min(yindex + 1, gheight - 1);

  for (i = i0; i <= i1; i++)
    for (j = j0; j <= j1; j++)
      if (grid[i][j] != null)
        if (dist(grid[i][j].x, grid[i][j].y, p.x, p.y) < radius) return false;

  /* If we get here, return true */
  return true;
}

function insertPoint(grid, cellsize, point) {
  xindex = floor(point.x / cellsize);
  yindex = floor(point.y / cellsize);
  grid[xindex][yindex] = point;
}

function poissonDiskSampling(radius, k) {
  N = 2;
  /* The final set of points to return */
  points = [];
  /* The currently "active" set of points */
  active = [];
  /* Initial point p0 */
  p0 = createVector(
    int(random(avoidBorder, width - avoidBorder)),
    int(random(avoidBorder, height - avoidBorder))
  );
  grid = [];
  cellsize = floor(radius / sqrt(N));

  /* Figure out no. of cells in the grid for our canvas */
  ncells_width = ceil(width - (avoidBorder * 2) / cellsize) + 1;
  ncells_height = ceil(height - (avoidBorder * 2) / cellsize) + 1;

  /* Allocate the grid an initialize all elements to null */
  for (i = 0; i < ncells_width; i++) {
    grid[i] = [];
    for (j = 0; j < ncells_height; j++) grid[i][j] = null;
  }

  insertPoint(grid, cellsize, p0);
  points.push(p0);
  active.push(p0);

  while (active.length > 0) {
    random_index = int(random(active.length));
    p = active[random_index];

    found = false;
    for (tries = 0; tries < k; tries++) {
      theta = int(random(360));
      new_radius = int(random(radius, 2 * radius));
      pnewx = int(p.x + new_radius * cos(radians(theta)));
      pnewy = int(p.y + new_radius * sin(radians(theta)));
      pnew = createVector(pnewx, pnewy);

      if (
        !isValidPoint(grid, cellsize, ncells_width, ncells_height, pnew, radius)
      )
        continue;

      points.push(pnew);
      insertPoint(grid, cellsize, pnew);
      active.push(pnew);
      found = true;
      break;
    }

    /* If no point was found after k tries, remove p */
    if (!found) active.splice(random_index, 1);
  }

  return points;
}
