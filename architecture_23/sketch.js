let canvas;
let seed;
let simplexNoise;
let space;

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";

let L
let M
let palette

let heightValuesWeighted

function drawFaceBox(boxWidth, boxHeight, boxDepth,
  front, bottom, right, top, left, back) {
  angleMode(DEGREES); 
  let w = boxWidth;
  let h = boxHeight;
  let d = boxDepth;

  // Center the box.
  translate(-w / 2, -h / 2, d/2);

  fill(front);
  stroke(darkColor)
  strokeWeight(2)
  quad(0, 0, w, 0, w, h, 0, h, 1000, 1000);

  push();
  fill(left);
  translate(0, 0, -d);
  rotateY(-90);
  quad(0, 0, d, 0, d, h, 0, h, 1000, 1000);

  pop();
  push();
  fill(bottom);
  translate(0, 0, -d);
  rotateX(90);
  quad(0, 0, w, 0, w, d, 0, d, 1000, 1000);

  pop();
  push();
  fill(right);
  translate(w, 0, 0);
  rotateY(90);
  quad(0, 0, d, 0, d, h, 0, h, 1000, 1000);

  pop();
  push();
  fill(top);
  translate(0, h, 0);
  rotateX(-90);
  quad(0, 0, w, 0, w, d, 0, d, 1000, 1000);

  pop();
  push();
  fill(back);
  rotateY(180);
  translate(-w, 0, d);
  quad(0, 0, w, 0, w, h, 0, h, 1000, 1000);
  pop()
  angleMode(RADIANS);
}

class Quad {
  constructor(x, y, w, h) {
    this.x = x
    this.y = y
    this.w = w
    this.h = h
    this.subParts = [{ x, y, w, h, l: L, c: random(palette) }]
  }

  subdivide(deep) {
    if (deep == 0) return;
    const newSubParts = []
    for (let i = 0; i < this.subParts.length; i++) {
      const { x, y, w, h, l, c } = this.subParts[i];
      if(random() < SETTINGS.subdivitionChances) {
        if (w < h) { // cut horizontally
          newSubParts.push({ x, y: y - h/4, w, h: h/2, l: constrain(myWeightedRandom(heightValuesWeighted).height + randomGaussian(0, SETTINGS.gaussianHeightSpread), 2, 1000000), c: random(palette) })
          newSubParts.push({ x, y: y + h/4, w, h: h/2, l: constrain(myWeightedRandom(heightValuesWeighted).height + randomGaussian(0, SETTINGS.gaussianHeightSpread), 2, 1000000), c: random(palette) })
        } else { // cut vertically
          newSubParts.push({ x: x - w/4, y, w: w/2, h, l: constrain(myWeightedRandom(heightValuesWeighted).height + randomGaussian(0, SETTINGS.gaussianHeightSpread), 2, 1000000), c: random(palette) })
          newSubParts.push({ x: x + w/4, y, w: w/2, h, l: constrain(myWeightedRandom(heightValuesWeighted).height + randomGaussian(0, SETTINGS.gaussianHeightSpread), 2, 1000000), c: random(palette) })
        }
      } else {
        newSubParts.push({ x, y, w, h, l, c });
      }
    }
    this.subParts = newSubParts;
    this.subdivide(deep - 1);
  }

  show(sx = 0, sz = 0) {
    stroke(darkColor)
    strokeWeight(5)
    // noStroke()
    push()
    translate(sx, 0, sz)
    for (let i = 0; i < this.subParts.length; i++) {
      push()
      const { x, y, w, h, l, c } = this.subParts[i];
      fill(c)
      translate(x, -l/2, y)
      // if(w === h && l >= L/2) {
      //   sphere(w,h,l)
      // } else {
      // drawFaceBox(w, l, h, color(hue(c), saturation(c), brightness(c) * .7), color(hue(c), saturation(c), brightness(c) * 1.3), c, c, c, c)
      // drawFaceBox(w, l, h, lightColor, lightColor, lightColor, lightColor, lightColor, lightColor, lightColor, lightColor)
      // }
      box(w,l,h)
      pop()
    }
    pop()
  }
}

/**
 * TODO
 * - more values in weighted random
 * - weighted random for colors
 * - different palette according to height
 * - subdivide randomly along the length
 * - more shapes
 */

function setup() {
  canvas = createCanvas(...SETTINGS.DIM, WEBGL);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  palette = random(SETTINGS.palettes);
  space = SETTINGS.border;
  L = SETTINGS.L
  M = SETTINGS.M
  colorMode(HSB)
  // perspective(undefined, undefined, undefined, 1000000, 1000000, 1000000);
  // ortho(-width / 2, width / 2, height / 2, -height / 2, -1000, 5000);
  // ortho(-width/5, width/5, -height/5, height/5, -8000, 10000);
  ortho(-width/2, width/2, -height/2, height/2, -8000, 10000);
  // perspective(undefined, undefined, undefined, undefined, undefined, 10000);

  heightValuesWeighted = [
    { value: { height: L/2 }, weight: 30 },
    { value: { height: L }, weight: 30 },
    { value: { height: 300 }, weight: 1 },
    { value: { height: 10 }, weight: 500 },
  ]
}

function draw() {
  let dirX = (mouseX / width - 0.5) * 2;
  let dirY = (mouseY / height - 0.5) * 2;
  // directionalLight(color(255), -dirX, -dirY, -1);
  randomSeed(seed)
  orbitControl()
  // noLoop()
  background(lightColor);

  // const cols = 4
  // const rows = 4
  const cols = 40
  const rows = 40

  push()
  rotateX(-PI / 6);
  rotateY(-PI / 4);
  // translate(-L * cols/2 + L/2,L * rows/2 - L*1.5)
  // translate(-600,1500)
  translate(-width/2 - cols/2 * L,100,-width/2 - 700)
  let quad
  if (SETTINGS.repeatAll) {
    quad = new Quad(0,0,L, M);
    quad.subdivide(SETTINGS.sameSubdivisionEveryWhere ? SETTINGS.subdivitions : int(random(SETTINGS.subdivisionMin, SETTINGS.subdivisionMax)))
  }
  for (let x = 0; x < cols; x++) {
    if (!SETTINGS.repeatAll) {
      quad = new Quad(0,0,L, M);
      quad.subdivide(SETTINGS.sameSubdivisionEveryWhere ? SETTINGS.subdivitions : int(random(SETTINGS.subdivisionMin, SETTINGS.subdivisionMax)))
    }
    for (let z = 0; z < rows; z++) {
      quad.show(x * L, z * M);
    }
  }
  pop()

  if (SETTINGS.drawBorder) {
    translate(-width/2, -height/2, 8000)
    fill(lightColor);
    noStroke();
    strokeWeight(4)
    rect(0, 0, space, height);
    rect(0, 0, width, space);
    rect(0, height - space, width, space);
    rect(width - space, 0, space, height);
    noFill()
    stroke(lightColor)
    line(space, space, space, height - space + 2)
    line(space - 2, space, width - space + 2, space)
    line(width - space + 2, height - space, space, height - space)
    line(width - space, height - space, width - space, space)
  }
}

/**
 * { value: '', weight: 4 }
 */
const myWeightedRandom = (weightedRandoms) => random(weightedRandoms.reduce((acc, cur) => [...acc, ...new Array(cur.weight).fill(cur.value)],[]))

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `architecture_2_${seed}`, "png");
}
