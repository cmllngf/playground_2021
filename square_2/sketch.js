let canvas;
let seed;
let simplexNoise;
let t = 0;
let palette = [];
let img;

const circleCount = 35;
const circleGap = 10;
const noiseScale = 0.01;
const noiseScale2 = 0.012;
const tRadius = 0.5;
const numFrame = 120;
const maxDisplacement = 40;
const minDisplacement = 1;

const recording = false;

palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];

function setup() {
  canvas = createCanvas(1080, 1080);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  if (recording) capturer.start();
}

function draw() {
  translate(width / 2, height / 2);
  randomSeed(seed);
  background("#f5f5f5");
  fill("#f5f5f5");
  stroke(10);
  t = frameCount / numFrame;

  let w = 500;
  for (let i = 0; i < 80; i++) {
    drawRect(-w / 2, -w / 2, w, w, i === 0);
    w += 10;
  }
  noLoop();

  if (recording) {
    capturer.capture(canvas.canvas);
    if (t === 3) {
      capturer.stop();
      capturer.save();
      noLoop();
    }
  }
}

const drawRect = (x, y, w, h, filled) => {
  if (filled) {
    fill(10);
  } else {
    noFill();
  }
  beginShape();
  //top
  for (let dx = x; dx < w + x; dx++) {
    curveVertex(dx, y);
  }

  //right
  for (let dy = y; dy < h + y; dy++) {
    curveVertex(x + w, dy);
  }

  //bottom
  for (let dx = x + w; dx > x; dx--) {
    // const noiseY = simplexNoise.noise2D(dx * 0.05, y + h * 0.05) * 100;
    const noiseY = simplexNoise.noise2D(dx * 0.05, 0) * 100;
    curveVertex(dx, y + h + noiseY);
  }

  //left
  for (let dy = y + h; dy > y; dy--) {
    curveVertex(x, dy);
  }
  endShape(CLOSE);
};

const drawRect2 = (x, y, w, h, seed = 0) => {
  beginShape();
  //top
  for (let dx = x; dx < w + x; dx++) {
    let amp = getBrightnessAmp(dx, y);
    curveVertex(dx + amp, y - amp);
  }

  //right
  for (let dy = y; dy < h + y; dy++) {
    let amp = getBrightnessAmp(x + w, dy);
    curveVertex(x + w + amp, dy - amp);
  }

  //bottom
  for (let dx = x + w; dx > x; dx--) {
    // const noiseY = simplexNoise.noise2D(dx * 0.05, y + h * 0.05);
    let amp = getBrightnessAmp(dx, y + h);
    curveVertex(dx + amp, y + h - amp);
  }

  //left
  for (let dy = y + h; dy > y; dy--) {
    let amp = getBrightnessAmp(x, dy);
    curveVertex(x + amp, dy - amp);
  }
  endShape(CLOSE);
};

const getAmpForTime = () => {
  // return abs(20 * sin(t * TWO_PI));
  return abs(20 * sin(ease2(t, 1) * PI));
};

const get1DIndex = (x, y, minX = 0, maxX = width, minY = 0, maxY = height) => {
  const xx = int(map(x, minX, maxX, 110, img.width - 110));
  const yy = int(map(y, minY, maxY, 0, img.height - 0));
  return (yy * img.width + xx) * 4;
};

const getBrightnessAmp = (x, y) => {
  const index = get1DIndex(
    int(x),
    int(y),
    -width / 2 + 100,
    width / 2 - 100,
    -height / 2 + 100,
    height / 2 - 100
  );
  const R = img.pixels[index];
  const G = img.pixels[index + 1];
  const B = img.pixels[index + 2];
  const A = img.pixels[index + 3];
  if (A === 0) {
    return 0;
  }
  const ll = 0.2126 * R + 0.7152 * G + 0.0722 * B;
  // return map(ll, 0, 255, 100, 1);
  // return map(ll, 0, 255, getAmpForTime(), 0);
  return map(ll, 0, 255, 20, 0);
};

function ease(p) {
  return 3 * p * p - 2 * p * p * p;
}

function ease2(p, g) {
  if (p < 0.5) return 0.5 * pow(2 * p, g);
  else return 1 - 0.5 * pow(2 * (1 - p), g);
}

function softplus(q, p) {
  const qq = q + p;
  if (qq <= 0) {
    return 0;
  }
  if (qq >= 2 * p) {
    return qq - p;
  }
  return (1 / (4 * p)) * qq * qq;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `square_2_${seed}`, "png");
}
