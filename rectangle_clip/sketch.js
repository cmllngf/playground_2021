let canvas;
let seed;
let simplexNoise;
let ctx

const anchors = []; 

function setup() {
  canvas = createCanvas(800, 800);
  seed = random(99999);
  randomSeed(seed);
  simplexNoise = openSimplexNoise(seed);
  ctx = canvas.drawingContext

  for (let i = 0; i < palette.length; i++) {
    // const co = color(palette[i]);
    const co = palette[i];
    // co.setAlpha(100)
    console.log(co)
    anchors.push({
      x: random(width),
      y: random(height),
      color: co,
      id: i
    });
  }
  background(255)
}

const ROWS = 60;
const COLS = 40;
const SIZE = 20;
const MARGIN = 0;
let palette;

palette = ["#3352c6", "#bb4ab6", "#ff5692", "#ff856c", "#ffc058", "#f9f871"]
// palette = ["#151412", "#272E1E", "#354A29", "#53734A", "#84B188", "#D5D7D4"]
palette = [("#f8a1b1"), ("#ffd0c7"), ("#ffffff"), ("#d6d2e0"), ("#9a95a9"), ("#5d5871"), ("#131121"), ("#88f1ff"), ("#63c1ff"), ("#3d91ff")]
// palette = ["#021118", "#14383D", "#025F71", "#00ACB3", "#9CDACF", "#EBE3D8"];
// palette = [
//   "#03045e",
//   "#023e8a",
//   "#0077b6",
//   "#0096c7",
//   "#00b4d8",
//   "#48cae4",
//   "#90e0ef",
//   "#ade8f4",
//   "#caf0f8",
// ];
// palette = [
//   "#5f0f40",
//   "#9a031e",
//   "#fb8b24",
//   "#e36414",
//   "#0f4c5c",
//   "#fed8b1",
// ]

let currentDrawingStep = 0;

function draw() {
  if (currentDrawingStep === 0) {
    betterBackground()
  } else if (currentDrawingStep === 1) {
    for (let i = 0; i < COLS; i++) {
      for (let j = 0; j < ROWS; j++) {
        const x = i * (SIZE + MARGIN * 2)
        const y = j * (SIZE + MARGIN * 2)
        drawLinePatternWithClip(x, y, 1)
        drawLinePatternWithClip(x, y, 1.05)
        // drawLinePatternWithClip(x, y, 1.1)
      }
    }
    granulate(10)
    noLoop()
  }
}

const betterBackground = () => {
  // blendMode(MULTIPLY)
  for (let index = 0; index < 10; index++) {
    const x = random(-300, width + 300)
    const y = random(-300, height + 300)
    // const w = randomGaussian(300, 20)
    // const h = randomGaussian(100, 50)
    const w = abs(randomGaussian(100, 10))
    const h = abs(randomGaussian(100, 10))
    noStroke()
    // const col = color(getColorFromCoord(x, y))
    const col = color(getColorFromGaussianAnchor(x, y))
    col.setAlpha(30)
    fill(col)
    rectMode(RADIUS)
    // rect(x, y, w, h, 20)
    rectMode(CORNER)
    ellipse(x, y, w, h)
  }
}

const getColorFromCoord = (x, y) => {
  // vary mean along x axes
  let mappedIndex;
  // if (dist(x,y,width/2,height/2) < 400) {
    mappedIndex = map(y, height, 0, 0, palette.length, true);
  // } else {
  //   mappedIndex = map(y, 0, height, 0, palette.length-1, true);
  // }
  // const mappedIndex = map(y, 0, height, 0, palette.length-1, true);
  const noiseMean = 1.3
  // const noiseMean = noise(x) * 1
  const gaussianIndex = randomGaussian(mappedIndex, noiseMean)
  const index = int(constrain(gaussianIndex, 0, palette.length - 1))
  return palette[index];
}

const drawLinePattern = (x, y) => {
  push()
  translate(x, y)
  strokeWeight(.5)
  stroke(0)
  const angle = map(simplexNoise.noise2D(x*.001,y*.001), -1, 1,0, TWO_PI)
  rotate(angle)
  const step = (SIZE * 2) / 10
  for (let i = 0; i < 30; i++) {
    line(-SIZE*2 + step*i, -SIZE * 2, -SIZE*2 + step*i, SIZE*2)
  }
  pop()
}

const drawLinePattern2 = (x, y) => {
  push()
  translate(x, y)
  strokeWeight(.5)
  stroke(0)
  const angle = map(simplexNoise.noise2D(x*.001,y*.001), -1, 1,0, TWO_PI)
  rotate(angle)
  strokeWeight(3)
  const step = 5
  for (let i = 0; i < step; i++) {
    stroke(getGaussianColorFromCoordinates(x+step*i, y))
    stroke(getRandomColor())
    line(step*i, 0, step*i, SIZE)
  }
  pop()
}

const drawLinePatternWithClip = (x, y, seed = 0) => {
  canvas.drawingContext.save();
  push()
  translate(x, y)
  strokeWeight(0)
  stroke(0)
  noFill()
  rect(MARGIN,MARGIN,SIZE,SIZE)
  canvas.drawingContext.clip()
  strokeWeight(3)
  const angle = map(simplexNoise.noise3D(x*.001,y*.001, seed), -1, 1,0, TWO_PI * .8)
  rotate(angle)
  // const randomCount = random(40, 80)
  const randomCount = random(10, 5)
  for (let i = 0; i < randomCount; i++) {
    const xx = map(i, 0, randomCount, -SIZE*2, SIZE * 4)
    // const xx = map(i, 0, randomCount, -SIZE/2, SIZE)
    // stroke(getGaussianColorFromCoordinates(x+xx, y))
    // stroke(getRandomColor())
    // stroke(getColorFromAnchor(x+xx, y))
    const col = color(getColorFromGaussianAnchor(x+xx, y))
    col.setAlpha(190)
    stroke(col)
    line(xx, -SIZE*2, xx, SIZE*4)
  }
  pop()
  canvas.drawingContext.restore()
}

const getRandomColor = () => random(palette)

const getGaussianColorFromCoordinates = (x, y) => {
  const yToPaletteIndex = map(y, 0, height, 0, palette.length - 1, true)
  const gaussianPalntteIndex = constrain(randomGaussian(yToPaletteIndex, .5), 0, palette.length - 1)
  return palette[int(gaussianPalntteIndex)]
}

const getColorFromAnchor = (x, y) => {
  const results = anchors.reduce((acc, cur) => {
    return {
      sum: acc.sum + dist(cur.x, cur.y, x, y),
      dists: [...acc.dists, dist(cur.x, cur.y, x, y)],
    }
  }, {
    sum: 0,
    dists: [],
  })
  const colors = results.dists.flatMap((d, i) => new Array(ceil((d * 50)/results.sum)).fill(null).map(() => palette[i]))
  return random(colors);
}

const getColorFromGaussianAnchor = (x, y) => {
  const sortedAnchors = anchors.sort((a, b) => dist(a.x, a.y, x, y) - dist(b.x, b.y, x, y))
  const randomCloseAnchor = random(anchors.filter((a) => dist(a.x, a.y, x, y) < 350))
  const distFromFirst = randomCloseAnchor ? dist(randomCloseAnchor.x, randomCloseAnchor.y, x, y) : dist(sortedAnchors[0].x, sortedAnchors[0].y, x, y)
  // const distFromFirst = dist(sortedAnchors[0].x, sortedAnchors[0].y, x, y)
  const spread =  map(distFromFirst, 0, 150, 0, 1)
  const gaussianIndex = constrain(int(abs(randomGaussian(0, spread))), 0, sortedAnchors.length - 1)
  return sortedAnchors[gaussianIndex].color;
}

// const drawCircleClip = (x, y, r) => {
//   drawingContext.save();
//   noStroke()
//   fill(180, 170, 190)
//   circle(x,y,r+10)
//   // noFill()
//   stroke(180, 170, 190)
//   strokeWeight(0)
//   ctx.shadowColor = 'black';
//   ctx.shadowBlur = 20;
//   circle(x, y, r)
//   ctx.clip()
//   drawLinePattern()
//   ctx.shadowBlur = 0;
//   ctx.restore()
// }

function granulate(gA){
  loadPixels();
  let d = pixelDensity();
  let halfImage = 4 * (width * d) * (height * d);
  for (let ii = 0; ii < halfImage; ii += 4) {
    grainAmount = random(-gA,gA)
    pixels[ii] = pixels[ii]+grainAmount;
    pixels[ii + 1] = pixels[ii+1]+grainAmount;
    pixels[ii + 2] = pixels[ii+2]+grainAmount;
    pixels[ii + 3] = pixels[ii+3]+grainAmount;
  }
  updatePixels();
}

const nextDrawingStep = () => currentDrawingStep++;

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `rectangle_clip_1_${seed}`, "png");
  if (key.keyCode === 78) //n
    nextDrawingStep()
}
