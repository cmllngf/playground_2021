let canvas;
let seed;
let simplexNoise;
let t = 0;
let palette = [];
const numFrame = 30;

const recording = false;

palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
// palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
// palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
// palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
// palette = ["#020224", "#01104F", "#003C81", "#01669E", "#5075A2", "#6F91AD"];
// palette = ["#9C7B5A", "#CDAF7D", "#7CA98A", "#216C58", "#204534"];
// palette = ["#5E101E", "#732427", "#8BD6C2", "#175450", "#082B2F"];
// palette = ["#742228", "#721821", "#67181D", "#3B1816", "#0F0F0F"];
// palette = ["#760000", "#86502A", "#B87B5E", "#4A6077", "#1F4B3E"];
// palette = ["#00314F", "#6CA5AE", "#E5F1D9", "#B4A387", "#D38C86"];
// palette = ["#D5B5F3", "#75D8EC", "#F3A9C5", "#A9F3D3", "#F3C4A9"];
// palette = ["#082C59", "#0A72A1", "#4183B1", "#526C81", "#DDCEDA"];
palette = ["#012023", "#1B443C", "#93907D", "#855352", "#6C252D"];
// palette = ["#840000", "#C58402", "#8B929C", "#18434C", "#1F2113"];

// palette = ["#151412", "#272E1E", "#354A29", "#53734A", "#84B188", "#D5D7D4"];
// palette = ["#220507", "#450A04", "#6A2B02", "#8F5201", "#C98A43", "#E6D19F"];
// palette = ["#5C3C49", "#8D5964", "#AF747F", "#D1617E", "#E788A2", "#EEAEB8"];
// palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
// palette = ["#AF747F", "#D1617E", "#E788A2", "#EEAEB8", "#F9D0C3", "#F6EEE1"];
// palette = [0, 250, 100, 150, 200, 50];
// palette = [150, 250, 100, 50, 0, 200];
// palette = ["#24130C", "#70381F", "#C04700", "#F07800", "#EFB178", "#CCECEB"];
// palette = ["#6F2B3A", "#DB6578", "#E1B971", "#FBEDC7", "#949971", "#5B5D45"];
// palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
// palette = ["#092A2F", "#652B05", "#B65302", "#E7A700", "#FAE647", "#00B5AE"];
// palette = ["#162E21", "#063F39", "#30685B", "#73967E", "#C68C20", "#D8D091"];
// palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
// palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
// palette = ["#021118", "#14383D", "#025F71", "#00ACB3", "#9CDACF", "#EBE3D8"];
// palette = ["#845ec2", "#2c73d2", "#0081cf", "#0089ba", "#008e9b", "#008f7a"];
// palette = ["#845ec2", "#d65db1", "#ff6f91", "#ff9671", "#ffc75f", "#f9f871"];
// palette = ["#5ec2b5", "#69d1ae", "#81dea2", "#a3e991", "#ccf27f", "#f9f871"];
// palette = ["#c25eb5", "#f6669e", "#ff7f82", "#ffa56a", "#ffcf5f", "#f9f871"];
// palette = ["#3352c6", "#bb4ab6", "#ff5692", "#ff856c", "#ffc058", "#f9f871"];
// palette = [255];

const targets = [];
const particles = [];
let currentTarget;

const symmetry = 20;
const mirror = false;

const border = 20;

function setup() {
  canvas = createCanvas(1080, 1080);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);

  const n = 10;
  for (let i = 0; i < n; i++) {
    const minR = 800;
    const maxR = 800;
    const r = random(minR, maxR);
    const x = cos(i * (TWO_PI / n)) * r + width / 2;
    const y = sin(i * (TWO_PI / n)) * r + height / 2;
    noStroke();
    // const c = color(random(360), random(80, 100), 80);
    const c = color(palette[i % palette.length]);
    // const c = color(random(100, 250));
    targets.push({
      color: c,
      pos: createVector(x, y),
    });
  }

  currentTarget = targets[int(random(targets.length))];

  for (let i = 0; i < 100; i++) {
    const minMass = 2;
    const maxMass = 5;
    // const x = random(width / 2 - 100, width / 2 + 100);
    // const y = random(height / 2 - 100, height / 2 + 100);
    // const x = random(width);
    // const y = random(height);
    const x = width / 2;
    const y = height / 2;
    particles.push(
      // new Particle(random(minMass, maxMass), random(width), random(height))
      new Particle(
        random(minMass, maxMass),
        x,
        y,
        targets[int(random(targets.length))]
      )
    );
  }
  randomSeed(seed);
  background(10);

  if (recording) capturer.start();
}

function draw() {
  t = frameCount / numFrame;

  wagadoo();

  fill("rgb(248,248,255)");
  noStroke();
  rect(0, 0, width, border);
  rect(0, 0, border, height);
  rect(width - border, 0, border, height);
  rect(0, height - border, width, border);

  if (recording) {
    capturer.capture(canvas.canvas);
    if (t === 3) {
      capturer.stop();
      capturer.save();
      noLoop();
    }
  }
}

function wagadoo() {
  if (random() < 3 / 100) {
    currentTarget = targets[int(random(targets.length))];
    particles.forEach((particle) => {
      particle.currentTarget = targets[int(random(targets.length))];
    });
  }

  let c;
  // if(opts.ColorMode == colorModes.colorModeNoise)
  //   c = color(noise(t) * 360, 70, 70, opts.Alpha)

  for (let i = 0; i < particles.length; i++) {
    particles[i].target(currentTarget.pos);
    // particles[i].target();
    particles[i].update();

    for (let j = 0; j < symmetry; j++) {
      push();
      translate(width / 2, height / 2);
      rotate((TWO_PI / symmetry) * j);
      if (mirror && j % 2 == 0) scale(1, -1);
      particles[i].pos = createVector(
        particles[i].pos.x - width / 2,
        particles[i].pos.y - height / 2
      );
      // currentTarget.color.setAlpha(opts.Alpha)

      // if(opts.ColorMode == colorModes.colorModeRandom)
      particles[i].display(currentTarget.color);
      // particles[i].display(particles[i].currentTarget.color);
      // else if(opts.ColorMode == colorModes.colorModeNoise)
      //   particles[i].display(c)
      // particles[i].display();

      particles[i].pos = createVector(
        particles[i].pos.x + width / 2,
        particles[i].pos.y + height / 2
      );
      pop();
    }
  }
  t += 0.1;
}

function ease(p) {
  return 3 * p * p - 2 * p * p * p;
}

function ease2(p, g) {
  if (p < 0.5) return 0.5 * pow(2 * p, g);
  else return 1 - 0.5 * pow(2 * (1 - p), g);
}

function softplus(q, p) {
  const qq = q + p;
  if (qq <= 0) {
    return 0;
  }
  if (qq >= 2 * p) {
    return qq - p;
  }
  return (1 / (4 * p)) * qq * qq;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `gravity_1_${seed}`, "png");
}
