let canvas;
let seed;
let simplexNoise;
let t = 0;
let palette = [];
const numFrame = 30;

const recording = false;

palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
palette = ["#020224", "#01104F", "#003C81", "#01669E", "#5075A2", "#6F91AD"];
palette = [255];

function setup() {
  canvas = createCanvas(1080, 1350);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  if (recording) capturer.start();
  background(10);
  randomSeed(seed);
}

let scalar = 20;
let phi = 0;

const scalarStep = 0.03;
const scalarStepMin = 0.01;
const scalarStepMax = 0.04;

const phiStep = 0.04;
const phiStepMin = 0.02;
const phiStepMax = 0.05;

const noiseScale = 0.5;

function draw() {
  translate(width / 2, height / 2);
  // noStroke();
  noLoop();
  t = frameCount / numFrame;

  // some kind of clock

  if (recording) {
    capturer.capture(canvas.canvas);
    if (t === 3) {
      capturer.stop();
      capturer.save();
      noLoop();
    }
  }
}

function ease(p) {
  return 3 * p * p - 2 * p * p * p;
}

function ease2(p, g) {
  if (p < 0.5) return 0.5 * pow(2 * p, g);
  else return 1 - 0.5 * pow(2 * (1 - p), g);
}

function softplus(q, p) {
  const qq = q + p;
  if (qq <= 0) {
    return 0;
  }
  if (qq >= 2 * p) {
    return qq - p;
  }
  return (1 / (4 * p)) * qq * qq;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `noise_circle_2_${seed}`, "png");
}
