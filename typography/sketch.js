let canvas;
let seed;
let simplexNoise;
const border = 160;
let font
const recording = true;

function preload() {
  font = loadFont('../assets/VCR_OSD_MONO_1.001.ttf')
}

function setup() {
  canvas = createCanvas(1080, 1080);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  colorMode(HSB)
  if (recording) capturer.start();
}

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";
let t = 0

const palette = ['#065143', '#129490', '#70b77e', '#e0a890', '#ce1483']

const chars = ['.', '.', '?', '/']
const s = 400
function draw() {
  background(darkColor);
  // noLoop();
  const numFrame = 300
  const p = (frameCount/numFrame);

  const noiseScaleX = .01
  const noiseScaleY = .002
  const noiseScaleP = 1
  const w = 61
  const h = 51.3
  const stepX = width / w
  const stepY = height / h
  for (let i = border; i <= width - border; i+=stepX) {
    for (let j = border; j <= height - border + 15; j+=stepY) {
      textSize(25)
      textFont(font)
      fill(lightColor)
      noStroke()
      // const char = simplexNoise.noise3D(i*noiseScale,j*noiseScale, t) > 0 ? '1' : '0'
      const charIndex = map(simplexNoise.noise4D(i*noiseScaleX,j*noiseScaleY, cos(p * TWO_PI) * noiseScaleP, sin(p * TWO_PI) * noiseScaleP), -1, 1, 0, chars.length)
      const char = getChars(i,j)[int(charIndex)]
      // const char = random(chars)
      // const char = getChar(i, j)
      
      text(char, i, j)
    }
  }

  // noFill()
  // stroke('red')
  // push()
  // translate(width/2, height/2)
  // rect(-s/2, -s/2, s, s)
  // rotate(PI/4)
  // rect(-s/2, -s/2, s, s)
  // circle(0,0,s/2)
  // pop()

  rectMode(CORNER);
  fill(lightColor);
  noStroke();
  rect(0, 0, width, border);
  rect(0, 0, border, height);
  rect(width - border, 0, border, height);
  rect(0, height - border, width, border);

  if (recording) {
    capturer.capture(canvas.canvas);
    if (p === 3) {
      capturer.stop();
      capturer.save();
      noLoop();
    }
  }
}

const getChar = (x, y) => {
  push()
  translate(width/2, height/2)
  const isInFirst = x > (-s/2) +  height/2 && x < (-s/2) + height/2 + s && y > (-s/2) + height/2 && y < (-s/2) + s +  height/2
  pop()

  return isInFirst ? '.' : random(chars)
}

const getChars = (x, y) => {
  push()
  translate(width/2, height/2)
  const isInFirst = x > (-s/2) +  height/2 && x < (-s/2) + height/2 + s && y > (-s/2) + height/2 && y < (-s/2) + s +  height/2
  pop()

  return !isInFirst ? chars : ['/','?','.','.']
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `vhs_${seed}`, "png");
}
