class Particle {
  constructor(x = int(random(width)), y = int(random(height))) {
    this.pos = createVector(x, y);
    this.prevPros = createVector(x, y);
    // this.scale = 0.3;
    // this.step = 10;
    this.scale = 2.2;
    this.step = 1;
    this.grey = random(150, 255);
    this.weight = random(2);
    this.col = palette[0];
    // this.col = getColorFromShader(this.pos.x, this.pos.y);
    this.col = getColor(this.pos.x, this.pos.y);
    // this.setColor(this.vectorField(this.pos.x, this.pos.y));
  }

  update() {
    // this.setColor(this.vectorField(this.pos.x, this.pos.y));
    this.pos.add(this.vectorField(this.pos.x, this.pos.y).mult(this.step));
    this.display();
    this.edges();
  }

  setColor(v) {
    // console.log(v.x, v.y);
    const i = int(map(v.x, 2, 0, 0, palette.length - 1, true));
    const i2 = int(map(v.y, -1, 2, 0, palette.length - 1, true));
    this.col = palette[(i + i2) % palette.length];
  }

  display() {
    stroke(this.col);
    stroke(this.grey);
    // stroke(255);
    // stroke(getColor(this.pos.x, this.pos.y));
    strokeWeight(this.weight);
    // stroke(getColorFromShader(this.pos.x, this.pos.y));
    point(this.pos.x, this.pos.y);
  }

  vectorField(x, y) {
    x = map(x, 0, width, -this.scale, this.scale);
    y = map(y, 0, height, -this.scale, this.scale);

    let k1 = 0.5;
    let k2 = 0.1;

    let u = cos(k1 * y) - sin(k2 * y); //+ sin(x + y);
    let v = sin(k1 * x) + cos(k1 * x); //+ tan(x + y);

    return createVector(u, v);
  }

  edges() {
    if (this.pos.x < border) {
      this.pos.x = width - border;
      this.prevPos = this.pos.copy();
    }
    if (this.pos.x > width - border) {
      this.pos.x = border;
      this.prevPos = this.pos.copy();
    }
    if (this.pos.y < border) {
      this.pos.y = height - border;
      this.prevPos = this.pos.copy();
    }
    if (this.pos.y > height - border) {
      this.pos.y = border;
      this.prevPos = this.pos.copy();
    }
  }
}
