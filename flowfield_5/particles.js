class Particle {
  constructor(x = int(random(width)), y = int(random(height))) {
    this.pos = createVector(x, y);
    this.prevPros = createVector(x, y);
    this.scale = 9;
    this.step = 1;
    // this.scale = 12;
    // this.step = 1;
  }

  update() {
    this.pos.add(this.vectorField(this.pos.x, this.pos.y).mult(this.step));
    this.display();
    this.edges();
  }

  display() {
    stroke(255);
    strokeWeight(1);
    stroke(getColor(this.pos.x, this.pos.y));
    // stroke(getColorFromShader(this.pos.x, this.pos.y));
    point(this.pos.x, this.pos.y);
  }

  vectorField(x, y) {
    x = map(x, 0, width, -this.scale, this.scale);
    y = map(y, 0, height, -this.scale, this.scale);

    let k1 = 2;
    let k2 = 5;

    let u = cos(k1 * y) - sin(k2 * y); //+ sin(x + y);
    let v = sin(k1 * x) + cos(k1 * x); //+ tan(x + y);

    return createVector(u, v);
  }

  edges() {
    if (this.pos.x < border) {
      this.pos.x = width - border;
      this.prevPos = this.pos.copy();
    }
    if (this.pos.x > width - border) {
      this.pos.x = border;
      this.prevPos = this.pos.copy();
    }
    if (this.pos.y < border) {
      this.pos.y = height - border;
      this.prevPos = this.pos.copy();
    }
    if (this.pos.y > height - border) {
      this.pos.y = border;
      this.prevPos = this.pos.copy();
    }
  }
}
