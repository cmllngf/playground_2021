let canvas;
let simplexNoise;

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";

const space = 160

function setup() {
  seed = random(9999);
  randomSeed(seed);
  simplexNoise = openSimplexNoise(seed);
  canvas = createCanvas(1080, 1080, WEBGL);
  colorMode(HSB);
  randomSeed(seed);
}

function draw() {
  orbitControl();
  background(darkColor)
  strokeWeight(4)
  stroke(darkColor)
  fill(lightColor)

  push()
  translate(0, 0, -200)
  rotateX(-PI/6)
  rotateY(-PI/8)
  const noiseScaleX = .0018
  const noiseScaleY = .002
  const step = 100
  for (let x = -5000; x < 600; x += step) {
    for (let z = -300; z < 6900; z += step) {

      const noiseValue = simplexNoise.noise3D(x * noiseScaleX, z * noiseScaleY, frameCount * 0) * 100

      push()
      translate(x, 500 + noiseValue, -z);
      box(80)
      pop()
    }
  }
  pop()

  translate(-width/2, -height/2, 0)
  fill(lightColor);
  noStroke();
  rect(0, 0, space, height);
  rect(0, 0, width, space);
  rect(0, height - space, width, space);
  rect(width - space, 0, space, height);
  noFill()
  stroke(darkColor)
  line(space, space, space, height - space + 2)
  line(space - 2, space, width - space, space + 2)
  line(width - space + 2, height - space, space, height - space)
  line(width - space, height - space, width - space, space)
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `sea_shape_${seed}`, "png");
}
