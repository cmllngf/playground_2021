/*
	Raymarching | GPU
		License: MIT - feel free to learn from and manipulate this
*/
// ------------
const WIDTH = 1080;
const HEIGHT = 1080;

// ------------
/* Fragment Shader 
		- write main fragment shader here
*/
const mainFragmentShader = `
	
	const float EPS = .00005;
	const float T_MAX = 100.;
	const int MARCH_STEPS = 88;

	float scene(vec3 p) {
		// more  sdf functions at: http://www.iquilezles.org/www/articles/distfunctions/distfunctions.htm
		
		// repeat the space
		p = mod(p + 1., 2.) - 1.; 

		// sphere
		return length(p) - .2;

	}

	void main() {

		// normalized uv coordinates
		vec2 uv = (2.* gl_FragCoord.xy - resolution) / resolution.y;

		// ray origin
		// a static ray origin
		// vec3 ro = vec3(1., 0., -3.);
		
		// a moving ray origin
		// to traverse through repeated space
		vec3 ro = vec3(1., -3. + 5. * tan(time * .25), time);		

		vec3 rd = vec3(uv, 1.);
	
		// raymarch
		// t - how far along ray

		float t = 0.;
		for (int i = 0; i < MARCH_STEPS; i++) {
			
			vec3 p = ro + rd * t;
			float d = scene(p);
			
			/*
				if d is negative then we are inside or touching the object
				or if surpasses the max dist we want to check - then we break the loop
			*/
			if (d < EPS || t > T_MAX) break;
			
			t += .5 * d;

		}
		
		// background color
		gl_FragColor = vec4(0., 0., 0., 1.);

		// check t (depth) is in range
		if (t < T_MAX) {
				
			// get the position
			vec3 p = ro + rd * t;
			
			// we can use t and position to color our objects
			// set object color		
		
			float fog = 1. / (1. + t * t);
			vec3 color = vec3(fog, 0., (p.y + 4.) * .05);
			

			gl_FragColor = vec4(color, 1.);
		
		}
			


	}

`

// ------------
let mainShader,
		time,
		resolution;

// ------------
function setupShader() {
	
	const vertexShader = `
		attribute vec3 aPosition;
		void main() {
			gl_Position = vec4(aPosition, 1.);
		}
	`
	const fragmentShader = `
		#ifdef GL_ES
		precision mediump float;
		#endif

		uniform float time;
		uniform vec2 resolution;
		uniform vec2 mouse;
		${mainFragmentShader}
 	`
	mainShader = new p5.Shader(this._renderer, vertexShader, fragmentShader);
	
	time = 0;
	resolution = [width, height]
	mouse = [mouseX, mouseY];
	
}

function drawPlane() {
	shader(mainShader);
	mainShader.setUniform("resolution", resolution);
	mainShader.setUniform("mouse", mouse);
	mainShader.setUniform("time", time);
	beginShape(TRIANGLE_STRIP);
	vertex(-1, -1, 0);
	vertex(-1, 1, 0);
	vertex(1, -1, 0);
	vertex(1, 1, 0);
	endShape(CLOSE);
}
// ------------

// ------------
function setup() {
	createCanvas(WIDTH, HEIGHT, WEBGL);
	background(100);
	setupShader();	
	console.log(mainShader);
}

function draw() {	
	time += 0.01;
	drawPlane();
}

function mouseMoved() {
	mouse = [mouseX, mouseY];
}

function resize() {
	resolution = [width, height];
}