class Thing {
  constructor(x, y, phi, c = accentColor, { offsetX = 0, offsetY = 0 }) {
    this.x = x;
    this.y = y;
    this.phi = phi;
    this.weight = random(3);
    this.c = c;
    this.offsetX = offsetX;
    this.offsetY = offsetY;
  }

  display(c = this.c) {
    if (c === null) return;
    stroke(255);
    stroke(c);
    strokeWeight(this.weight);

    push();
    translate(this.x, this.y);
    rotate(this.phi);
    // point(this.offsetX, this.offsetY);
    point(0, 0);
    pop();
    // point(this.x, this.y);
  }

  update(index) {
    this.x += cos(this.phi) * 1;
    this.y += sin(this.phi) * 1;

    // this.v = noise(this.x, this.y);
    const v = (noise(this.x * 0.1, this.y * 0.1) + 0.045 * (index - 6 / 3)) % 1;

    this.phi += 2 * map(v, 0, 1, -1, 1);
    // this.phi += 3 * random(-1, 1);
  }

  edge() {
    if (this.x < -widthBorder) {
      this.x = widthBorder;
    }
    if (this.x > widthBorder) {
      this.x = -widthBorder;
    }
    if (this.y < -heightBorder) {
      this.y = heightBorder;
    }
    if (this.y > heightBorder) {
      this.y = heightBorder;
    }
  }
}
