let canvas;
let seed;
let simplexNoise;
let t = 0;
let palette = [];
const numFrame = 30;

const recording = false;

const rectangles = [];
const things = [];

let img;

// palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
// palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
// palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
// palette = ["#020224", "#01104F", "#003C81", "#01669E", "#5075A2", "#6F91AD"];
// palette = [255];
// palette = ["#845ec2", "#2c73d2", "#0081cf", "#0089ba", "#008e9b", "#008f7a"];
// palette = ["#845ec2", "#d65db1", "#ff6f91", "#ff9671", "#ffc75f", "#f9f871"];
// palette = ["#5ec2b5", "#69d1ae", "#81dea2", "#a3e991", "#ccf27f", "#f9f871"];
// palette = ["#c25eb5", "#f6669e", "#ff7f82", "#ffa56a", "#ffcf5f", "#f9f871"];
// palette = ["#3352c6", "#bb4ab6", "#ff5692", "#ff856c", "#ffc058", "#f9f871"];
// palette = ["#091C3C", "#142B57", "#142D5C", "#152D53", "#193E68", "#244F79"];

const rows = 82;
const cols = 28;
const gapRow = 10;
const gapCol = 10;

const w = 50;
const h = 10;

let widthBorder;
let heightBorder;

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";

let bgColor = darkColor;
let accentColor = lightColor;

function preload() {
  // img = loadImage("../assets/shiny_head.png");
  img = loadImage("../assets/lady_earring.png");
  // img = loadImage("../assets/venus_de_milo2.png");
  // img = loadImage("../assets/the scream.png");
  // img = loadImage("../assets/face.png");
  // img = loadImage("../assets/falling_dude.png");
}

function setup() {
  canvas = createCanvas(1080, 1080);
  widthBorder = width / 1.25;
  heightBorder = height / 1.25;
  seed = random(99999);
  img.loadPixels();
  randomSeed(seed);
  simplexNoise = openSimplexNoise(seed);
  if (recording) capturer.start();
  background(bgColor);

  const mid = rows / 2;
  const ys = [mid + 5, mid + 7, mid + 9];

  for (let x = 0; x < cols; x++) {
    for (let y = 0; y < rows; y++) {
      const offset = y % 2 == 0 ? 0 : w / 2 + gapCol / 2;
      const xx = x * (w + gapCol) - widthBorder - offset;
      const yy = y * (h + gapRow) - heightBorder;
      rectangles.push(
        new Rectangle(
          xx,
          yy,
          w,
          h,
          palette[int(random(palette.length))],
          // y % 2 == 0 ? 1 : 0
          // y == rows / 2 ? -1 : 0
          ys.includes(y) ? -1 : 0
        )
      );
    }
  }

  for (let i = 0; i < 400; i++) {
    const x = randomGaussian(0, 100);
    const y = randomGaussian(0, 150);
    things.push(
      new Thing(
        x,
        y,
        random(TWO_PI),
        // getBrightnessColor(x, y),
        palette[int(random(palette.length))],
        // {
        //   offsetX: randomGaussian(0, 10),
        //   offsetY: randomGaussian(0, 10),
        // }
        {
          offsetX: randomGaussian(0, randomGaussian(0, 7)),
          offsetY: randomGaussian(0, randomGaussian(0, 7)),
        }
        // {
        //   offsetX: random(-5, 5),
        //   offsetY: random(-5, 5),
        // }
        // {}
      )
    );
  }

  // const radius = 35;
  // const k = 5;
  // const network = poissonDiskSampling(radius, k);
  // for (let i = 0; i < min(network.length, 700); i++) {
  //   const { x, y } = network[i];
  //   things.push(
  //     new Thing(
  //       x - width / 2,
  //       y - height / 2,
  //       //       random(TWO_PI),
  //       random(TWO_PI),
  //       // getBrightnessColor(x, y),
  //       palette[int(random(palette.length))]
  //       // isShader(x, y) ? true : random() > 0.7,
  //       // isShader(x, y) ? 0.2 : 0.5
  //     )
  //   );
  // }
  // translate(width / 2, height / 2);
  // rotate(QUARTER_PI * 3);
  // for (let i = 0; i < rectangles.length; i++) {
  //   const rectangle = rectangles[i];
  //   rectangle.display(0);
  // }
}

function draw() {
  translate(width / 2, height / 2);
  rotate(QUARTER_PI * 3);
  t = (frameCount / numFrame) % 1;
  // noLoop();

  for (let k = 0; k < 4; k++) {
    for (let j = 0; j < things.length; j++) {
      const thing = things[j];
      // thing.update(5);
      thing.update(map(j, 0, things.length, 0, 5));
      for (let i = 0; i < rectangles.length; i++) {
        const rectangle = rectangles[i];
        // rectangle.display();
        if (rectangle.isIn(thing.x, thing.y)) {
          // thing.display(rectangle.c);
          // thing.display();
          const { x, y } = rotateCoord(
            thing.x + thing.offsetX + rectangle.offset * (w + gapRow),
            thing.y + thing.offsetY,
            QUARTER_PI * 3
          );
          thing.display(getColorPaletteFromImage(x, y));
          // thing.display(accentColor);
        }
      }
    }
  }

  if (recording) {
    capturer.capture(canvas.canvas);
    if (frameCount === numFrame * 6) {
      capturer.stop();
      capturer.save();
      noLoop();
    }
  }
}

const get1DIndex = (x, y, minX = 0, maxX = width, minY = 0, maxY = height) => {
  const xx = int(map(x, minX, maxX, 0, img.width));
  const yy = int(map(y, minY, maxY, 0, img.height));
  return (yy * img.width + xx) * 4;
};

const getColorPaletteFromImage = (x, y) => {
  const index = get1DIndex(
    x,
    y,
    -width / 2,
    width / 2,
    -height / 2,
    height / 2
  );
  const R = img.pixels[index];
  const G = img.pixels[index + 1];
  const B = img.pixels[index + 2];
  const A = img.pixels[index + 3];

  if (A == 0) {
    // return getColorPaletteGaussian(x, y);
    return null;
  }

  // colorMode(RGB);
  // const c = color(R, G, B);
  // colorMode(HSB);
  // return c;
  const ll = 0.2126 * R + 0.7152 * G + 0.0722 * B;
  return palette[int(map(ll, 0, 255, 0, palette.length, true))];
};

const rotateCoord = (x, y, phi) => {
  return {
    x: int(x * cos(phi) - y * sin(phi)),
    y: int(x * sin(phi) + y * cos(phi)),
  };
};

function ease(p) {
  return 3 * p * p - 2 * p * p * p;
}

function ease2(p, g) {
  if (p < 0.5) return 0.5 * pow(2 * p, g);
  else return 1 - 0.5 * pow(2 * (1 - p), g);
}

function softplus(q, p) {
  const qq = q + p;
  if (qq <= 0) {
    return 0;
  }
  if (qq >= 2 * p) {
    return qq - p;
  }
  return (1 / (4 * p)) * qq * qq;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `bricks_3_${seed}`, "png");
}

function isValidPoint(grid, cellsize, gwidth, gheight, p, radius) {
  /* Make sure the point is on the screen */
  if (p.x < 0 || p.x >= width || p.y < 0 || p.y >= height) return false;

  /* Check neighboring eight cells */
  xindex = floor(p.x / cellsize);
  yindex = floor(p.y / cellsize);
  i0 = max(xindex - 1, 0);
  i1 = min(xindex + 1, gwidth - 1);
  j0 = max(yindex - 1, 0);
  j1 = min(yindex + 1, gheight - 1);

  for (i = i0; i <= i1; i++)
    for (j = j0; j <= j1; j++)
      if (grid[i][j] != null)
        if (dist(grid[i][j].x, grid[i][j].y, p.x, p.y) < radius) return false;

  /* If we get here, return true */
  return true;
}

function insertPoint(grid, cellsize, point) {
  xindex = floor(point.x / cellsize);
  yindex = floor(point.y / cellsize);
  grid[xindex][yindex] = point;
}

function poissonDiskSampling(radius, k) {
  N = 2;
  /* The final set of points to return */
  points = [];
  /* The currently "active" set of points */
  active = [];
  /* Initial point p0 */
  p0 = createVector(int(random(0, width)), int(random(0, height)));
  grid = [];
  cellsize = floor(radius / sqrt(N));

  /* Figure out no. of cells in the grid for our canvas */
  ncells_width = ceil(width / cellsize) + 1;
  ncells_height = ceil(height / cellsize) + 1;

  /* Allocate the grid an initialize all elements to null */
  for (i = 0; i < ncells_width; i++) {
    grid[i] = [];
    for (j = 0; j < ncells_height; j++) grid[i][j] = null;
  }

  insertPoint(grid, cellsize, p0);
  points.push(p0);
  active.push(p0);

  while (active.length > 0) {
    random_index = int(random(active.length));
    p = active[random_index];

    found = false;
    for (tries = 0; tries < k; tries++) {
      theta = int(random(360));
      new_radius = int(random(radius, 2 * radius));
      pnewx = int(p.x + new_radius * cos(radians(theta)));
      pnewy = int(p.y + new_radius * sin(radians(theta)));
      pnew = createVector(pnewx, pnewy);

      if (
        !isValidPoint(grid, cellsize, ncells_width, ncells_height, pnew, radius)
      )
        continue;

      points.push(pnew);
      insertPoint(grid, cellsize, pnew);
      active.push(pnew);
      found = true;
      break;
    }

    /* If no point was found after k tries, remove p */
    if (!found) active.splice(random_index, 1);
  }

  return points;
}
