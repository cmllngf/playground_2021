class Rectangle {
  constructor(x, y, w, h, c = accentColor, offset) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.c = c;
    this.offset = offset;
  }

  display(c = this.c) {
    fill(c);
    stroke(c);
    rect(this.x, this.y, this.w, this.h);
  }

  isIn(x, y) {
    return (
      x > this.x && x < this.x + this.w && y > this.y && y < this.y + this.h
    );
  }
}
