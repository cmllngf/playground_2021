let canvas;
let seed;
let simplexNoise;

const points = []
const circles = []

function setup() {
  canvas = createCanvas(800, 800);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);

  const N = 10
  const R = 200
  for (let i = 0; i < N; i++) {
    const phi = (TWO_PI / N) * (i % N)
    const x = cos(phi) * R
    const y = sin(phi) * R
    const x1 = cos(phi-PI) * R
    const y1 = sin(phi-PI) * R
    points.push(new Point(x, y, x1, y1))
  }

  circles.push(new Circle(0, 0))
}

function draw() {
  randomSeed(seed);
  background(10) 

  translate(width/2, height/2)

  for (let i = 0; i < points.length; i++) {
    const p = points[i];
    p.update()
  };
  for (let i = 0; i < circles.length; i++) {
    const c = circles[i];
    c.show()
  }
}

class Point {
  constructor(x1, y1, x2, y2) {
    this.x = x1
    this.x1 = x1
    this.x2 = x2
    this.y = y1
    this.y1 = y1
    this.y2 = y2
    this.p = random(0, .7)
    this.pStep = .01
    this.direction = 1
    // console.log({x1, y1, x2, y2})
  }

  update() {
    this.p = this.p + this.pStep * this.direction
    const p1 = createVector(this.x1, this.y1)
    const p2 = createVector(this.x2, this.y2)
    const v = p5.Vector.lerp(p1, p2, this.p);

    this.x = v.x
    this.y = v.y

    if (this.p >= .95) this.direction = -1
    if (this.p <= .05) this.direction = 1
  }
}

class Circle {
  constructor(x, y) {
    this.x = x
    this.y = y
    this.radius = 150
  }

  getClosests() {
    return points.filter((p) => dist(p.x, p.y, this.x, this.y) <= this.radius)
  }

  show() {
    const ps = this.getClosests()
    if (ps.length < 2) return;
    beginShape()
    for (let i = 0; i <= ps.length + 2; i++) {
      const p = ps[i % ps.length];
      stroke('red')
      // circle(p.x, p.y, 5)
      noFill()
      stroke('white')
      strokeWeight(3)
      curveVertex(p.x, p.y)
    };
    endShape()
  }
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `tests_${seed}`, "png");
}
