let canvas;
let seed;
let simplexNoise;
let t = 0;
let img;
let palette = [];
const numFrame = 30;

const recording = false;

const rectangles = [];
const things = [];

// palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
// palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
// palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
// palette = ["#020224", "#01104F", "#003C81", "#01669E", "#5075A2", "#6F91AD"];
// palette = [255];
// palette = ["#845ec2", "#2c73d2", "#0081cf", "#0089ba", "#008e9b", "#008f7a"];
// palette = ["#845ec2", "#d65db1", "#ff6f91", "#ff9671", "#ffc75f", "#f9f871"];
// palette = ["#5ec2b5", "#69d1ae", "#81dea2", "#a3e991", "#ccf27f", "#f9f871"];
// palette = ["#c25eb5", "#f6669e", "#ff7f82", "#ffa56a", "#ffcf5f", "#f9f871"];
palette = ["#3352c6", "#bb4ab6", "#ff5692", "#ff856c", "#ffc058", "#f9f871"];
// palette = ["#091C3C", "#142B57", "#142D5C", "#152D53", "#193E68", "#244F79"];
// palette = ["#572D39", "#6E4D45", "#918B7D", "#A69B9D", "#C1C098"];
// palette = ["#231811", "#802A1B", "#CC8328", "#D5AC49", "#F4EC9D"]; //sunset

// palette = ["#151412", "#272E1E", "#354A29", "#53734A", "#84B188", "#D5D7D4"];
// palette = ["#220507", "#450A04", "#6A2B02", "#8F5201", "#C98A43", "#E6D19F"];
// palette = ["#5C3C49", "#8D5964", "#AF747F", "#D1617E", "#E788A2", "#EEAEB8"];
// palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
// palette = ["#AF747F", "#D1617E", "#E788A2", "#EEAEB8", "#F9D0C3", "#F6EEE1"];
// palette = [0, 250, 100, 150, 200, 50];s
// palette = [150, 250, 100, 50, 0, 200];
// palette = ["#24130C", "#70381F", "#C04700", "#F07800", "#EFB178", "#CCECEB"];
// palette = ["#6F2B3A", "#DB6578", "#E1B971", "#FBEDC7", "#949971", "#5B5D45"];
// palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
// palette = ["#092A2F", "#652B05", "#B65302", "#E7A700", "#FAE647", "#00B5AE"];
// palette = ["#162E21", "#063F39", "#30685B", "#73967E", "#C68C20", "#D8D091"];
// palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
// palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
// palette = ["#021118", "#14383D", "#025F71", "#00ACB3", "#9CDACF", "#EBE3D8"];

const rows = 49;
const cols = 15;
const gapRow = 20;
const gapCol = 20;

const w = 100;
const h = 20;

let widthBorder;
let heightBorder;

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";

let bgColor = lightColor;
let accentColor = darkColor;

function preload() {
  // img = loadImage("../assets/la_joconde.png");
  // img = loadImage("../assets/lady_earring.png");
  img = loadImage("../assets/head1.png");
  // img = loadImage("../assets/shiny_head.png");
  // img = loadImage("../assets/the scream.png");
}

function setup() {
  // canvas = createCanvas(1080, 1350);
  canvas = createCanvas(1080, 1080);
  widthBorder = width / 1.25;
  heightBorder = height / 1.25;
  seed = random(99999);
  smooth();
  randomSeed(seed);
  img.loadPixels();
  simplexNoise = openSimplexNoise(seed);
  // rectangles.push(new Rectangle(200, 200, 100, 50));
  // for (let x = 0; x < cols; x++) {
  //   for (let y = 0; y < rows; y++) {
  //     const offset = y % 2 == 0 ? 0 : w / 2 + gapCol / 2;
  //     const xx = x * (w + gapCol) - widthBorder - offset;
  //     const yy = y * (h + gapRow) - heightBorder;
  //     rectangles.push(
  //       new Rectangle(xx, yy, w, h, palette[int(random(palette.length))])
  //     );
  //   }
  // }

  const radius = 35;
  const k = 5;
  const network = poissonDiskSampling(radius, k);
  for (let i = 0; i < min(network.length, 700); i++) {
    const { x, y } = network[i];
    things.push(
      new Thing(
        x,
        y,
        //       random(TWO_PI),
        random(TWO_PI),
        getBrightnessColor(x, y),
        isShader(x, y) ? true : random() > 0.7,
        isShader(x, y) ? 0.2 : 0.5
      )
    );
  }
  // for (let i = 0; i < 700; i++) {
  //   // const x = random(-width / 2, width / 2);
  //   // const y = random(-height / 2, height / 2);
  //   // const x = random(-widthBorder, widthBorder);
  //   // const y = random(-heightBorder, heightBorder);
  //   // const x = randomGaussian(0, 100);
  //   // const y = randomGaussian(0, 100);
  //   const x = random(width);
  //   const y = random(height);
  //   things.push(
  //     new Thing(
  //       x,
  //       y,
  //       // random(-widthBorder, widthBorder),
  //       // random(-heightBorder, heightBorder),
  //       random(TWO_PI),
  //       getBrightnessColor(x, y),
  //       isShader(x, y) ? true : random() > 0.7,
  //       isShader(x, y) ? 0.2 : 0.5
  //       // palette[int(random(palette.length))]
  //       // getColor(y)
  //     )
  //   );
  // }
  if (recording) capturer.start();
  background(bgColor);
}

const get1DIndex = (x, y, minX = 0, maxX = width, minY = 0, maxY = height) => {
  const xx = int(map(x, minX, maxX, 0, img.width));
  const yy = int(map(y, minY, maxY, 0, img.height));
  return (yy * img.width + xx) * 4;
};

const isShader = (x, y) => {
  const index = get1DIndex(int(x), int(y));
  return img.pixels[index + 3] > 0;
};

const getBrightnessColor = (x, y) => {
  const index = get1DIndex(int(x), int(y));
  const R = img.pixels[index];
  const G = img.pixels[index + 1];
  const B = img.pixels[index + 2];
  if (!isShader(x, y)) {
    // return 0;
    return palette[int(random(palette.length))];
  }
  const ll = 0.2126 * R + 0.7152 * G + 0.0722 * B;
  // return map(ll, 0, 255, 100, 1);
  return palette[int(map(ll, 0, 255, 0, palette.length - 1))];
};

function getColor(y) {
  const i = map(y, 0, height, palette.length - 1, 0, true);
  const index = constrain(randomGaussian(i, 0.5), 0, palette.length);
  return palette[int(index)];
}

function draw() {
  // translate(width / 2, height / 2);
  // rotate(QUARTER_PI * 3);
  t = frameCount / numFrame;
  // noLoop();

  // for (let k = 0; k < 4; k++) {
  //   for (let j = 0; j < things.length; j++) {
  //     const thing = things[j];
  //     // thing.update(5);
  //     thing.update(map(j, 0, things.length, 0, 5));
  //     for (let i = 0; i < rectangles.length; i++) {
  //       const rectangle = rectangles[i];
  //       // rectangle.display();
  //       if (rectangle.isIn(thing.x, thing.y)) {
  //         // thing.display(rectangle.c);
  //         // thing.display();
  //         thing.display(accentColor);
  //       }
  //     }
  //   }
  // }
  for (let k = 0; k < 10; k++) {
    for (let j = 0; j < things.length; j++) {
      const thing = things[j];
      // thing.update(5);
      thing.update(map(j, 0, things.length, 0, 5));
      thing.display();
      // for (let i = 0; i < rectangles.length; i++) {
      //   const rectangle = rectangles[i];
      //   // rectangle.display();
      //   if (rectangle.isIn(thing.x, thing.y)) {
      //     // thing.display(rectangle.c);
      //   }
      // }
    }
  }
  // for (let i = 0; i < rectangles.length; i++) {
  //   const rectangle = rectangles[i];
  //   rectangle.display(accentColor);
  // }

  if (recording) {
    capturer.capture(canvas.canvas);
    if (t === 3) {
      capturer.stop();
      capturer.save();
      noLoop();
    }
  }
}

function ease(p) {
  return 3 * p * p - 2 * p * p * p;
}

function ease2(p, g) {
  if (p < 0.5) return 0.5 * pow(2 * p, g);
  else return 1 - 0.5 * pow(2 * (1 - p), g);
}

function softplus(q, p) {
  const qq = q + p;
  if (qq <= 0) {
    return 0;
  }
  if (qq >= 2 * p) {
    return qq - p;
  }
  return (1 / (4 * p)) * qq * qq;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `texture_1_${seed}`, "png");
}

function isValidPoint(grid, cellsize, gwidth, gheight, p, radius) {
  /* Make sure the point is on the screen */
  if (p.x < 0 || p.x >= width || p.y < 0 || p.y >= height) return false;

  /* Check neighboring eight cells */
  xindex = floor(p.x / cellsize);
  yindex = floor(p.y / cellsize);
  i0 = max(xindex - 1, 0);
  i1 = min(xindex + 1, gwidth - 1);
  j0 = max(yindex - 1, 0);
  j1 = min(yindex + 1, gheight - 1);

  for (i = i0; i <= i1; i++)
    for (j = j0; j <= j1; j++)
      if (grid[i][j] != null)
        if (dist(grid[i][j].x, grid[i][j].y, p.x, p.y) < radius) return false;

  /* If we get here, return true */
  return true;
}

function insertPoint(grid, cellsize, point) {
  xindex = floor(point.x / cellsize);
  yindex = floor(point.y / cellsize);
  grid[xindex][yindex] = point;
}

function poissonDiskSampling(radius, k) {
  N = 2;
  /* The final set of points to return */
  points = [];
  /* The currently "active" set of points */
  active = [];
  /* Initial point p0 */
  p0 = createVector(int(random(0, width)), int(random(0, height)));
  grid = [];
  cellsize = floor(radius / sqrt(N));

  /* Figure out no. of cells in the grid for our canvas */
  ncells_width = ceil(width / cellsize) + 1;
  ncells_height = ceil(height / cellsize) + 1;

  /* Allocate the grid an initialize all elements to null */
  for (i = 0; i < ncells_width; i++) {
    grid[i] = [];
    for (j = 0; j < ncells_height; j++) grid[i][j] = null;
  }

  insertPoint(grid, cellsize, p0);
  points.push(p0);
  active.push(p0);

  while (active.length > 0) {
    random_index = int(random(active.length));
    p = active[random_index];

    found = false;
    for (tries = 0; tries < k; tries++) {
      theta = int(random(360));
      new_radius = int(random(radius, 2 * radius));
      pnewx = int(p.x + new_radius * cos(radians(theta)));
      pnewy = int(p.y + new_radius * sin(radians(theta)));
      pnew = createVector(pnewx, pnewy);

      if (
        !isValidPoint(grid, cellsize, ncells_width, ncells_height, pnew, radius)
      )
        continue;

      points.push(pnew);
      insertPoint(grid, cellsize, pnew);
      active.push(pnew);
      found = true;
      break;
    }

    /* If no point was found after k tries, remove p */
    if (!found) active.splice(random_index, 1);
  }

  return points;
}
