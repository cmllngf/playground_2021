class Thing {
  constructor(x, y, phi, c = accentColor, startedInInShader, noiseScale = 0.2) {
    this.x = x;
    this.y = y;
    this.phi = phi;
    this.weight = random(2, 4);
    this.c = c;
    this.startedInInShader = startedInInShader;
    this.noiseScale = noiseScale;
  }

  display(c = this.c) {
    let cc;

    // if (this.startedInInShader) {
    //   cc = c;
    // } else {
    //   if (isShader(this.x, this.y)) {
    //     cc = getBrightnessColor(this.x, this.y);
    //   } else {
    //     cc = c;
    //   }
    // }
    if (this.startedInInShader) {
      // cc = c;
      if (isShader(this.x, this.y)) cc = getBrightnessColor(this.x, this.y);
      else cc = c;
    } else {
      cc = c;
    }
    // if (isShader(this.x, this.y)) {
    //   cc = getBrightnessColor(this.x, this.y);
    // } else {
    //   cc = c;
    // }

    stroke(cc);
    strokeWeight(this.weight);
    push();
    translate(this.x, this.y);
    rotate(this.phi);
    point(0, 0);
    pop();
    // point(this.x, this.y);
    // this.edge();
  }

  update(index) {
    this.x += cos(this.phi) * 1;
    this.y += sin(this.phi) * 1;

    // this.v = noise(this.x, this.y);
    const v =
      (noise(this.x * this.noiseScale, this.y * this.noiseScale) +
        0.045 * (index - 6 / 3)) %
      1;

    this.phi += 2 * map(v, 0, 1, -1, 1);
    // this.phi += 3 * random(-1, 1);
  }

  edge() {
    if (this.x < -width / 2) {
      this.x = width / 2;
    }
    if (this.x > width / 2) {
      this.x = -width / 2;
    }
    if (this.y < -height / 2) {
      this.y = height / 2;
    }
    if (this.y > height / 2) {
      this.y = height / 2;
    }
  }
}
