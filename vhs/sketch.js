let canvas;
let seed;
let simplexNoise;
const border = 160;
let font

function preload() {
  font = loadFont('../assets/VCR_OSD_MONO_1.001.ttf')
}

function setup() {
  canvas = createCanvas(1080, 1080);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  colorMode(HSB)
}

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";

// const palette = ['#0c090d', '#e01a4f', '#f15946', '#f9c22e', '#53b3cb']
const palette = ['#065143', '#129490', '#70b77e', '#e0a890', '#ce1483']

function draw() {
  background(darkColor);
  noLoop();

  // stroke(lightColor)
  // strokeWeight(90)
  // noFill()
  // const r = 150
  // circle(width/3, 450, r)
  // circle(width/1.5, 450, r)


  const s = 32.2
  stroke(lightColor)
  strokeWeight(1.5)
  for (let x = border - 1-5; x < width - border; x+=s) {
    line(x, 0, x, height)
  }
  for (let y = border - 1-5; y < height - border; y+=s) {
    line(0, y, width, y)
  }

  const w = width - border * 2
  {
    let step = w / 200 
    for (let i = border; i < width - border; i+=step) {
      const hue = map(i, border, width - border, 0, 360);
      
      fill(color(hue, 100, 80))
      noStroke()
      rect(i, 900, step + 2, 10)
    }
  }
  
  {
    let currentX = border
    step = w / 100;
    let white = true
    while(currentX <= width - border) {
      noStroke()
      fill(white ? lightColor : darkColor)
      rect(currentX, 800, step, 100)
      white = !white
      currentX += step;
      step = map(currentX, border, width - border, w / 100, w / 500)
    }
  }

  {
    let currentY = 600
    step = 20
    let i = 0
    while(i < 5) {
      const hue = map(i, 0, 4, 0, 200)
      noStroke()
      fill(color(hue, 80, 90))
      fill(palette[i])
      rect(0, currentY, width, step)
      currentY += step
      currentY += 5
      step = map(i, 1, 4, 20, 60, true)
      i++;
    }
  }

  {
    let currentY = 180
    step = 10
    let i = 0
    while(i < 5) {
      const hue = map(i, 0, 4, 0, 200)
      noStroke()
      fill(color(hue, 80, 90))
      fill(palette[i])
      rect(650, currentY, width, step)
      currentY += step
      i++;
    }
    noStroke()
    fill(lightColor)
    rect(0, 180, 640, 50)

    let currentX = border
    step = w / 100;
    let white = true
    while(currentX <= 635) {
      noStroke()
      fill(white ? lightColor : darkColor)
      rect(currentX, 185, step, 40)
      white = !white
      currentX += step;
      step = map(currentX, border, width - border, w / 100, w / 500)
    }
  }

  stroke(lightColor)
  fill(darkColor)
  rect(0, height/4, width, 200)

  textSize(200);
  textFont(font);
  textAlign(CENTER, CENTER);
  stroke(lightColor)
  strokeWeight(3)
  fill(random(palette))
  text('CML', width/2, height/3);

  // for (let i = 0; i < 6; i++) {
  //   push()
  //   rotate(PI/4)
  //   translate(250 + i * 20, -400)
  //   stroke(lightColor)
  //   strokeWeight(10)
  //   line(0, 0, 0, height)
  //   pop()
  // }

  rectMode(CORNER);
  fill(lightColor);
  noStroke();
  rect(0, 0, width, border);
  rect(0, 0, border, height);
  rect(width - border, 0, border, height);
  rect(0, height - border, width, border);
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `vhs_${seed}`, "png");
}
