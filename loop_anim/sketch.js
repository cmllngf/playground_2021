let canvas;
let seed;
let t = 0;
let recording = false;
let network = [];
let simplexNoise;

const avoidBorder = 20;

const poisson_radius = 10;
const poisson_k = 2;
const circleRdius = 300;

function setup() {
  createCanvas(1080, 1080);
  background(0);
  seed = random(9999);

  simplexNoise = openSimplexNoise(seed);
  randomSeed(seed);
}

function draw() {
  translate(width / 2, height / 2);
  background(10);
  t = frameCount / 120;

  noStroke();
  fill("white");

  let n = 4;
  const step = easeInOutQuart(sin(t)) * 2;
  // const step = sin(t) * 2;
  for (let i = 1; i <= 40; i++) {
    for (let j = 0 ; j < n; j++) {
      const a = TWO_PI / n;
      const x = cos(a * j) * i * 20;
      const y = sin(a * j) * i * 20;
      circle(x, y, 10);
    }
    n += step;
    // n *= step;
    // n = constrain(n, 1, 500)
  }
}

function easeOutExpo(x) {
  return x === 1 ? 1 : 1 - pow(2, -10 * x);
}

function easeInOutQuart(x) {
  return x < 0.5 ? 8 * x * x * x * x : 1 - pow(-2 * x + 2, 4) / 2;
}

function ease(p, g) {
  if (p < 0.5) return 0.5 * pow(2 * p, g);
  else return 1 - 0.5 * pow(2 * (1 - p), g);
}
function easeInBack(x) {
  const c1 = 1.70158;
  const c3 = c1 + 1;

  return c3 * x * x * x - c1 * x * x;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `loop_anim_${seed}`, "png");
}

function easeOutBounce(x) {
  const n1 = 7.5625;
  const d1 = 2.75;

  if (x < 1 / d1) {
    return n1 * x * x;
  } else if (x < 2 / d1) {
    return n1 * (x -= 1.5 / d1) * x + 0.75;
  } else if (x < 2.5 / d1) {
    return n1 * (x -= 2.25 / d1) * x + 0.9375;
  } else {
    return n1 * (x -= 2.625 / d1) * x + 0.984375;
  }
}
