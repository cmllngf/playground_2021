class Particle {
  constructor(pos = createVector(random(BORDER, width - BORDER), random(BORDER, height - BORDER))) {
    this.start = pos;
    this.pos = pos;
    this.prevpos = pos;
    this.vel = createVector(0);
    this.acc = createVector(0, 0);
    // this.maxSpeed = random(3, 9);
    this.maxSpeed = 9;
    // this.color = color(190, random(30,100), 80)
    this.color = palette[int(random(palette.length))]
    // this.color = color(
    //   map(this.pos.y, 0, height, 100, 200),
    //   random(30, 100),
    //   80
    // );
    this.maxLifetime = random(30, 400);
    this.lifeTime = 0;
    this.startSize = 50;
    this.endSize = 10;
    this.circles = [];
  }

  update() {
    this.lifeTime++;
    this.vel.add(this.acc);
    this.vel.limit(this.maxSpeed);
    this.prevpos = this.pos.copy();
    this.pos.add(this.vel);
    this.acc.mult(0);
    return this.edges();
  }

  applyForce(force) {
    this.acc.add(force);
  }

  follow(ffData) {
    const v = getVector(this.pos.x, this.pos.y, ffData);
    this.applyForce(v);
  }

  display(particles) {
    if (this.maxLifetime > this.lifeTime) {
      const size = map(
        this.lifeTime,
        0,
        this.maxLifetime,
        this.startSize,
        this.endSize
      );
      this.circles.push({
        x: this.pos.x,
        y: this.pos.y,
        size,
      });
      if (NO_TOUCH) {
        if (particles.some((p) => this.willEncounter(p))) {
          return false;
        }
      }
      fill(this.color);
      stroke(this.color);
      strokeWeight(size);
      line(this.pos.x, this.pos.y, this.prevpos.x, this.prevpos.y);
      return true;
    }
    return false;
  }

  displayCurve(particles) {
    if (this.lifeTime < this.maxLifetime) {
      this.circles.push({
        x: this.pos.x,
        y: this.pos.y,
        size: this.startSize,
      });
      if (NO_TOUCH) {
        if (particles.some((p) => this.willEncounter(p))) {
          return false;
        }
      }

      // fill(this.color);
      noFill()
      stroke(this.color);
      strokeWeight(this.startSize);
      vertex(this.pos.x, this.pos.y);
      return true;
    }
    return false
  }

  isAlive() {
    return this.lifeTime < this.maxLifetime;
  }

  isIn(that) {
    const s = 10
    const ts = 10
    // const s = this.startSize
    // const ts = that.startSize
    return (
      dist(this.pos.x, this.pos.y, that.pos.x, that.pos.y) <
      s/2 + ts/2 + GAP
    );
  }

  willEncounter(p) {
    if (this.start.x === p.start.x && this.start.y === p.start.y) {
      return false;
    }
    const lastCircle = this.circles[this.circles.length - 1];
    // const s = lastCircle.size
    const s = 10
    return p.circles.some((c) => 
      dist(c.x, c.y, lastCircle.x, lastCircle.y) <
      s/2 + s/2 + GAP
      // c.size/2 + lastCircle.size/2 + GAP
    );
  }

  edges() {
    if (this.pos.x < BORDER) {
      this.pos.x = width - BORDER;
      this.prevpos = this.pos.copy();
      return true;
    }
    if (this.pos.x > width + BORDER) {
      this.pos.x = BORDER;
      this.prevpos = this.pos.copy();
      return true;
    }
    if (this.pos.y < BORDER) {
      this.pos.y = height - BORDER;
      this.prevpos = this.pos.copy();
      return true;
    }
    if (this.pos.y > height - BORDER) {
      this.pos.y = BORDER;
      this.prevpos = this.pos.copy();
      return true;
    }
    return false
  }
}
