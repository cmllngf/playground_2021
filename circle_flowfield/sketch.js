let canvas;
let seed;
let simplexNoise;

const SCALE = 20;
const BORDER = -20;
const PARTICLES_COUNT = 400;
const NO_TOUCH = false;
const MAX_TRIES = 40;
const GAP = 1

let flowfieldData = {};

const xNoiseScale = .01
const yNoiseScale = .01

const particles = [];

let tries = 0;

function setup() {
  canvas = createCanvas(1080, 1080);
  seed = random(99999);
  background(10);
  // background("#dfdfdf");
  colorMode(HSB)
  simplexNoise = openSimplexNoise(seed);
  flowfieldData = initFlowfield(SCALE);
  if(NO_TOUCH) {
    while(particles.length < PARTICLES_COUNT && tries < MAX_TRIES) {
      const p = new Particle();
      if(!particles.some(particle => particle.isIn(p))) {
        particles.push(p);
        tries = 0;
      } else {
        tries++;
      }
    }
  } else {
    for (let i = 0; i < PARTICLES_COUNT; i++) {
      particles.push(new Particle())
    }
  }
}

const initFlowfield = (scl) => {
  const ff = {
    scale: scl,
    flowfield: []
  }

  const rows = int(height / scl);
  const cols = int(width / scl);

  for (let y = 0; y < rows; y++) {
    ff.flowfield.push([]);
    for (let x = 0; x < cols; x++) {
      const phi = simplexNoise.noise2D(x * xNoiseScale,y * yNoiseScale) * PI;
      const v = p5.Vector.fromAngle(phi)
      v.normalize();
      ff.flowfield[y].push(v);
    }
  }

  return ff;
}

const getVector = (x, y, ffData) => {
  const {
    scale, flowfield
  } = ffData;

  const sx = int(x/scale);
  const sy = int(y/scale);

  return flowfield[sy] && flowfield[sy][sx] ? flowfield[sy][sx] : null;
}

function draw() {
  randomSeed(seed);
  // background(10);

  for (let i = 0; i < 100; i++) {    
    // drawAllAtTheSameTime();
    drawOneAfterAnother();
    // drawOneAfterAnotherCurve();
  }
}

const drawAllAtTheSameTime = () => {
  particles.forEach(particle => {
    particle.follow(flowfieldData)
    particle.update()
    particle.display();
  });
}

let currentParticleIndex = 0;
const drawOneAfterAnother = () => {
  particles[currentParticleIndex].follow(flowfieldData);
  particles[currentParticleIndex].update()
  if(!particles[currentParticleIndex].display(particles)) {
    currentParticleIndex++;
    if(!particles[currentParticleIndex]) {
      noLoop();
    }
  }
}

const drawOneAfterAnotherCurve = () => {
  beginShape()
  while (particles[currentParticleIndex].isAlive()) {
    particles[currentParticleIndex].follow(flowfieldData);
    if(!particles[currentParticleIndex].update()) {
      particles[currentParticleIndex].displayCurve(particles);
    } else {
      particles[currentParticleIndex].lifeTime = 19000000
    }
  }
  endShape(CLOSE)
  currentParticleIndex++;
  if(!particles[currentParticleIndex]) {
    noLoop();
  }
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `circle_flowfield_${seed}`, "png");
}

// palette = ["#151412", "#272E1E", "#354A29", "#53734A", "#84B188", "#D5D7D4"];
// palette = ["#220507", "#450A04", "#6A2B02", "#8F5201", "#C98A43", "#E6D19F"];
// palette = ["#5C3C49", "#8D5964", "#AF747F", "#D1617E", "#E788A2", "#EEAEB8"];
// palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
// palette = ["#AF747F", "#D1617E", "#E788A2", "#EEAEB8", "#F9D0C3", "#F6EEE1"];
// palette = [0, 250, 100, 150, 200, 50];
// palette = [150, 250, 100, 50, 0, 200];
// palette = ["#24130C", "#70381F", "#C04700", "#F07800", "#EFB178", "#CCECEB"];
// palette = ["#6F2B3A", "#DB6578", "#E1B971", "#FBEDC7", "#949971", "#5B5D45"];
// palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
palette = ["#012023", "#1B443C", "#93907D", "#855352", "#6C252D"];
// palette = ["#092A2F", "#652B05", "#B65302", "#E7A700", "#FAE647", "#00B5AE"];
// palette = ["#162E21", "#063F39", "#30685B", "#73967E", "#C68C20", "#D8D091"];
// palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
// palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
// palette = ["#021118", "#14383D", "#025F71", "#00ACB3", "#9CDACF", "#EBE3D8"];
// palette = ["#845ec2", "#2c73d2", "#0081cf", "#0089ba", "#008e9b", "#008f7a"];
// palette = ["#845ec2", "#d65db1", "#ff6f91", "#ff9671", "#ffc75f", "#f9f871"];
// palette = ["#5ec2b5", "#69d1ae", "#81dea2", "#a3e991", "#ccf27f", "#f9f871"];
// palette = ["#c25eb5", "#f6669e", "#ff7f82", "#ffa56a", "#ffcf5f", "#f9f871"];
// palette = ["#3352c6", "#bb4ab6", "#ff5692", "#ff856c", "#ffc058", "#f9f871"];
// palette = ["#7ab6ff","#7ab6ff","#7ab6ff","#7ab6ff","#ffce7a","#ff8c7a","#7ab6ff","#7ab6ff"]
