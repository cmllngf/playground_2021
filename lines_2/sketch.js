let canvas;
let seed;
let simplexNoise;
let t = 0;
let palette = [];
const numFrame = 60;

const recording = true;

palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
palette = ["#020224", "#01104F", "#003C81", "#01669E", "#5075A2", "#6F91AD"];
palette = [255];

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";

// let bgColor = darkColor;
// let accentColor = lightColor;
let bgColor = lightColor;
let accentColor = darkColor;

const lines = [];

function setup() {
  canvas = createCanvas(1080, 1080);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  if (recording) capturer.start();

  const n = 120;
  for (let i = 0; i < n; i++) {
    const y = ((height + 20) / n) * i - 10;
    const nn = int(random(3, 17));
    const pattern = [];
    for (let j = 0; j < nn; j++) {
      pattern.push(
        j % 2 == 0 ? int(random(random(14, 40))) : int(random(random(7, 10)))
      );
    }
    lines.push(
      new Line(-50, y, width + 50, y, {
        pattern,
      })
    );
  }
}

function draw() {
  randomSeed(seed);
  background(bgColor);
  t = frameCount / numFrame;
  // noLoop();

  for (let i = 0; i < lines.length; i++) {
    const line = lines[i];
    line.display(i);
  }

  if (recording) {
    capturer.capture(canvas.canvas);
    if (frameCount === numFrame * 3) {
      capturer.stop();
      capturer.save();
      noLoop();
    }
  }
}

function ease(p) {
  return 3 * p * p - 2 * p * p * p;
}

function ease2(p, g) {
  if (p < 0.5) return 0.5 * pow(2 * p, g);
  else return 1 - 0.5 * pow(2 * (1 - p), g);
}

function softplus(q, p) {
  const qq = q + p;
  if (qq <= 0) {
    return 0;
  }
  if (qq >= 2 * p) {
    return qq - p;
  }
  return (1 / (4 * p)) * qq * qq;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `lines_2_${seed}`, "png");
}
