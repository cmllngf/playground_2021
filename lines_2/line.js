class Line {
  constructor(x1, y1, x2, y2, { pattern = [5, 2, 5, 10], offset = 0 }) {
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;
    this.p1 = createVector(x1, y1);
    this.p2 = createVector(x2, y2);
    this.pattern = pattern;
    this.offsetIndex = 0;
    this.currentPatternIndex = 0;
    this.offset(offset);
  }

  offset(v) {
    const total = this.pattern.reduce((acc, cur) => acc + cur, 0);
    const vv = int(v % total);
    let t = 0;
    for (let i = 0; i < this.pattern.length; i++) {
      const patternV = this.pattern[i];
      t += patternV;
      if (vv < t) {
        this.currentPatternIndex = i;
        this.offsetIndex = vv % this.pattern[i];
        break;
      }
    }
  }

  display2(yy = 0) {
    noFill();
    stroke(accentColor);
    const steps = dist(this.x1, this.y1, this.x2, this.y2) / 2;

    let index = this.offsetIndex;
    let currentPatternIndex = this.currentPatternIndex;
    let drawing = currentPatternIndex % 2 === 0;
    beginShape();
    for (let i = 0; i < steps; i++) {
      if (index >= this.pattern[currentPatternIndex]) {
        index = 0;
        currentPatternIndex++;
        currentPatternIndex = currentPatternIndex % this.pattern.length;
        if (drawing) {
          drawing = false;
          endShape();
        } else {
          drawing = true;
          beginShape();
          // strokeWeight(random(1, 5));
        }
      }

      const p = i / steps;
      const vector = p5.Vector.lerp(this.p1, this.p2, p);
      if (drawing) {
        const o = this.getNoiseValue(vector.x, yy);
        vertex(vector.x, vector.y + o);
        // vertex(vector.x, vector.y);
      }

      index++;
    }
    if (drawing) {
      endShape();
    }
  }

  display(yy = 0) {
    noFill();
    stroke(accentColor);
    const steps = dist(this.x1, this.y1, this.x2, this.y2) / 2;

    let index = this.offsetIndex;
    let currentPatternIndex = this.currentPatternIndex;
    let drawing = currentPatternIndex % 2 === 0;
    beginShape();
    for (let i = 0; i < steps; i++) {
      if (index >= this.pattern[currentPatternIndex]) {
        index = 0;
        currentPatternIndex++;
        currentPatternIndex = currentPatternIndex % this.pattern.length;
        if (drawing) {
          drawing = false;
          endShape();
        } else {
          drawing = true;
          beginShape();
          strokeWeight(6);
          // strokeWeight(random(3, 5));
        }
      }

      const p = i / steps;
      const vector = p5.Vector.lerp(this.p1, this.p2, p);
      if (drawing) {
        const o = this.getNoiseValue(vector.x, yy);
        vertex(vector.x, vector.y + o);
        // vertex(vector.x, vector.y);
      }

      index++;
    }
    if (drawing) {
      endShape();
    }
  }

  getNoiseValue(x, y) {
    const noiseScale = 0.01;
    const r = 0.3;
    const nv =
      simplexNoise.noise4D(
        x * noiseScale,
        y * noiseScale,
        cos(t * TWO_PI) * r,
        sin(t * TWO_PI) * r
      ) * 10;
    return nv;
  }
}
