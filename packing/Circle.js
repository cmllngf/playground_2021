class Circle extends Shape {
    isIn(shape) {
        if(shape instanceof Circle) {
            return this.circleInCircle(shape)
        } else if (shape instanceof Square) {
            return this.circleInSquare(shape)
        }
    }

    isSpotFree(x, y) {
        return dist(this.x, this.y, x, y) > this.s/2 + this.spaceAllowedForSpwan
    }

    show() {
        noFill()
        stroke(lightColor)
        strokeWeight(3)
        circle(this.x, this.y, this.s);
    }

    // out or just too big for its own sake
    isTooBig() {
        const r = (this.s + this.spaceAllowed)
        return this.s > this.maxSize || [0, PI/2, PI, -PI/2].some((a) => dist(width/2, height/2, cos(a) * r, sin(a) * r) > width - border)
    }
}