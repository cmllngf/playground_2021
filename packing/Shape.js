class Shape {
    constructor(x,y,s, growSpeed = 2, maxSize = 500) {
        this.x = x
        this.y = y
        this.s = s
        this.spaceAllowedForSpwan = 10
        this.spaceAllowed = 5
        this.growSpeed = growSpeed
        this.maxSize = maxSize
        this.isGrowing = true
        this.id = random(9999999999999999)
    }

    setS (_s) {
        this.s = _s
    }

    grow() {
        this.s+=this.growSpeed;
    }

    setIsGrowing(flag) {
        this.isGrowing = flag
    }

    isTooBig() {}

    isSpotFree(x, y) {}

    show() {}

    isIn(shape) {}

    circleInCircle(circle) {
        return dist(this.x, this.y, circle.x, circle.y) < (max(this.spaceAllowed, circle.spaceAllowed) + this.s/2 + circle.s/2)
    }

    circleInSquare(square) {
        return (new Array(10)).fill(null).map((_, i) => (TWO_PI / 20) * i).some((a) => {
            const x = cos(a) * (this.s/2)
            const y = sin(a) * (this.s/2)
            return !square.isSpotFree(x, y)
        })
    }

    squareInSquare(square) {
        return [
            [square.x - this.s/2, square.y - this.s/2],
            [square.x + this.s/2, square.y - this.s/2],
            [square.x - this.s/2, square.y + this.s/2],
            [square.x + this.s/2, square.y + this.s/2]
        ].some(([x,y]) => {
            return !this.isSpotFree(x, y)
        })
    }
}