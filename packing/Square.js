class Square extends Shape {
    isIn(shape) {
        if(shape instanceof Circle) {
            return shape.circleInSquare(this)
        } else if (shape instanceof Square) {
            return shape.squareInSquare(this)
        }
    }

    isSpotFree(x, y) {
        return (x < this.x - this.s/2 || x > this.x + this.s/2) && (y < this.y - this.s/2 || y > this.y + this.s/2)
    }

    show() {
        noFill()
        stroke(lightColor)
        strokeWeight(3)
        rectMode(CENTER)
        rect(this.x, this.y, this.s, this.s);
        rectMode(CORNER)
    }

    // out or just too big for its own sake
    isTooBig() {
        const r = (this.s + this.spaceAllowed)
        return this.s > this.maxSize || [0, PI/2, PI, -PI/2].some((a) => dist(width/2, height/2, cos(a) * r, sin(a) * r) > width - border)
    }
}