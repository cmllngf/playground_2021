let canvas;
let seed;
let simplexNoise;
const border = 160;
let shapes = []

const SHAPES = [Circle]

/** TODO
 * - Triangles
 * - Process les shapes après leur fin de vie (tranformer en spwaner, y dessiner une texture, ...)
 * - Couleurs
 * - Meilleur noise
 * - 
 */

function setup() {
  canvas = createCanvas(1080, 1080);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  shapes.push(new Circle(width/2, height/2, 1))
}

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";

const K = 120
let currentTries = 0

function draw() {
  background(darkColor);
  // noLoop();

  noFill()
  stroke(lightColor)
  strokeWeight(3)

  if (frameCount % 2 == 0) {
    if (currentTries < K) {
      while(true) {
        if(currentTries >= K) break;

        const [x, y] = [int(random(border, width - border)), int(random(border, height - border))]

        // fill('red')
        // circle(x,y,10)

        if (shapes.every((s) => s.isSpotFree(x, y))) {
          const noiseValue = simplexNoise.noise2D(x * .01, y * .01)
          const shapeIndex = int(map(noiseValue, -1, 1, 0, SHAPES.length))
          const maxSize = int(map(noiseValue, -1, 1, 100, 50))
          const gS = map(noiseValue, -1, 1, 10, 1)

          shapes.push(new SHAPES[shapeIndex](x, y, 1, gS, maxSize))
          currentTries = 0
          break;
        }
        
        currentTries++;
      }
    }
  }

  shapes.forEach((shape) => {
    if(shape.isGrowing && !shape.isTooBig() && shapes.every((s) => s.id === shape.id || !s.isIn(shape))) {
      shape.grow()
    } else {
      shape.setIsGrowing(false)
    }
  })

  shapes.forEach((shape) => {
    shape.show()
  })

  rectMode(CORNER);
  fill(lightColor);
  noStroke();
  rect(0, 0, width, border);
  rect(0, 0, border, height);
  rect(width - border, 0, border, height);
  rect(0, height - border, width, border);
}

function easeInCirc(x) {
  return 1 - sqrt(1 - pow(x, 2));
}


function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `machin_learning_${seed}`, "png");
}
