class Line {
  constructor(y) {
    this.y = y;
  }

  update(p, gap) {
    stroke(accentColor);
    const yy = gap * (p % 1);
    const n = 100;
    // line(0, this.y + yy, width, this.y + yy);
    beginShape();
    for (let i = 0; i < n; i++) {
      const x = (width / n) * i;
      const noiseValue = -abs(
        simplexNoise.noise2D(x * noiseScaleX, (this.y + yy) * noiseScaleY) * 60
      );
      curveVertex(x, this.y + yy + noiseValue);
    }
    endShape();
  }
}
