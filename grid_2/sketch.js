let canvas;
let seed;
let simplexNoise;
let t = 0;
let palette = [];

const numFrame = 60;
const recording = true;

palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
palette = ["#020224", "#01104F", "#003C81", "#01669E", "#5075A2", "#6F91AD"];
palette = [255];

const lines = [];

function setup() {
  canvas = createCanvas(1080, 1080);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  if (recording) capturer.start();
  let spaceBetweenLines = (height + 100) / numberOfLines;
  for (let i = 0; i < numberOfLines; i++) {
    lines.push(new Line(i * spaceBetweenLines));
  }
}

const numberOfLines = 100;
const numberDotsPerLine = 20;
const noiseScaleX = 0.01;
const noiseScaleY = 0.01;
const timeNoiseRadius = 0.15;

const border = 100;
const border2 = 120;

const bgColor = "#010101";
const accentColor = "#f5f5ff";

function draw() {
  background(bgColor);
  t = frameCount / numFrame;
  // noLoop();
  stroke(accentColor);
  strokeWeight(3);
  fill("#003C81");

  let spaceBetweenLines = (height + 100) / numberOfLines;

  for (let i = 0; i < lines.length; i++) {
    const lineO = lines[i];
    lineO.update(t, spaceBetweenLines);
  }

  // for (let i = 1; i < numberOfLines; i++) {
  //   const y = i * spaceBetweenLines;
  //   beginShape();
  //   vertex(0, y);
  //   for (let j = 0; j <= numberDotsPerLine; j++) {
  //     const x = (width / numberDotsPerLine) * j;
  //     const displacement = map(
  //       dist(x, y, width / 2, height / 2),
  //       0,
  //       500,
  //       500,
  //       1,
  //       true
  //     );
  //     // const noiseValue = -abs(
  //     //   simplexNoise.noise4D(
  //     //     x * noiseScaleX,
  //     //     y * noiseScaleY,
  //     //     timeNoiseRadius * cos(t * TWO_PI),
  //     //     timeNoiseRadius * sin(t * TWO_PI)
  //     //   ) * 100
  //     // );
  //     const noiseValue = -abs(
  //       simplexNoise.noise2D(x * noiseScaleX, y * noiseScaleY - t * 0.2) * 100
  //     );
  //     curveVertex(x, y + noiseValue);
  //   }
  //   vertex(width, y);
  //   endShape();
  // }

  noStroke();
  fill(accentColor);
  rect(0, 0, border2, height);
  rect(0, 0, width, border2);
  rect(width - border2, 0, border2, height);
  rect(0, height - border2, width, border2);

  fill(bgColor);
  rect(0, 0, border, height);
  rect(0, 0, width, border);
  rect(width - border, 0, border, height);
  rect(0, height - border, width, border);

  if (recording) {
    capturer.capture(canvas.canvas);
    if (t === 3) {
      capturer.stop();
      capturer.save();
      noLoop();
    }
  }
}

function ease(p) {
  return 3 * p * p - 2 * p * p * p;
}

function ease2(p, g) {
  if (p < 0.5) return 0.5 * pow(2 * p, g);
  else return 1 - 0.5 * pow(2 * (1 - p), g);
}

function softplus(q, p) {
  const qq = q + p;
  if (qq <= 0) {
    return 0;
  }
  if (qq >= 2 * p) {
    return qq - p;
  }
  return (1 / (4 * p)) * qq * qq;
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `grid_1_${seed}`, "png");
}
