let canvas;
let simplexNoise;

let lightColor = "#f5f8f8";
let darkColor = "#080F0F";

const palette = [
  "#845ec2",
  "#d65db1",
  "#ff6f91",
  "#ff9671",
  "#ffc75f",
  "#f9f871",
];

function setup() {
  seed = random(9999);
  randomSeed(seed);
  simplexNoise = openSimplexNoise(seed);
  canvas = createCanvas(1080, 1080);
  background(darkColor);
  colorMode(HSB);
}

function draw() {
  noLoop();
  // translate(-width / 2, -height / 2);
  background(darkColor);
  randomSeed(seed);

  const space = 150;

  noFill();
  
  for (let x = space; x <= width - space; x += 10) {
    const p = map(x, space, width - space, 0, 1);
    const y = height / 1.8 - 300 * easeInCirc(p);
    
    for (let i = 0; i < random(10, 250); i++) {
      strokeWeight(random(1,4));
      stroke(
        random() > 0.9 ? palette[int(random(palette.length))] : lightColor
      );
      const phiy = map(p, 0, 1, 100, 30);
      const phix = map(p, 0, 1, 10, 2);
      const py = randomGaussian(y, phiy);
      const px = randomGaussian(x, phix);
      point(px, py);
    }
  }

  fill(darkColor);
  noStroke();
  rect(0, 0, space, height);
  rect(0, 0, width, space);
  rect(0, height - space, width, space);
  rect(width - space, 0, space, height);

  noFill();
  stroke(lightColor);
  strokeWeight(2);
  rect(space, space, width - space * 2, height - space * 2);

  // const radius = 100;
  // noStroke();
  // fill(darkColor);
  // circle(width-space, height / 1.5, radius * 2);
  // fill(lightColor);
  // for (let i = 0; i < 1000; i++) {
  //   let a = random(TWO_PI);
  //   let y = random(-1, 1);
  //   let r = sqrt(1 - y * y);
  //   let z = sin(a);
  //   if (z > 0)
  //     circle(
  //       cos(a) * radius * r + width-space,
  //       y * radius + z * r * 5 + height / 1.5,
  //       5
  //     );
  // }
}

const easeInSine = (x) => {
  return 1 - cos((x * PI) / 2);
};

const easeInCirc = (x) => {
  return 1 - sqrt(1 - pow(x, 2));
};

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `space_${seed}`, "png");
}
