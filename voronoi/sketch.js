p5.disableFriendlyErrors = true;

const NUM = 1300;
const HALFPI = Math.PI;
const RAND = (min, max) => Math.random() * (max - min) + min; //d3.randomUniform

let W, H, polygons, voronoi, positions, velocities, colors;

const borderOffsetY = 200;
const borderOffsetX = 100;

let img;
let seed;
const numFrame = 180 * 5;
const recording = false;
// palette = ["#9C7B5A", "#CDAF7D", "#7CA98A", "#216C58", "#204534"];
// palette = ["#5E101E", "#732427", "#8BD6C2", "#175450", "#082B2F"];
// palette = ["#742228", "#721821", "#67181D", "#3B1816", "#0F0F0F"];
// palette = ["#760000", "#86502A", "#B87B5E", "#4A6077", "#1F4B3E"];
// palette = ["#00314F", "#6CA5AE", "#E5F1D9", "#B4A387", "#D38C86"];
// palette = ["#D5B5F3", "#75D8EC", "#F3A9C5", "#A9F3D3", "#F3C4A9"];
// palette = ["#082C59", "#0A72A1", "#4183B1", "#526C81", "#DDCEDA"];
// palette = ["#012023", "#1B443C", "#93907D", "#855352", "#6C252D"];
// palette = ["#840000", "#C58402", "#8B929C", "#18434C", "#1F2113"];

// palette = ["#151412", "#272E1E", "#354A29", "#53734A", "#84B188", "#D5D7D4"];
// palette = ["#220507", "#450A04", "#6A2B02", "#8F5201", "#C98A43", "#E6D19F"];
// palette = ["#5C3C49", "#8D5964", "#AF747F", "#D1617E", "#E788A2", "#EEAEB8"];
// palette = ["#AA6A82", "#B69792", "#C7ACA1", "#DDC1BE", "#DBD1C8", "#F6EEE1"];
palette = ["#AF747F", "#D1617E", "#E788A2", "#EEAEB8", "#F9D0C3", "#F6EEE1"];
// palette = [0, 250, 100, 150, 200, 50];
// palette = [150, 250, 100, 50, 0, 200];
// palette = ["#24130C", "#70381F", "#C04700", "#F07800", "#EFB178", "#CCECEB"];
// palette = ["#6F2B3A", "#DB6578", "#E1B971", "#FBEDC7", "#949971", "#5B5D45"];
// palette = ["#341C1C", "#5E101E", "#732427", "#8B473E", "#8BD6C2", "#388072"];
// palette = ["#092A2F", "#652B05", "#B65302", "#E7A700", "#FAE647", "#00B5AE"];
// palette = ["#162E21", "#063F39", "#30685B", "#73967E", "#C68C20", "#D8D091"];
// palette = ["#0F0F0F", "#221715", "#3F1715", "#54191B", "#67181D", "#68191E"];
// palette = ["#02110C", "#162E21", "#073E38", "#30685B", "#73967E", "#DACE94"];
palette = ["#021118", "#14383D", "#025F71", "#00ACB3", "#9CDACF", "#EBE3D8"];
// palette = ["#845ec2", "#2c73d2", "#0081cf", "#0089ba", "#008e9b", "#008f7a"];
// palette = ["#845ec2", "#d65db1", "#ff6f91", "#ff9671", "#ffc75f", "#f9f871"];
// palette = ["#5ec2b5", "#69d1ae", "#81dea2", "#a3e991", "#ccf27f", "#f9f871"];
// palette2 = ["#c25eb5", "#f6669e", "#ff7f82", "#ffa56a", "#ffcf5f", "#f9f871"];
// palette = ["#3352c6", "#bb4ab6", "#ff5692", "#ff856c", "#ffc058", "#f9f871"];

function preload() {
  img = loadImage("../assets/lady_earring.png");
}

function setup() {
  seed = random(999999);
  randomSeed(seed);
  img.loadPixels();
  ctx = canvas.getContext("2d");
  [W, H] = [1080, 1080];
  createCanvas(W, H);
  //frameRate(1000);
  strokeWeight(1);
  fill("#fff");
  //noStroke()

  positions = d3.range(NUM).map(
    (_) =>
      Float64Array.from({ length: 2 }, (_, i) =>
        i == 0
          ? constrain(
              randomGaussian(width / 2, width / 6.5),
              -borderOffsetX,
              W + borderOffsetX
            )
          : random(-borderOffsetY, H + borderOffsetY)
      )
    // Float64Array.from({ length: 2 }, (_, i) => Math.random() * (i ? H : W))
  );
  velocities = d3.range(NUM).map((_, i) => [
    0,
    map(
      dist(positions[i][0], 0, width / 2, 0),
      0,
      width / 3,
      5,
      // random() > 0.5 ? 3 : 3,
      0,
      true
    ),
  ]);
  // velocities = d3
  //   .range(NUM)
  //   .map((_) => Float64Array.from({ length: 2 }, (_) => RAND(-0.1, 0.1)));
  voronoi = d3.voronoi().extent([
    [-borderOffsetX, -borderOffsetY],
    [W + borderOffsetX, H + borderOffsetY],
  ]);

  // colors = d3.range(NUM).map((i) => d3.interpolateGnBu(norm(i, 0, NUM)));
  // colors = d3.range(NUM).map((i) => random(100));
  // colors = d3.range(NUM).map((i) => palette[int(random(palette.length))]);
  // colors = d3.range(NUM).map((i) => palette[int(random(palette.length))]);
  colors = d3.range(NUM).map((i) => getGaussianColor(...positions[i]));
  //interpolateGnBu
  //interpolateCool
  //interpolateWarm
  //interpolateRainbow
  //d3.schemeTableau10[i%10]
  // noLoop();
  if (recording) capturer.start();
}

function draw() {
  //background('#666')
  ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

  polygons = voronoi(positions).polygons();

  for (let i = 0; i < NUM; i++) {
    // EULER
    pos = positions[i];
    vel = velocities[i];
    // vel[0] += RAND(-0.1, 0.1);
    // vel[1] += RAND(-0.1, 0.1);
    pos[0] += vel[0];
    pos[1] += vel[1];
    // vel[0] *= 0.99;
    // vel[1] *= 0.99;

    const offset = 0;

    // WALLS
    if (pos[0] > W + borderOffsetX) pos[0] = -borderOffsetX;
    if (pos[0] < -borderOffsetX) pos[0] = W + borderOffsetX;
    if (pos[1] > H + borderOffsetY) pos[1] = -borderOffsetY;
    if (pos[1] < -borderOffsetY) pos[1] = H + borderOffsetY;

    // ALGO
    vertices = polygons[i].map((v) => createVector(v[0], v[1]));
    // rverts = roundCorners(vertices, 1);

    // RENDER (cell)
    push();
    // fill(getColorPaletteFromImage(...pos) ?? colors[i]);
    fill(colors[i]);
    stroke(colors[i]);
    // noStroke();
    // noFill();
    beginShape();
    vertices.map((v) => vertex(v.x, v.y));
    endShape(CLOSE);
    pop();

    // RENDER (site)
    // point(pos[0], pos[1]);
  }

  // text(frameRate(), 20, 20);
  // noLoop();

  if (recording) {
    capturer.capture(ctx.canvas);
    if (frameCount === numFrame) {
      capturer.stop();
      capturer.save();
      noLoop();
    }
  }
}

const getColor = (x, y) => {
  return map(y, -borderOffsetY, H + borderOffsetY, 20, 190);
};

const getColor2 = (x, y) => {
  return map(x, -borderOffsetY, H + borderOffsetY, 20, 190);
};

const getGaussianColor = (x, y) => {
  const i = map(
    dist(x, 0, width / 2, 0),
    0,
    width / 2 + borderOffsetX,
    palette.length - 1,
    0
  );
  const gaussIndex = constrain(
    int(randomGaussian(i, 1.2)),
    0,
    palette.length - 1
  );
  return palette[gaussIndex];
};

const get1DIndex = (x, y, minX = 0, maxX = width, minY = 0, maxY = height) => {
  const xx = int(map(x, minX, maxX, 0, img.width - 1, true));
  const yy = int(map(y, minY, maxY, 0, img.height - 1, true));
  return (yy * img.width + xx) * 4;
};

const getColorPaletteFromImage = (x, y) => {
  const index = get1DIndex(x, y);
  const R = img.pixels[index];
  const G = img.pixels[index + 1];
  const B = img.pixels[index + 2];
  const A = img.pixels[index + 3];

  if (A == 0) {
    // return getColorPaletteGaussian(x, y);
    return null;
  }

  // colorMode(RGB);
  // const c = color(R, G, B);
  // colorMode(HSB);
  // return c;
  const ll = 0.2126 * R + 0.7152 * G + 0.0722 * B;
  // return map(ll, 0, 255, 0, 150);
  return palette2[int(map(ll, 0, 255, 0, palette2.length, true))];
};

// function draw() {
//   //background('#666')
//   ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

//   polygons = voronoi(positions).polygons();

//   for (let i = 0; i < NUM; i++) {
//     // EULER
//     pos = positions[i];
//     vel = velocities[i];
//     vel[0] += RAND(-0.1, 0.1);
//     vel[1] += RAND(-0.1, 0.1);
//     pos[0] += vel[0];
//     pos[1] += vel[1];
//     vel[0] *= 0.99;
//     vel[1] *= 0.99;

//     // WALLS
//     if (pos[0] >= W - 4 || pos[0] <= 4) vel[0] *= -1;
//     if (pos[1] >= H - 4 || pos[1] <= 4) vel[1] *= -1;

//     // ALGO
//     vertices = polygons[i].map((v) => createVector(v[0], v[1]));
//     rverts = roundCorners(vertices, 5);

//     // RENDER (cell)
//     push();
//     fill(colors[i]);
//     noStroke();
//     beginShape();
//     rverts.map((v) => vertex(v.x, v.y));
//     endShape(CLOSE);
//     pop();

//     // RENDER (site)
//     // point(pos[0], pos[1]);
//   }

//   // text(frameRate(), 20, 20);
// }

function roundCorners(verts, m) {
  const P = [];
  const N = verts.length;
  const n = 15;

  for (let i = 0; i < N; i++) {
    sv = verts[(i + N - 2) % N];
    pv = verts[(i + N - 1) % N];
    cv = verts[i];
    nv = verts[(i + 1) % N];
    ev = verts[(i + 2) % N];

    let pbis = bisector(sv, pv, cv, m);
    let cbis = bisector(pv, cv, nv, m);
    let nbis = bisector(cv, nv, ev, m);

    v = cbis[1];

    minDist = 10000;
    nearest = null;

    for (let ibis of [pbis, nbis]) {
      isec = intersect(cbis[0], cbis[1], ibis[0], ibis[1]);
      if (isec) {
        d = cv.dist(isec);
        if (d < minDist) {
          minDist = d;
          nearest = isec;
        }
      }
    }

    if (nearest) v = nearest;

    d1 = p5.Vector.sub(cv, pv).normalize();
    d2 = p5.Vector.sub(nv, cv).normalize();

    //dsum = p5.Vector.add(d1, d2) //sum of directions

    //s = dsum.mag() / d1.dot(dsum) //speed
    //b = dsum.rotate(-HALFPI) //bisector
    //v = b.setMag(s*m).add(cv) //offsetted vertex

    op_prev = orthoProjection(d1, cv, v);
    op_next = orthoProjection(d2, cv, v);

    dp = p5.Vector.sub(op_prev, v);
    dn = p5.Vector.sub(op_next, v);

    a = dn.angleBetween(dp);
    t = a / n; //theta
    p = Math.atan2(op_next.y - v.y, op_next.x - v.x); // phi
    r = op_prev.dist(v); // radius

    for (let j = n; j > 0; j--) {
      x = Math.cos(t * j + p) * r * 0.9;
      y = Math.sin(t * j + p) * r * 0.9;
      P.push(createVector(x + v.x, y + v.y));
    }
  }
  return P;
}

function orthoProjection(dir, ep, op) {
  let dv = p5.Vector.sub(op, ep);
  dir.mult(dv.dot(dir));

  return p5.Vector.add(ep, dir);
}

function intersect(p1, p2, p3, p4) {
  uA =
    ((p4.x - p3.x) * (p1.y - p3.y) - (p4.y - p3.y) * (p1.x - p3.x)) /
    ((p4.y - p3.y) * (p2.x - p1.x) - (p4.x - p3.x) * (p2.y - p1.y));
  uB =
    ((p2.x - p1.x) * (p1.y - p3.y) - (p2.y - p1.y) * (p1.x - p3.x)) /
    ((p4.y - p3.y) * (p2.x - p1.x) - (p4.x - p3.x) * (p2.y - p1.y));

  if (uA >= 0 && (uA <= 1) & (uB >= 0) & (uB <= 1)) {
    secX = p1.x + uA * (p2.x - p1.x);
    secY = p1.y + uA * (p2.y - p1.y);

    return createVector(secX, secY);
  }
}

function bisector(pv, cv, nv, m) {
  d1 = p5.Vector.sub(cv, pv).normalize();
  d2 = p5.Vector.sub(nv, cv).normalize();

  dsum = p5.Vector.add(d1, d2); //sum of directions

  s = dsum.mag() / d1.dot(dsum); //speed
  v = dsum.rotate(-HALF_PI); //vector of bisector
  p = v.setMag(s * m).add(cv); //offsetted vertex

  return [cv, p];
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `voronoi_1_${seed}`, "png");
}
