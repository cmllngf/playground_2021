class Walker {
  constructor(p, lifespan = -1, center, maxDist) {
    this.startP = p;
    this.unprocessed_points = [p];
    this.alive = true;
    this.lifespan = lifespan;
    this.life = 0;
    this.maxDist = maxDist;
    this.center = center;
    this.currentColor = palette[int(random(palette.length))];
  }

  isValid(p) {
    return dist(p.x, p.y, this.center.x, this.center.y) < this.maxDist;
  }

  gaussian_color(c, colorAim) {
    // const col = random() > 0.997 ? palette[int(random(palette.length))] : c;
    const col = c;
    const h = hue(col) + randomGaussian(0, 2) + 4;
    const s = saturation(col) + randomGaussian(0, 1);
    const b = brightness(col) + randomGaussian(0, 1);
    return color(h, s, b);
  }

  update(visited, colors, pixel_size, color_target) {
    if (
      this.unprocessed_points.length === 0 ||
      (this.lifespan !== -1 && this.life >= this.lifespan)
    ) {
      this.alive = false;
      return;
    }
    this.life++;
    const random_index = int(random(this.unprocessed_points.length));
    const random_point = this.unprocessed_points[random_index];
    this.unprocessed_points.splice(random_index, 1);

    for (let y = random_point.y - 1; y <= random_point.y + 1; y++) {
      for (let x = random_point.x - 1; x <= random_point.x + 1; x++) {
        if (y == random_point.y && x == random_point.x) continue;

        if (visited[y * width + x]) continue;

        if (!this.isValid(createVector(x, y))) continue;

        visited[y * width + x] = true;
        this.unprocessed_points.push(createVector(x, y));
        // if(dist(x, y, width/2, height/2) > 200)

        // if (random() > 0.999) {
        //   this.currentColor = palette[int(random(palette.length))];
        // }

        // colors[y * width + x] = this.currentColor;
        colors[y * width + x] = this.gaussian_color(
          colors[random_point.y * width + random_point.x]
        );
        // else
        //   colors[y * width + x] = this.gaussian_color(colors[random_point.y * width + random_point.x], color(0))
        stroke(colors[y * width + x]);
        strokeWeight(pixel_size);
        line(random_point.x, random_point.y, x, y);
      }
    }
  }
}
