let visited = [];
let colors = [];
let canvas;
let seed;
let simplexNoise;
let systems = []
let anchors = []
let system;
let currentSystemIndex = 0;
let img

function preload() {
  img = loadImage('../assets/athena/portrait_1.jpg')
}

const shuffleArray = (array) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    const temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
};

const border = 0;
function setup() {
  canvas = createCanvas(1080, 1080);
  img.loadPixels();
  // canvas = createCanvas(1080, 1080);
  seed = random(99999);
  simplexNoise = openSimplexNoise(seed);
  colorMode(HSB);
  system = new System(300, 300, 300, 300, PI/6);
  background('#F4F4F9');
  rectMode(CENTER);
  const cols = 14;
  const rows = 14;
  const W = (width-border)/cols;
  const H = (height-border)/rows;
  for (let x = 0; x < cols; x++) {
    anchors.push([])
    for (let y = 0; y < rows; y++) {
      const sx = x * W + border + W/2;
      const sy = y * H + border + H/2;
      anchors[x].push({
        x: sx,
        y: sy,
        taken: false,
      })
      fill('red');
      circle(sx,sy, 10);
      // noFill()
      // rect(sx, sy, W, H)
      // systems.push(new System(sx, sy, 20, 90, PI/4.5))
      // systems.push(new System(sx, sy, 50, 150, PI/4.5))
      // systems.push(new System(sx, sy, 50, 150, PI/5.8));
      // systems.push(new System(sx, sy, 50, 150))
    }
  }

  const size = 76
  const eastW = size*2
  const southH = size*2
  
  for (let x = 0; x < cols; x++) {
    for (let y = 0; y < rows; y++) {
      const { x: sx, y: sy, taken } = anchors[x][y]
      if(!taken) {
        if ((x == cols - 1 || y == rows - 1)) {
          anchors[x][y] = {
            ...anchors[x][y],
            taken: true,
          }
          systems.push(new System(sx, sy, size, size))
        } else {
          let empty = true
          const eastAnchor = anchors[x + 1][y]
          const southAnchor = anchors[x][y + 1]
          if(random() < .1 && !eastAnchor.taken) { // East
            const xdiff = abs(sx - eastAnchor.x) - size
            systems.push(new System((sx + eastAnchor.x) /2, sy, eastW + xdiff, size))
            anchors[x + 1][y] = {
              ...anchors[x + 1][y],
              taken: true,
            }
            empty = false
          } else if (random() < .1 && !southAnchor.taken) { // South
            const ydiff = abs(sy - southAnchor.y) - size
            systems.push(new System(sx, (sy + southAnchor.y) /2, size, southH + ydiff))
            anchors[x][y + 1] = {
              ...anchors[x][y + 1],
              taken: true,
            }
            // if (southAnchor.taken) {
            //   anchors[x + x][y + 1] = {
            //     ...anchors[x + 1][y + 1],
            //     taken: true,
            //   }
            // }
            empty = false
          }
          if(empty) {
            systems.push(new System(sx, sy, size, size))
          }
          anchors[x][y] = {
            ...anchors[x][y],
            taken: true,
          }
        }
      }
    }
  }
  shuffleArray(systems)
}

function draw() {
  for (let i = 0; i < 70; i++) {
    if (!systems[currentSystemIndex].update()) { noFill()
      // stroke("black")
      stroke(palette[int(random(palette.length))])
      strokeWeight(3)
      if(systems[currentSystemIndex]) {
        // rect(systems[currentSystemIndex].x + randomGaussian(0,5), systems[currentSystemIndex].y + randomGaussian(0,5), systems[currentSystemIndex].width + randomGaussian(0,5), systems[currentSystemIndex].height)
        const x = systems[currentSystemIndex].x
        const y = systems[currentSystemIndex].y
        const w = systems[currentSystemIndex].width
        const h = systems[currentSystemIndex].height
        // beginShape()
        // vertex(x - w/2 + randomGaussian(0, 2), y - h/2 + randomGaussian(0, 2))
        // vertex(x + w/2 + randomGaussian(0, 2), y - h/2 + randomGaussian(0, 2))
        // vertex(x + w/2 + randomGaussian(0, 2), y + h/2 + randomGaussian(0, 2))
        // vertex(x - w/2 + randomGaussian(0, 2), y + h/2 + randomGaussian(0, 2))
        // endShape(CLOSE)
      }
      if (currentSystemIndex == floor(systems.length/2)) {
        noFill()
        stroke('#B80300')
        // stroke('#3F6264')
        // stroke('#9a031e')
        for (let j = 0; j < 1; j++) {
          myCircleNoise(width/2,height/2,250 + j * 5)
        }
      }
      currentSystemIndex++;
     }
    if (!systems[currentSystemIndex]) {
      noLoop();
      return;
    }
  }
}

const myCircleNoise = (x, y, radius) => {
  const n = 600;
  beginShape()
  for (let i = 0; i < n; i++) {
    const a = (TWO_PI / n) * i;
    const nv = simplexNoise.noise2D(cos(a) * .5, sin(a) * .5) * 40;
    const cx = x + cos(a) * (radius + nv)
    const cy = y + sin(a) * (radius + nv)
    vertex(cx, cy)
  }
  endShape(CLOSE)
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `rectangle_color_${seed}`, "png");
}
const k = 300;
const pixel_size = 3;

// const palette = ["#7ab6ff","#7ab6ff","#7ab6ff","#7ab6ff","#ffce7a","#ff8c7a","#7ab6ff","#7ab6ff"]
// const palette = ['#DE8C9E', '#697A77', '#5F7A75', '#CFD4D3', '#364F4A']
// const palette = ['#FEC89A', '#FEC89A', '#FEC89A', '#FEC5BB', '#FCD5CE']
// const palette = ['#FFC857', '#119DA4', '#19647E', '#4B3F72', '#1F2041']
// const palette = ['#DDFFF7', '#93E1D8', '#FFA69E', '#AA4465', '#861657']
// const palette = ["#fec5bb","#fcd5ce","#fae1dd","#f8edeb","#e8e8e4","#d8e2dc","#ece4db","#ffe5d9","#ffd7ba","#fec89a"]
// const palette = ["#5f0f40","#9a031e","#fb8b24","#e36414","#0f4c5c"]
// const palette = ["#590d22","#800f2f","#a4133c","#c9184a","#ff4d6d","#ff758f","#ff8fa3","#ffb3c1","#ffccd5","#fff0f3"]
// const palette = ["#0e47e3","#24408c","#6273a1","#6c6d70","#ced1d9"]
// const palette = ["#03045e","#023e8a","#0077b6","#0096c7","#00b4d8","#48cae4","#90e0ef","#ade8f4","#caf0f8"]
// const palette = ["#c9cebd","#b2bcaa","#838e83","#6c6061","#64403e"]
// const palette = ["#b7094c","#a01a58","#892b64","#723c70","#5c4d7d","#455e89","#2e6f95","#1780a1","#0091ad"]
const palette = ["#f8f9fa","#e9ecef","#dee2e6","#ced4da","#adb5bd","#6c757d","#495057","#343a40","#212529"]
const get1DIndex = (x, y, minX = 0, maxX = width, minY = 0, maxY = height) => {
  const xx = int(map(x, minX, maxX, 0, img.width));
  const yy = int(map(y, minY, maxY, 0, img.height));
  return (yy * img.width + xx) * 4;
};

const getColorPaletteFromImage = (x, y) => {
  const index = get1DIndex(x, y, border, width-border, border, height-border);
  const R = img.pixels[index];
  const G = img.pixels[index + 1];
  const B = img.pixels[index + 2];
  const ll = 0.2126 * R + 0.7152 * G + 0.0722 * B;
  return palette[int(map(ll, 0, 255, 0, palette.length, true))];
};

const getColorImg = (x, y) => {
  let c
  const index = get1DIndex(
    int(x),
    int(y),
  );
  const R = img.pixels[index];
  const G = img.pixels[index + 1];
  const B = img.pixels[index + 2];
  const A = img.pixels[index + 3];
  colorMode(RGB)
  c = color(R,G,B,A)
  colorMode(HSB)
  return c
}

const getColorFromCoord = (x, y) => {
  // vary mean along x axes
  let mappedIndex;
  // if (dist(x,y,width/2,height/2) < 400) {
    mappedIndex = map(y, height-border, border, 0, palette.length, true);
  // } else {
  //   mappedIndex = map(y, 0, height, 0, palette.length-1, true);
  // }
  // const mappedIndex = map(y, 0, height, 0, palette.length-1, true);
  const noiseMean = .4
  // const noiseMean = noise(x) * 1
  const gaussianIndex = randomGaussian(mappedIndex, noiseMean)
  const index = int(constrain(gaussianIndex, 0, palette.length - 1))
  return palette[index];
}

const getColorRandom = () => {
  return palette[int(random(palette.length))]
}

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `rectangle_color_${seed}`, "png");
}