class Walker {
  constructor(p, lifespan = -1, minX, maxX, minY, maxY) {
    this.startP = p;
    this.unprocessed_points = [p];
    this.alive = true;
    this.lifespan = lifespan;
    this.life = 0;
    this.minX = minX;
    this.maxX = maxX;
    this.minY = minY;
    this.maxY = maxY;
    this.currentColor = palette[int(random(palette.length))];
    // seed(p, this.currentColor);

    // this.downScale = int(random(1, 10))
    // this.upScale = int(random(1, 10))
    // this.leftScale = int(random(1, 10))
    // this.rightScale = int(random(1, 10))
    
    this.downScale = constrain(int(randomGaussian(2, 1)), 1,5)
    this.upScale = constrain(int(randomGaussian(2, 1)), 1,5)
    this.leftScale = constrain(int(randomGaussian(2, 1)), 1,5)
    this.rightScale = constrain(int(randomGaussian(2, 1)), 1,5)
  }

  isValid(p) {
    return (
      p.x >= this.minX && p.x < this.maxX && p.y >= this.minY && p.y < this.maxY
    );
  }

  gaussian_color(c, colorAim) {
    const step = 1;
    // const col = random() > 0.997 ? palette[int(random(palette.length))] : c;
    const col = c;
    // const h = randomGaussian(hue(col), random(0, 0));
    const h = hue(col)// + randomGaussian();
    // (colorAim ? (hue(col) < hue(colorAim) ? step : -step) : 0);
    // const s = randomGaussian(saturation(col), random(0, 0));
    const s = saturation(col)// randomGaussian(0,1);
    // (colorAim ? (saturation(col) < saturation(colorAim) ? step : -step) : 0);
    // const b = randomGaussian(brightness(col), random(0, 0));
    const b = brightness(col) + randomGaussian(0,1)
    //+ (colorAim ? (brightness(col) < brightness(colorAim) ? step : -step) : 0);
    return color(h, s, b);
  }

  update(visited, colors, pixel_size, color_target) {
    if (
      this.unprocessed_points.length === 0 ||
      (this.lifespan !== -1 && this.life >= this.lifespan)
    ) {
      this.alive = false;
      return;
    }
    this.life++;
    const random_index = int(random(this.unprocessed_points.length));
    const random_point = this.unprocessed_points[random_index];
    this.unprocessed_points.splice(random_index, 1);

    for (let y = random_point.y - this.downScale; y <= random_point.y + this.upScale; y++) {
      for (let x = random_point.x - this.leftScale; x <= random_point.x + this.rightScale; x++) {
        if (y == random_point.y && x == random_point.x) continue;

        if (visited[y * width + x]) continue;

        if (!this.isValid(createVector(x, y))) continue;

        visited[y * width + x] = true;
        this.unprocessed_points.push(createVector(x, y));
        // if(dist(x, y, width/2, height/2) > 200)

        // if (random() > 0.999) {
        //   this.currentColor = palette[int(random(palette.length))];
        // }

        // colors[y * width + x] = this.currentColor;
        colors[y * width + x] = this.gaussian_color(
          colors[random_point.y * width + random_point.x],
          //! Interessant
          getColorFromCoord(random_point.x, random_point.y)
        );
        // else
        //   colors[y * width + x] = this.gaussian_color(colors[random_point.y * width + random_point.x], color(0))
        stroke(colors[y * width + x]);
        strokeWeight(3);
        strokeCap(PROJECT)
        line(random_point.x, random_point.y, x, y);
      }
    }
  }
}
